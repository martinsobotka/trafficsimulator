package at.fhv.itm14.trafsim;

import at.fhv.itm14.trafsim.components.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.WindowEvent;

import java.util.HashMap;
import java.util.Map;

public class ScenarioBuilderController {
	private static ScenarioBuilderController instance;

	@FXML private Pane pnlMain;
	@FXML private VBox vbxProperties;
	@FXML private ListView<Label> lstComponents;
	private Map<Label, TrafficComponent> components;
	private TrafficComponent selectedComponent;
	private ProducerConnector producerForConnection;
	private double placeX;
	private double placeY;

	public ScenarioBuilderController() {
		instance = this;
		components = new HashMap<>();
	}

	public void addTrafficComponent(TrafficComponent trafficComponent) {
		pnlMain.getChildren().addAll(trafficComponent);
		trafficComponent.relocate(placeX, placeY);
		lstComponents.getItems().addAll(trafficComponent.getLabel());
		components.put(trafficComponent.getLabel(), trafficComponent);
	}

	public void selectComponent(TrafficComponent toSelect) {
		if (selectedComponent == toSelect) {
			return;
		}
		if (selectedComponent != null) {
			selectedComponent.deselect();
			int end = vbxProperties.getChildren().size();
			vbxProperties.getChildren().remove(0, end);
		}
		selectedComponent = toSelect;
		selectedComponent.select();
		lstComponents.getSelectionModel().select(toSelect.getLabel());
		vbxProperties.getChildren().addAll(selectedComponent.getComponentProperties());
	}

	public static ScenarioBuilderController getInstance() {
		return instance;
	}


	public void lstSelectComponent(MouseEvent mouseEvent) {
		Label selection = lstComponents.getSelectionModel().getSelectedItem();
		if (components.containsKey(selection)) {
			TrafficComponent toSelect = components.get(selection);
			selectComponent(toSelect);
		}
	}

	public void startConnect(ProducerConnector producer) {
		producerForConnection = producer;
	}

	public void endConnect(ConsumerConnector consumer) {
		if (producerForConnection != null) {
			ProducerConsumerConnection connection = new ProducerConsumerConnection(producerForConnection, consumer);
			pnlMain.getChildren().addAll(connection);
			producerForConnection = null;
		}
	}

	public Pane getMain() {
		return pnlMain;
	}

	public void mouseClicked(MouseEvent mouseEvent) {
		placeX = mouseEvent.getX();
		placeY = mouseEvent.getY();
	}


	public void addOneWayStreet(ActionEvent actionEvent) {
		OneWayStreet trafficComponent = new OneWayStreet();
		addTrafficComponent(trafficComponent);
	}

	public void addSink(ActionEvent actionEvent) {
		Sink sink = new Sink();
		addTrafficComponent(sink);
	}

	public void addSource(ActionEvent actionEvent) {
		Source source = new Source();
		addTrafficComponent(source);
	}

	public void addFourIntersection(ActionEvent actionEvent) {
		FourIntersection intersection = new FourIntersection();
		addTrafficComponent(intersection);
	}

	public void deleteConnection(ProducerConsumerConnection connection) {
		pnlMain.getChildren().remove(connection);
		connection.getConsumerConnector().setConnection(null);
		connection.getProducerConnector().setConnection(null);
	}

	public void onShowing(WindowEvent windowEvent) {
		System.out.println("on showing");;
	}

	public void onShown(WindowEvent windowEvent) {
		System.out.println("on shown");
	}
}
