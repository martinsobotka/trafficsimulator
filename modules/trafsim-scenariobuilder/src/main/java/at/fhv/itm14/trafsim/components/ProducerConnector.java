package at.fhv.itm14.trafsim.components;

import at.fhv.itm14.trafsim.ScenarioBuilderController;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

public class ProducerConnector extends AbstractConnector {

	private static final String FXML = "producer_connector.fxml";

	@Override
	protected String getFXMLFile() {
		return FXML;
	}

	@Override
	protected void mouseClicked(MouseEvent mouseEvent) {
		if (!isConnected()) {
			ScenarioBuilderController.getInstance().startConnect(this);
			getScene().setCursor(Cursor.CROSSHAIR);
		}
	}

	@Override
	protected void mouseEntered(MouseEvent mouseEvent) {
		if (!isConnected()) {
			getScene().setCursor(Cursor.HAND);
			circle.setStroke(Color.LIGHTGREEN);
			circle.setFill(Color.LIGHTGREEN);
		}
	}

	@Override
	protected void mouseExited(MouseEvent mouseEvent) {
		if (!isConnected()) {
			if (getScene().getCursor() != Cursor.CROSSHAIR) {
				getScene().setCursor(Cursor.DEFAULT);
				circle.setStroke(Color.DARKGREEN);
				circle.setFill(Color.DARKGREEN);
			}
		}
	}
}
