package at.fhv.itm14.trafsim.components;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

import java.util.LinkedList;
import java.util.List;

public class OneWayStreet extends TrafficComponent {

	private static final String TYPE = "OneWayStreet";
	private static final String FXML = "one_way_street.fxml";
	private static final double TRAVERSE_TIME_FACTOR = 10.0;

	@FXML
	private BorderPane pnlOneWayStreet;
	@FXML
	private ProducerConnector producerConnector;
	@FXML
	private ConsumerConnector consumerConnector;

	private List<ComponentProperty> properties;
	private Double traverseTime;

	@Override
	protected void initComponent() {
		producerConnector.setTrafficComponent(this);
		consumerConnector.setTrafficComponent(this);
		this.properties = new LinkedList<>();
		initProperties();
	}

	@Override
	protected String getFXMLFile() {
		return FXML;
	}

	private void initProperties() {
		TextField txtTraverseTime = new TextField();
		txtTraverseTime.setPrefWidth(80);
		ComponentProperty<Double, TextField> traversionTime = new ComponentProperty<Double, TextField>(
				this,
				"traverseTime",
				"How long a car needs to traverse this street",
				10.0,
				txtTraverseTime,
				this::applyTraverseTime,
				this::showTraverseTime
		);
		properties.add(traversionTime);
	}

	private void showTraverseTime(Double value, TextField field) {
		field.setText(Double.toString(value));
	}

	private Double applyTraverseTime(TextField field) {
		traverseTime = Double.parseDouble(field.getText());
		pnlOneWayStreet.setPrefWidth(traverseTime * TRAVERSE_TIME_FACTOR);
		return traverseTime;
	}


	@Override
	protected List<ComponentProperty> getSpecificProperties() {
		return properties;
	}

	@Override
	protected String getType() {
		return TYPE;
	}

	@Override
	public List<ProducerConnector> getProducers() {
		List<ProducerConnector> producers = new LinkedList<>();
		producers.add(producerConnector);
		return producers;
	}

	@Override
	public List<ConsumerConnector> getConsumers() {
		List<ConsumerConnector> consumers = new LinkedList<>();
		consumers.add(consumerConnector);
		return consumers;
	}
}
