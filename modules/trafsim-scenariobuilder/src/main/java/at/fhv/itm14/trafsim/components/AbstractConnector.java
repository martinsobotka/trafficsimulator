package at.fhv.itm14.trafsim.components;

import at.fhv.itm14.trafsim.ScenarioBuilderController;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;

import java.io.IOException;

public abstract class AbstractConnector extends Pane {
	@FXML protected Circle circle;
	protected TrafficComponent trafficComponent;
	protected DoubleProperty centerX = new SimpleDoubleProperty();
	protected DoubleProperty centerY = new SimpleDoubleProperty();
	protected ProducerConsumerConnection connection;
	private ComponentProperty<Boolean, Button> property;
	private Button btnDeleteConnection;

	public AbstractConnector() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource(getFXMLFile()));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		initMouseEffects();
		initProperty();
	}

	private void initProperty() {
		btnDeleteConnection = new Button();
		btnDeleteConnection.disableProperty().setValue(!isConnected());
		btnDeleteConnection.setOnAction(
				actionEvent -> ScenarioBuilderController.getInstance().deleteConnection(connection)
		);
		property = new ComponentProperty<>(
				trafficComponent,
				"Connector",
				"",
				Boolean.FALSE,
				btnDeleteConnection,
				button -> isConnected(),
				(aBoolean, button) -> {}
		);
	}

	protected abstract String getFXMLFile();

	private void initMouseEffects() {
		setOnMouseEntered(this::mouseEntered);
		setOnMouseExited(this::mouseExited);
		setOnMouseClicked(this::mouseClicked);
	}

	protected abstract void mouseClicked(MouseEvent mouseEvent);

	protected abstract void mouseEntered(MouseEvent mouseEvent);

	protected abstract void mouseExited(MouseEvent mouseEvent);


	public TrafficComponent getTrafficComponent() {
		return trafficComponent;
	}

	public void setTrafficComponent(TrafficComponent trafficComponent) {
		this.trafficComponent = trafficComponent;
		trafficComponent.boundsInParentProperty().addListener(
				(observableValue, oldBounds, newBounds) -> this.calcCenter()
		);
	}

	private void calcCenter() {
		Pane main = ScenarioBuilderController.getInstance().getMain();

		double x = circle.getCenterX()
				 + circle.localToScene(circle.getBoundsInLocal()).getMinX()
				 - main.localToScene(main.getBoundsInLocal()).getMinX();
		double y = circle.getCenterY()
				+ circle.localToScene(circle.getBoundsInLocal()).getMinY()
				- main.localToScene(main.getBoundsInLocal()).getMinY();

		centerX.set(x);
		centerY.set(y);
	}

	public DoubleProperty centerXProperty() {
		return centerX;
	}

	public DoubleProperty centerYProperty() {
		return centerY;
	}

	public boolean isConnected() {
		return connection != null;
	}

	public ProducerConsumerConnection getConnection() {
		return connection;
	}

	public void setConnection(ProducerConsumerConnection connection) {
		this.connection = connection;
		this.btnDeleteConnection.disableProperty().setValue(!isConnected());
	}

	public ComponentProperty<Boolean, Button> getProperty() {
		return property;
	}

}
