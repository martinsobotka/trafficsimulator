package at.fhv.itm14.trafsim.components;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

public class ProducerConsumerConnection extends Line {
	private ProducerConnector producerConnector;
	private ConsumerConnector consumerConnector;
	private ContextMenu contextMenu;

	public ProducerConsumerConnection(ProducerConnector producerConnector, ConsumerConnector consumerConnector) {
		this.producerConnector = producerConnector;
		this.consumerConnector = consumerConnector;

		this.producerConnector.setConnection(this);
		this.consumerConnector.setConnection(this);

		this.contextMenu = new ContextMenu();
		this.contextMenu.getItems().add(new MenuItem("delete"));
		initMouseEffects();
		paint();
	}

	private void initMouseEffects() {
		setOnMouseReleased(this::mouseReleased);
	}

	private void mouseReleased(MouseEvent mouseEvent) {
		if (mouseEvent.getButton() == MouseButton.SECONDARY) {
			this.contextMenu.show(this, mouseEvent.getScreenX(), mouseEvent.getScreenY());
		}
	}

	private void paint() {
		setStrokeWidth(1);
		setStroke(Color.DARKGREEN);
		startXProperty().bind(producerConnector.centerXProperty());
		startYProperty().bind(producerConnector.centerYProperty());
		endXProperty().bind(consumerConnector.centerXProperty());
		endYProperty().bind(consumerConnector.centerYProperty());
	}

	public ProducerConnector getProducerConnector() {
		return producerConnector;
	}

	public ConsumerConnector getConsumerConnector() {
		return consumerConnector;
	}
}
