package at.fhv.itm14.trafsim.components;

import javafx.fxml.FXML;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class FourIntersection extends TrafficComponent {
	private static final String FXML = "four_intersection.fxml";
	private static final String TYPE = "4-Intersection";

	@FXML private ConsumerConnector consumer0Connector;
	@FXML private ConsumerConnector consumer1Connector;
	@FXML private ConsumerConnector consumer2Connector;
	@FXML private ConsumerConnector consumer3Connector;
	@FXML private ProducerConnector producer0Connector;
	@FXML private ProducerConnector producer1Connector;
	@FXML private ProducerConnector producer2Connector;
	@FXML private ProducerConnector producer3Connector;

	@Override
	protected void initComponent() {
		consumer0Connector.setTrafficComponent(this);
		consumer1Connector.setTrafficComponent(this);
		consumer2Connector.setTrafficComponent(this);
		consumer3Connector.setTrafficComponent(this);
		producer0Connector.setTrafficComponent(this);
		producer1Connector.setTrafficComponent(this);
		producer2Connector.setTrafficComponent(this);
		producer3Connector.setTrafficComponent(this);
	}

	@Override
	protected String getFXMLFile() {
		return FXML;
	}

	@Override
	protected String getType() {
		return TYPE;
	}

	@Override
	protected List<ComponentProperty> getSpecificProperties() {
		return Collections.EMPTY_LIST;
	}

	@Override
	public List<ProducerConnector> getProducers() {
		List<ProducerConnector> producers = new LinkedList<>();
		producers.add(producer0Connector);
		producers.add(producer1Connector);
		producers.add(producer2Connector);
		producers.add(producer3Connector);
		return producers;
	}

	@Override
	public List<ConsumerConnector> getConsumers() {
		List<ConsumerConnector> consumers = new LinkedList<>();
		consumers.add(consumer0Connector);
		consumers.add(consumer1Connector);
		consumers.add(consumer2Connector);
		consumers.add(consumer3Connector);
		return consumers;
	}
}
