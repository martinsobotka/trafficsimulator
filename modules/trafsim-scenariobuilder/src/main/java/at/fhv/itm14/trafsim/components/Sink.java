package at.fhv.itm14.trafsim.components;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Sink extends TrafficComponent {

	private static final String TYPE = "Sink";
	private static final String FXML = "sink.fxml";

	@FXML
	private Label lblCounter;
	@FXML
	private ConsumerConnector consumerConnector;


	@Override
	protected List<ComponentProperty> getSpecificProperties() {
		return new LinkedList<>();
	}

	@Override
	protected void initComponent() {
		this.consumerConnector.setTrafficComponent(this);
	}

	@Override
	protected String getFXMLFile() {
		return FXML;
	}

	@Override
	protected String getType() {
		return TYPE;
	}

	@Override
	public List<ProducerConnector> getProducers() {
		return Collections.EMPTY_LIST;
	}

	@Override
	public List<ConsumerConnector> getConsumers() {
		List<ConsumerConnector> consumers = new LinkedList<>();
		consumers.add(consumerConnector);
		return consumers;
	}
}
