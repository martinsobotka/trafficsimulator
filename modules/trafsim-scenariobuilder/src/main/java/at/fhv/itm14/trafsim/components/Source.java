package at.fhv.itm14.trafsim.components;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


public class Source extends TrafficComponent {

	private static final String TYPE = "Source";
	private static final String FXML = "source.fxml";
	private static final String DIST_PACKAGE = "desmoj.core.dist";

	@FXML
	private Label lblCounter;
	@FXML
	private ProducerConnector producerConnector;
	private List<ComponentProperty> properties;
	private String distribution = "ContDistConstant";
	private double[] distributionValues;

	public Source() {
		properties = new LinkedList<>();
		initProperties();
	}

	private void initProperties() {
		ObservableList<String> distributionTypes =
				FXCollections.observableArrayList(
						"ContDistConstant",
						"ContDistUniform",
						"ContDistExponential"
				);
		ComboBox cmbDistributionTypes = new ComboBox(distributionTypes);
		ComponentProperty<String, ComboBox<String>> dist = new ComponentProperty<String, ComboBox<String>>(
				this,
				"Dist",
				"Distribution type",
				distribution,
				cmbDistributionTypes,
				this::applyDist,
				this::showDist
		);
		properties.add(dist);
		TextField txtValues = new TextField();
		txtValues.setPrefWidth(80);
		ComponentProperty<double[], TextField> values = new ComponentProperty<double[], TextField>(
				this,
				"Values",
				"Values for distribution, separated by ;",
				new double[] {0.5},
				txtValues,
				this::applyValues,
				this::showValues
		);
		properties.add(values);
	}

	private void showValues(double[] doubles, TextField field) {
		StringBuilder values = new StringBuilder();
		for (double value : doubles) {
			values.append(value);
			values.append(';');
		}
		field.setText(values.toString());
	}

	private double[] applyValues(TextField field) {
		String[] splittedValues = field.getText().split(";");
		distributionValues = new double[splittedValues.length];
		for (int i = 0; i < splittedValues.length; i++) {
			distributionValues[i] = Double.parseDouble(splittedValues[i]);
		}
		return distributionValues;
	}


	private void showDist(String dist, ComboBox<String> comboBox) {
		comboBox.getSelectionModel().select(dist);
	}

	private String applyDist(ComboBox<String> comboBox) {
		distribution = DIST_PACKAGE + "." + comboBox.selectionModelProperty().getValue();
		return distribution;
	}


	@Override
	protected List<ComponentProperty> getSpecificProperties() {
		return properties;
	}

	@Override
	protected void initComponent() {
		this.producerConnector.setTrafficComponent(this);
	}

	@Override
	protected String getFXMLFile() {
		return FXML;
	}

	@Override
	protected String getType() {
		return TYPE;
	}

	@Override
	public List<ProducerConnector> getProducers() {
		List<ProducerConnector> producers = new LinkedList<>();
		producers.add(producerConnector);
		return producers;
	}

	@Override
	public List<ConsumerConnector> getConsumers() {
		return Collections.EMPTY_LIST;
	}

	public String getDistribution() {
		return DIST_PACKAGE + "." + distribution;
	}

	public double[] getDistributionValues() {
		return distributionValues;
	}
}
