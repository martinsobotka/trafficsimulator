package at.fhv.itm14.trafsim;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ScenarioBuilder extends Application {

    private static ScenarioBuilderController controller;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(ScenarioBuilder.class.getClassLoader().getResource("scenario_builder.fxml"));
        primaryStage.setTitle("TrafSim - Scenario Builder");
        primaryStage.setScene(new Scene(root, 1000, 800));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
