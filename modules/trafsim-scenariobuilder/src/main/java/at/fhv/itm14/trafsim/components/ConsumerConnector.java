package at.fhv.itm14.trafsim.components;

import at.fhv.itm14.trafsim.ScenarioBuilderController;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

public class ConsumerConnector extends AbstractConnector {

	private static final String FXML = "consumer_connector.fxml";

	@Override
	protected void mouseClicked(MouseEvent mouseEvent) {
		if (!isConnected()) {
			ScenarioBuilderController.getInstance().endConnect(this);
			getScene().setCursor(Cursor.DEFAULT);
		}
	}

	@Override
	protected void mouseEntered(MouseEvent mouseEvent) {
		if (!isConnected() && (getScene().getCursor() == Cursor.CROSSHAIR)) {
			circle.setStroke(Color.LIGHTGREEN);
		}
	}

	@Override
	protected void mouseExited(MouseEvent mouseEvent) {
		if (!isConnected() && (getScene().getCursor() == Cursor.CROSSHAIR)) {
			circle.setStroke(Color.DARKGREEN);
		}
	}

	@Override
	protected String getFXMLFile() {
		return FXML;
	}
}
