package at.fhv.itm14.trafsim.components;

import at.fhv.itm14.trafsim.ScenarioBuilderController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class TrafficComponent extends Pane {

	private final static String STYLE = "-fx-background-color: LIGHTGREY;";

	private static int idCounter = 0;

	private Label label;
	private List<ComponentProperty> standardProperties;
	private String name;
	private double mouseX;
	private double mouseY;

	public TrafficComponent() {
		this.idCounter++;
		this.name = Integer.toString(idCounter);

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource(getFXMLFile()));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		setStyle(STYLE);

		this.standardProperties = new LinkedList<>();
		initStandardProperties();
		initMouseEffects();

		initComponent();
	}

	protected abstract void initComponent();

	private void initMouseEffects() {
		setOnMouseReleased(this::mouseReleased);
		setOnMousePressed(this::mousePressed);
		setOnMouseDragged(this::mouseDragged);
	}

	protected abstract String getFXMLFile();

	protected abstract String getType();

	private void updateLabel() {
		this.label.setText(name + " (" + getType() + ")");
	}

	private void initStandardProperties() {
		TextField txtName = new TextField();
		txtName.setPrefWidth(80);
		standardProperties.add(
				new ComponentProperty<>(
						this,
						"Name",
						"Name of this component",
						Integer.toString(idCounter),
						txtName,
						this::applyName,
						this::showName
				)
		);
		TextField txtRotate = new TextField();
		txtName.setPrefWidth(80);
		standardProperties.add(
				new ComponentProperty<>(
						this,
						"Rotation",
						"Rotation of this component",
						0.0,
						txtRotate,
						this::applyRotation,
						this::showRotation
				)
		);

		standardProperties.addAll(
				getConsumers().stream()
						.map(consumerConnector -> consumerConnector.getProperty()).collect(Collectors.toList())
		);
	}

	private Void showRotation(Double value, TextField field) {
		field.setText(Double.toString(value));
		return null;
	}

	private Double applyRotation(TextField field) {
		Double rotation = Double.parseDouble(field.getText());
		this.rotateProperty().set(rotation);
		return rotation;
	}

	private Void showName(String name, TextField field) {
		field.setText(name);
		return null;
	}

	public String applyName(TextField field) {
		name = field.getText();
		updateLabel();
		return name;
	}


	public void mousePressed(MouseEvent mouseEvent) {
		mouseX = mouseEvent.getSceneX();
		mouseY = mouseEvent.getSceneY();
	}

	public void mouseReleased(MouseEvent mouseEvent) {
		getScene().setCursor(Cursor.DEFAULT);
		ScenarioBuilderController.getInstance().selectComponent(this);
	}

	public void mouseDragged(MouseEvent mouseEvent) {
		getScene().setCursor(Cursor.MOVE);
		double deltaX = mouseEvent.getSceneX() - mouseX;
		double deltaY = mouseEvent.getSceneY() - mouseY;
		relocate(getLayoutX() + deltaX, getLayoutY() + deltaY);
		mouseX = mouseEvent.getSceneX();
		mouseY = mouseEvent.getSceneY();
	}

	public void deselect() {
		setEffect(null);
	}


	public void select() {
		int depth = 5;
		DropShadow borderGlow = new DropShadow();
		borderGlow.setOffsetY(0f);
		borderGlow.setOffsetX(0f);
		borderGlow.setColor(Color.BLUE);
		borderGlow.setWidth(depth);
		borderGlow.setHeight(depth);
		setEffect(borderGlow);
	}

	public List<ComponentProperty> getComponentProperties() {
		List<ComponentProperty> componentProperties = new LinkedList<>(standardProperties);
		componentProperties.addAll(getSpecificProperties());
		return componentProperties;
	}

	protected abstract List<ComponentProperty> getSpecificProperties();

	public Label getLabel() {
		if (label == null) {
			label = new Label();
			updateLabel();
		}
		return label;
	}

	public abstract List<ProducerConnector> getProducers();

	public abstract List<ConsumerConnector> getConsumers();

}
