package at.fhv.itm14.trafsim.components;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.FlowPane;

import java.io.IOException;
import java.util.function.BiConsumer;
import java.util.function.Function;

public class ComponentProperty<T, R extends Node> extends FlowPane {
	private final String name;
	private final String description;
	private final R valueRepresentation;
	private final TrafficComponent trafficComponent;
	private final Tooltip tooltip;
	@FXML private Label lblName;
	private T value;
	private BiConsumer<T, R> showValue;

	public ComponentProperty(TrafficComponent trafficComponent, String name, String description, T startValue,
							 R valueRepresentation, Function<R, T> applyValue, BiConsumer<T, R> showValue) {

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("component_property.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();

			this.trafficComponent = trafficComponent;
			this.description = description;
			this.name = name;
			this.tooltip = new Tooltip(description);
			this.lblName.setText(name + ": ");
			this.lblName.setOnMouseEntered(event -> this.tooltip.show(this, event.getScreenX(), event.getScreenY()));
			this.lblName.setOnMouseExited(event -> this.tooltip.hide());
			this.showValue = showValue;
			this.valueRepresentation = valueRepresentation;
			this.valueRepresentation.focusedProperty().addListener(
					(observableValue, oldValue, newValue) -> {
						if (!newValue && oldValue) {
							// Focus lost
							value = applyValue.apply(valueRepresentation);
						}
					}
			);
			this.valueRepresentation.setOnKeyReleased(
					keyEvent -> {
						if (keyEvent.getCode() == KeyCode.ENTER) {
							value = applyValue.apply(valueRepresentation);
						}
					}
			);
			this.showValue.accept(startValue, valueRepresentation);
			this.getChildren().add(valueRepresentation);

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
		this.showValue.accept(value, valueRepresentation);
	}
}
