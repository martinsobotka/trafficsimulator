package Interfaces;

import Callbacks.ISimulationCallback;
import DataTypes.ResultParameter;
import DataTypes.SettingParameter;

import java.util.LinkedList;


public interface ISimulationAdapter extends Runnable {
    //Abstract Methods
    /*
    Use this method for initialization.
     */
    void initializeParameters();

    /*
        This method has to return the objective value (objective Function) of the experiment run.
        The best way is to use the _resultParameters
         */
    Double getObjectiveValue();
    
    Double[] getMultiObjectiveValues();

    /*
        This method has to set up the Desmo-J Experiment (like the main method in the tutorials of Desmo-J )
         */
    void StartSimulation();

    /*
        Use this method to give a description of the simulation/model
         */
    String getSimulationDescription();

    /*
        This method sets the setting parameters if there is a setter in the
        model class with regular naming convention
        Variable: thisIsATest / _thisIsATest
        Setter: setThisIsATest
         */
    void assignSettingParameters(Object modelClass);

    /*
        This method gets the result parameters if there is a getter in the
        model class with regular naming convention and saves these values
        in this class
        Variable: thisIsATest / _thisIsATest
        Getter: getThisIsATest
        */
    void extractResultParameters(Object modelClass);

    /*
        This method is called after the "StartSimulation" and calls the registered callbacks
         */
    void runCallbacks();
    
    boolean isMultiObjectiveSimulation();

    LinkedList<ResultParameter> getResultParameters();

    void addResultParameter(ResultParameter resultParameter);

    void addResultParameters(LinkedList<ResultParameter> resultParameters);

    void overwriteResultParameters(LinkedList<ResultParameter> resultParameters);

    LinkedList<SettingParameter> getSettingParameters();

    void setSettingParameters(LinkedList<SettingParameter> settingParameters);

    void addSettingParameter(SettingParameter settingParameter);

    void addSettingParameters(LinkedList<SettingParameter> settingParameters);
    
    void addMultiSettingParameter(SettingParameter settingParameter, int count);

    void overwriteSettingParameters(LinkedList<SettingParameter> settingParameters);

    long getSimulationTime();

    void setSimulationTime(long _simulationTime);

    void registerCallback(ISimulationCallback callback);

    void unregisterCallback(ISimulationCallback callback);

    String getID();

    void setID(String ID);

    void run();
}
