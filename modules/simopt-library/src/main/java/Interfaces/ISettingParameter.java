package Interfaces;


public interface ISettingParameter extends IParameter {

    boolean hasRange();

    Comparable getMinValue();

    Comparable getMaxValue();
}
