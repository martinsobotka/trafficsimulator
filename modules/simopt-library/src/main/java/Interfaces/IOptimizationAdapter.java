package Interfaces;

import Callbacks.IOptimizationCallback;
import DataTypes.Optimization.Child;
import DataTypes.Optimization.OptimizationGoal;
import DataTypes.SettingParameter;
import DataTypes.Simulation.SimRunData;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;


public interface IOptimizationAdapter {
    /*
        Use this method to initialize the setting parameters of the optimizer
         */
    void initializeParameters();

    /*
        Use this method to give a brief description of the optimizer
         */
    String getOptimizerDescription();

    /*
        This method calculates the next generation
        (the next set of "SettingParameters" / input parameters for the
        simulation/calculation of the objective value)

        Save the "new Generation" in actualValues
         */
    void calcNextGeneration();

    void calcFirstGeneration();

    /*
    This method should check if the optimizer is finished
     */
    boolean checkIfFinished();

    void isOptimizationFinished();

    boolean isFinished();

    void setFinished(boolean finished);

    void setObjectiveValues(LinkedList<Child> calculatedSimulation);

    LinkedList<SettingParameter> getSettingParameters();

    LinkedList<Child> getBestChildren(LinkedList<Child> children, int numberOfBest);

    Child getBestChild(LinkedList<Child> children);

    Child getBestActualChild();

    Child getBestChildOverall();

    Child getBestChild(Child c1, Child c2);
    
    List<Child> getAllChildren();

    /*
        checks if all childs where calculated
    */
    boolean allChildsEvaluated();

    /*
        This method sets the setting parameters if there is a setter in the
        model class with regular naming convention
        Variable: thisIsATest / _thisIsATest
        Setter: setThisIsATest
    */
    void assignSettingParameters(Object optimization);

    void addSettingParameter(SettingParameter settingParameter);

    OptimizationGoal getOptimizationGoal();

    void setOptimizationGoal(OptimizationGoal optimizationGoal);

    LinkedList<Child> getActualValues();

    Child getChildByID(String ID);


    /*
    This method sets the "objectiveValue" and "validRun" to the corresponding child
    */
    void addFinishedSimulation(SimRunData data);

    void setActualValues(LinkedList<Child> value);

    LinkedList<Child> getOldValues();

    Object getValueOfSettingParameter(String name);

    void runCallbacks();

    void registerCallback(IOptimizationCallback callback);

    void unregisterCallback(IOptimizationCallback callback);

    void resetIterationCounter();

    int getIterationCounter();

    void run();

    String getID();

    void setID(String ID);

    LinkedList<IOptimizationCallback> getCallback();

    void setCallback(LinkedList<IOptimizationCallback> callback);

    LinkedList<SettingParameter> getSimulationParameters();

    void assignSettingParameters(LinkedList<SettingParameter> settingParameters, IOptimizationAdapter obj);

    void setSettingParameters(LinkedList<SettingParameter> simulationParameters);

    void setSimulationParameters(LinkedList<SettingParameter> simulationParameters);

    Child[] calculateAllPossibleChildren(LinkedList<SettingParameter> simParams);

    LinkedList<Double[]> getAllCombinations(LinkedList<Double[]> allValues);

    boolean calcNextPer(HashMap<Integer, Integer> mi, HashMap<Integer, Integer> it, int dim);

    Double[] getAllPossibleValues(SettingParameter sp);

}
