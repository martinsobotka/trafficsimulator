package Interfaces;


public interface IParameter {

    String getName();

    Class getTypeDefinition();

    String getDescription();

    Comparable getValue();

    void setValue(Comparable value);
}
