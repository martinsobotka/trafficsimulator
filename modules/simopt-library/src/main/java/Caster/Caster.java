package Caster;

import DataTypes.SettingParameter;

public class Caster {

    public static Double getParaAsDouble(SettingParameter settingParameter){
        return (Double) Caster.castValue(Double.class, settingParameter.getValue());
    }

    public static Double getMinAsDouble(SettingParameter settingParameter){

        if(settingParameter.getMinValue()==null)
            return 0d;

        return (Double) Caster.castValue(Double.class, settingParameter.getMinValue());
    }

    public static Double getMaxAsDouble(SettingParameter settingParameter){

        if(settingParameter.getMaxValue()==null)
            return 0d;

        return (Double) Caster.castValue(Double.class, settingParameter.getMaxValue());
    }

    public static Double getStepSizeAsDouble(SettingParameter settingParameter){
        if(settingParameter.getMaxValue()==null)
            return 0d;
        return (Double) Caster.castValue(Double.class, settingParameter.getStepSize());
    }

    public static Comparable castToOriginalType(Double value, SettingParameter settingParameter) {
        return Caster.castValue(settingParameter.getTypeDefinition(),value);
    }

    public static Comparable castValue(Class clazz, Object value){

        boolean worked = true;
        try {
            return (Comparable)clazz.cast(value);
        }catch(Exception ex) {

            if (clazz.getName().toLowerCase().contains("double")) {
                if(value.getClass().getName().toLowerCase().equals("integer")){
                    return ((Integer)value).doubleValue();
                }
                return Double.parseDouble(value.toString());

            } else if (clazz.getName().toLowerCase().contains("integer")) {
                if(value.getClass().getName().toLowerCase().contains("double")){
                    return new Integer(((Double)value).intValue());
                }
                return ((int) Double.parseDouble(value.toString()) );

            }else if (clazz.getName().toLowerCase().contains("int")) {
                if(value.getClass().getName().toLowerCase().equals("integer")){
                    return ((Integer)value).intValue();
                }
                return ((int) Double.parseDouble(value.toString()) );
            } else if (clazz.getName().toLowerCase().contains("float")) {
                if(value.getClass().getName().toLowerCase().equals("integer")){
                    return ((Integer)value).floatValue();
                }
                return Float.parseFloat(value.toString());
            } else if (clazz.getName().toLowerCase().contains("bool")){
                return Boolean.parseBoolean(value.toString());
            } else{
                worked = false;
            }
        }
        if(!worked){
            throw new ClassCastException( value.toString() + " can't be casted to " + clazz.toString() );
        }
        return null;
    }
}
