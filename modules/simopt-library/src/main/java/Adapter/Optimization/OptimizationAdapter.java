package Adapter.Optimization;

import Callbacks.IOptimizationCallback;
import Caster.Caster;
import DataTypes.Optimization.Child;
import DataTypes.Optimization.OptimizationGoal;
import DataTypes.SettingParameter;
import DataTypes.Simulation.SimRunData;
import Interfaces.IOptimizationAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

@XmlAccessorType(XmlAccessType.NONE)
public abstract class OptimizationAdapter implements IOptimizationAdapter {

    //Variables
    private LinkedList<IOptimizationCallback> callback;
    private LinkedList<SettingParameter> settingParameters;
    private LinkedList<SettingParameter> simulationParameters;
    private ConcurrentLinkedQueue<Child> oldValues;
    private ConcurrentLinkedQueue<Child> actualValues;
    private int iterationCounter;
    private OptimizationGoal optimizationGoal;
    private boolean firstRun;
    private boolean finished;
    private boolean finishedCalled;
    private String ID;

    //Constructors
    public OptimizationAdapter() {
        this(new LinkedList<SettingParameter>());
    }

    public OptimizationAdapter(LinkedList<SettingParameter> simulationParameters) {
        this.simulationParameters = simulationParameters;
        this.settingParameters = new LinkedList<SettingParameter>();
        this.callback = new LinkedList<IOptimizationCallback>();
        this.actualValues = new ConcurrentLinkedQueue<Child>();
        this.oldValues = new ConcurrentLinkedQueue<Child>();
        this.iterationCounter = 0;
        this.firstRun = true;
        this.finished = false;
        this.finishedCalled = false;
        this.optimizationGoal = optimizationGoal.Min;
    }


    //Implemented Methods

    /*
    checks if value is in range of parameters min and max value
     */
    public boolean inRange(SettingParameter sp, Object value){

        if(sp.hasRange()){

            if(((Double)(Caster.castValue(Double.class,sp.getMinValue()))) <=
                    ((Double)(Caster.castValue(Double.class,value)))&&
                    ((Double)(Caster.castValue(Double.class,sp.getMaxValue()))) >=
                            ((Double)(Caster.castValue(Double.class, value)))
                    ){
                return true;
            }
            return false;
        }else{
            return true;
        }
    }
    
    /*
    This method sets the "objectiveValue" and "validRun" to the corresponding child
    */
    @Override
    public void addFinishedSimulation(SimRunData data){

        Child c = getChildByID(data.getID());

        if(c!= null){
        	if (data.getObjectiveValue() == null) {
        		c.setObjectiveValue(data.getObjectiveValues()[0]);
        	} else {
        		c.setObjectiveValue(data.getObjectiveValue());
        	}
            c.setMultiObjective(toPrimitiveDouble(data.getObjectiveValues()));
            c.setValidRun(data.isValidRun());
        }

        isOptimizationFinished();

        if(allChildsEvaluated()){

            run();

        }
    }
    
    private double[] toPrimitiveDouble(Double[] arr) {
    	double[] result = new double[arr.length];
    	for (int i = 0; i < arr.length; i++) {
    		result[i] = arr[i];
    	}
    	return result;
    }

    @Override
    public void run(){

      while (!finished)  {
        if(firstRun){
            calcFirstGeneration();
            //runCallbacks();
            this.firstRun = false;

        }else{
            isOptimizationFinished();
            if(!finished) {
                this.iterationCounter++;
                calcNextGeneration();
            }
        }
        runCallbacks();
      }
    }

    @Override
    public void isOptimizationFinished(){
        this.finished = checkIfFinished();
    }

    public Double getParaAsDouble(SettingParameter settingParameter){
       return (Double) Caster.castValue(Double.class, settingParameter.getValue());
    }

    public Double getStepSizeAsDouble(SettingParameter settingParameter){
        return (Double) Caster.castValue(Double.class, settingParameter.getStepSize());
    }

    public Object castToOriginalType(Double value, SettingParameter settingParameter){
        return Caster.castValue(settingParameter.getClass(),value);
    }

    @Override
    public Child getChildByID(String ID){

        for(Child c: this.getActualValues()){
            if(c.getID().equals(ID)){
                return c;
            }
        }
        return null;
    }

    @Override
    public void setObjectiveValues(LinkedList<Child> calculatedSimulation){
        this.oldValues = this.actualValues;
        ConcurrentLinkedQueue<Child> temp = new ConcurrentLinkedQueue<Child>();
        temp.addAll(calculatedSimulation);
        this.actualValues = temp;
        if(allChildsEvaluated()){
            run();
        }
    }

    @Override
    public LinkedList<SettingParameter> getSettingParameters() {
        return settingParameters;
    }

    @Override
    public LinkedList<Child> getBestChildren(LinkedList<Child> children, int numberOfBest){

        LinkedList<Child> bestChildren = new LinkedList<Child>();

        for (int i = numberOfBest; i >0 ; i--) {

            Child best = null;
            for(Child c : children){
                if(!bestChildren.contains(c)){

                    if(best == null){
                        best = c;
                    }else{
                        best = getBestChild(best, c);
                    }
                }
            }
            bestChildren.addLast(best);
        }
        return bestChildren;
    }

    @Override
    public Child getBestChild(LinkedList<Child> children){

        Child best = null;

        for(Child c : children){
            if(c.isValidRun()) {
                if (best == null) {
                    best = c;
                } else {
                    best = getBestChild(best, c);
                }
            }
        }
        return best;
    }
    
    @Override
    public List<Child> getAllChildren() {
        List<Child> allChilds = new ArrayList<Child>();
        allChilds.addAll(this.actualValues);
        allChilds.addAll(this.oldValues);
        return allChilds;
    }

    /*
    returns best child of all valid children (actualValues)
     */
    @Override
    public Child getBestActualChild(){

        LinkedList<Child> temp = new LinkedList<Child>();
        temp.addAll(this.actualValues);
        return getBestChild(temp);
    }

    /*
    returns best child of all valid children (oldValues +  actualValues)
     */
    @Override
    public Child getBestChildOverall(){

        Child best = null;
        LinkedList<Child> allChilds = new LinkedList<Child>();
        allChilds.addAll(this.actualValues);
        allChilds.addAll(this.oldValues);

        return getBestChild(allChilds);
    }

    /*
    compares the children based on the objectiveValue
     */
    @Override
    public Child getBestChild(Child c1, Child c2){

        if(this.optimizationGoal == OptimizationGoal.Min){

            return (c1.getObjectiveValue() < c2.getObjectiveValue())? c1 : c2;
        }else{

            return (c1.getObjectiveValue() > c2.getObjectiveValue())? c1 : c2;
        }
    }

    @Override
    public boolean isFinished() {
        return finished;
    }

    @Override
    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    /*
    method takes a list ( of Double[]) and creates a Double[] with the
     dimension of the list (list.size()) with every possible permutation
    */
    @Override
    public LinkedList<Double[]> getAllCombinations(LinkedList<Double[]> allValues){

        LinkedList<Double[]> returnValue = new LinkedList<Double[]>();
        HashMap<Integer,Integer> it = new HashMap<Integer, Integer>();
        HashMap<Integer,Integer> mi = new HashMap<Integer, Integer>();
        int maxSize = 1;
        int dim = allValues.size();

        for (int i = 0; i < dim ; i++) {
            int size = allValues.get(i).length;
            maxSize *= size;
            it.put(i, 0);
            mi.put(i, size - 1);
        }

        do{
            Double[] da = new Double[dim];
            for (int j = 0; j < da.length ; j++) {
                da[j] = allValues.get(j)[it.get(j)];
            }
            returnValue.add(da);
        }while(calcNextPer(mi,it,dim));

        return returnValue;
    }

    /*
    Calculates the next permutation. Returns false, if last
    possible permutation is reached
    */
    @Override
    public boolean calcNextPer(HashMap<Integer, Integer> mi, HashMap<Integer, Integer> it, int dim) {

        boolean valueWasSet = false;
        int maxIndexOfDim = dim-1;
        for (int i = maxIndexOfDim; i >= 0 && !valueWasSet; i--) {

            int indexDim = it.get(i);
            int maxIndex = mi.get(i);

            if(indexDim < maxIndex){
                it.put(i,it.get(i)+1);
                valueWasSet = true;
            }else{
                if(i==0){
                    return false;
                }
                it.put(i,0);
            }
        }

        return valueWasSet;
    }

    /*
    This method returns all possible values for a SettingParameter,
    based on minValue, maxValue and stepSize
     */
    @Override
    public Double[] getAllPossibleValues(SettingParameter sp){

        Double minVal = Caster.getMinAsDouble(sp);
        Double maxVal = Caster.getMaxAsDouble(sp);
        Double stepSize = Caster.getStepSizeAsDouble(sp);

        ArrayList<Double> returnValue = new ArrayList<Double>();
        Double actualValue = minVal;

        while (actualValue<=maxVal) {

            returnValue.add( actualValue);
            actualValue+=stepSize;
        }

        Double[] returnV = new Double[returnValue.size()];
        returnV = returnValue.toArray(returnV);

        return returnV;
    }

    /*
    This method creates an array of all possible children, based on the
    minValue,maxValue and stepSize of the passed settingParameters
     */
    @Override
    public Child[] calculateAllPossibleChildren(LinkedList<SettingParameter> simParams){

        LinkedList<Double[]> allValues = new LinkedList<Double[]>();

        LinkedList<SettingParameter> params;
        LinkedList<Child> childs = new LinkedList<Child>();

        for(SettingParameter sp: simParams){
            Double[] pV = getAllPossibleValues(sp);
            allValues.add(pV);
        }

        LinkedList<Double[]> permutations = getAllCombinations(allValues);

        for(Double[] da : permutations){
            params = new LinkedList<SettingParameter>();

            for (int i = 0; i < da.length ; i++) {
                SettingParameter sp = simParams.get(i);
                SettingParameter newSp = null;
                try {
                    newSp = new SettingParameter(sp.getName(),sp.getTypeDefinition(), sp.getDescription(), sp.getValue(), sp.getMinValue(), sp.getMaxValue(), sp.getStepSize());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                newSp.setValue(Caster.castToOriginalType(da[i], sp));
                params.add(newSp);
            }

            childs.add(new Child(params));
        }

        Child[] ca = new Child[childs.size()];
        ca = childs.toArray(ca);
        return ca;
    }

    @Override
    public void setSettingParameters(LinkedList<SettingParameter> settingParameters) {
        this.settingParameters = settingParameters;
    }

    @Override
    public void assignSettingParameters(LinkedList<SettingParameter> settingParameters, IOptimizationAdapter obj) {
        this.settingParameters = settingParameters;
		if (settingParameters != null) {
			this.assignSettingParameters(obj);
		}
    }


    /*
    checks every child if the objectiveValue is actual
     */
    @Override
    public boolean allChildsEvaluated(){

        for(Child c: this.actualValues){
            if(!c.isLatestObjectiveValue()){
                return false;
            }
        }
        return true;
    }

    /*
    This method sets the setting parameters if there is a setter in the
    model class with regular naming convention
    Variable: thisIsATest / _thisIsATest
    Setter: setThisIsATest
     */
    @Override
    public void assignSettingParameters(Object optimizer){

        for(SettingParameter sp : this.settingParameters){

            String name = sp.getName().split("\\.")[0];

            if(name.charAt(0)=='_'){
                name = name.substring(1);
            }

            String setterName = "set" + Character.toUpperCase(name.charAt(0)) + name.substring(1);
            boolean setterFound = true;
            
            try {
            	if (sp.getName().contains(".") == false) {
            		System.out.println("Trying to find setting parameter: " + setterName);
            		Method setterMethod = optimizer.getClass().getMethod( setterName, sp.getTypeDefinition() );
            		setterMethod.invoke(optimizer,sp.getTypeDefinition().cast(Caster.castValue(sp.getTypeDefinition(), sp.getValue())));
            	} else {
            		System.out.println("Trying to find indexed setting parameter: " + setterName);
            		String[] s = sp.getName().split("\\.");
            		int index = Integer.parseInt(s[1]);
            		Method setterMethod = optimizer.getClass().getMethod( setterName, Integer.class, sp.getTypeDefinition() );
            		setterMethod.invoke(optimizer, index, sp.getTypeDefinition().cast(Caster.castValue(sp.getTypeDefinition(), sp.getValue())));
            	}
            } catch (Exception e) {
                setterFound = false;
            }


            if(!setterFound){
                throw new NoSuchMethodError("No setter found for " + sp.getName());
            }
        }
    }

    @Override
    public void addSettingParameter(SettingParameter settingParameter){
        this.settingParameters.addLast(settingParameter);
    }

    @Override
    public OptimizationGoal getOptimizationGoal() {
        return optimizationGoal;
    }

    @Override
    public void setOptimizationGoal(OptimizationGoal optimizationGoal) {
        this.optimizationGoal = optimizationGoal;
    }

    @Override
    public LinkedList<Child> getActualValues(){
        LinkedList<Child> temp = new LinkedList<Child>();
        temp.addAll(this.actualValues);
        return temp;
    }

    @Override
    public  void setActualValues(LinkedList<Child> value){
        this.oldValues.addAll(value);
        ConcurrentLinkedQueue<Child> temp = new ConcurrentLinkedQueue<Child>();
        temp.addAll(value);
        this.actualValues = temp;
    }

    @Override
    public LinkedList<Child> getOldValues(){
        LinkedList<Child> temp = new LinkedList<Child>();
        temp.addAll(this.oldValues);
        return temp;
    }

    @Override
    public Object getValueOfSettingParameter(String name){

        for(SettingParameter sp : this.settingParameters){

            if(sp.getName().equals(name)){
                return sp.getValue();
            }
        }

        return null;
    }

    @Override
    public synchronized void runCallbacks(){


        if(this.callback != null && !this.callback.isEmpty()){

            for(IOptimizationCallback callback: this.callback){

                if(finished && !finishedCalled){
                    callback.optimizationFinished(this.getBestActualChild(), this.getBestChildOverall());
                    this.finishedCalled = true;
                }else {
                    LinkedList<Child> temp = new LinkedList<Child>();
                    temp.addAll(this.actualValues);
                    callback.getChildren(temp);
                }
            }
        }
    }

    @Override
    public void registerCallback(IOptimizationCallback callback){
        this.callback.add(callback);
    }

    @Override
    public void unregisterCallback(IOptimizationCallback callback){
        this.callback.remove(callback);
    }

    @Override
    public void resetIterationCounter(){
        this.iterationCounter = 0;
    }

    @Override
    public int getIterationCounter(){
        return this.iterationCounter;
    }

    @Override
    public String getID() {
        return ID;
    }

    @Override
    public void setID(String ID) {
        this.ID = ID;
    }

    @Override
    public LinkedList<IOptimizationCallback> getCallback() {
        return callback;
    }

    @Override
    public void setCallback(LinkedList<IOptimizationCallback> callback) {
        this.callback = callback;
    }

    @Override
    public LinkedList<SettingParameter> getSimulationParameters() {
        return simulationParameters;
    }

    @Override
    public void setSimulationParameters(LinkedList<SettingParameter> simulationParameters) {
        this.simulationParameters = simulationParameters;
    }
}
