package Adapter.Simulation;

import Callbacks.ISimulationCallback;
import Caster.Caster;
import DataTypes.ResultParameter;
import DataTypes.SettingParameter;
import DataTypes.Simulation.SimRunData;
import Interfaces.ISimulationAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.concurrent.Callable;

@XmlAccessorType(XmlAccessType.NONE)
public abstract class SimulationAdapter implements Callable<SimRunData>, ISimulationAdapter { //extends Thread implements ISimulationAdapter {

    //Variables
    private LinkedList<ISimulationCallback> callback;
    private LinkedList<ResultParameter> resultParameters;
    private LinkedList<SettingParameter> settingParameters;
    private String ID;
    private long simulationTime;

    //Constructor
    /*
    If you create your own constructor call super(), or initialize the variables by your own
     */

    public SimulationAdapter(){
        callback = new LinkedList<ISimulationCallback>();
        resultParameters = new LinkedList<ResultParameter>();
        settingParameters = new LinkedList<SettingParameter>();
    }

    //Implemented Methods

        /*
        This method sets the setting parameters if there is a setter in the
        model class with regular naming convention
        Variable: thisIsATest / _thisIsATest
        Setter: setThisIsATest
         */
    @Override
    public void assignSettingParameters(Object model){

        for(SettingParameter sp : this.settingParameters){

            String name = sp.getName().split("\\.")[0];

            if(name.charAt(0)=='_'){
                name = name.substring(1);
            }

            String setterName = "set" + Character.toUpperCase(name.charAt(0)) + name.substring(1);
            boolean setterFound = true;

            try {
            	if (sp.getName().contains(".") == false) {
            		//System.out.println("Trying to find setting parameter: " + setterName);
            		Method setterMethod = model.getClass().getMethod( setterName, sp.getTypeDefinition() );
            		setterMethod.invoke(model,sp.getTypeDefinition().cast(Caster.castValue(sp.getTypeDefinition(), sp.getValue())));
            	} else {
            		//System.out.println("Trying to find indexed setting parameter: " + setterName);
            		String[] s = sp.getName().split("\\.");
            		int index = Integer.parseInt(s[1]);
					Method setterMethod = model.getClass().getMethod( setterName, Integer.class, sp.getTypeDefinition() );
					setterMethod.invoke(model, index, sp.getTypeDefinition().cast(Caster.castValue(sp.getTypeDefinition(), sp.getValue())));
            	}
            } catch (Exception e) {
				e.printStackTrace();
                setterFound = false;
            }


            if(!setterFound){
                throw new NoSuchMethodError("No setter found for " + sp.getName());
            }
        }
    }

    /*
    This method gets the result parameters if there is a getter in the
    model class with regular naming convention and saves these values
    in this class
    Variable: thisIsATest / _thisIsATest
    Getter: getThisIsATest
    */
    @Override
    public void extractResultParameters(Object model){

        for(ResultParameter rp : this.resultParameters){

            String name = rp.getName();

            if(name.charAt(0)=='_'){
                name = name.substring(1);
            }

            String setterName = "get" + Character.toUpperCase(name.charAt(0)) + name.substring(1);
            boolean setterFound = true;

            try {
                Method setterMethod = model.getClass().getMethod( setterName );
                Comparable value = (Comparable)setterMethod.invoke(model);
                rp.setValue(value);
            } catch (Exception e) {
                setterFound = false;
            }

            if(!setterFound){
                throw new NoSuchMethodError("No getter found for " + rp.getName());
            }
        }
    }

    /*
    This method is called after the "StartSimulation" and calls the registered callbacks
     */
    @Override
    public void runCallbacks(){

        if(this.callback != null && !this.callback.isEmpty()){

            SimRunData simRunData = new SimRunData(getID(),getSettingParameters(), getResultParameters(), getObjectiveValue(), true, isMultiObjectiveSimulation(), getMultiObjectiveValues());

            for(ISimulationCallback callback: this.callback){
                callback.getCalled(simRunData);
            }
        }
    }

    @Override
    public LinkedList<ResultParameter> getResultParameters(){
        return this.resultParameters;
    }

    @Override
    public void addResultParameter(ResultParameter resultParameter){
        this.resultParameters.addLast(resultParameter);
    }

    @Override
    public void addResultParameters(LinkedList<ResultParameter> resultParameters){
        this.resultParameters.addAll(resultParameters);
    }

    @Override
    public void overwriteResultParameters(LinkedList<ResultParameter> resultParameters){
        this.resultParameters = resultParameters;
    }

    @Override
    public LinkedList<SettingParameter> getSettingParameters(){
        return this.settingParameters;
    }

    @Override
    public void setSettingParameters(LinkedList<SettingParameter> settingParameters){
        overwriteSettingParameters(settingParameters);
    }

    @Override
    public void addSettingParameter(SettingParameter settingParameter){
        this.settingParameters.addLast(settingParameter);
    }

    @Override
    public void addSettingParameters(LinkedList<SettingParameter> settingParameters){
        this.settingParameters.addAll(settingParameters);
    }
    
    @Override
    public void addMultiSettingParameter(SettingParameter sp, int count) {
    	for (int i = 0; i < count; i++) {
            try {
				addSettingParameter(new SettingParameter(sp.getName() + "." + i, sp.getTypeDefinition(), sp.getDescription() + " (index=" + i + ")", sp.getValue(), sp.getMinValue(), sp.getMaxValue(), sp.getStepSize()));
			} catch (Exception e) {
				e.printStackTrace();
			}
    	}
    }

    @Override
    public void overwriteSettingParameters(LinkedList<SettingParameter> settingParameters){
        this.settingParameters = settingParameters;
    }

    @Override
    public long getSimulationTime() {
        return simulationTime;
    }

    @Override
    public void setSimulationTime(long _simulationTime) {
        this.simulationTime = _simulationTime;
    }

    public LinkedList<ISimulationCallback> getCallback() {
        return callback;
    }

    public void setCallback(LinkedList<ISimulationCallback> callback) {
        this.callback = callback;
    }

    @Override
    public void registerCallback(ISimulationCallback callback){
        this.callback.add(callback);
    }

    @Override
    public void unregisterCallback(ISimulationCallback callback){
        this.callback.remove(callback);
    }

    @Override
    public String getID() {
        return ID;
    }

    @Override
    public void setID(String ID) {
        this.ID = ID;
    }

    @Override
    public void run(){
    	System.err.println("run of SimulationAdapter: should not be called any more");
        StartSimulation();
        runCallbacks();
    }
    
    public SimRunData call() {
        StartSimulation();
        SimRunData simRunData = new SimRunData(getID(),getSettingParameters(), getResultParameters(), getObjectiveValue(), true, isMultiObjectiveSimulation(), getMultiObjectiveValues());        
//    	return getMultiObjectiveValues();
        return simRunData;
    }
    
    @Override
    public boolean isMultiObjectiveSimulation() {
    	return false;
    }
    
    @Override
    public Double[] getMultiObjectiveValues() {
    	return null;
    }

}
