package DataTypes.Simulation;

import DataTypes.ResultParameter;
import DataTypes.SettingParameter;

import java.util.LinkedList;

public class SimRunData {

    private LinkedList<SettingParameter> settingParameters;
    private LinkedList<ResultParameter> resultParameters;
    private Double objectiveValue;
    private boolean isMultiObjectiveSimulation;
    private Double[] objectiveValues;
    private String ID;
    private boolean validRun;
/*
    public SimRunData(String id, LinkedList<SettingParameter> settingParameters, LinkedList<ResultParameter> resultParameters, Double objectiveValue) {
        this(id, settingParameters, resultParameters, objectiveValue, true);
    }

    public SimRunData(String id, LinkedList<SettingParameter> settingParameters, LinkedList<ResultParameter> resultParameters, Double objectiveValue, boolean validRun) {
    	this(id, settingParameters, resultParameters, objectiveValue, validRun, false, null);
    }
    */
    public SimRunData(String id, LinkedList<SettingParameter> settingParameters, LinkedList<ResultParameter> resultParameters, Double objectiveValue, boolean validRun, boolean isMultiObjective, Double[] objectiveValues) {
    	this.ID = id;
    	this.settingParameters = settingParameters;
    	this.resultParameters = resultParameters;
    	this.objectiveValue = objectiveValue;
    	this.validRun = validRun;
    	this.isMultiObjectiveSimulation = isMultiObjective;
    	this.objectiveValues = objectiveValues;
    }
    
    public Double getObjectiveValue() {
        return objectiveValue;
    }

    public void setObjectiveValue(Double objectiveValue) {
        this.objectiveValue = objectiveValue;
    }

    public LinkedList<ResultParameter> getResultParameters() {
        return resultParameters;
    }

    public LinkedList<SettingParameter> getSettingParameters() {
        return settingParameters;
    }

    public String getID() {
        return this.ID;
    }

    public boolean isValidRun() {
        return validRun;
    }

    public void setValidRun(boolean validRun) {
        this.validRun = validRun;
    }

	public boolean isMultiObjectiveSimulation() {
		return isMultiObjectiveSimulation;
	}

	public void setMultiObjectiveSimulation(boolean isMultiObjectiveSimulation) {
		this.isMultiObjectiveSimulation = isMultiObjectiveSimulation;
	}

	public Double[] getObjectiveValues() {
		return objectiveValues;
	}

	public void setObjectiveValues(Double[] objectiveValues) {
		this.objectiveValues = objectiveValues;
	}

}
