package DataTypes;

import Interfaces.IResultParameter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ResultParameter")
public class ResultParameter extends Parameter implements IResultParameter {

    public ResultParameter(){
        super();
    }

    public ResultParameter(String name, Class typeDefinition, String description) throws Exception {
        super(name, typeDefinition, description);
    }


}

