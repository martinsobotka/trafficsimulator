package DataTypes;

import Interfaces.IParameter;

import javax.xml.bind.annotation.*;
import java.util.UUID;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Parameter")
public abstract class Parameter implements IParameter {

    @XmlElement(type=Object.class)
    private Comparable value;

    private String name;
    private Class typeDefinition;
    private String description;


    public String getID() {
        return ID;
    }

    @XmlTransient
    private String ID;

    public Parameter() {
    }

    public Parameter(String name, Class typeDefinition, String description) throws Exception {

        if(name == null || name.isEmpty()){
            throw new Exception("Name can't be null or empty");
        }else if(typeDefinition== null ){
            throw new Exception("Type definition can't be null");
        }

        this.ID = UUID.randomUUID().toString();
        this.name = name;
        this.typeDefinition = typeDefinition;
        this.description = description;
    }



    public String getName() {
        return name;
    }

    public Class getTypeDefinition() {
        return typeDefinition;
    }

    public String getDescription() {
        return description;
    }

    public void setValue(Comparable value){
        if(this.value!=null){
            this.ID = UUID.randomUUID().toString();
        }
        this.value = value;
    }

    public Comparable getValue(){
        return this.value;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTypeDefinition(Class typeDefinition) {
        this.typeDefinition = typeDefinition;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setID(String ID) {
        this.ID = ID;
    }
}
