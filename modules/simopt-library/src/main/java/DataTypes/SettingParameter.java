package DataTypes;

import Interfaces.ISettingParameter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SettingParameter")
public class SettingParameter extends Parameter implements ISettingParameter {

    private boolean hasRange = false;
    @XmlElement(type=Object.class)
    private Comparable minValue;
    @XmlElement(type=Object.class)
    private Comparable maxValue;
    @XmlElement(type=Object.class)
    private Comparable stepSize;
    private boolean forOptimization;

    public SettingParameter(){
        super();
        this.forOptimization = true;
    }

    public SettingParameter(String name, Class typeDefinition,String description) throws Exception {

        super(name, typeDefinition, description);
        this.forOptimization = true;
    }

    public SettingParameter(String name, Class typeDefinition,String description, Comparable value) throws Exception {

        super(name, typeDefinition, description);
        setValue(value);
        this.forOptimization = true;
    }

    public SettingParameter(String name, Class typeDefinition, String description, Comparable value, Comparable minValue, Comparable maxValue) throws Exception {

        this(name, typeDefinition, description, value);

        if(minValue==null || maxValue == null){
            throw new Exception("Range (min,max) can't be null");
        }else {
            this.hasRange = true;
            this.maxValue = maxValue;
            this.minValue = minValue;
        }
    }

    public SettingParameter(String name, Class typeDefinition, String description, Comparable value, Comparable minValue, Comparable maxValue, Comparable stepSize) throws Exception {

        this(name, typeDefinition, description, value, minValue, maxValue);
        this.stepSize = stepSize;
        this.forOptimization = true;
    }

    public boolean hasRange(){
        return hasRange;
    }

    public void setRange(Comparable min, Comparable max){
        this.minValue = min;
        this.maxValue = max;
        this.hasRange = true;
    }

    public Comparable getMinValue(){

        return minValue;
    }

    public Comparable getMaxValue(){
        return maxValue;
    }

    public boolean isHasRange() {
        return hasRange;
    }

    public void setHasRange(boolean hasRange) {
        this.hasRange = hasRange;
    }

    public void setMinValue(Comparable minValue) {
        this.minValue = minValue;
    }

    public void setMaxValue(Comparable maxValue) {
        this.maxValue = maxValue;
    }

    public Comparable getStepSize() {
        return stepSize;
    }

    public void setStepSize(Comparable stepSize) {
        this.stepSize = stepSize;
    }

    public boolean isForOptimization() {
        return forOptimization;
    }

    public void setForOptimization(boolean forOptimization) {
        this.forOptimization = forOptimization;
    }

}
