package DataTypes.Optimization;

import DataTypes.Parameter;
import DataTypes.SettingParameter;

import javax.xml.bind.annotation.*;
import java.util.LinkedList;
import java.util.UUID;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Child")
public class Child {

    @XmlElement(type=Parameter.class)
    private LinkedList<SettingParameter> modelParameters;
    private double objectiveValue;
    private double[] multiObjective;
    @XmlTransient
    private boolean latestObjectiveValue;
    @XmlTransient
    private String ID;
    private boolean validRun;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public Child() {
    }

    public Child(LinkedList<SettingParameter> parameters) {
        this.modelParameters = parameters;
        this.latestObjectiveValue = false;
        this.ID = UUID.randomUUID().toString();
        this.validRun = true;
    }

    public Child(LinkedList<SettingParameter> parameters, double objectiveValue, String id) {
        this.modelParameters = parameters;
        this.objectiveValue = objectiveValue;
        this.ID = id;
        this.latestObjectiveValue = true;
        this.validRun = true;
    }

    public Child(LinkedList<SettingParameter> parameters, double objectiveValue) {
        this.modelParameters = parameters;
        this.objectiveValue = objectiveValue;
        this.ID = UUID.randomUUID().toString();
        this.latestObjectiveValue = true;
        this.validRun = true;
    }

    public boolean isLatestObjectiveValue() {
        return latestObjectiveValue;
    }

    public LinkedList<SettingParameter> getModelParameters() {
        return modelParameters;
    }

    public void setModelParameters(LinkedList<SettingParameter> modelParameters) {
        this.modelParameters = modelParameters;
        this.latestObjectiveValue = false;
    }

    public double getObjectiveValue() {
        return objectiveValue;
    }

    public void setObjectiveValue(double objectiveValue) {
        this.objectiveValue = objectiveValue;
        this.latestObjectiveValue = true;
    }

    public Child copy(){
        LinkedList<SettingParameter> sp = new LinkedList<SettingParameter>();
        sp.addAll(this.modelParameters);
        Child copy = new Child(sp);
        return copy;
    }

    public boolean isValidRun() {
        return validRun;
    }

    public void setValidRun(boolean validRun) {
        this.validRun = validRun;
    }

    public void setLatestObjectiveValue(boolean latestObjectiveValue) {
        this.latestObjectiveValue = latestObjectiveValue;
    }

	public double[] getMultiObjective() {
		return multiObjective;
	}

	public void setMultiObjective(double[] multiObjective) {
		this.multiObjective = multiObjective;
	}
	@Override
	public String toString()  {
		return "";
	}
}
