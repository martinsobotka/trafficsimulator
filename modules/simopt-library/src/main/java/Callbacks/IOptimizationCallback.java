package Callbacks;

import DataTypes.Optimization.Child;

import java.util.LinkedList;

public interface IOptimizationCallback {
    void getChildren(LinkedList<Child> childrenToEvaluate);
    /*
    This method returns the bestActualChild and the bestOverallChild
     */
    void optimizationFinished(Child bestActualChild, Child bestOverallChild);
}
