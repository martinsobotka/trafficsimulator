package Callbacks;

import DataTypes.Simulation.SimRunData;

public interface ISimulationCallback {
    void getCalled(SimRunData simRunData);
}
