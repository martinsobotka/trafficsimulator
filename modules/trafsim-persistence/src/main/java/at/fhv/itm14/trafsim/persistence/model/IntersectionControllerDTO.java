package at.fhv.itm14.trafsim.persistence.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
public class IntersectionControllerDTO extends DTO {

	private double turnaroundTime;
	private double startOffset;
	@XmlElementWrapper(name = "phases")
	@XmlElement(name = "phase")
	private List<IntersectionPhaseDTO> phases;

	public double getStartOffset() {
		return startOffset;
	}

	public void setStartOffset(double startOffset) {
		this.startOffset = startOffset;
	}

	public List<IntersectionPhaseDTO> getPhases() {
		return phases;
	}

	public void setPhases(List<IntersectionPhaseDTO> phases) {
		this.phases = phases;
	}

	public double getTurnaroundTime() {
		return turnaroundTime;
	}

	public void setTurnaroundTime(double turnaroundTime) {
		this.turnaroundTime = turnaroundTime;
	}
}
