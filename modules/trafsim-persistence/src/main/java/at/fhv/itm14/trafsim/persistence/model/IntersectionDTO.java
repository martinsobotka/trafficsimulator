package at.fhv.itm14.trafsim.persistence.model;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
public class IntersectionDTO extends DTO {

	private int size;
	private double serviceDelay;
	@XmlElementWrapper(name = "in")
	@XmlElement(name = "producer")
	private List<ElementDTO> in;
	@XmlElementWrapper(name = "out")
	@XmlElement(name = "consumer", nillable = true)
	private List<ElementDTO> out;
	@XmlElementWrapper(name = "connectionQueues")
	@XmlElement(name = "queue", nillable = true)
	private List<ConnectionQueueDTO> connectionQueues;
	private IntersectionControllerDTO controller;

	@XmlAccessorType(XmlAccessType.FIELD)
	public static class ConnectionQueueDTO {
		public Integer inIndex;
		@XmlElementWrapper(name = "outConnections")
		@XmlElement(name = "outIndex")
		public List<OutConnectionDTO> outConnection;
	}
	
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class OutConnectionDTO {
		@XmlValue
		public Integer outIndex;
		@XmlAttribute
		public Double probability;
		@XmlAttribute
		public Double traverseTime;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public double getServiceDelay() {
		return serviceDelay;
	}

	public void setServiceDelay(double serviceDelay) {
		this.serviceDelay = serviceDelay;
	}

	public List<ElementDTO> getIn() {
		return in;
	}

	public void setIn(List<ElementDTO> in) {
		this.in = in;
	}

	public List<ElementDTO> getOut() {
		return out;
	}

	public void setOut(List<ElementDTO> out) {
		this.out = out;
	}

	public List<ConnectionQueueDTO> getConnectionQueues() {
		return connectionQueues;
	}

	public void setConnectionQueues(List<ConnectionQueueDTO> connectionQueues) {
		this.connectionQueues = connectionQueues;
	}

	public IntersectionControllerDTO getController() {
		return controller;
	}

	public void setController(IntersectionControllerDTO controller) {
		this.controller = controller;
	}

	public int getInDirectionId(DTO inDTO) {
		for (ElementDTO e : in) {
			if (e.getRef().equals(inDTO)) {
				return e.getIndex();
			}
		}
		return -1;
	}

	public int getOutDirectionId(DTO outDTO) {
		for (ElementDTO e : out) {
			if (e.getRef().equals(outDTO)) {
				return e.getIndex();
			}
		}
		return -1;
	}
}
