package at.fhv.itm14.trafsim.persistence.model;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class ElementDTO {
	@XmlAttribute
	private int index;
	@XmlValue
	@XmlIDREF
	private DTO ref;

	public ElementDTO(int order, DTO ref) {
		this.index = order;
		this.ref = ref;
	}

	public ElementDTO() {
	}
	
	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public DTO getRef() {
		return ref;
	}

	public void setRef(DTO ref) {
		this.ref = ref;
	}
}
