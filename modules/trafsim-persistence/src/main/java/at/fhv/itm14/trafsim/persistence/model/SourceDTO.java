package at.fhv.itm14.trafsim.persistence.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlIDREF;

@XmlAccessorType(XmlAccessType.FIELD)
public class SourceDTO extends DTO {
	@XmlIDREF
	private DTO consumer;
	private NumericalDistDTO dist;

	public DTO getConsumer() {
		return consumer;
	}

	public void setConsumer(DTO consumer) {
		this.consumer = consumer;
	}

	public NumericalDistDTO getDist() {
		return dist;
	}

	public void setDist(NumericalDistDTO dist) {
		this.dist = dist;
	}
}
