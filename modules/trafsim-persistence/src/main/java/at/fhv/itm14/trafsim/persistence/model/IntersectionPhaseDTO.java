package at.fhv.itm14.trafsim.persistence.model;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
public class IntersectionPhaseDTO extends DTO {

	private double greenDurationPart;
	private double yellowDuration;
	@XmlElementWrapper(name = "phaseConnections")
	@XmlElement(name = "phaseConnection")
	private List<PhaseConnectionDTO> connections;
	
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class PhaseConnectionDTO {
		@XmlAttribute
		public int inIndex;
		@XmlAttribute
		public int outIndex;
	}

	public double getGreenDurationPart() {
		return greenDurationPart;
	}

	public void setGreenDurationPart(double greenDurationPart) {
		this.greenDurationPart = greenDurationPart;
	}

	public double getYellowDuration() {
		return yellowDuration;
	}

	public void setYellowDuration(double yellowDuration) {
		this.yellowDuration = yellowDuration;
	}

	public List<PhaseConnectionDTO> getConnections() {
		return connections;
	}

	public void setConnections(List<PhaseConnectionDTO> connections) {
		this.connections = connections;
	}
}
