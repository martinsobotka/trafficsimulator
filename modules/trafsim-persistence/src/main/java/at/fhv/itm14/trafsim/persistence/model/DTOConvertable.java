package at.fhv.itm14.trafsim.persistence.model;

public interface DTOConvertable<T extends DTO> {
	/**
	 * Convert object to a DTO (data transfer object)
	 * @return DTO
	 */
	T toDTO();
}
