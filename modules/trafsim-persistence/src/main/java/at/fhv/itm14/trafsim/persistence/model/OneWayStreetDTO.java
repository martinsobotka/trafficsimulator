package at.fhv.itm14.trafsim.persistence.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlIDREF;

@XmlAccessorType(XmlAccessType.FIELD)
public class OneWayStreetDTO extends DTO {
	private double traverseTime;
	@XmlIDREF
	private DTO consumer;
	private int capacity;
	
	public double getTraverseTime() {
		return traverseTime;
	}

	public void setTraverseTime(double traverseTime) {
		this.traverseTime = traverseTime;
	}

	public DTO getConsumer() {
		return consumer;
	}

	public void setConsumer(DTO consumer) {
		this.consumer = consumer;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
}
