package at.fhv.itm14.trafsim.persistence.model;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "scenario")
@XmlAccessorType (XmlAccessType.FIELD)
public class ScenarioDTO extends DTO {
	@XmlElementWrapper(name = "sources")
	@XmlElements(@XmlElement(name = "source"))
	private List<SourceDTO> sources;
	@XmlElementWrapper(name = "sinks")
	@XmlElements(@XmlElement(name = "sink"))
	private List<SinkDTO> sinks;
	@XmlElementWrapper(name = "streets")
	@XmlElements(@XmlElement(name = "street"))
	private List<OneWayStreetDTO> streets;
	@XmlElementWrapper(name = "intersections")
	@XmlElements(@XmlElement(name = "intersection"))
	private List<IntersectionDTO> intersections;

	public List<SourceDTO> getSources() {
		return sources;
	}

	public void setSources(List<SourceDTO> sources) {
		this.sources = sources;
	}

	public List<SinkDTO> getSinks() {
		return sinks;
	}

	public void setSinks(List<SinkDTO> sinks) {
		this.sinks = sinks;
	}
	
	public List<OneWayStreetDTO> getStreets() {
		return streets;
	}

	public void setStreets(List<OneWayStreetDTO> streets) {
		this.streets = streets;
	}

	public List<IntersectionDTO> getIntersections() {
		return intersections;
	}

	public void setIntersections(List<IntersectionDTO> intersections) {
		this.intersections = intersections;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (SourceDTO s : sources) {
			sb.append("(");
			sb.append(s.getName());
			sb.append(" -> ");
			sb.append(s.getConsumer().getName());
			//sb.append(" [");
			//sb.append(s.getConsumer().getId());
			sb.append("\n");
		}
		for (OneWayStreetDTO s : streets) {
			sb.append(s.getName());
			//sb.append(" [");
			//sb.append(s.getId());
			//sb.append("] --> ");
			sb.append(" -> ");
			sb.append(s.getConsumer().getName());
			//sb.append(" [");
			//sb.append(s.getConsumer().getId());
			sb.append("\n");
		}
		for (IntersectionDTO i : intersections) {
			//sb.append(" [");
			//sb.append(i.getId());
			//sb.append("] --> \n");
			for (ElementDTO e : i.getOut()) {
				sb.append(i.getName());
				sb.append("  ");
				sb.append(" -> ");
				sb.append(e.getRef().getName());
			//	sb.append(" [");
			//	sb.append(e.getRef().getId());
				sb.append("\n");
			}
		}
		return sb.toString();
	}

	
}
