package Callbacks;

import DataTypes.Simulation.SimRunData;

import java.util.LinkedList;

/*
This class handles the callbacks from the simulation in the class AbstactExperimentExecutor
 */
public class SimExecutorCallbackThread extends Thread {

    LinkedList<ISimulationCallback> callbacks;
    SimRunData data;

    public void addData(LinkedList<ISimulationCallback> callbacks, SimRunData data){
        this.callbacks = callbacks;
        this.data = data;
    }

    @Override
    public void run() {
        for(ISimulationCallback c:this.callbacks){
            c.getCalled(this.data);
        }
    }
}
