package Callbacks;


import Adapter.Optimization.OptimizationAdapter;
import DataTypes.Simulation.SimRunData;

/*
This class handles a callback from the simulation and redirects the data to the optimizer
 */
public class OptExecutorCallbackThread extends Thread {

    private SimRunData data;
    private OptimizationAdapter optimization;

    public void addData(OptimizationAdapter optimization, SimRunData data){
        this.data = data;
        this.optimization = optimization;
    }

    @Override
    public void run() {
        this.optimization.addFinishedSimulation(data);
    }
}
