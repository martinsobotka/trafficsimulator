package Callbacks;


import DataTypes.Optimization.Child;

import java.util.LinkedList;

/*
This class handles the ExperimentCallback
 */
public class ExpExecutorCallbackThread extends Thread {
    LinkedList<ExperimentCallback> callbacks;
    Child data;

    public void addData(LinkedList<ExperimentCallback> callbacks, Child data){
        this.callbacks = callbacks;
        this.data = data;
    }

    @Override
    public void run() {
        for(ExperimentCallback c:this.callbacks){
            c.optRunFinished(this.data);
        }
    }
}
