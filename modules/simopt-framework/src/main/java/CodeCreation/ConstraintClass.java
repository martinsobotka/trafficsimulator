package CodeCreation;

import DataTypes.Parameter;
import DataTypes.ResultParameter;
import DataTypes.SettingParameter;

import java.util.LinkedList;

/*
this class is used as the base class for the generated ConstraintClasses
 */
public abstract class ConstraintClass extends GenericClass {

    public ConstraintClass(){
        super();
    }

    public ConstraintClass(LinkedList<Parameter> params){
        super(params);
    }

    public ConstraintClass(LinkedList<SettingParameter> settingParameters, LinkedList<ResultParameter> resultParameters) {
        super(settingParameters, resultParameters);
    }

    public abstract ConstraintClass copy();

    public abstract boolean checkConstraint();
}
