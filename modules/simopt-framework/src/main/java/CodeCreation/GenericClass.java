package CodeCreation;


import DataTypes.Parameter;
import DataTypes.ResultParameter;
import DataTypes.SettingParameter;

import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedQueue;

/*
this class implements the methods that are needed for ObjectiveClass and GenericClass
 */
public abstract class GenericClass {

    private ConcurrentLinkedQueue<Parameter> _parameters;

    public GenericClass(){
        this._parameters = new ConcurrentLinkedQueue<Parameter>();
    }

    public GenericClass(LinkedList<SettingParameter> settingParameters, LinkedList<ResultParameter> resultParameters) {
        this._parameters = new ConcurrentLinkedQueue<Parameter>();
        this._parameters.addAll( settingParameters);
        this._parameters.addAll( resultParameters);
    }

    public GenericClass(LinkedList<Parameter> params){
        ConcurrentLinkedQueue<Parameter> p = new ConcurrentLinkedQueue<Parameter>();
        p.addAll(params);
        this._parameters = p;
    }

    public void overwriteList(LinkedList<SettingParameter> settingParameters, LinkedList<ResultParameter> resultParameters){
        this._parameters.clear();
        this._parameters.addAll( settingParameters);
        this._parameters.addAll( resultParameters);
    }

    public Object getParameterValue(String name){

        for(Parameter p : _parameters){
            if(p.getName().toLowerCase().equals(name.toLowerCase())){
                return p.getValue();
            }
        }
        return null;
    }

    public Class getParameterType(String name){

        for(Parameter p : _parameters){
            if(p.getName().toLowerCase().equals(name.toLowerCase())){
                return p.getTypeDefinition();
            }
        }
        return null;
    }

    public LinkedList<Parameter> get_parameters() {
        LinkedList<Parameter> p = new LinkedList<Parameter>();
        p.addAll(_parameters);
        return p;
    }

    public void set_parameters(LinkedList<Parameter> param) {
        ConcurrentLinkedQueue<Parameter> p = new ConcurrentLinkedQueue<Parameter>();
        p.addAll(param);
        this._parameters = p;
    }

    public abstract GenericClass copy();


}
