package CodeCreation;

import DataTypes.Parameter;
import DataTypes.ResultParameter;
import DataTypes.SettingParameter;

import java.util.LinkedList;

/*
this class is used as the base class for the generated ObjectiveClasses
 */
public abstract class ObjectiveClass extends GenericClass {

    public ObjectiveClass(){
        super();
    }

    public ObjectiveClass(LinkedList<Parameter> params){
        super(params);
    }

    public ObjectiveClass(LinkedList<SettingParameter> settingParameters, LinkedList<ResultParameter> resultParameters) {
        super(settingParameters, resultParameters);
    }

    public abstract ObjectiveClass copy();

    public abstract double calculateObjective();

	public abstract Double[] calculateMultiObjectives();

}
