package CodeCreation;


import DataTypes.ResultParameter;
import DataTypes.SettingParameter;

import javax.tools.JavaCompiler;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;
import java.io.*;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.UUID;

public class CodeGenerator {

    private static String  method = "((Double)Caster.castValue(Double.class,getParameterValue(\"[N]\")))";
    private static String tempDir = "temp";
    private static String pathToDir;
    private static URL directory;

    private static  LinkedList<String> keywords = new LinkedList<String>(
            Arrays.asList("Double","double","Float","float","int", "Integer","boolean", "byte","short","long","char","new","private","class","this","try","while"));

    /*
    returns the text of the file which is defined in the path
     */
    public static String GetTextFromFile(String path){

        try {

            InputStream ipstream = ObjectiveClass.class.getResourceAsStream(path);
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(ipstream));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            return sb.toString();

        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /*
    generates a random string
     */
    private static String GetRandomClassName(){
        return "class" + (UUID.randomUUID().toString()).replace("-","");
    }

    /*
    creates the path of the compiled class file
     */
    private static String CreatePath(String className)  {

        return GetPathPrefix() + className + ".java";
    }

    /*
    returns the path to the folder of the running jar
     */
    private static String GetPathPrefix(){

        if(pathToDir!=null){
            return pathToDir;
        }
        try {
            String prefix = CodeGenerator.class.getProtectionDomain().getCodeSource().getLocation().getPath();
            String pathToJar = URLDecoder.decode(prefix, "UTF-8");
            String pathOfParent = new File(pathToJar).getParentFile().getPath();
            String fileSeparator = System.getProperty("file.separator");
            String newDir = pathOfParent + fileSeparator + tempDir + fileSeparator;
            File file = new File(newDir);
            file.mkdir();
            directory = file.toURL();
            pathToDir = newDir;
            return newDir;

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /*
    generates the code for a ObjectiveClass with the specified function
     */
    private static String GetObjectiveCodeBlock(String className, String function, String multiFunction){

       // String code =  GetTextFromFile(GetPathPrefix()+ "CodeCreation/ObjectiveClassText");
        String code =  GetTextFromFile("ObjectiveClassText");
        code = code.replace("[NAME]", className);
        code = code.replace("[X]",function);
        code = code.replace("[Y]",multiFunction);
        return code;
    }

    /*
    generates the code for a ConstraintClass with the specified function
     */
    private static String GetConstraintCodeBlock(String className, String function){

        String code =  GetTextFromFile("ConstraintClassText");
        code = code.replace("[NAME]", className);
        code = code.replace("[X]",function);
        return code;
    }

    /*
    generates a GenericClass, based on the GenericClassType, with the specified function
     */
    private static GenericClass CreateClass(String function, String multiFunction, GenericClassType genericClassType) throws Exception {

        String className = GetRandomClassName();
        String path = CreatePath(className);
        String code = "";

        if(genericClassType.equals(GenericClassType.OBJECTIVE)){

            code = GetObjectiveCodeBlock(className, function, multiFunction);
        }else if(genericClassType.equals(GenericClassType.CONSTRAINT)){

            code = GetConstraintCodeBlock(className, function);
        }

        File file = new File(path);
        file.delete();
        FileWriter fw = new FileWriter(file);
        fw.write(code, 0, code.getBytes().length);
        fw.close();

        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);

        fileManager.setLocation(StandardLocation.CLASS_OUTPUT, Arrays.asList(file.getParentFile()));


        compiler.getTask(null,fileManager,null,null,null,fileManager.getJavaFileObjectsFromFiles(Arrays.asList(file))).call();
        fileManager.close();

        URL[] urls = new URL[]{file.toURL()};

        String s = file.getAbsolutePath().replace(".java", ".class");
        File classFile = new File(s);

        URL[] newUrls = new URL[]{directory};

        URLClassLoader urll = URLClassLoader.newInstance(newUrls);
        Class clazz = Class.forName(className, true, urll);

        Constructor<? extends Runnable> ctor = clazz.getConstructor();
        Object object = ctor.newInstance();

        return (GenericClass) object;

    }

    private static String ReplaceVarWithMeth(String function){

        //Add blank between all variables
        function = function.replaceAll("\\W+|\\W+", " $0 ");

        //Delete everything that is certainly no variable name
        String a = function.replaceAll("\\W+|\\W+", " ");

        String[] segments = a.split(" ");

        LinkedList<String> variables = new LinkedList<String>();

        //search for every variable name
        for(String v : segments){
            if(!v.isEmpty()) {
                try {
                    Double.parseDouble(v);
                } catch (Exception e) {
                    if (!variables.contains(v) && !keywords.contains(v))
                        variables.addLast(v);
                }
            }
        }

        //sort the variable names by their length start with longest
        variables.sort(new Comparator<String>() {
            @Override
            public int compare(String str1, String str2 ) {
                return str2.length() - str1.length();
            }
        });

        //replace the variable by a method call
        String newFunction = function;
        for(String v : variables){
            newFunction = newFunction.replace(" "+v+" ",method.replace("[N]",v));
        }

        return newFunction;
    }

    /*
    this method replaces the variables that are specified in the function with an call
    of the method getParameterValue(variableName)
     */
    private static String TuneConstraintCode(String function){

        function = "((boolean)("+function+"))";
        String newFunction = ReplaceVarWithMeth( function);
        return newFunction;
    }

    /*
    this method replaces the variables that are specified in the function with
    an call of the method getParameterValue(variableName)
    */
    private static String TuneFunctionCode(String function){

        function = "((Double)("+function+"))";
        String newFunction = ReplaceVarWithMeth( function);
        return newFunction;
    }

    /*
    This method creates a ObjectiveClass with the given function
     */
    public static ObjectiveClass CreateObjectiveClass(String function, String multiFunction) {

        ObjectiveClass fc = null;
        function = TuneFunctionCode(function);

        try{
            fc = (ObjectiveClass)CreateClass(function, multiFunction, GenericClassType.OBJECTIVE);
            System.out.println("ObjectiveClass created!");
        }catch(Exception e){
            System.out.println("ObjectiveClass could not be created. You need to");
            System.out.println("launch the JAR in the JDK if you want to use objective ");
        }
        return fc;
    }

    /*
    This method creates a ConstraintClass with the given function
     */
    public static ConstraintClass CreateConstraintClass(String constraint) {

        ConstraintClass cc = null;
        constraint = TuneConstraintCode(constraint);

        try{
            cc = (ConstraintClass)CreateClass(constraint, null, GenericClassType.CONSTRAINT);
            System.out.println("ConstraintClass created!");
        }catch(Exception e){
            System.out.println("ConstraintClass could not be created. You need to");
            System.out.println("lauch the JAR in the JDK if you want to use constraints");
        }
        return cc;
    }

    /*
    This method tries to run the "ObjectiveClass" and returns true if no exception occurs
     */
    public static boolean isClassRunnable(ObjectiveClass clazz, LinkedList<SettingParameter> sp, LinkedList<ResultParameter> rp){

        try {
            clazz.overwriteList(sp, rp);
            Double d = clazz.calculateObjective();
            return true;
        }catch(Exception e){
            return false;
        }
    }

    /*
    This method tries to run the "ConstraintClass" and returns true if no exception occurs
   */
    public static boolean isClassRunnable(ConstraintClass clazz, LinkedList<SettingParameter> sp, LinkedList<ResultParameter> rp){

        try {
            clazz.overwriteList(sp, rp);
            boolean b = clazz.checkConstraint();
            return true;
        }catch(Exception e){
            return false;
        }
    }



}
