package CodeCreation;

/*
This enum defines the type of the GenericClass (ConstraintClass/ObjectiveClass)
 */
public enum GenericClassType {
    CONSTRAINT,
    OBJECTIVE
}
