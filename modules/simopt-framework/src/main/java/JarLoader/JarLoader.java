package JarLoader;

import Adapter.Optimization.OptimizationAdapter;
import Adapter.Simulation.SimulationAdapter;
import Definition.ProjectDefinition;
import Executor.AbstractExecutor.AbstractExperimentExecutor;
import Executor.AbstractExecutor.AbstractOptExecutor;
import Executor.AbstractExecutor.AbstractSimExecutor;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


public class JarLoader {

    /*
    returns all classes that are defined in the given path
     */
    public static LinkedList<Class> GetAllClasses(String path) {

        try {
            LinkedList<String> availableClasses = GetClassNames(path);
            URLClassLoader loader = LoadJar(path);
            LinkedList<Class> classes = new LinkedList<Class>();

            for (String s : availableClasses) {

                try {
                    Class<?> c = Class.forName(s, true, loader);
					if (c != null) {
                        classes.addLast(c);
                    }
                } catch(ClassNotFoundException ex) {
					ex.printStackTrace();
				}
            }
            return classes;
        } catch(Exception ex){
            return null;
        }
    }

    /*
    returns the name of all classes that are defined in the given path
     */
    public static LinkedList<String> GetClassNames(String path) {

        try {
            LinkedList<String> classNames = new LinkedList<String>();
            ZipInputStream zip = new ZipInputStream(new FileInputStream(path));
            for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry()) {

                if (entry.getName().endsWith(".class") && !entry.isDirectory()) {

                    StringBuilder className = new StringBuilder();
                    for (String part : entry.getName().split("/")) {
                        if (className.length() != 0)
                            className.append(".");
                        className.append(part);
                        if (part.endsWith(".class"))
                            className.setLength(className.length() - ".class".length());
                    }
                    classNames.addLast(className.toString());
                }
            }
            return classNames;
        }catch(Exception ex){
            return null;
        }
    }

    /*
    returns an URLClassLoader that is capable to load classes from the given path
     */
    public static URLClassLoader LoadJar(String path) throws MalformedURLException {

        File file  = new File(path);
        URL url = file.toURL();
        URL[] urls = new URL[]{url};

        return URLClassLoader.newInstance(urls);
    }

    /*
    returns all fields of a class
     */
    public static LinkedList<Field> GetFieldsOfClass(String jarPath, String absolutClassName){

        try {
            URLClassLoader loader =  LoadJar(jarPath);
            Class<?> c = Class.forName(absolutClassName, true,loader);
            return GetFieldsOfClass(c);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /*
   returns all fields of a class
    */
    public static LinkedList<Field> GetFieldsOfClass(URLClassLoader loader, String name){

        try {
            Class<?> c = Class.forName(name, true, loader);
            return GetFieldsOfClass(c);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /*
   returns all declared fields of the passed class
    */
    public static LinkedList<Field> GetFieldsOfClass(Class c){
        return new LinkedList<Field>( Arrays.asList(c.getDeclaredFields()));
    }

    /*
   returns the class that is derived from SimulationAdapter, if one is specified in the given path
    */
    public static Class SearchForSimulationAdapterClass(String path) throws Exception {

        LinkedList<String> availableClasses = GetClassNames(path);
        URLClassLoader loader = LoadJar(path);
        System.out.println("Found " + availableClasses.size() + " classes in Simulation");

        for(String s: availableClasses){
			if (!s.contains("Adapter")) {
				continue;
			}
            try {
            	System.out.println("Checking " + s);
                Class<?> c = Class.forName(s, true, loader);
                if(SimulationAdapter.class.isAssignableFrom(c)){
                	System.out.println("Assignable");
                    if(!Modifier.isAbstract(c.getModifiers())) {
                        return c;
                    }
                } else {
                	System.out.println("Not Assignable!");
                }
            }catch(NoClassDefFoundError ex){
            	System.out.println("Could not load class.");
            }
        }

        return null;
    }

    /*
   returns the class that is derived from OptimizationAdapter, if one is specified in the given path
    */
    public static Class SearchForOptimizationAdapterClass(String path) throws Exception {

        LinkedList<String> availableClasses = GetClassNames(path);
        URLClassLoader loader = LoadJar(path);
        System.out.println("Found " + availableClasses.size() + " classes in Optimizer");

        for(String s: availableClasses){
			if (!s.contains("Optimizer") && !s.contains("NSGA")) {
				continue;
			}
            try {

                Class<?> c = Class.forName(s ,true, loader);
                System.out.println("Checking " + c.getSimpleName());
                if(OptimizationAdapter.class.isAssignableFrom(c)){
                	System.out.println("Assignable");
                    if(!Modifier.isAbstract(c.getModifiers())) {
                        return c;
                    }
                } else {
                	System.out.println("Not Assignable!");
                }
            }catch(NoClassDefFoundError ex){
            }
        }

        return null;
    }

    /*
   returns an instance of class that is derived from SimulationAdapter, if one is specified in the given path
    */
    public static SimulationAdapter GetSimulationAdapter(String path) throws Exception {

        Class c = SearchForSimulationAdapterClass(path);
        if (c == null) {
        	System.out.println("ERROR: Could not find simulation adapter!");
        	System.out.println("Aborting!");
        	System.exit(0);
        }
        Class<? extends Runnable> runClass = c.asSubclass(Runnable.class);
        Constructor<? extends Runnable> ctor = runClass.getConstructor();
        SimulationAdapter abstractSim = (SimulationAdapter) ctor.newInstance();
        abstractSim.initializeParameters();

        return abstractSim;
    }

    /*
   returns an instance of class that is derived from OptimizationAdapter, if one is specified in the given path
    */
    public static OptimizationAdapter GetOptimizationAdapter(String path) throws Exception {

        Class c = SearchForOptimizationAdapterClass(path);
        Constructor<? extends Runnable> ctor = c.getConstructor();
        OptimizationAdapter abstractOpt = (OptimizationAdapter) ctor.newInstance();
        abstractOpt.initializeParameters();

        return abstractOpt;
    }

    /*
    returns an instance of the passed class, if the class is derived from SimulationAdapter
     */
    public static SimulationAdapter GetNewInstance(Class<? extends SimulationAdapter> clazz) throws Exception {

        Constructor<? extends Runnable> ctor = clazz.getConstructor();
        SimulationAdapter abstractOpt = (SimulationAdapter) ctor.newInstance();
//        abstractOpt.initializeParameters();

        return abstractOpt;
    }

    /*
    returns an instance of the class (derived from AbstractOptExecutor) with the given classname (if exists)
    the instance is initialized with the passed optimizer and definition
     */
    public static AbstractOptExecutor GetOptExecutor(String className, OptimizationAdapter optimizer, ProjectDefinition projectDefinition) throws Exception{

        Class clazz = ClassLoader.getSystemClassLoader().loadClass(className);
        Constructor ctor = clazz.getConstructor(new Class[]{OptimizationAdapter.class, ProjectDefinition.class});
        AbstractOptExecutor optExecutor = (AbstractOptExecutor) ctor.newInstance(optimizer, projectDefinition);

        return  optExecutor;
    }

    /*
    returns an instance of the class (derived from AbstractSimExecutor) with the given classname (if exists)
    the instance is initialized with the passed simulation and definition
     */
    public static AbstractSimExecutor GetSimExecutor(String className, SimulationAdapter simulation, ProjectDefinition projectDefinition) throws Exception{

        Class clazz = ClassLoader.getSystemClassLoader().loadClass(className);
        Constructor ctor = clazz.getConstructor(new Class[]{SimulationAdapter.class, ProjectDefinition.class});
        AbstractSimExecutor simExecutor = (AbstractSimExecutor) ctor.newInstance(simulation, projectDefinition);

        return  simExecutor;
    }

    /*
    returns an instance of the class (derived from AbstractExperimentExecutor) with the given classname (if exists)
    the instance is initialized with the passed definition
     */
    public static AbstractExperimentExecutor GetExpExecutor(String className, ProjectDefinition projectDefinition) throws Exception{

        Class clazz = ClassLoader.getSystemClassLoader().loadClass(className);
        Constructor ctor = clazz.getConstructor(new Class[]{ProjectDefinition.class});
        AbstractExperimentExecutor expExexutor = (AbstractExperimentExecutor) ctor.newInstance(projectDefinition);

        return expExexutor;
    }


    /*
    returns a list of all classes that are derived from SimulationAdapter in the given path
     */
    public static LinkedList<Class> SearchForSimulationAdapterClasses(String path) {

        LinkedList<Class> abstractSimClasses = new LinkedList<Class>();
        try {
            LinkedList<String> availableClasses = GetClassNames(path);
            URLClassLoader loader = LoadJar(path);

            for (String s : availableClasses) {
                try {

                    Class<?> c = Class.forName(s, true, loader);

                    if (SimulationAdapter.class.isAssignableFrom(c)) {
                        if(!Modifier.isAbstract(c.getModifiers())) {
                            abstractSimClasses.addLast(c);
                        }
                    }
                } catch (NoClassDefFoundError ex) {
                }
            }
        }catch(Exception e){
        }
        return abstractSimClasses;
    }

}
