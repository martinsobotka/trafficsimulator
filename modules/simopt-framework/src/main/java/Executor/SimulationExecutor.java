package Executor;

import Adapter.Simulation.SimulationAdapter;
import DataTypes.ResultParameter;
import DataTypes.SettingParameter;
import DataTypes.Simulation.SimRunData;
import Definition.ProjectDefinition;
import Executor.AbstractExecutor.AbstractSimExecutor;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/*
This implementation of the AbstractSimExecutor does:
    - calculate the mean of the objective value of all replicationruns
        (check method evaluateReplications)
    - executes as many replications as is defined in replicationsCount
        (check method nextReplicationRunNeeded)
 */
public class SimulationExecutor extends AbstractSimExecutor{

    public SimulationExecutor(SimulationAdapter simulation, ProjectDefinition projectDefinition) {
        super(simulation, projectDefinition);
    }

    public SimulationExecutor(SimulationAdapter actualSimulation) {
        super(actualSimulation);
    }

    @Override
    public SimRunData evaluateReplications(String s){
    	// NOTE(Nico): This whole method seems pretty wrong, nevertheless, I adapted
    	// this method to multi-objectiveness
        Double meanObjective = Double.MAX_VALUE;
        List<Double> meanMultiObjectives = new ArrayList<>();
        LinkedList<SettingParameter> settingP = null;
        LinkedList<ResultParameter> resultP = null;
        String ID = s;

        for(SimRunData sr : this.getReplicationSim().get(s)){

            if(meanObjective==Double.MAX_VALUE){
                meanObjective = sr.getObjectiveValue();
                settingP = sr.getSettingParameters();
                resultP = sr.getResultParameters();
                Double[] mos = sr.getObjectiveValues();
                if (mos != null) {
                	for (int i = 0; i < mos.length; i++) {
                		meanMultiObjectives.add(mos[i]);
                	}
                }
            }else{
                meanObjective = (meanObjective+sr.getObjectiveValue())/2;
                Double[] mos = sr.getObjectiveValues();
                if (mos != null) {
                	for (int i = 0; i < mos.length; i++) {
                		meanMultiObjectives.set(i, (meanMultiObjectives.get(i) + mos[i]) / 2);
                	}
                }
            }
        }

       return (new SimRunData(ID,settingP, resultP, meanObjective, true, (meanMultiObjectives.size() != 0), meanMultiObjectives.toArray(new Double[meanMultiObjectives.size()])));
    }

    @Override
    public boolean nextReplicationRunNeeded(String id) {

        return (this.getReplicationCount().get(id) >= this.getReplicationsCount())?false:true;
    }
}
