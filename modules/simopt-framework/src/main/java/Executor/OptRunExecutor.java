package Executor;

import Adapter.Optimization.OptimizationAdapter;
import Adapter.Simulation.SimulationAdapter;
import DataTypes.Optimization.Child;
import DataTypes.Simulation.SimRunData;
import Definition.ProjectDefinition;
import Executor.AbstractExecutor.AbstractExperimentExecutor;
import Executor.AbstractExecutor.AbstractOptExecutor;
import Executor.AbstractExecutor.AbstractSimExecutor;

import java.util.LinkedList;

public class OptRunExecutor extends AbstractExperimentExecutor {

    private SimulationAdapter simulation;
    private OptimizationAdapter optimization;
    private AbstractSimExecutor simExecutor;
    private AbstractOptExecutor optExecutor;
    private ProjectDefinition projectDefinition;
    private Child bestChild;
    private int simulationRuns;


    public OptRunExecutor(ProjectDefinition projectDefinition){
        try {
            this.setFinished( false);
            this.projectDefinition = projectDefinition;

            this.simExecutor = projectDefinition.getExperimentDefinition().getSimExecutor();
            this.simExecutor.registerCallback(this);

            this.optExecutor = projectDefinition.getExperimentDefinition().getOptExecutor();
            this.optExecutor.registerCallback(this);
            this.simulation = simExecutor.getActualSimulation();
            this.optimization = optExecutor.getOptimization();
            this.optExecutor.initialize();

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void start(){
        this.getOptExecutor().run();
    }

    @Override
    public void getChildren(LinkedList<Child> children) {
    	SimRunData simResult;
        for (int i = 0; i < children.size(); i++) {

            Child c = children.get(i);
            try {
                simulationRuns++;
                simResult = simExecutor.runNewSimulation(c.getModelParameters(), c.getID());
                this.optExecutor.addFinishedSimulation(simResult);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("generation finished");
    }

    private double[] toDoubleArray(Double[] arr) {
        	double[] result = new double[arr.length];
        	for (int i = 0; i < arr.length; i++) {
        		result[i] = arr[i];
        	}
        	return result;
        
	}

	/*
    child = bestActualChild
    child1 = bestOverallChild

     */
    @Override
    public void optimizationFinished(Child child, Child child1) {


        if(!this.isFinished()) {
            this.simExecutor.setStop(true);
            System.out.println("\nOptimization Finished!");
            this.bestChild = child;
            this.callCallbacks(child);
            this.setFinished(true);
        }
    }

    @Override
    public void getCalled(SimRunData simRunData) {

        this.optExecutor.addFinishedSimulation(simRunData);
    }

    @Override
    public void initialize(){
        if(this.projectDefinition != null){
            initialize(this.projectDefinition);
        }
    }

    public void initialize(ProjectDefinition projectDefinition){

        if(projectDefinition== null){

            throw new NullPointerException("Projectdefinition can't be null");
        }

        if(projectDefinition.getSimulationDefinition()!= null &&
                projectDefinition.getSimulationDefinition().getSettingParameters() != null &&
                !projectDefinition.getSimulationDefinition().getSettingParameters().isEmpty()){

            this.simulation.setSettingParameters(projectDefinition.getSimulationDefinition().getSettingParameters());

        }

        if(projectDefinition.getOptimizationDefinition()!= null &&
                projectDefinition.getOptimizationDefinition().getSettingParameters() != null &&
                !projectDefinition.getOptimizationDefinition().getSettingParameters().isEmpty()){

            this.optimization.setSettingParameters(projectDefinition.getOptimizationDefinition().getSettingParameters());
        }

    }

    public SimulationAdapter getSimulation() {
        return simulation;
    }

    public void setSimulation(SimulationAdapter simulation) {
        this.simulation = simulation;
    }

    public OptimizationAdapter getOptimization() {
        return optimization;
    }

    public void setOptimization(OptimizationAdapter optimization) {
        this.optimization = optimization;
    }

    public ProjectDefinition getProjectDefinition() {
        return projectDefinition;
    }

    public void setProjectDefinition(ProjectDefinition projectDefinition) {
        this.projectDefinition = projectDefinition;
        this.simExecutor.setProjectDefinition(this.projectDefinition);
        this.optExecutor.setProjectDefinition(this.projectDefinition);
    }

    public AbstractSimExecutor getSimExecutor() {
        return simExecutor;
    }

    public void setSimExecutor(SimulationExecutor simExecutor) {
        this.simExecutor = simExecutor;
    }

    public AbstractOptExecutor getOptExecutor() {
        return optExecutor;
    }

    public void setOptExecutor(OptimizationExecutor optExecutor) {
        this.optExecutor = optExecutor;
    }

    public int getSimulationRuns() {
        return simulationRuns;
    }

    public void setSimulationRuns(int simulationRuns) {
        this.simulationRuns = simulationRuns;
    }


}
