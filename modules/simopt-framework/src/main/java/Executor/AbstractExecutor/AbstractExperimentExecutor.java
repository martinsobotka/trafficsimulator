package Executor.AbstractExecutor;


import Callbacks.ExpExecutorCallbackThread;
import Callbacks.ExperimentCallback;
import Callbacks.IOptimizationCallback;
import Callbacks.ISimulationCallback;
import DataTypes.Optimization.Child;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.LinkedList;

@XmlAccessorType(XmlAccessType.NONE)
public abstract class AbstractExperimentExecutor implements ISimulationCallback, IOptimizationCallback {

    private LinkedList<ExperimentCallback> callbacks;
    private boolean finished;

    public abstract void start();
    public abstract void initialize();

    public void registerCallback(ExperimentCallback callback){

        if(this.callbacks==null){
            this.callbacks = new LinkedList<ExperimentCallback>();
        }
        this.callbacks.add(callback);
    }

    public void unregisterCallback(ExperimentCallback callback){

        if(this.callbacks!=null){
            this.callbacks.remove(callback);
        }
    }

    /*
    runs the callbacks in own thread
     */
    public void callCallbacks(Child c){
        if(this.callbacks!=null && !this.callbacks.isEmpty()){
            ExpExecutorCallbackThread thread = new ExpExecutorCallbackThread();
            thread.addData(this.callbacks, c);
            thread.start();
        }
    }

    public void optimizationFinished(Child child, Child child1) {

        this.callCallbacks(child);
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }
}
