package Executor.AbstractExecutor;


import Adapter.Simulation.SimulationAdapter;
import Callbacks.ISimulationCallback;
import Callbacks.SimExecutorCallbackThread;
import CodeCreation.CodeGenerator;
import CodeCreation.ConstraintClass;
import CodeCreation.ObjectiveClass;
import DataTypes.SettingParameter;
import DataTypes.Simulation.SimRunData;
import Definition.ProjectDefinition;
import JarLoader.JarLoader;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.*;

/*
this abstract class is used to execute an simulation that extends SimulationAdapter
 */
@XmlAccessorType(XmlAccessType.NONE)
public abstract class AbstractSimExecutor implements ISimulationCallback {
    private SimulationAdapter actualSimulation;
    private ProjectDefinition projectDefinition;
    private LinkedList<ISimulationCallback> callbacks;
    private LinkedList<ConstraintClass> constraints;
    private ObjectiveClass objectiveClass;
    private int simulationRuns;
    private int replicationsCount;
    private boolean useReplication;
    private boolean useObjective;
    private boolean useConstraints;
    private boolean useMultithreading;
    private boolean stop;
    private ConcurrentHashMap<String, Integer> replicationCount;
    private ConcurrentHashMap<String,LinkedBlockingDeque<SimRunData>> replicationSim;
	private ArrayList<Future<SimRunData>> replicationFutures;
	protected Double[] simResult;  
	private Thread awaitFinishThread;
	private ExecutorService replicationTaskExecutor;

    public AbstractSimExecutor(SimulationAdapter actualSimulation) {
        this.actualSimulation = actualSimulation;
        this.callbacks = new LinkedList<ISimulationCallback>();
        this.actualSimulation.registerCallback(this);
        this.simulationRuns = 0;
        this.constraints = new LinkedList<ConstraintClass>();
        this.useReplication = false;
        this.useObjective = false;
        this.useConstraints = false;
        this.useMultithreading = true;
        this.stop = false;
		this.replicationFutures = new ArrayList<Future<SimRunData>>();

//		replicationTaskExecutor = Executors.newFixedThreadPool( Runtime.getRuntime().availableProcessors());        		


    }

    public AbstractSimExecutor(SimulationAdapter simulation, ProjectDefinition projectDefinition) {
        this.actualSimulation = simulation;
        this.callbacks = new LinkedList<ISimulationCallback>();
        this.actualSimulation.registerCallback(this);
        this.projectDefinition = projectDefinition;
        this.simulationRuns = 0;
        this.constraints = new LinkedList<ConstraintClass>();
        this.useReplication = false;
        this.useObjective = false;
        this.useConstraints = false;
        this.useMultithreading = true;
        this.stop = false;
		this.replicationFutures = new ArrayList<Future<SimRunData>>();

        if(projectDefinition.getExperimentDefinition()!=null) {

            this.useMultithreading = projectDefinition.getExperimentDefinition().getMultithreading();

            if (projectDefinition.getExperimentDefinition().getConstraints() != null) {

                for (String s : projectDefinition.getExperimentDefinition().getConstraints()) {

                    this.useConstraints = true;
                    constraints.addLast(CodeGenerator.CreateConstraintClass(s));
                }
            }

            if (projectDefinition.getExperimentDefinition().getObjective() != null) {

                this.useObjective = true;
                objectiveClass = CodeGenerator.CreateObjectiveClass(projectDefinition.getExperimentDefinition().getObjective(), projectDefinition.getExperimentDefinition().getMultiObjective());
            }

            this.replicationsCount = projectDefinition.getExperimentDefinition().getReplicationCounter();
        }

        this.useReplication = (this.replicationsCount>1)?true:false;

        if(useReplication) {
            this.replicationSim = new ConcurrentHashMap<String, LinkedBlockingDeque<SimRunData>>();
            this.replicationCount = new ConcurrentHashMap<String, Integer>();
    		replicationTaskExecutor = Executors.newFixedThreadPool( Runtime.getRuntime().availableProcessors());        		
        }
    }

    /*
    handles the callbacks from simulations
     */
    @Override
    public synchronized void getCalled(SimRunData simRunData) {


        if(useObjective){
            try{
                ObjectiveClass c = this.objectiveClass.copy();
                c.overwriteList(simRunData.getSettingParameters(),simRunData.getResultParameters());
                simRunData.setObjectiveValue(c.calculateObjective());
                simRunData.setMultiObjectiveSimulation(actualSimulation.isMultiObjectiveSimulation());
                simRunData.setObjectiveValues(c.calculateMultiObjectives());
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        if(useConstraints){
            boolean check = true;
            for(ConstraintClass c : this.constraints){
                try{
                    ConstraintClass cc = c.copy();
                    cc.overwriteList(simRunData.getSettingParameters(),simRunData.getResultParameters());
                    check = cc.checkConstraint();
                    if(!check){
                        break;
                    }
                }catch(Exception e){
                }
            }
            simRunData.setValidRun(check);
        }

        if(useReplication){

            replicationSim.get(simRunData.getID()).addLast(simRunData);
            checkReplicationFinished();
        }else{
            callCallbacks(simRunData);
        }

    }

    public void checkReplicationFinished(){
        SimRunData evaluated = null;
        for(String s: this.replicationSim.keySet()){
            if(this.replicationSim.get(s).size() == this.replicationsCount){
                evaluated = evaluateReplications(s);
                callCallbacks(evaluated);
            }
        }

    }

    /*
    this method is called if all replication runs are finished, and has to return
    a SimRunData object that extract the necessary data from all replication runs,
    for example a mean over all objective values
     */
    public abstract SimRunData evaluateReplications(String s);

    /*
    this method executes a simulationrun based on the passed settingParameters
    if multithreading is used, the simulation is started in an own thread

    if a ressource-manager is implemented this method needs redesign
     */
/* method obsolete
    public void executeExperiment(LinkedList<SettingParameter> settingParameters, String id){

        try {

            SimulationAdapter sim = initializeNewSimulation(settingParameters, id);

            if (this.useMultithreading) {
                sim.start();
            } else {

                sim.StartSimulation();
                this.callCallbacks(new SimRunData(sim.getID(), sim.getSettingParameters(), sim.getResultParameters(), sim.getObjectiveValue(), true, sim.isMultiObjectiveSimulation(), sim.getMultiObjectiveValues()));
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }
    */
    /*
    this method executes a simulationrun,
    if replications are used the method starts simulationruns until the
    nextReplicationRunNeeded returns false

    if a ressource-manager is implemented this method needs redesign
    */
    public SimRunData runNewSimulation(LinkedList<SettingParameter> settingParameters, String id){

        if(stop){
            return null;
        }
        try {
            SimRunData repResults=null;
            if(useReplication){

                this.replicationSim.put(id, new LinkedBlockingDeque<SimRunData>());
                this.replicationCount.put(id, 0);

//        		int processorCount = Runtime.getRuntime().availableProcessors();  //now done in constructor
//        		ExecutorService replicationTaskExecutor = Executors.newFixedThreadPool( processorCount);        		
/*        		ExecutorService replicationTaskExecutor = Executors.newFixedThreadPool( processorCount, new ThreadFactory() {
        			public Thread newThread( Runnable r ) {
        				return new Thread( r, "Replication-Thread" );
        			}
        		} );
        		*/
        		int numReplications=0;
        		this.replicationFutures = new ArrayList<Future<SimRunData>>();        		
                while(nextReplicationRunNeeded(id)){
                    this.simulationRuns++;
                    numReplications++;
//                    executeExperiment(settingParameters, id);
                    SimulationAdapter sim = initializeNewSimulation(settingParameters, id);
                    incrementReplicationCount(id);
        			Future<SimRunData> future = replicationTaskExecutor.submit( (Callable<SimRunData>)sim );
//        			sim.setFuture( future );

        			this.replicationFutures.add( future );

                }
        		

//        		this.awaitFinishThread = new Thread( new Runnable() {
    //    			@Override
    //    			public void run() {
                simResult = null;
        		for ( Future<SimRunData> f : replicationFutures ) {
        					try {
        						repResults = f.get();
        						if (simResult == null)
        							simResult = new Double[]{new Double(0), new Double(0)};
        						addRepResults(repResults);
        					} catch (InterruptedException e) {
        						e.printStackTrace();
        					} catch (ExecutionException e) {
        						e.printStackTrace();
        					}
        		}
        		finishRepResults(numReplications);
        		repResults.setObjectiveValues(simResult);
  //      			}
  //      		} );
                
  
            }else {
                this.simulationRuns++;
//                executeExperiment(settingParameters, id);
                SimulationAdapter sim = initializeNewSimulation(settingParameters, id);
                repResults = sim.call();
            }
            return repResults;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void finishRepResults(int numReps) {
		// we calculate means
    	if (numReps>0)
    		for (int i = 0; i<simResult.length; i++)  
    			simResult[i] /= numReps;			
	}

	private void addRepResults(SimRunData repResults) {
		// we calculate means
    	for (int i = 0; i<simResult.length; i++)  
    		simResult[i] += repResults.getObjectiveValues()[i];
		
	}

	/*
    with this method is determined if another replication needs to be started
     */
    public abstract boolean nextReplicationRunNeeded(String id);

    public void incrementReplicationCount(String id){

        this.replicationCount.put(id, this.replicationCount.get(id) + 1);
    }

    /*
    runs the registered callbacks in own thread
     */
    public void callCallbacks(SimRunData simRunData){
        if(this.callbacks!=null && !this.callbacks.isEmpty()){

            SimExecutorCallbackThread thread = new SimExecutorCallbackThread();
            thread.addData(this.callbacks, simRunData);
            thread.start();
        }
    }

    public SimulationAdapter getActualSimulation() {
        return actualSimulation;
    }

    public ProjectDefinition getProjectDefinition() {
        return projectDefinition;
    }

    public void setProjectDefinition(ProjectDefinition projectDefinition) {

        if(projectDefinition==null)
            return;

        this.projectDefinition = projectDefinition;

        if(this.projectDefinition.getSimulationDefinition()!=null &&
                this.projectDefinition.getSimulationDefinition().getSettingParameters()!=null){

            this.actualSimulation.setSettingParameters(this.projectDefinition.getSimulationDefinition().getSettingParameters());
        }
    }

    public void setActualSimulation(SimulationAdapter actualSimulation) {
        this.actualSimulation = actualSimulation;
    }

    public long getSimulationTimeFromDefinition(){
        if(projectDefinition==null||projectDefinition.getSimulationDefinition()==null){
            return 0;
        }
        return projectDefinition.getSimulationDefinition().getSimTime();
    }

    public SettingParameter getSettingParameterFromDefinition(SettingParameter spToSearch){

        if(projectDefinition==null || projectDefinition.getSimulationDefinition()==null || projectDefinition.getSimulationDefinition().getSettingParameters()==null || projectDefinition.getSimulationDefinition().getSettingParameters().isEmpty()){
            return null;
        }

        for(SettingParameter sp : projectDefinition.getSimulationDefinition().getSettingParameters()){
            if(sp!=null) {
                String name = (spToSearch == null) ? "" : spToSearch.getName();
                if (sp.getName().equals(name)) {
                    return sp;
                }
            }
        }
        return null;
    }

    /*
    This method creates a new instance of the simulation
    adopts the values from actualSimulation and sets the id
     */
    public SimulationAdapter initializeNewSimulation(String id) throws Exception {

        SimulationAdapter newInstance = JarLoader.GetNewInstance(actualSimulation.getClass());
        newInstance.initializeParameters();
        newInstance.setCallback(actualSimulation.getCallback());
        newInstance.setSimulationTime(actualSimulation.getSimulationTime());
        newInstance.setSettingParameters(actualSimulation.getSettingParameters());
        newInstance.setID(id);
        return newInstance;
    }

    /*
    This method creates a new instance of the simulation
    adopts the callbacks from actualSimulation and sets the id
    and passes the list to the simulation and overwrites the simulationtime
     */
    public SimulationAdapter initializeNewSimulation(LinkedList<SettingParameter> settingParameters, String id, long simTime) throws Exception {

        SimulationAdapter newInstance = JarLoader.GetNewInstance(actualSimulation.getClass());
        newInstance.initializeParameters();
        newInstance.setCallback(actualSimulation.getCallback());
        newInstance.setSimulationTime(simTime);
        newInstance.setSettingParameters(settingParameters);
        newInstance.setID(id);
        return newInstance;
    }

    /*
    This method creates a new instance of the simulation
    adopts the callbacks from actualSimulation and sets the id
    and passes the list to the simulation
     */
    public SimulationAdapter initializeNewSimulation(LinkedList<SettingParameter> settingParameters, String id) throws Exception {

        SimulationAdapter newInstance = JarLoader.GetNewInstance(actualSimulation.getClass());
        newInstance.initializeParameters();
        newInstance.setCallback(actualSimulation.getCallback());
        newInstance.setSimulationTime(actualSimulation.getSimulationTime());
        newInstance.overwriteSettingParameters(settingParameters);
        newInstance.setID(id);
        return newInstance;
    }

    public void registerCallback(ISimulationCallback callback) {
        this.callbacks.addLast(callback);
    }

    public void unregisterCallback(ISimulationCallback callback) {
        this.callbacks.remove(callback);
    }

    public int getSimulationRuns() {
        return simulationRuns;
    }

    public LinkedList<ISimulationCallback> getCallbacks() {
        return callbacks;
    }

    public void setCallbacks(LinkedList<ISimulationCallback> callbacks) {
        this.callbacks = callbacks;
    }

    public LinkedList<ConstraintClass> getConstraints() {
        return constraints;
    }

    public void setConstraints(LinkedList<ConstraintClass> constraints) {
        this.constraints = constraints;
    }

    public ObjectiveClass getObjectiveClass() {
        return objectiveClass;
    }

    public void setObjectiveClass(ObjectiveClass objectiveClass) {
        this.objectiveClass = objectiveClass;
    }

    public void setSimulationRuns(int simulationRuns) {
        this.simulationRuns = simulationRuns;
    }

    public int getReplicationsCount() {
        return replicationsCount;
    }

    public void setReplicationsCount(int replicationsCount) {
        this.replicationsCount = replicationsCount;
        checkIfReplicationNeeded();
    }

    public boolean isUseReplication() {
        return useReplication;
    }

    public void setUseReplication(boolean useReplication) {

        this.useReplication = useReplication;

    }

    public boolean isUseObjective() {
        return useObjective;
    }

    public void setUseObjective(boolean useObjective) {
        this.useObjective = useObjective;
    }

    public boolean isUseConstraints() {
        return useConstraints;
    }

    public void setUseConstraints(boolean useConstraints) {
        this.useConstraints = useConstraints;
    }

    public ConcurrentHashMap<String, LinkedBlockingDeque<SimRunData>> getReplicationSim() {
        return replicationSim;
    }

    public void setReplicationSim(ConcurrentHashMap<String, LinkedBlockingDeque<SimRunData>> replicationSim) {
        this.replicationSim = replicationSim;
    }

    public boolean getUseMultithreading() {
        return useMultithreading;
    }

    public void setUseMultithreading(boolean useMultithreading) {
        this.useMultithreading = useMultithreading;
    }

    public boolean isStop() {
        return stop;
    }

    public void setStop(boolean stop) {
        this.stop = stop;
    }

    public ConcurrentHashMap<String, Integer> getReplicationCount() {
        return replicationCount;
    }

    public void setReplicationCount(ConcurrentHashMap<String, Integer> replicationCount) {
        this.replicationCount = replicationCount;
    }

    private void checkIfReplicationNeeded(){
        this.useReplication = (this.replicationsCount>1)?true:false;
        if(this.useReplication) {
            this.replicationSim = new ConcurrentHashMap<String, LinkedBlockingDeque<SimRunData>>();
            this.replicationCount = new ConcurrentHashMap<String, Integer>();
        }
    }
}
