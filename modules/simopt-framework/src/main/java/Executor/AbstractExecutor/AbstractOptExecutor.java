package Executor.AbstractExecutor;

import Adapter.Optimization.OptimizationAdapter;
import Callbacks.IOptimizationCallback;
import Callbacks.OptExecutorCallbackThread;
import DataTypes.SettingParameter;
import DataTypes.Simulation.SimRunData;
import Definition.ProjectDefinition;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.LinkedList;

/*
this abstract class is used to execute an optimizer that extends OptimizationAdapter
 */
@XmlAccessorType(XmlAccessType.NONE)
public abstract class AbstractOptExecutor{

    public OptimizationAdapter optimization;
    public ProjectDefinition projectDefinition;
    private boolean run = true;
    public boolean initialized = false;

    public AbstractOptExecutor( OptimizationAdapter optimization, ProjectDefinition projectDefinition) {
        this.optimization = optimization;
        this.projectDefinition = projectDefinition;
    }

    public OptimizationAdapter getOptimization() {
        return optimization;
    }

    /*
    this method is called from the object (derived from SimulationAdapter) and passes the data to the
    optimization
     */
    public synchronized void addFinishedSimulation(SimRunData simRunData) {

        OptExecutorCallbackThread thread = new OptExecutorCallbackThread();
        thread.addData(this.optimization, simRunData);
        thread.run();   //HV: no threads!!
    }

    public boolean isRun() {
        return run;
    }

    public void setRun(boolean run) {
        this.run = run;
    }

    public ProjectDefinition getProjectDefinition() {
        return projectDefinition;
    }

    public void setProjectDefinition(ProjectDefinition projectDefinition) {

        if(projectDefinition== null)
            return;

        this.projectDefinition = projectDefinition;

        if(this.projectDefinition.getOptimizationDefinition()!=null &&
                this.projectDefinition.getOptimizationDefinition().getSimulationParameters()!=null &&
                this.projectDefinition.getOptimizationDefinition().getSettingParameters()!= null){

            this.initialize(this.projectDefinition.getOptimizationDefinition().getSimulationParameters(), this.projectDefinition.getOptimizationDefinition().getSettingParameters());
        }
    }

    public void setOptimization(OptimizationAdapter optimization) {
        this.optimization = optimization;
    }

    public long getSimulationTimeFromDefinition(){
        if(projectDefinition==null||projectDefinition.getSimulationDefinition()==null){
            return 0;
        }
        return projectDefinition.getSimulationDefinition().getSimTime();
    }

    public SettingParameter getSettingParameterFromDefinition(SettingParameter spToSearch){

        if(projectDefinition==null || projectDefinition.getOptimizationDefinition()==null || projectDefinition.getOptimizationDefinition().getSettingParameters()==null || projectDefinition.getOptimizationDefinition().getSettingParameters().isEmpty()){
            return null;
        }

        for(SettingParameter sp : projectDefinition.getOptimizationDefinition().getSettingParameters()){
            if(sp!=null) {
                String name = (spToSearch == null) ? "" : spToSearch.getName();
                if (sp.getName().equals(name)) {
                    return sp;
                }
            }
        }
        return null;
    }

    public SettingParameter getSimulationParameterFromDefinition(SettingParameter spToSearch){

        if(projectDefinition==null || projectDefinition.getSimulationDefinition()==null || projectDefinition.getOptimizationDefinition().getSimulationParameters()==null || projectDefinition.getSimulationDefinition().getSettingParameters().isEmpty()){
            return null;
        }

        for(SettingParameter sp : projectDefinition.getOptimizationDefinition().getSimulationParameters()){
            if(sp!=null) {
                String name = (spToSearch == null) ? "" : spToSearch.getName();
                if (sp.getName().equals(name)) {
                    return sp;
                }
            }
        }
        return null;
    }

    public boolean optimizationFinished(){
        return this.optimization.isFinished();
    }

    /*
    checks if the optimization is finished
     */
    public boolean allChildsEvaluated(){

        return this.optimization.allChildsEvaluated();
    }

    /*
    registers a callback in the optimizer
     */
    public void registerCallback(IOptimizationCallback callback) {
        this.optimization.registerCallback(callback);
    }

    /*
    unregisters a callback in the optimizer
     */
    public void unregisterCallback(IOptimizationCallback callback) {
        this.optimization.unregisterCallback(callback);
    }

    public abstract void initialize();

    public abstract void initialize(LinkedList<SettingParameter> settingParameters, LinkedList<SettingParameter> simulationParameters);

    /*
    starts the optimizer if the optimizationExecutor is initialized
     */
    public void run(){
        if(!initialized){
            return;
        }

        optimization.run();
    }

}
