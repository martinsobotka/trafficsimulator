package Executor;

import Adapter.Optimization.OptimizationAdapter;
import DataTypes.SettingParameter;
import Definition.ProjectDefinition;
import Executor.AbstractExecutor.AbstractOptExecutor;

import java.util.LinkedList;

public class OptimizationExecutor extends AbstractOptExecutor{

    public OptimizationExecutor(OptimizationAdapter optimization, ProjectDefinition projectDefinition) {
        super(optimization, projectDefinition);
    }

    public void initialize(){


        LinkedList<SettingParameter> settingParameters = optimization.getSettingParameters();
        LinkedList<SettingParameter> simulationParameters = optimization.getSimulationParameters();

        if(settingParameters==null || settingParameters.isEmpty()){
            settingParameters = projectDefinition.getOptimizationDefinition().getSettingParameters();
        }

        if(simulationParameters==null || simulationParameters.isEmpty()){
            simulationParameters = projectDefinition.getOptimizationDefinition().getSimulationParameters();
        }

		if (settingParameters != null) {
			for(SettingParameter sp : settingParameters){
				if(sp.getValue()==null) {
					SettingParameter temp = getSettingParameterFromDefinition(sp);

					if (temp!=null &&temp.getValue() != null) {
						sp.setValue(temp.getValue());
					} else if(sp.getValue()!=null){
						sp.setValue(sp.getValue());
					}else if(sp.hasRange()){
						sp.setValue((sp.getMinValue()));
					}else {
						sp.setValue(getSettingParameterFromDefinition(sp).getValue());

						//if there is no value found, set the parameter to 1
						if(sp.getValue()==null){
							sp.setValue(1d);
						}
					}
				}
			}
		}

		if (simulationParameters != null) {
			for(SettingParameter sp : simulationParameters){
				if(sp.getValue()==null) {
					SettingParameter temp = getSimulationParameterFromDefinition(sp);

					if (temp!=null &&temp.getValue() != null) {
						sp.setValue(temp.getValue());
					} else if(sp.getValue()!=null){
						sp.setValue(sp.getValue());
					}else if(sp.hasRange()){
						sp.setValue((sp.getMinValue()));
					}else {
						sp.setValue(getSettingParameterFromDefinition(sp).getValue());

						//if there is no value found, set the parameter to 1
						if(sp.getValue()==null){
							sp.setValue(1d);
						}
					}
				}
			}
		}

        optimization.assignSettingParameters(settingParameters, optimization);
        optimization.setSimulationParameters(simulationParameters);
        optimization.setID("1");
        initialized = true;
    }

    public void initialize(LinkedList<SettingParameter> settingParameters, LinkedList<SettingParameter> simulationParameters){
        optimization.setSettingParameters(settingParameters);
        optimization.setSimulationParameters(simulationParameters);
        optimization.setID("1");
        initialized = true;
    }
}
