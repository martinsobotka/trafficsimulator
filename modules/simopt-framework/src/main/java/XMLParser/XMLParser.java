package XMLParser;

import Definition.ProjectDefinition;
import Definition.SimulationDefinition;
import Result.ExperimentResult;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;


public class XMLParser {

    public static void SimulationDefinitionToXML(SimulationDefinition djd, String path){
        JAXB.marshal(djd,new File(path));
    }

    public static SimulationDefinition getSimulationDefinition(String path){
        return JAXB.unmarshal(new File(path), SimulationDefinition.class);
    }

    public static void ProjectDefinitionToXML(ProjectDefinition pd, String path){
        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(ProjectDefinition.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING,"UTF-8");
            jaxbMarshaller.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION, "");
            jaxbMarshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );
            jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "");

            jaxbMarshaller.marshal( pd, new File( path ) );

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public static void ExperimentResultToXML(ExperimentResult er, String path){
        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(ExperimentResult.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING,"UTF-8");
            jaxbMarshaller.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION, "");
            jaxbMarshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );
            jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "");

            jaxbMarshaller.marshal( er, new File( path ) );

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public static ProjectDefinition getProjectDefinition(String path){
        return JAXB.unmarshal(new File(path), ProjectDefinition.class);
    }

    public static ExperimentResult getExperimentResult(String path){
        return JAXB.unmarshal(new File(path), ExperimentResult.class);
    }
}
