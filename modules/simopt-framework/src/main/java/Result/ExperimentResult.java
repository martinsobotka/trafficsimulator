package Result;

import Adapter.Optimization.OptimizationAdapter;
import DataTypes.Optimization.Child;
import Definition.ExperimentDefinition;
import Definition.ProjectDefinition;
import Definition.SimulationDefinition;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;

@XmlRootElement(name = "ExperimentResults")
public class ExperimentResult {

    private String optimizer;
    private String simulation;
    private String optExecutor;
    private String simExecutor;
    private String expExecutor;
    private Child optimalChild;
    private LinkedList<Child> allChilds;

    public ExperimentResult() {
    }

    public ExperimentResult(ProjectDefinition pDef){

        ExperimentDefinition ed =  pDef.getExperimentDefinition();
        this.expExecutor = ed.getExpExecutorName();
        this.optExecutor = ed.getOptExecutorName();
        this.simExecutor = ed.getSimExecutorName();

        SimulationDefinition sd = pDef.getSimulationDefinition();
        this.simulation = sd.getSimulation().getClass().getName();

        OptimizationAdapter oa = pDef.getOptimizationDefinition().getOptimization();

        this.optimizer = oa.getClass().getName();
        this.optimalChild = oa.getBestChildOverall();
        this.allChilds = new LinkedList<Child>();
        this.allChilds.addAll(oa.getOldValues());
        this.allChilds.addAll(oa.getActualValues());

    }

    public ExperimentResult(String optimizer, String simulation, String optExecutor, String simExecutor, String expExecutor, Child optimalChild, LinkedList<Child> allChilds) {
        this.optimizer = optimizer;
        this.simulation = simulation;
        this.optExecutor = optExecutor;
        this.simExecutor = simExecutor;
        this.expExecutor = expExecutor;
        this.optimalChild = optimalChild;
        this.allChilds = allChilds;
    }

    public String getOptimizer() {
        return optimizer;
    }

    public void setOptimizer(String optimizer) {
        this.optimizer = optimizer;
    }

    public String getSimulation() {
        return simulation;
    }

    public void setSimulation(String simulation) {
        this.simulation = simulation;
    }

    public String getOptExecutor() {
        return optExecutor;
    }

    public void setOptExecutor(String optExecutor) {
        this.optExecutor = optExecutor;
    }

    public String getSimExecutor() {
        return simExecutor;
    }

    public void setSimExecutor(String simExecutor) {
        this.simExecutor = simExecutor;
    }

    public String getExpExecutor() {
        return expExecutor;
    }

    public void setExpExecutor(String expExecutor) {
        this.expExecutor = expExecutor;
    }

    public Child getOptimalChild() {
        return optimalChild;
    }

    public void setOptimalChild(Child optimalChild) {
        this.optimalChild = optimalChild;
    }

    public LinkedList<Child> getAllChilds() {
        return allChilds;
    }

    public void setAllChilds(LinkedList<Child> allChilds) {
        this.allChilds = allChilds;
    }
}
