package Examples;


import Adapter.Optimization.OptimizationAdapter;
import Adapter.Simulation.SimulationAdapter;
import Definition.*;
import Executor.OptRunExecutor;
import Executor.OptimizationExecutor;
import Executor.SimulationExecutor;
import JarLoader.JarLoader;


public class CreateDefinition {

    public ProjectDefinition pdef;

    public String dPath ;
    public String oPath;
    public String defPath ;

    public CreateDefinition(){
    }

    public CreateDefinition(String dPath, String oPath, String defPath) {
        this.dPath = dPath;
        this.oPath = oPath;
        this.defPath = defPath;
    }

    public void execute() throws Exception {

        /*
        Load the simulation and optimizer from the file system
         */
        SimulationAdapter simulation = JarLoader.GetSimulationAdapter(dPath);
        OptimizationAdapter optimizer = JarLoader.GetOptimizationAdapter(oPath);
        optimizer.setSimulationParameters(simulation.getSettingParameters());
        /*
        let the DefinitionCreator instanciate the minimal simulation and optimization definition
         */
        SimulationDefinition simulationDefinition = DefinitionCreator.createSimulationDefinition(dPath, simulation);
        OptimizationDefinition optimizationDefinition = DefinitionCreator.createOptimizationDefinition(oPath, optimizer);
        optimizationDefinition.setSimulationParameters(optimizer.getSimulationParameters());

        ExperimentDefinition edef = DefinitionCreator.createExperimentDefinition();

        edef.setSimExecutorName(SimulationExecutor.class.getName());
        edef.setOptExecutorName(OptimizationExecutor.class.getName());
        edef.setExpExecutorName(OptRunExecutor.class.getName());

        /*
        create a ProjectDefinition...with a fancy name
         */
        ProjectDefinition pd = DefinitionCreator.createProjectDefinition("Generated",simulationDefinition,optimizationDefinition,edef );
        this.pdef = pd;

        /*
        save the ProjectDefintion to the file system
        it's converted to a XML-file
         */
        DefinitionCreator.SaveProjectDefinition(defPath,pd);
    }
}
