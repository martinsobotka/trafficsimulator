package Examples;

import Callbacks.ExperimentCallback;
import DataTypes.Optimization.Child;
import DataTypes.SettingParameter;
import Definition.DefinitionCreator;
import Definition.ExperimentDefinition;
import Definition.ProjectDefinition;
import Executor.AbstractExecutor.AbstractExperimentExecutor;
import Executor.AbstractExecutor.AbstractSimExecutor;
import Result.ExperimentResult;

import java.io.File;
import java.io.OutputStream;
import java.io.PrintStream;

/*
This example implementatation loads a ProjectDefinition from the file system,
and executes the defined experiment
 */
public class ExistingDefinition implements ExperimentCallback {

    private String definitionPath;
    private PrintStream original;
    private PrintStream nullStream;
    private boolean called;
    private boolean noOutput;
    private long expStartTime;
    private long expStopTime;
    private ProjectDefinition projectDefinition;

    public ExistingDefinition(String definitionPath, boolean noOutput) {

        this.definitionPath = definitionPath;
        this.called = false;
        this.noOutput = noOutput;
    }

    public void execute() throws Exception {

        original = System.out;

        /*
        Generate a nullStream and redirect System.out if no output in console is wanted
         */
        nullStream = (new PrintStream(new OutputStream() {
            public void write(int b) {
            }
        }));

        if(noOutput) {
            System.setOut(nullStream);
        }
        /*
        Load a ProjectDefinition from the file system
         */
        ProjectDefinition pdLoaded = DefinitionCreator.LoadProjectDefinition(definitionPath);

        /*
        ProjectDefinition.tryLoadClasses tries to load the classes, that are defined in the ProjectDefinition
        returns true if all classes are loaded and instanciated.
        If it doesn't work, and you want the exception, use ProjectDefinition.loadClasses instead
         */
        if(pdLoaded.tryLoadClasses()){
            this.projectDefinition = pdLoaded;
            AbstractExperimentExecutor exec = pdLoaded.getExperimentDefinition().getExpExecutor();
            //initialize the experiment executor
            exec.initialize();

            //register the ExperimentCallback
            exec.registerCallback(this);

            this.expStartTime = System.currentTimeMillis();
            exec.start();
        }
    }

    /*
    this method is called when the OptRunExecutor is finished
    this implementation prints the best objective value and
    the used modelparameters
     */
    @Override
    public void optRunFinished(Object o) {

        if(!called) {
            this.expStopTime = System.currentTimeMillis();
            called=true;
            Child c = (Child) o;
            System.setOut(original);
            System.out.println("Best parameters are:");
            System.out.println("Objective: " + c.getObjectiveValue());
            for (SettingParameter sp : c.getModelParameters()) {
                System.out.println(sp.getName() + ": " + sp.getValue());
            }
            long elapsedTime = (this.expStopTime -  this.expStartTime)/1000;
            System.out.println("Execution time of experiment: " + elapsedTime + " sec");
            try {
                ExperimentDefinition ed = projectDefinition.getExperimentDefinition();
                AbstractSimExecutor simExecutor = ed.getSimExecutor();
                int simRuns = simExecutor.getSimulationRuns();
                System.out.println("Eexecuted simulations: " + simRuns);

            }catch (Exception e){
            }
            CreateExperimentResult();
            if(noOutput){
                System.setOut(nullStream);
            }
        }
    }

    public void CreateExperimentResult(){
        String experimentResultPath = "";
        try {
            String pathOfParent = new File(this.definitionPath).getParentFile().getPath();
            String fileSeparator = System.getProperty("file.separator");
            experimentResultPath = pathOfParent+fileSeparator+ "ExperimentResult_" + projectDefinition.getProjectName() +".xml";

            ExperimentResult experimentResult = new ExperimentResult(this.projectDefinition);
            DefinitionCreator.SaveExperimentResults(experimentResultPath, experimentResult);
            System.out.println("Result of experiment "+projectDefinition.getProjectName()+ " written to file: \n" + experimentResultPath+"\n");
        }catch (Exception e){
            System.out.println("Result of experiment could not be written to file: \n" + experimentResultPath+"\n");
            System.out.println(e.getClass().getName() + "occured");
        }
    }
}
