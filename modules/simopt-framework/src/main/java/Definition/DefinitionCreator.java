package Definition;


import Adapter.Optimization.OptimizationAdapter;
import Adapter.Simulation.SimulationAdapter;
import DataTypes.ResultParameter;
import DataTypes.SettingParameter;
import Result.ExperimentResult;
import XMLParser.XMLParser;

import java.util.LinkedList;

/*
this class generates the definitions based on the passed parameters
 */
public class DefinitionCreator {

    public static SimulationDefinition createSimulationDefinition(String path, SimulationAdapter simulation){

        String entryPoint = simulation.getClass().getName();
        LinkedList<SettingParameter> settingParameters = simulation.getSettingParameters();
        LinkedList<ResultParameter> resultParameters = simulation.getResultParameters();
        long simTime = simulation.getSimulationTime();

        return new SimulationDefinition(path,entryPoint,settingParameters,resultParameters,simTime);
    }

    public static OptimizationDefinition createOptimizationDefinition(String path, OptimizationAdapter optimization){

        String entryPoint = optimization.getClass().getName();
        LinkedList<SettingParameter> settingParameters = optimization.getSettingParameters();

        return new OptimizationDefinition(path,entryPoint, settingParameters);
    }

    public static ExperimentDefinition createExperimentDefinition(){

        return new ExperimentDefinition();
    }

    public static ProjectDefinition createProjectDefinition(String name, SimulationDefinition sdef, OptimizationDefinition odef, ExperimentDefinition edef){
        return new ProjectDefinition(name,sdef,odef,edef);
    }

    public static ExperimentDefinition createExperimentDefinition(LinkedList<String> constraints, String objective, int replicationCount){

        return new ExperimentDefinition(constraints, objective, replicationCount);
    }

    public static void SaveProjectDefinition(String path, ProjectDefinition pd){

        XMLParser.ProjectDefinitionToXML(pd, path);
    }

    public static void SaveExperimentResults(String path, ExperimentResult er){
        XMLParser.ExperimentResultToXML(er, path);
    }

    public static ProjectDefinition LoadProjectDefinition(String path){

        return XMLParser.getProjectDefinition(path);
    }
}
