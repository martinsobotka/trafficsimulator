package Definition;


import Adapter.Simulation.SimulationAdapter;
import DataTypes.ResultParameter;
import DataTypes.SettingParameter;

import javax.xml.bind.annotation.XmlTransient;
import java.util.LinkedList;

/*
this class is a data collector for a simulation
 */
public class SimulationDefinition {

    private String pathToJar;
    private String entryPoint;
    private LinkedList<SettingParameter> settingParameters;
    private LinkedList<ResultParameter> resultParameters;

    @XmlTransient
    private SimulationAdapter simulation;

    private long simTime;

    public SimulationDefinition(){}

    public SimulationDefinition(String pathToJar, String entryPoint, LinkedList<SettingParameter> settingParameters, LinkedList<ResultParameter> resultParameters, long simTime) {
        this.pathToJar = pathToJar;
        this.entryPoint = entryPoint;
        this.settingParameters = settingParameters;
        this.resultParameters = resultParameters;
        this.simTime = simTime;
    }

    public String getPathToJar() {
        return pathToJar;
    }

    public void setPathToJar(String pathToJar) {
        this.pathToJar = pathToJar;
    }

    public String getEntryPoint() {
        return entryPoint;
    }

    public void setEntryPoint(String entryPoint) {
        this.entryPoint = entryPoint;
    }

    public LinkedList<SettingParameter> getSettingParameters() {
        return settingParameters;
    }

    public void setSettingParameters(LinkedList<SettingParameter> settingParameters) {
        this.settingParameters = settingParameters;
    }

    public LinkedList<ResultParameter> getResultParameters() {
        return resultParameters;
    }

    public void setResultParameters(LinkedList<ResultParameter> resultParameters) {
        this.resultParameters = resultParameters;
    }

    public long getSimTime() {
        return simTime;
    }

    public void setSimTime(long simTime) {
        this.simTime = simTime;
    }

    @XmlTransient
    public SimulationAdapter getSimulation() {
        return simulation;
    }

    public void setSimulation(SimulationAdapter simulation) {
        this.simulation = simulation;
    }
}
