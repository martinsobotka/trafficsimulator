package Definition;


import Adapter.Optimization.OptimizationAdapter;
import DataTypes.SettingParameter;

import javax.xml.bind.annotation.XmlTransient;
import java.util.LinkedList;

/*
this class is a data collector for an optimization
 */
public class OptimizationDefinition {

    private String pathToJar;
    private String entryPoint;

    @XmlTransient
    private OptimizationAdapter optimization;

    private LinkedList<SettingParameter> settingParameters;
    private LinkedList<SettingParameter> simulationParameters;

    public OptimizationDefinition() {
    }

    public OptimizationDefinition(String pathToJar, String entryPoint) {
        this.pathToJar = pathToJar;
        this.entryPoint = entryPoint;
    }

    public OptimizationDefinition(String pathToJar, String entryPoint, LinkedList<SettingParameter> settingParameters) {
        this.pathToJar = pathToJar;
        this.entryPoint = entryPoint;
        this.settingParameters = settingParameters;
    }

    public OptimizationDefinition(String pathToJar, String entryPoint, LinkedList<SettingParameter> settingParameters, LinkedList<SettingParameter> simulationParameters) {
        this.pathToJar = pathToJar;
        this.entryPoint = entryPoint;
        this.settingParameters = settingParameters;
        this.simulationParameters = simulationParameters;
    }

    public LinkedList<SettingParameter> getSettingParameters() {
        return settingParameters;
    }

    public void setSettingParameters(LinkedList<SettingParameter> settingParameters) {
        this.settingParameters = settingParameters;
    }

    public String getPathToJar() {
        return pathToJar;
    }

    public void setPathToJar(String pathToJar) {
        this.pathToJar = pathToJar;
    }

    public String getEntryPoint() {
        return entryPoint;
    }

    public void setEntryPoint(String entryPoint) {
        this.entryPoint = entryPoint;
    }

    public LinkedList<SettingParameter> getSimulationParameters() {
        return simulationParameters;
    }

    public void setSimulationParameters(LinkedList<SettingParameter> simulationParameters) {
        this.simulationParameters = simulationParameters;
    }

    @XmlTransient
    public OptimizationAdapter getOptimization() {
        return optimization;
    }

    public void setOptimization(OptimizationAdapter optimization) {
        this.optimization = optimization;
    }
}
