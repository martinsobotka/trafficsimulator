package Definition;


import Executor.AbstractExecutor.AbstractExperimentExecutor;
import Executor.AbstractExecutor.AbstractOptExecutor;
import Executor.AbstractExecutor.AbstractSimExecutor;

import java.util.LinkedList;

/*
this class is a data collector for an experiment
 */
public class ExperimentDefinition {

    private String experimentName;
    private int replicationCounter;
    private AbstractExperimentExecutor expExecutor;
    private String expExecutorName;
    private AbstractSimExecutor simExecutor;
    private String simExecutorName;
    private AbstractOptExecutor optExecutor;
    private String optExecutorName;
    private LinkedList<String> constraints;
    private String objective;
    private String multiObjective;
    private boolean multithreading;

    public ExperimentDefinition() {
        this.constraints = null;
        this.objective = null;
        this.replicationCounter = 0;
        this.multithreading = true;
    }

    public ExperimentDefinition(LinkedList<String> constraints) {
        this.constraints = constraints;
        this.objective = null;
        this.replicationCounter = 0;
    }

    public ExperimentDefinition(String objective) {
        this.constraints = null;
        this.objective = objective;
        this.replicationCounter = 0;
    }

    public ExperimentDefinition(LinkedList<String> constraints, String objective) {
        this.constraints = constraints;
        this.objective = objective;
        this.replicationCounter = 0;
    }

    public ExperimentDefinition(LinkedList<String> constraints, String objective, int replicationCount) {
        this.constraints = constraints;
        this.objective = objective;
        this.replicationCounter = replicationCount;
    }

    public LinkedList<String> getConstraints() {
        return constraints;
    }

    public void setConstraints(LinkedList<String> constraints) {
        this.constraints = constraints;
    }

    public String getObjective() {
        return objective;
    }
    
    public String getMultiObjective() {
    	return multiObjective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }
    
    public void setMultiObjective(String multiObjective) {
    	this.multiObjective = multiObjective;
    }

    public String getExperimentName() {
        return experimentName;
    }

    public void setExperimentName(String experimentName) {
        this.experimentName = experimentName;
    }

    public int getReplicationCounter() {
        return replicationCounter;
    }

    public void setReplicationCounter(int replicationCounter) {
        this.replicationCounter = replicationCounter;
    }

    public boolean getMultithreading() {
        return multithreading;
    }

    public void setMultithreading(boolean useMultithreading) {
        this.multithreading = useMultithreading;
    }

    public String getSimExecutorName() {
        return simExecutorName;
    }

    public void setSimExecutorName(String simExecutorName) {
        this.simExecutorName = simExecutorName;
    }

    public String getOptExecutorName() {
        return optExecutorName;
    }

    public void setOptExecutorName(String optExecutorName) {
        this.optExecutorName = optExecutorName;
    }

    public AbstractSimExecutor getSimExecutor() {
        return simExecutor;
    }

    public void setSimExecutor(AbstractSimExecutor simExecutor) {
        this.simExecutor = simExecutor;
    }

    public AbstractOptExecutor getOptExecutor() {
        return optExecutor;
    }

    public void setOptExecutor(AbstractOptExecutor optExecutor) {
        this.optExecutor = optExecutor;
    }

    public AbstractExperimentExecutor getExpExecutor() {
        return expExecutor;
    }

    public void setExpExecutor(AbstractExperimentExecutor expExecutor) {
        this.expExecutor = expExecutor;
    }

    public String getExpExecutorName() {
        return expExecutorName;
    }

    public void setExpExecutorName(String expExecutorName) {
        this.expExecutorName = expExecutorName;
    }
}
