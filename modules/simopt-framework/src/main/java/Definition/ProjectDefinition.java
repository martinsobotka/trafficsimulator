package Definition;

import Adapter.Optimization.OptimizationAdapter;
import Executor.AbstractExecutor.AbstractExperimentExecutor;
import JarLoader.JarLoader;

import javax.xml.bind.annotation.XmlRootElement;

/*
this class is a data collector for a project
this class is marhalled into an xml-file
 */
@XmlRootElement(name = "ProjectDefinition")
public class ProjectDefinition {

    private String projectName;
    private SimulationDefinition simulationDefinition;
    private OptimizationDefinition optimizationDefinition;
    private ExperimentDefinition experimentDefinition;

    public ProjectDefinition(){}

    public ProjectDefinition(String projectName) {
        this.projectName = projectName;
    }

    public ProjectDefinition(String projectName, SimulationDefinition simulationDefinition) {
        this.projectName = projectName;
        this.simulationDefinition = simulationDefinition;
    }

    public ProjectDefinition(String projectName, SimulationDefinition simulationDefinition, OptimizationDefinition optimizationDefinition) {
        this.projectName = projectName;
        this.simulationDefinition = simulationDefinition;
        this.optimizationDefinition = optimizationDefinition;
    }

    public ProjectDefinition(String projectName, SimulationDefinition simulationDefinition, OptimizationDefinition optimizationDefinition, ExperimentDefinition experimentDefinition) {
        this.projectName = projectName;
        this.simulationDefinition = simulationDefinition;
        this.optimizationDefinition = optimizationDefinition;
        this.experimentDefinition = experimentDefinition;
    }

    public SimulationDefinition getSimulationDefinition() {
        return simulationDefinition;
    }

    public void setSimulationDefinition(SimulationDefinition simulationDefinition) {
        this.simulationDefinition = simulationDefinition;
    }

    public ExperimentDefinition getExperimentDefinition() {
        return experimentDefinition;
    }

    public void setExperimentDefinition(ExperimentDefinition experimentDefinition) {
        this.experimentDefinition = experimentDefinition;
    }

    public OptimizationDefinition getOptimizationDefinition() {
        return optimizationDefinition;
    }

    public void setOptimizationDefinition(OptimizationDefinition optimizationDefinition) {
        this.optimizationDefinition = optimizationDefinition;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public boolean tryLoadClasses(){

        try {

           loadClasses();
        }catch(Exception e){
			e.printStackTrace();
            return false;
        }
        return true;
    }

    public void loadClasses() throws Exception {

        if(this.simulationDefinition!=null) {
            this.simulationDefinition.setSimulation(JarLoader.GetSimulationAdapter(simulationDefinition.getPathToJar()));
        }
        if(this.optimizationDefinition!=null) {
            OptimizationAdapter opt = JarLoader.GetOptimizationAdapter(optimizationDefinition.getPathToJar());
            opt.assignSettingParameters(this.optimizationDefinition.getSettingParameters(), opt);
            this.optimizationDefinition.setOptimization(opt);
            this.optimizationDefinition.getOptimization().setSimulationParameters(this.optimizationDefinition.getSimulationParameters());
        }

        if(this.experimentDefinition!=null) {

            if(this.experimentDefinition.getSimExecutorName()!=null) {

                this.experimentDefinition.setSimExecutor(
                        JarLoader.GetSimExecutor(this.experimentDefinition.getSimExecutorName(), this.simulationDefinition.getSimulation(), this));
            }

            if(this.experimentDefinition.getOptExecutorName()!=null) {

                this.experimentDefinition.setOptExecutor(
                        JarLoader.GetOptExecutor(this.experimentDefinition.getOptExecutorName(), this.optimizationDefinition.getOptimization(), this));
            }

            if(this.experimentDefinition.getExpExecutorName()!=null) {

                AbstractExperimentExecutor ex = JarLoader.GetExpExecutor(this.experimentDefinition.getExpExecutorName(), this);
                this.experimentDefinition.setExpExecutor(ex);
            }

        }
    }

}
