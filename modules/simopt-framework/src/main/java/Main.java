import Examples.CreateDefinition;
import Examples.ExistingDefinition;

import java.io.File;

public class Main{


    public static void main(String[] args) {
		// Fake
		//args = new String[]{"simopt-framework/description.xml"};
		//

        try {
           if (args.length <= 2) {

               checkRuntime();
               boolean silent = false;

               if(args.length == 2){
                   String bool = args[1];
                   silent = (bool.contains("true"))? true: false;
               }

               String path = args[0];
               String fileExtension = path.substring(path.lastIndexOf('.') + 1).toLowerCase();
               File f = new File(path);

               if (f.exists() && fileExtension.equals("xml")) {

                   ExistingDefinition ed = new ExistingDefinition(path, silent);
                   ed.execute();
               } else {

                   System.out.println("File doesn't exist or is no xml-File!");
               }

            }else if(args.length == 3){
               try {

                   System.out.println("Try to create definition for a simulation-based optimization");
                   checkRuntime();

                   String simPath = args[0];
                   String optPath = args[1];
                   String defPath = args[2];

                   CreateDefinition cd = new CreateDefinition(simPath, optPath, defPath);

                   cd.execute();
                   System.out.println("Definition successfully created:\n"+defPath+"\n");
               }catch(Exception e){

                   System.err.println("Exception occured!");
                   e.printStackTrace();
               }
           }else{
               System.out.println("Nothing to be done");
           }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void checkRuntime()
    {
        boolean found = false;
        try{
            String jdkVersion = System.getProperty("java.version");
            System.out.println("Using java version jre/jdk" + jdkVersion);
            String pathVariable = System.getenv("PATH");
            System.out.println(pathVariable);
            String[] paths = pathVariable.split(";");
            String jdkPath = "";

            for(String item : paths) {
                if(item.toLowerCase().contains(jdkVersion.toLowerCase()) || item.toLowerCase().contains(jdkVersion.split("_")[0])) {
                    if (!item.endsWith("bin") && !item.endsWith("bin/")) continue;
                	jdkPath = item;
                    File jdk = new File(jdkPath);
                    System.out.println("Found potential jdk: " + item);
                    if(jdk.exists()) {
                        File jdkParent = new File(jdk.getParent());

                        if(jdkParent.exists()) {
                            System.out.println("Setting java.home");
                        	System.setProperty("java.home", jdkParent.getAbsolutePath()); // + "\\jre");
                            found = true;
                            break;
                        }
                    }
                }
            }

        }catch (Exception e){
        }catch (Throwable t){
        }finally {
            if(found) {
                System.out.println("java.home set to jdk");
            }else {
                System.out.println("no jdk found!");
            }
        }
    }

}
