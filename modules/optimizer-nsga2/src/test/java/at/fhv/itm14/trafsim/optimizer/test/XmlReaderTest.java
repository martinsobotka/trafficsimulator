package at.fhv.itm14.trafsim.optimizer.test;

import at.fhv.itm14.trafsim.optimizer.Configuration;
import at.fhv.itm14.trafsim.optimizer.xml.XmlReader;
import org.junit.Assert;

public class XmlReaderTest {

	public static final String TEST_CONFIGURATION_PATH = "./src/main/java/at/fhv/itm14/trafsim/optimizer/test/TestConfiguration.xml";
	public static final double epsilon = 0.0000001;

	//@Test
	public void test() {
		Configuration configuration = XmlReader.readConfiguration(TEST_CONFIGURATION_PATH);

		// Check if nothing is null
		Assert.assertNotNull(configuration);
		Assert.assertNotNull(configuration.NonDominationSortParams);
		Assert.assertNotNull(configuration.ReplaceParams);
		Assert.assertNotNull(configuration.SelectionParams);
		Assert.assertNotNull(configuration.GeneticParams);

		// Compare values
		Assert.assertEquals(configuration.Generations, 100);
		Assert.assertEquals(configuration.NonDominationSortParams.NumberOfObjectives, 2);
		Assert.assertEquals(configuration.ReplaceParams.PopulationSize, 15);
		Assert.assertEquals(configuration.SelectionParams.PoolSize, 15);
		Assert.assertEquals(configuration.SelectionParams.TournamentSize, 7);
		Assert.assertEquals(configuration.GeneticParams.NumberOfUsedParents, 2);
		Assert.assertEquals(configuration.GeneticParams.MuCrossover, 20d, epsilon);
		Assert.assertEquals(configuration.GeneticParams.MuMutation, 20d, epsilon);
	}

}
