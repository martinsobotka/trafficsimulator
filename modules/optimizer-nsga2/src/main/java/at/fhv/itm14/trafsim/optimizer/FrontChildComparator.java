package at.fhv.itm14.trafsim.optimizer;

import java.util.Comparator;

public class FrontChildComparator implements Comparator<FrontChild> {

	private static final int COMPARATOR_PRECISION = 10000; // 10^4 precision
	
	// Fields
	private final int index;

	// Constructor
	public FrontChildComparator(int index) {
		this.index = index;
	}

	// Methods
	@Override
	public int compare(FrontChild child1, FrontChild child2) {
		return (int) (child1.child.getMultiObjective()[index] * COMPARATOR_PRECISION - child2.child.getMultiObjective()[index] * COMPARATOR_PRECISION);
	}

}
