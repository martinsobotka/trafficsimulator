package at.fhv.itm14.trafsim.optimizer.geneticOperator;

import DataTypes.Optimization.Child;
import DataTypes.SettingParameter;
import at.fhv.itm14.trafsim.optimizer.DecisionVariable;
import at.fhv.itm14.trafsim.optimizer.FrontChild;
import at.fhv.itm14.trafsim.optimizer.utils.OptimizerHelper;
import at.fhv.itm14.trafsim.optimizer.utils.SettingParameterFactory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class GeneticOperator extends GeneticOperatorBase<GeneticOperatorParams> {

	protected double turnaroundTime; //HV: trafsim specific - no problem, since the package itself makes it specific
	public final List<DecisionVariable> DecisionVariables;
	public final int NumberOfDecisionVariables;

	public GeneticOperator(List<DecisionVariable> decisionVariables) {
		this.DecisionVariables = decisionVariables;
		this.NumberOfDecisionVariables = decisionVariables.size();
	}

	// Methods
	@Override
	public List<FrontChild> geneticOperator(List<FrontChild> parents, GeneticOperatorParams params) {
		List<FrontChild> children = new ArrayList<>();

		double crossoverProbability = 0.9;

		for (int i = 0; i < parents.size(); i++) {

			// With probability of 90% perform crossover
			if (OptimizerHelper.checkProbability(crossoverProbability)) {

				// Create two crossover children
				List<FrontChild> crossoverChildren = createCrossOverChildren(parents, params);
				
				// Add the created children
				children.addAll(crossoverChildren);

			}
			// With probability 10% perform mutation
			else {

				// Create a mutation child
				FrontChild child = createMutationChild(parents, params);

				// Add the mutation child to the children
				children.add(child);
			}
		}

		return children;
	}


	private List<FrontChild> createCrossOverChildren(List<FrontChild> parents, GeneticOperatorParams params) {

		double crossoverBoundary = 0.5;

		// Select parents
		List<FrontChild> selectedParents = OptimizerHelper.selectRandomCandidates(parents, params.NumberOfUsedParents);
		FrontChild father = selectedParents.get(0);
		FrontChild mother = selectedParents.get(1);

		// List with parameters
		LinkedList<SettingParameter> paramsBoy;
		LinkedList<SettingParameter> paramsGirl;

		double tT=0;  //HV
		
		boolean paramsInRange;
		do {
			paramsInRange = true;

			// List with parameters
			paramsBoy = new LinkedList<>();
			paramsGirl = new LinkedList<>();
			double boyMax = 0;
			double girlMax = 0;

			for (int j = 0; j < NumberOfDecisionVariables; j++) {

				double probability = Math.random();
				double crossoverFactor = calcCrossoverFactor(params, crossoverBoundary, probability);

				// Get father and mother value
				double fatherValue = getSettingsParameterValue(father.child.getModelParameters().get(j));
				double motherValue = getSettingsParameterValue(mother.child.getModelParameters().get(j));

				// Use father and mother value to create children values
				double boyValue = 0.5 * (((1 + crossoverFactor) * fatherValue) + (1 - crossoverFactor) * motherValue);
				double girlValue = 0.5 * (((1 - crossoverFactor) * fatherValue) + (1 + crossoverFactor) * motherValue);

				// Min and max ist needed for the setting parameter
				double min = DecisionVariables.get(j).minRangeOfDecisionVariable;
				double max = 0;
//start HV
				if  ( j==0) {
					max = DecisionVariables.get(0).maxRangeOfDecisionVariable;
					boyMax = boyValue = (boyValue+max)%max;					
					girlMax = girlValue = (girlValue+max)%max;
				}
				else {
					boyValue = (boyValue+boyMax)%boyMax;
					girlValue = (girlValue+girlMax)%girlMax;
				}
//end HV
				String name = DecisionVariables.get(j).name;

				SettingParameter parameterBoy;
				SettingParameter parameterGirl;
				// Create the setting parameters via factory
				if (j==0)  {
				  parameterBoy = SettingParameterFactory.createDoubleSettingsParameter(name, j, boyValue,
						min, max);
				  parameterGirl = SettingParameterFactory.createDoubleSettingsParameter(name, j,
						girlValue, min, max);
				}
				else {
				  parameterBoy = SettingParameterFactory.createDoubleSettingsParameter(name, j, boyValue,
								min, boyMax);
				  parameterGirl = SettingParameterFactory.createDoubleSettingsParameter(name, j,
								girlValue, min, girlMax);
				}

				if (!isInRange(parameterBoy) || !isInRange(parameterGirl)) {
					paramsInRange = false;
					break;
				}

				// Add the created setting parameters to the depending child
				paramsBoy.add(parameterBoy);
				paramsGirl.add(parameterGirl);
			}

		} while (!paramsInRange);

		// Create children with the depending parameters
		FrontChild boy = new FrontChild(new Child(paramsBoy));
		FrontChild girl = new FrontChild(new Child(paramsGirl));

		// TODO: Evaluate boy and girl

		List<FrontChild> children = new ArrayList<>();
		children.add(boy);
		children.add(girl);

		return children;
	}

	private boolean isInRange(SettingParameter param) {
		// TODO Auto-generated method stub
		return param.getValue().compareTo(param.getMinValue()) >= 0
				&& param.getValue().compareTo(param.getMaxValue()) <= 0;
	}

	private double calcCrossoverFactor(GeneticOperatorParams params, double crossoverBoundary, double probability) {
		double crossoverFactor;
		if (probability <= crossoverBoundary) {
			crossoverFactor = Math.pow(2 * probability, 1 / (params.MuCrossover + 1));
		} else {
			crossoverFactor = Math.pow(1 / (2 * (1 - probability)), 1 / (params.MuCrossover + 1));
		}
		return crossoverFactor;
	}

	private FrontChild createMutationChild(List<FrontChild> parents, GeneticOperatorParams params) {

		// Select random parent
		FrontChild parent = OptimizerHelper.selectRandomCandidates(parents, 1).get(0);
		double mutationBoundary = 0.5;
        double tT = 0;  //HV
		LinkedList<SettingParameter> paramsChild = new LinkedList<>();
		for (int j = 0; j < NumberOfDecisionVariables; j++) {

			// Calculate mutation factor
			double probability = Math.random();
			double mutationFactor;
			if (probability <= mutationBoundary) {
				// 2 * prob ^ (1/mu+1) - 1
				mutationFactor = Math.pow(2 * probability, 1 / (params.MuMutation + 1)) - 1;
			} else {
				// 1 - (2 * (1 - prob) ^ (1/mu+1))
				mutationFactor = 1 - Math.pow((2 * (1 - probability)), 1 / (params.MuMutation + 1));
			}

			// Get parent setting parameter value
			double parentValue = getSettingsParameterValue(parent.child.getModelParameters().get(j));
			double childValue = parentValue + mutationFactor;
//HV:			
			double max;
			if (j==0) { //we assume first parameter to be turnAroundTime
				tT = childValue;
			    max = DecisionVariables.get(0).maxRangeOfDecisionVariable;
			}
			else {
				childValue = childValue%tT;
				max = tT;
			}
//end HV
			// Min and max ist needed for the setting parameter
			double min = DecisionVariables.get(j).minRangeOfDecisionVariable;
			String name = DecisionVariables.get(j).name;

			// Create setting parameter and add it to the parameter list of the
			// child
			SettingParameter parameterChild = SettingParameterFactory.createDoubleSettingsParameter(name, j, childValue,
					min, max);
			paramsChild.add(parameterChild);
		}

		// Create new child with the depending parameters
		return new FrontChild(new Child(paramsChild));
	}

	private double getSettingsParameterValue(SettingParameter settingParameter) {
		return (double) settingParameter.getValue();
	}

}
