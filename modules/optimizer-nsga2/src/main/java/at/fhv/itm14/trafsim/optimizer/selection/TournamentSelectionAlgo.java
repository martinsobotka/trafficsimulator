package at.fhv.itm14.trafsim.optimizer.selection;

import at.fhv.itm14.trafsim.optimizer.FrontChild;
import at.fhv.itm14.trafsim.optimizer.utils.OptimizerHelper;

import java.util.ArrayList;
import java.util.List;

public class TournamentSelectionAlgo extends SelectionAlgoBase<TournamentSelectionParams> {

	// Methods
	@Override
	public List<FrontChild> selectChromosomes(List<FrontChild> chromosomes, TournamentSelectionParams params) {
		int poolSize = params.PoolSize; 
		int tournamenSize = params.TournamentSize;
		
		List<FrontChild> selectedChromosomes = new ArrayList<>();
		
		for (int i = 0; i < poolSize; i++) {
			// Select specific number of candidates
			List<FrontChild> candidates = OptimizerHelper.selectRandomCandidates(chromosomes, tournamenSize);
			
			// Find candidate with min rank
			FrontChild minCandidate = findMinCandidate(candidates);
			
			// Add this candidate to the selected chromosomes
			selectedChromosomes.add(minCandidate);
		}
		
		return selectedChromosomes;
	}
	
	private FrontChild findMinCandidate(List<FrontChild> candidates) {
		FrontChild minCandidate = candidates.get(0);
		
		for (int j = 1; j < candidates.size(); j++) {
			FrontChild currentCandidate = candidates.get(j);
			
			// If < current min rank 
			if (minCandidate.getRank() > currentCandidate.getRank()) {
				minCandidate = currentCandidate;
			} 
			// If rank equal, check distance should be >	
			else if (minCandidate.getRank() == currentCandidate.getRank() && 
					minCandidate.getDistance() < currentCandidate.getDistance()) {
				minCandidate = currentCandidate;
			}
		}
		
		return minCandidate;
	}
}
