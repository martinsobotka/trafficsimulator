package at.fhv.itm14.trafsim.optimizer.sorting;

import DataTypes.Optimization.Child;
import at.fhv.itm14.trafsim.optimizer.FrontChild;
import at.fhv.itm14.trafsim.optimizer.FrontChildComparator;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class NonDominationSortAlgo extends SortingAlgorithmBase<NonDominationSortParams> {

	// Methods
	@Override
	public ArrayList<FrontChild> sort(List<Child> individuals, NonDominationSortParams params) {
		ArrayList<ArrayList<FrontChild>> fronts = new ArrayList<ArrayList<FrontChild>>();
		fronts.add(new ArrayList<FrontChild>());

		LinkedList<FrontChild> frontChildren = createFrontChildren(individuals);
		ArrayList<FrontChild> sortedFrontChildren = new ArrayList<FrontChild>();

		int currentFront = 0;
		int size = frontChildren.size();

		for (int i = 0; i < size; i++) {

			FrontChild currentChild = frontChildren.get(i);
			for (int j = 0; j < size; j++) {

				// Jump over current child
				if (i == j) {
					continue;
				}

				// Calculate and set the dominated values
				FrontChild innerChild = frontChildren.get(j);
				DomValues domValues = new DomValues();

				calculateDomValues(domValues, params.NumberOfObjectives, currentChild, innerChild);
				setDomValues(domValues, params.NumberOfObjectives, currentChild, innerChild);
			}

			// Build first front, so this front is not dominated
			if (currentChild.getDominatedBy() == 0) {
				currentChild.setFrontNumber(0);
				fronts.get(currentFront).add(currentChild);
			}
		}

		// Find the subsequent fronts
		while (!fronts.get(currentFront).isEmpty()) {
			currentFront++;

			// TODO: check if last one is empty
			fronts.add(new ArrayList<FrontChild>());

			ArrayList<FrontChild> previousFront = fronts.get(currentFront - 1);
			for (int i = 1; i < previousFront.size(); i++) { // FIXME: Maybe this should start from 0

				FrontChild currentChild = previousFront.get(i);
				if (!currentChild.getDominatedChilds().isEmpty()) {

					// Create fronts
					createFronts(fronts, currentFront, currentChild);
				}
			}
		}

		java.util.Iterator<ArrayList<FrontChild>> it = fronts.iterator();
		while (it.hasNext()) {
			ArrayList<FrontChild> element = it.next();
			if (element.size() == 0) {
				it.remove();
			}
		}

		ArrayList<FrontChild> sortedBasedOnFront = new ArrayList<FrontChild>();
		for (int i = 0; i < fronts.size(); i++) {
			sortedBasedOnFront.addAll(fronts.get(i));
		}

		// Find the crowding distance for each individual in each front
		int currentIndex = 0;
		for (currentFront = 0; currentFront < fronts.size(); currentFront++) {

			ArrayList<FrontChild> currentFrontChildren = new ArrayList<FrontChild>();
			int i = 0;

			// Fill the current front children
			for (i = 0; i < fronts.get(currentFront).size(); i++) {
				currentFrontChildren.add(sortedBasedOnFront.get(currentIndex + i));
			}

			// Jump over children which are added to the current front
			currentIndex += i;

			ArrayList<FrontChild> sortedBasedOnObjective;
			clearTempValueOfChild(currentFrontChildren);
			
			for (i = 0; i < params.NumberOfObjectives; i++) {

				sortedBasedOnObjective = new ArrayList<FrontChild>();
				// List of front children sorted by objectives
				for (int j = 0; j < currentFrontChildren.size(); j++) {
					sortedBasedOnObjective.add(currentFrontChildren.get(j));
				}
				sortedBasedOnObjective.sort(new FrontChildComparator(i));

				// Last and first child
				FrontChild lastChild = sortedBasedOnObjective.get(sortedBasedOnObjective.size() - 1);
				FrontChild firstChild = sortedBasedOnObjective.get(0);

				// Get fitness values
				double fitnessMax = lastChild.child.getMultiObjective()[i];
				double fitnessMin = firstChild.child.getMultiObjective()[i];

				// Set value of first and last child to Infinity
				lastChild.getTmpValues().add(Double.POSITIVE_INFINITY);
				firstChild.getTmpValues().add(Double.POSITIVE_INFINITY);

				// Set tmp values
				for (int j = 1; j < sortedBasedOnObjective.size() - 1; j++) {

					double nextObj = sortedBasedOnObjective.get(j + 1).child.getMultiObjective()[i];
					double previousObj = sortedBasedOnObjective.get(j - 1).child.getMultiObjective()[i];
					FrontChild currentChild = sortedBasedOnObjective.get(j);

					calculateTempValues(fitnessMax, fitnessMin, nextObj, previousObj, currentChild);
				}
			}

			// Calc crowding distance
			calcCrowdingDistance(params, currentFrontChildren);

			// Add front children to the sorted list
			sortedFrontChildren.addAll(currentFrontChildren);
		}

		return sortedFrontChildren;
	}

	public void setDomValues(DomValues domValues, int numberOfObjectives, FrontChild currentChild,
			FrontChild innerChild) {

		// TODO: domMore > 0
		if (domValues.less == 0 && domValues.equal != numberOfObjectives) {
			currentChild.setDominatedBy(currentChild.getDominatedBy() + 1);
		} else if (domValues.more == 0 && domValues.equal != numberOfObjectives) {
			currentChild.getDominatedChilds().add(innerChild);
		}
	}

	public void calcCrowdingDistance(NonDominationSortParams params, ArrayList<FrontChild> currentFrontChildren) {
		int i;
		for (i = 0; i < currentFrontChildren.size(); i++) {
			double distance = 0;
			if (currentFrontChildren.get(i).getTmpValues().size() < params.NumberOfObjectives) {
				System.out.println("BRUTAL ERROR!");
			}
			for (int j = 0; j < params.NumberOfObjectives && distance < Double.POSITIVE_INFINITY; j++) {
				distance += currentFrontChildren.get(i).getTmpValues().get(j);
			}
			currentFrontChildren.get(i).setDistance(distance);
		}
	}

	public void calculateTempValues(double fitnessMax, double fitnessMin, double nextObj, double previousObj,
			FrontChild currentChild) {
		if (fitnessMax - fitnessMin == 0) { // FIXME: Probably we should compare doubles as one should compare doubles!
			currentChild.getTmpValues().add(Double.POSITIVE_INFINITY);
		} else {
			currentChild.getTmpValues().add((nextObj - previousObj) / (fitnessMax - fitnessMin));
		}
	}

	public void clearTempValueOfChild(ArrayList<FrontChild> currentFrontChildren) {
		for(FrontChild fc : currentFrontChildren){
			fc.clearTmpValues();
		}
	}

	public void createFronts(ArrayList<ArrayList<FrontChild>> fronts, int currentFront, FrontChild currentChild) {
		for (int j = 0; j < currentChild.getDominatedChilds().size(); j++) {

			FrontChild dominatedChild = currentChild.getDominatedChilds().get(j);
			dominatedChild.setDominatedBy(dominatedChild.getDominatedBy() - 1);

			if (dominatedChild.getDominatedBy() == 0) {
				dominatedChild.setFrontNumber(currentFront);
				fronts.get(currentFront).add(dominatedChild);
			}
		}
	}

	public LinkedList<FrontChild> createFrontChildren(List<Child> individuals) {
		LinkedList<FrontChild> children = new LinkedList<>();
		// Enhance normal children to front children
		for (int i = 0; i < individuals.size(); i++) {
			children.add(new FrontChild(individuals.get(i)));
		}

		return children;
	}

	public void calculateDomValues(DomValues values, int bjectivesSize, FrontChild currentChild,
			FrontChild innerChild) {
		for (int k = 0; k < bjectivesSize; k++) {

			double currentObjectiveValue = currentChild.child.getMultiObjective()[k];
			double innerObjectiveValue = innerChild.child.getMultiObjective()[k];

			if (currentObjectiveValue < innerObjectiveValue) {
				// TODO: fix me -> maybe only one per individual
				values.less++;
			} else if (currentObjectiveValue == innerObjectiveValue) {
				values.equal++;
			} else {
				values.more++;
			}
		}
	}

	// Inner class
	private class DomValues {
		// TODO: use hashmaps and iterate only once
		private int less = 0;
		private int equal = 0;
		private int more = 0;

	}
}
