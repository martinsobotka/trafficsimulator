package at.fhv.itm14.trafsim.optimizer;

import at.fhv.itm14.trafsim.optimizer.geneticOperator.GeneticOperatorParams;
import at.fhv.itm14.trafsim.optimizer.selection.ReplaceChromosomeParams;
import at.fhv.itm14.trafsim.optimizer.selection.TournamentSelectionParams;
import at.fhv.itm14.trafsim.optimizer.sorting.NonDominationSortParams;

public class Configuration {

	public static final String PATH = "nsga2.xml";

	public final int Generations;
	public final NonDominationSortParams NonDominationSortParams;
	public final ReplaceChromosomeParams ReplaceParams;
	public final TournamentSelectionParams SelectionParams;
	public final GeneticOperatorParams GeneticParams;

	public Configuration(int generations, NonDominationSortParams nonDominationSortParams,
			ReplaceChromosomeParams replaceParams, TournamentSelectionParams selectionParams,
			GeneticOperatorParams geneticParams) {

		this.Generations = generations;
		this.NonDominationSortParams = nonDominationSortParams;
		this.ReplaceParams = replaceParams;
		this.SelectionParams = selectionParams;
		this.GeneticParams = geneticParams;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Configuration:\n");
		sb.append("--------------\n");
		sb.append("Generations: " + this.Generations + "\n");
		sb.append("NonDominationSortParams\n");
		sb.append(" NumberOfObjectives: " + this.NonDominationSortParams.NumberOfObjectives);
		sb.append("ReplaceParams\n");
		sb.append(" PopulationSize: " + this.ReplaceParams.PopulationSize + "\n");
		sb.append("SelectionParams\n");
		sb.append(" PoolSize: " + this.SelectionParams.PoolSize + "\n");
		sb.append(" TournamentSize: " + this.SelectionParams.TournamentSize + "\n");
		sb.append("GeneticParams\n");
		sb.append(" MuCrossover: " + this.GeneticParams.MuCrossover + "\n");
		sb.append(" MuMutation: " + this.GeneticParams.MuMutation + "\n");
		sb.append(" NumberOfUsedParents: " + this.GeneticParams.NumberOfUsedParents + "\n");
		return sb.toString();
	}
}