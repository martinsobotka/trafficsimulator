package at.fhv.itm14.trafsim.optimizer.selection;

import at.fhv.itm14.trafsim.optimizer.FrontChild;

import java.util.List;

public abstract class SelectionAlgoBase<T extends SelectionAlgoParams> {

	public abstract List<FrontChild> selectChromosomes(List<FrontChild> chromosomes, T params);
	
}
