package at.fhv.itm14.trafsim.optimizer.geneticOperator;

public class GeneticOperatorParams {

	// Fields
	public final int NumberOfUsedParents;

	public final double MuCrossover;
	public final double MuMutation;

	// Constructor
	public GeneticOperatorParams(int numberOfUsedParents, double muCrossOver, double muMutation) {
		this.NumberOfUsedParents = numberOfUsedParents;

		this.MuCrossover = muCrossOver;
		this.MuMutation = muMutation;
	}
}
