package at.fhv.itm14.trafsim.optimizer;

import DataTypes.Optimization.Child;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class FrontChild {

	// Fields
	public final Child child;
	private int dominatedBy;
	private List<FrontChild> dominatedChilds;
	private int frontNumber;
	private double distance;
	private ArrayList<Double> tmpValues;

	// Constructor
	public FrontChild(Child child) {
		this.child = child;
		setDominatedChilds(new LinkedList<FrontChild>());
		tmpValues = new ArrayList<Double>();
	}

	// Methods
	public int getDominatedBy() {
		return dominatedBy;
	}

	public void setDominatedBy(int dominatedBy) {
		this.dominatedBy = dominatedBy;
	}

	public List<FrontChild> getDominatedChilds() {
		return dominatedChilds;
	}

	public void setDominatedChilds(List<FrontChild> dominatedChilds) {
		this.dominatedChilds = dominatedChilds;
	}

	public int getFrontNumber() {
		return frontNumber;
	}

	public void setFrontNumber(int frontNumber) {
		this.frontNumber = frontNumber;
	}

	public void setRank(int rank) {
		frontNumber = rank;
	}

	public int getRank() {
		return frontNumber;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public double getDistance() {
		return distance;
	}

	public ArrayList<Double> getTmpValues() {
		return tmpValues;
	}

	public void clearTmpValues() {
		tmpValues = new ArrayList<Double>();
	}
}
