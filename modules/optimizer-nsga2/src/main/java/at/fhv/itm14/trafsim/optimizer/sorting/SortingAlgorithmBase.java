package at.fhv.itm14.trafsim.optimizer.sorting;

import DataTypes.Optimization.Child;
import at.fhv.itm14.trafsim.optimizer.FrontChild;
import at.fhv.itm14.trafsim.optimizer.selection.SelectionAlgoParams;

import java.util.ArrayList;
import java.util.List;

public abstract class SortingAlgorithmBase<T extends SelectionAlgoParams> {

	public abstract ArrayList<FrontChild> sort(List<Child> childs, T params);
}
