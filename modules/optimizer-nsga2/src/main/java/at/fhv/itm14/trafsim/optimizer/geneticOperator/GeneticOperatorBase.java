package at.fhv.itm14.trafsim.optimizer.geneticOperator;

import at.fhv.itm14.trafsim.optimizer.FrontChild;

import java.util.List;

public abstract class GeneticOperatorBase<T extends GeneticOperatorParams> {

	public abstract List<FrontChild> geneticOperator(List<FrontChild> parents, T params);

}
