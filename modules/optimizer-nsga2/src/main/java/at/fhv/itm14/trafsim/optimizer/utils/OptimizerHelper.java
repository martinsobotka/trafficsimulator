package at.fhv.itm14.trafsim.optimizer.utils;

import at.fhv.itm14.trafsim.optimizer.FrontChild;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class OptimizerHelper {

	public static boolean checkProbability(double probability) {
		return Math.random() <= probability;
	}

	public static List<FrontChild> selectRandomCandidates(List<FrontChild> candidates, int numberOfCandidates) {
		List<FrontChild> selectedCandidates = new ArrayList<>();

		// Select random individuals
		for (int j = 0; j < Math.min(numberOfCandidates, candidates.size()); j++) {

			FrontChild selectedCandidate;
			// Find not selected candidate
			do {
				// TODO: set
				selectedCandidate = candidates.get(ThreadLocalRandom.current().nextInt(0, candidates.size()));
			} while (selectedCandidates.contains(selectedCandidate));

			selectedCandidates.add(selectedCandidate);
		}

		return selectedCandidates;
	}
}
