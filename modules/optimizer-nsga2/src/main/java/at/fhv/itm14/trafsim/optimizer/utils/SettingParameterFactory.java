package at.fhv.itm14.trafsim.optimizer.utils;

import DataTypes.SettingParameter;

public class SettingParameterFactory {

	public static SettingParameter createDoubleSettingsParameter(String name, int count, double value, double min, double max) {
		try {
			return new SettingParameter(name, Double.class, count + ". decision variable", value, min, max);
		} catch (Exception e) {
			return null;
		}
	}
	
}
