package at.fhv.itm14.trafsim.optimizer.xml;

import at.fhv.itm14.trafsim.optimizer.Configuration;
import at.fhv.itm14.trafsim.optimizer.geneticOperator.GeneticOperatorParams;
import at.fhv.itm14.trafsim.optimizer.selection.ReplaceChromosomeParams;
import at.fhv.itm14.trafsim.optimizer.selection.TournamentSelectionParams;
import at.fhv.itm14.trafsim.optimizer.sorting.NonDominationSortParams;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Iterator;

public class XmlReader {

	// Root element
	public static final String CONFIGURATION = "configuration";
	public static final String GENERATIONS = "generations";

	// Param elements
	public static final String NON_DOMINATION_SORT_PARAMS = "non_domination_sort_params";
	public static final String REPLACE_PARAMS = "replace_params";
	public static final String SELECTION_PARAMS = "selection_params";
	public static final String GENETIC_PARAMS = "genetic_params";

	// Sub params
	// NON_DOMINATION_SORT
	public static final String NUMBER_OF_OBJECTIVES = "number_of_objectives";
	// REPLACE
	public static final String POPULATION_SIZE = "population_size";
	// SELECTION
	public static final String TOURNAMENT_SIZE = "tournament_size";
	public static final String POOL_SIZE = "pool_size";
	// GENETIC
	public static final String NUMBER_OF_USED_PARENTS = "number_of_used_parents";
	public static final String MU_CROSSOVER = "mu_crossover";
	public static final String MU_MUTATION = "mu_mutation";

	@SuppressWarnings("unchecked")
	public static Configuration readConfiguration(String configFile) {

		int generations = 10;
		NonDominationSortParams nonDominationSortParams = null;
		ReplaceChromosomeParams replaceParams = null;
		TournamentSelectionParams selectionParams = null;
		GeneticOperatorParams geneticParams = null;

		try {
			// First, create a new XMLInputFactory
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			// Setup a new eventReader
			InputStream in = new FileInputStream(configFile);
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

			// read the XML document
			while (eventReader.hasNext()) {
				XMLEvent event = eventReader.nextEvent();

				if (event.isStartElement()) {
					StartElement startElement = event.asStartElement();
					if (startElement.getName().getLocalPart() == (CONFIGURATION)) {
						startElement = event.asStartElement();
						generations = getIntegerAttribute(GENERATIONS, startElement);

						while (eventReader.hasNext()) {
							event = eventReader.nextEvent();
							if (event.isStartElement()) {
								startElement = event.asStartElement();
								if (startElement.getName().getLocalPart() == (NON_DOMINATION_SORT_PARAMS)) {
									int numberOfObjectives = getIntegerAttribute(NUMBER_OF_OBJECTIVES, startElement);
									nonDominationSortParams = new NonDominationSortParams(numberOfObjectives);
								} else if (startElement.getName().getLocalPart() == (REPLACE_PARAMS)) {
									int populationSize = getIntegerAttribute(POPULATION_SIZE, startElement);
									replaceParams = new ReplaceChromosomeParams(populationSize);
								} else if (startElement.getName().getLocalPart() == (SELECTION_PARAMS)) {

									int tournamentSize = 0;
									int poolSize = 0;

									Iterator<Attribute> attributes = startElement.getAttributes();

									while (attributes.hasNext()) {
										Attribute attribute = attributes.next();
										if (attribute.getName().toString().equals(TOURNAMENT_SIZE)) {
											tournamentSize = Integer.parseInt(attribute.getValue());
										} else if (attribute.getName().toString().equals(POOL_SIZE)) {
											poolSize = Integer.parseInt(attribute.getValue());
										}
									}

									selectionParams = new TournamentSelectionParams(tournamentSize, poolSize);
								} else if (startElement.getName().getLocalPart() == (GENETIC_PARAMS)) {

									int numberOfUsedParents = 0;
									double muCrossOver = 0;
									double muMutation = 0;

									Iterator<Attribute> attributes = startElement.getAttributes();

									while (attributes.hasNext()) {
										Attribute attribute = attributes.next();
										if (attribute.getName().toString().equals(NUMBER_OF_USED_PARENTS)) {
											numberOfUsedParents = Integer.parseInt(attribute.getValue());
										} else if (attribute.getName().toString().equals(MU_CROSSOVER)) {
											muCrossOver = Double.parseDouble(attribute.getValue());
										} else if (attribute.getName().toString().equals(MU_MUTATION)) {
											muMutation = Double.parseDouble(attribute.getValue());
										}
									}

									geneticParams = new GeneticOperatorParams(numberOfUsedParents, muCrossOver,
											muMutation);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new Configuration(generations, nonDominationSortParams, replaceParams, selectionParams, geneticParams);
	}

	@SuppressWarnings("unchecked")
	public static int getIntegerAttribute(String name, StartElement startElement) {

		Iterator<Attribute> attributes = startElement.getAttributes();

		while (attributes.hasNext()) {
			Attribute attribute = attributes.next();
			if (attribute.getName().toString().equals(name)) {
				return Integer.parseInt(attribute.getValue());
			}
		}

		return -1;
	}
}
