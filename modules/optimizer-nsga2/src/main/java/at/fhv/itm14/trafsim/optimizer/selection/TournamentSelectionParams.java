package at.fhv.itm14.trafsim.optimizer.selection;

public class TournamentSelectionParams extends SelectionAlgoParams {

	// Fields
	public final int TournamentSize;
	public final int PoolSize;

	// Constructors
	public TournamentSelectionParams(int tournamentSize, int poolSize) {
		this.TournamentSize = tournamentSize;
		this.PoolSize = poolSize;
	}
}
