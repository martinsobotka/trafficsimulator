package at.fhv.itm14.trafsim.optimizer.selection;

public class ReplaceChromosomeParams extends SelectionAlgoParams {

	// Fields
	public final int PopulationSize;

	// Constructors
	public ReplaceChromosomeParams(int populationSize) {
		PopulationSize = populationSize;
	}
}
