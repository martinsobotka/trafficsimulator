package at.fhv.itm14.trafsim.optimizer.selection;

import at.fhv.itm14.trafsim.optimizer.FrontChild;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ReplaceChromosomeAlgo extends SelectionAlgoBase<ReplaceChromosomeParams> {

	@Override
	public List<FrontChild> selectChromosomes(List<FrontChild> chromosomes, ReplaceChromosomeParams params) {

		// Sort the chromosomes according to rank
		SortChromosomesByRank(chromosomes);

		int maxRank = chromosomes.get(chromosomes.size() - 1).getRank();
		int previousIndex = 0;
		int currentIndex = 0;
		int remaining;

		ArrayList<FrontChild> tmpChromosomes;
		ArrayList<FrontChild> newPopulation = new ArrayList<FrontChild>();

		for (int i = 0; i <= maxRank; i++) {

			// Get the index of the last element, that has i as rank
			for (; currentIndex < chromosomes.size() && chromosomes.get(currentIndex).getRank() == i; currentIndex++)
				;

			if (currentIndex <= 0) {
				continue;
			}
			currentIndex--;

			if (currentIndex > params.PopulationSize) {

				// Find the number of individuals within the current rank
				remaining = params.PopulationSize - previousIndex;

				// Get all individuals of this rank
				tmpChromosomes = new ArrayList<FrontChild>();
				for (int j = previousIndex; j <= currentIndex; j++) {
					tmpChromosomes.add(chromosomes.get(j));
				}

				// Sort the individuals based on the crowding distance
				SortChromosomesByCrowdingDistance(tmpChromosomes);

				// Fill the remaining individuals into the population
				for (int j = 0; j < remaining; j++) {
					newPopulation.add(tmpChromosomes.get(j));
				}

				return newPopulation;
			} else if (currentIndex < params.PopulationSize) {

				// Add all individuals with rank i to the new population
				for (int j = previousIndex; j <= currentIndex; j++) {
					newPopulation.add(chromosomes.get(j));
				}
			} else {
				// Add all individuals with rank i to the new population and
				// return it
				// could be also done in "currentIndex > params.PopulationSize",
				// but here you don't have to sort
				for (int j = previousIndex; j <= currentIndex; j++) {
					newPopulation.add(chromosomes.get(j));
				}

				return newPopulation;
			}
			previousIndex = currentIndex + 1;
		}
		return newPopulation;
	}

	public void SortChromosomesByCrowdingDistance(ArrayList<FrontChild> tmpChromosomes) {
		tmpChromosomes.sort(new Comparator<FrontChild>() {

			@Override
			public int compare(FrontChild child1, FrontChild child2) {
				// Bigger is better
				return (int) Math.round(child2.getDistance() - child1.getDistance());
			}

		});
	}

	public void SortChromosomesByRank(List<FrontChild> chromosomes) {
		chromosomes.sort(new Comparator<FrontChild>() {

			@Override
			public int compare(FrontChild child1, FrontChild child2) {
				return child1.getRank() - child2.getRank();
			}
		});
	}
}
