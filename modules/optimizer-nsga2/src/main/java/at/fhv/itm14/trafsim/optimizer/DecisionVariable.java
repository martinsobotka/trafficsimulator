package at.fhv.itm14.trafsim.optimizer;

public class DecisionVariable implements Comparable<DecisionVariable> {

	// Fields
	public final String name;
	public final double minRangeOfDecisionVariable;
	public final double maxRangeOfDecisionVariable;

	// Constructor
	public DecisionVariable(String name, double minRange, double maxRange) {
		this.name = name;
		minRangeOfDecisionVariable = minRange;
		maxRangeOfDecisionVariable = maxRange;
	}

	// Methods
	@Override
	public int compareTo(DecisionVariable o) {
		return (int) (minRangeOfDecisionVariable - o.minRangeOfDecisionVariable + maxRangeOfDecisionVariable
				- o.maxRangeOfDecisionVariable);
	}
}