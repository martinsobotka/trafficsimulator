package at.fhv.itm14.trafsim.optimizer.sorting;

import at.fhv.itm14.trafsim.optimizer.selection.SelectionAlgoParams;

public class NonDominationSortParams extends SelectionAlgoParams {

	// Fields
	public final int NumberOfObjectives;

	// Constructor
	public NonDominationSortParams(int numberOfObjectives) {
		NumberOfObjectives = numberOfObjectives;
	}

}
