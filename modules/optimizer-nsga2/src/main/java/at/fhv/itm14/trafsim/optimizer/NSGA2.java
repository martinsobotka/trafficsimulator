package at.fhv.itm14.trafsim.optimizer;

import Adapter.Optimization.OptimizationAdapter;
import Caster.Caster;
import DataTypes.Optimization.Child;
import DataTypes.Optimization.OptimizationGoal;
import DataTypes.SettingParameter;
import at.fhv.itm14.trafsim.optimizer.geneticOperator.GeneticOperator;
import at.fhv.itm14.trafsim.optimizer.selection.ReplaceChromosomeAlgo;
import at.fhv.itm14.trafsim.optimizer.selection.SelectionAlgoBase;
import at.fhv.itm14.trafsim.optimizer.selection.TournamentSelectionAlgo;
import at.fhv.itm14.trafsim.optimizer.selection.TournamentSelectionParams;
import at.fhv.itm14.trafsim.optimizer.sorting.NonDominationSortAlgo;
import at.fhv.itm14.trafsim.optimizer.utils.SettingParameterFactory;
import at.fhv.itm14.trafsim.optimizer.view.Visualisation;
import at.fhv.itm14.trafsim.optimizer.xml.XmlReader;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

public class NSGA2 extends OptimizationAdapter {

	private static final Logger LOGGER = Logger.getLogger(NSGA2.class.toString());

	// Fields
	private int currentGeneration;
	private List<DecisionVariable> decisionVariables;

	private Configuration configuration;
	private Visualisation visual;
	private volatile int dummy;
//	private double tT;  //HV, TrafSim specific

	// Description
	// decisionVariables: Umlaufzeit (),
	// objectiveValues: Anzahl Stops (), Mittlere Wartezeit ()


	public NSGA2() {
		LOGGER.info("Initialize NSGAII");
		configuration = XmlReader.readConfiguration(Configuration.PATH);

		LOGGER.info("Start visualisation");
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				Visualisation.main(new String[]{});
			}
		});
		t.start();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			LOGGER.severe("Error while waiting");
			Thread.currentThread().interrupt();
		}
		visual = //new Visualisation("Avg. Stops", "Avg. Waiting Time");
				Visualisation.getInstance();
	}

	// Methods
	@Override
	public void calcFirstGeneration() {
		LOGGER.info("NSGA2 calcFirstGeneration() called");


		// LinkedList<Child> children = getAllChildren();
		// children.get(0);
		// children.get(1).getMultiObjective();

		// Init variables
		LinkedList<Child> children = new LinkedList<Child>();
		LinkedList<SettingParameter> params = null;

		LinkedList<SettingParameter> simulationParams = getSimulationParameters();
		decisionVariables = new ArrayList<>();

		for (SettingParameter s : simulationParams) {
			decisionVariables.add(new DecisionVariable(s.getName(), Caster.getMinAsDouble(s), Caster.getMaxAsDouble(s)));
//			decisionVariables.add(new DecisionVariable(s.getName(), getMinAsDouble(s), getMaxAsDouble(s)));			
		}
		for (int i = 0; i < configuration.ReplaceParams.PopulationSize; i++) {
			params = new LinkedList<>();
			double maxAux = 0; //HV
			for (int j = 0; j < decisionVariables.size(); j++) {
				try {

					// f(i,j) = min(j) + (max(j) - min(j))*rand(1);
					double min = decisionVariables.get(j).minRangeOfDecisionVariable;
					double max;
					if ( decisionVariables.get(j).name.startsWith("phaseShifts"))  {
						max = maxAux;
					}
					else   
						max = decisionVariables.get(j).maxRangeOfDecisionVariable;
//HV trafsim specific code
					double value = min + (max - min) * Math.random();
					String name = decisionVariables.get(j).name;
					if (name.equals("turnAroundTime"))
						maxAux = value;
//HV end
					SettingParameter newSettingParameter = SettingParameterFactory.createDoubleSettingsParameter(name, j,
							value, min, max);
					params.add(newSettingParameter);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			children.add(new Child(params));
		}

		setActualValues(children);

		currentGeneration++;
	}

/*	
//HV: the next method is specific for the simulation problem: for general purposes, outcomment the code
//	  except for the last instruction 
	private double getMaxAsDouble(SettingParameter s) {
		
		if (s.getName().startsWith("phaseShifts"))  {
			return tT;
		}

		return Caster.getMaxAsDouble(s);
	}

	
	private double getMinAsDouble(SettingParameter s) {
		// HV
		return Caster.getMinAsDouble(s);
	}
*/
	@Override
	public void calcNextGeneration() {
		LOGGER.info("NSGA2 calcNextGeneration() called");

		// Non domination sort of intermediate population
		// TODO: check getActualValues size with params size
		NonDominationSortAlgo sort = new NonDominationSortAlgo();
		LinkedList<Child> actualValues = getActualValues();
		for (Child c : actualValues) {
			if (c != null) {
				visual.addPopulationChild(c.getMultiObjective()[0], c.getMultiObjective()[1]);
			} else {
				LOGGER.warning("Child is null");
			}
		}
		List<FrontChild> sortedParents = sort.sort(getActualValues(), configuration.NonDominationSortParams);

		int g = visual.newGeneration();
		for (FrontChild fc : sortedParents) {
			if (fc.getRank() == 0)
				visual.addValueGenerationParetoChild(
						fc.child.getMultiObjective()[0],
						fc.child.getMultiObjective()[1],
						g,
						"Rank=" + fc.getRank() + "<br>" +
								modelParametersToString(fc.child.getModelParameters()).replace(" ", "<br>"));
		}
		
		// Replace chromosome
		ReplaceChromosomeAlgo replaceAlgo = new ReplaceChromosomeAlgo();
		List<FrontChild> replacedParents = replaceAlgo.selectChromosomes(sortedParents, configuration.ReplaceParams);

		// Tournament selection
		// TODO: int poolSize = Math.round(replacedParents.size() / 2);
		SelectionAlgoBase<TournamentSelectionParams> selctionAlgorithm = new TournamentSelectionAlgo();
		List<FrontChild> selectedParents = selctionAlgorithm.selectChromosomes(replacedParents,
				configuration.SelectionParams);

		// Genetic_operator
		GeneticOperator geneticOperator = new GeneticOperator(decisionVariables);
		List<FrontChild> offsprings = geneticOperator.geneticOperator(selectedParents, configuration.GeneticParams);

		// Intermediate population
		List<FrontChild> intermediateChromosomes = new ArrayList<FrontChild>();
		intermediateChromosomes.addAll(replacedParents);
		intermediateChromosomes.addAll(offsprings);

		// Set the new actual values
		LinkedList<Child> newActualValues = new LinkedList<>();
		intermediateChromosomes.stream().forEach(child -> newActualValues.add(child.child));
		setActualValues(newActualValues);

		//visual.writeBulk();
		currentGeneration++;
	}

	private String modelParametersToString(
			LinkedList<SettingParameter> modelParameters) {
		StringBuilder result = new StringBuilder();
		for (SettingParameter sp : modelParameters) {
			result.append(sp.getName() + "=" + sp.getValue().toString() + " ");
		}
		return result.toString();
	}

	@Override
	public boolean checkIfFinished() {
		return currentGeneration >= configuration.Generations;
	}

	@Override
	public String getOptimizerDescription() {
		return "NSGAII";
	}

	@Override
	public void initializeParameters() {
		LOGGER.info("NSGA2 initializeParameters() called");

		try {
			setOptimizationGoal(OptimizationGoal.Min);
			addSettingParameter(new SettingParameter("dummy", Integer.class, "Dummy value", 1));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		LOGGER.warning("This program is not meant to be executed. Use SimOpt framework to use it");
		LOGGER.warning("For debug purposes, I try to read the optimizer values from xml: " + Configuration.PATH);
		Configuration c = XmlReader.readConfiguration(Configuration.PATH);
		Visualisation.main(new String[]{});
		new Visualisation("Avg. Stops", "Avg. Waiting Time");

	}
	
	public void setDummy(Integer value) { dummy = value; }
	public Integer getDummy() { return dummy; };

}
