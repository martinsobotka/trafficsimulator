package at.fhv.itm14.trafsim.optimizer;

import Caster.Caster;
import DataTypes.Optimization.Child;
import DataTypes.SettingParameter;

import java.util.LinkedList;
import java.util.Random;

public abstract class EvolutionaryAlgorithm {
	
	protected final Random rnd;

	public EvolutionaryAlgorithm() {
		this.rnd = new Random();
	}
	
	public abstract Individum[] generateOffsprings(Individum[] parents) throws Exception ;
	
	public abstract Individum[] selectNewParents(Individum[] parents, Individum[] offsprings);
	
	public abstract Individum[] initPopulation(LinkedList<SettingParameter> simulationParamenters);
	
	
	protected Individum generateOffspring(Individum parent, double offspringSigma) throws Exception {

		LinkedList<SettingParameter> params = new LinkedList<>();
		for (SettingParameter parentParam : parent.getChild().getModelParameters()) {
			// Mutation
			double value = Caster.getParaAsDouble(parentParam) + offspringSigma * rnd.nextGaussian();
			// Check bounds
			if (value <= Caster.getMinAsDouble(parentParam)) {
				value = Caster.getMinAsDouble(parentParam);
			}
			if (value >= Caster.getMaxAsDouble(parentParam)) {
				value = Caster.getMaxAsDouble(parentParam);
			}
			// Create new settings parameter
			SettingParameter offspringParam = new SettingParameter(
					parentParam.getName(),
					parentParam.getTypeDefinition(),
					parentParam.getDescription(),
					value,
					parentParam.getMinValue(),
					parentParam.getMaxValue());
			params.add(offspringParam);
		}
		
		Child child = new Child(params);
		return new Individum(child, offspringSigma);
	}

	public abstract boolean isFinished();
}
