package at.fhv.itm14.trafsim.optimizer;

import Adapter.Optimization.OptimizationAdapter;
import Caster.Caster;
import DataTypes.Optimization.Child;
import DataTypes.SettingParameter;
import at.fhv.itm14.trafsim.optimizer.view.Visualisation;
import org.apache.commons.configuration.PropertiesConfiguration;

import java.util.LinkedList;

public class ESOptimizer extends OptimizationAdapter {

	private int maxGenerations;
	private int generation;
	private Individum[] parentPopulation;
	private Individum[] offspringPopulation;
	private EvolutionaryAlgorithm ea;
	private Visualisation v;

	public ESOptimizer() {
		System.out.println("Creating ES");

		generation = 0;
		v = new Visualisation("x", "y");
		// ESOptimizer Type
		try {
			PropertiesConfiguration prop = new PropertiesConfiguration("es.properties");
			maxGenerations = prop.getInteger("es.max_generations", 1000);
			int objectives = prop.getInteger("es.objectives", 2);
			int mu = prop.getInteger("es.mu", 10);
			int lambda = prop.getInteger("es.lambda", 20);
			double sigma = prop.getDouble("es.sigma", 10.0);
			double sigmaStop = prop.getDouble("es.sigmaStop", 0.001);
			String esType = prop.getString("es.type", "mu_plus_lambda_sigma_sa");
			switch (esType) {
				case "mu_komma_lambda_sigma_sa":
				case "mu_plus_lambda_sigma_sa":
					ea = new MuPlusKommaLambdaSigmaSA(objectives, mu, lambda, sigma, sigmaStop, prop);
					break;
				default:
					throw new RuntimeException("ES Type not known or not specified.");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		System.out.println("  - Done");
	}

	@Override
	public void initializeParameters() {
	}

	@Override
	public String getOptimizerDescription() {
		return "Evolutionary Strategy (ES): "; //+ type;
	}

	@Override
	public void calcNextGeneration() {
		System.out.println("Generation: " + generation);
		
		//getActualValues();
		plotPopulation();
		
		try {
			// Evaluate last offsprings
			// Choose new parents from offsprings
			parentPopulation = ea.selectNewParents(parentPopulation, offspringPopulation);

			plotBest();
			
			// Generate a new offspring generation
			offspringPopulation = ea.generateOffsprings(parentPopulation);

			// Save offsprings for simulation
			setOffsprings();

		} catch (Exception e) {
			e.printStackTrace();
		}

		generation++;
	}

	@Override
	public void calcFirstGeneration() {
		System.out.println("Generation: " + generation);

		try {
			// Generate parent population
			parentPopulation = ea.initPopulation(getSimulationParameters());

			// First generation
			offspringPopulation = ea.generateOffsprings(parentPopulation);

			// Save offsprings for simulation
			setOffsprings();

		} catch (Exception e) {
			e.printStackTrace();
		}

		generation++;
	}

	@Override
	public boolean checkIfFinished() {
		return (generation >= maxGenerations) || ea.isFinished();
	}

	private void setOffsprings() {
		LinkedList<Child> offsprings = new LinkedList<>();
		for (Individum individum : offspringPopulation) {
			offsprings.add(individum.getChild());
		}
		setActualValues(offsprings);
	}

	private void plotPopulation() {
		// Plot - Last Generation
		for (Individum i : offspringPopulation) {
			v.addPopulationChild(
					i.getChild().getMultiObjective()[0],
					i.getChild().getMultiObjective()[1]
			);
		}
	}

	private void plotBest() {
		// Plot - Last Generation
		int g = v.newGeneration();
		for (Individum i : parentPopulation) {
			StringBuilder info = new StringBuilder();
			int p = 0;
			for (SettingParameter param : i.getChild().getModelParameters()) {
				info.append("Param ");
				info.append(p++);
				info.append(" = ");
				info.append(Caster.getParaAsDouble(param));
				info.append("\n");
			}
			info.append("sigma = ");
			info.append(i.getSigma());
			info.append("\n");
			v.addValueGenerationParetoChild(
					i.getChild().getMultiObjective()[0],
					i.getChild().getMultiObjective()[1],
					g,
					info.toString());
		}
	}
	

	public static void main(String[] args) {
		ESOptimizer es = new ESOptimizer();
		es.initializeParameters();
		es.calcFirstGeneration();
		es.calcNextGeneration();
		es.isFinished();
	}
}
