package at.fhv.itm14.trafsim.optimizer;

import Caster.Caster;
import DataTypes.Optimization.Child;
import DataTypes.SettingParameter;
import org.apache.commons.configuration.PropertiesConfiguration;

import java.util.LinkedList;

public class MuPlusKommaLambdaSigmaSA extends EvolutionaryAlgorithm {

	private final int numObjectives;
	private final int mu;
	private final int lambda;
	private double sigma;
	private final double sigmaStop;
	private final double[] factor;
	private final double tau;
	private final boolean kommaStrategy;
	private double[][] weights;

	public MuPlusKommaLambdaSigmaSA(int objectives, int mu, int lambda, double sigma, double sigmaStop, PropertiesConfiguration prop) {
		this.numObjectives = objectives;
		this.mu = mu;
		this.lambda = lambda;
		this.sigma = sigma;
		this.sigmaStop = sigmaStop;
		this.factor = new double[objectives];
		this.tau = prop.getDouble("es.tau", 0.35);
		String[] tmpFactor = prop.getStringArray("es.factor");
		for (int i = 0; i < objectives; i++) {
			this.factor[i] = Double.parseDouble(tmpFactor[i]);
		}
		if (prop.getString("es.type").contains("komma")) {
			kommaStrategy = true;
		} else {
			kommaStrategy = false;
		}
		this.weights = new double[mu][numObjectives];
		// TODO: Hardcoded: two objectices
		double weightStep = 1.0 / (double) (mu - 1);
		double[] objWeight = new double[]{0.0, 1.0};
		this.weights[0] = new double[]{0.0, 1.0};
		this.weights[mu - 1] = new double[]{1.0, 0.0};
		for (int i = 1; i < (mu - 1); i++) {
			objWeight[0] += weightStep;
			objWeight[1] -= weightStep;
			this.weights[i][0] = objWeight[0];
			this.weights[i][1] = objWeight[1];
		}
	}

	@Override
	public Individum[] generateOffsprings(Individum[] parents) throws Exception {
		Individum[] offsprings = new Individum[lambda];
		for (int l = 0; l < lambda; l++) {
			int parentId = rnd.nextInt(mu);
			double offspringSigma = parents[parentId].getSigma() * Math.exp(tau * rnd.nextGaussian());
			offsprings[l] = generateOffspring(parents[parentId], offspringSigma);
		}
		return offsprings;
	}

	@Override
	public Individum[] selectNewParents(Individum[] parents, Individum[] offsprings) {
		double sigmaSum = 0.0;
		// New empty parent population
		Individum[] newParents = new Individum[mu];

		try {
			for (int m = 0; m < mu; m++) {
				if (kommaStrategy) {
					// (mu,lambda) --> forget last parent generation
					newParents[m] = new Individum(null, sigma);
				} else {
					// (mu+lambda) --> keep last parent generation
					newParents[m] = parents[m];
				}
				for (int l = 0; l < lambda; l++) {
					if (isParent(offsprings[l], newParents)) {
						continue;
					}
					double fitness = calculateFitness(offsprings[l], weights[m]);
					if (newParents[m].getCalculatedFitness() >= fitness) {
						newParents[m] = offsprings[l].clone();
						newParents[m].setCalculatedFitness(fitness);
					}
				}
				sigmaSum += newParents[m].getSigma();
			}
		} catch (CloneNotSupportedException ex) {
			throw new RuntimeException(ex);
		}

		sigma = sigmaSum / (double) mu;
		System.out.println("  Sigma: " + sigma);
		return newParents;
	}

	private double calculateFitness(Individum individum, double[] weights) {
		double[] objectives = individum.getChild().getMultiObjective();
		double fitness = 0.0;
		for (int i = 0; i < numObjectives; i++) {
			fitness += objectives[i] * factor[i] * weights[i];
		}
		return fitness;
	}

	@Override
	public Individum[] initPopulation(LinkedList<SettingParameter> simulationParamenters) {
		Individum[] pop = new Individum[mu];
		for (int i = 0; i < mu; i++) {
			for (SettingParameter param : simulationParamenters) {
				double value = Caster.getParaAsDouble(param);
				param.setValue(value);
			}
			Child parent = new Child(simulationParamenters);
			pop[i] = new Individum(parent, sigma);
		}
		return pop;
	}

	@Override
	public boolean isFinished() {
		return sigma <= sigmaStop;
	}

	private boolean isParent(Individum offspring, Individum[] newParents) {
		for (Individum parent : newParents) {
			if (parent != null) {
				if (parent.getChild() == offspring.getChild()) {
					return true;
				}
			}
		}
		return false;
	}

}
