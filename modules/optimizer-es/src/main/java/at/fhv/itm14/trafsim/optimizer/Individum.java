package at.fhv.itm14.trafsim.optimizer;

import DataTypes.Optimization.Child;

public class Individum {
	private Child child;
	private final double sigma;
	private double calculatedFitness;

	public Individum(Child child, double sigma) {
		this.child = child;
		this.sigma = sigma;
		this.calculatedFitness = Double.MAX_VALUE;
	}

	public Child getChild() {
		return child;
	}

	public double getSigma() {
		return sigma;
	}

	public double getCalculatedFitness() {
		return calculatedFitness;
	}

	public void setCalculatedFitness(double calculatedFitness) {
		this.calculatedFitness = calculatedFitness;
	}

	public void setChild(Child child) {
		this.child = child;
	}

	@Override
	protected Individum clone() throws CloneNotSupportedException {
		Individum clone = new Individum(child, sigma);
		clone.calculatedFitness = calculatedFitness;
		return clone;
	}
}
