package at.fhv.itm14.trafsim.optimizer.view;

import com.opencsv.CSVWriter;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Visualisation extends Application implements IVisualisation {

	private static Visualisation instance;

	public static Visualisation getInstance() {
		return instance;
	}
	
	private CSVWriter writer;
	private String resultFile;
	
	
	public Visualisation() {
		this("x", "y");
	}
	
	public Visualisation(String xAxisName, String yAxisName) {
		this(xAxisName, yAxisName, getDefaultFilename());
	}
	
	public Visualisation(String xAxisName, String yAxisName, String resultFile) {
		this.resultFile = resultFile;
		//setXAxisName(xAxisName);
		//setYAxisName(yAxisName);
		instance = this;
	}
	
	private static String getDefaultFilename() {
		SimpleDateFormat f = new SimpleDateFormat("hhmmss_ddMMyyyy");
		String filename = "result_" + f.format(new Date()) + ".csv";
		return filename;
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent root = FXMLLoader.load(Visualisation.class.getClassLoader().getResource("visualisation.fxml"));
		primaryStage.setTitle("Optimization Visualisation");
		primaryStage.setScene(new Scene(root, 1000, 800));
		primaryStage.show();
	}

	/**
	 * Main entry point
	 * @param args 
	 */
	public static void main(String[] args) {
		launch(args);
	}
	
	private void writeToResults(String[] data) {
		if (writer == null) {
			try {
				writer = new CSVWriter(new FileWriter(resultFile));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (!writer.checkError()) {
			writer.writeNext(data);
			try {
				writer.flush();
			} catch (IOException ex) {
				Logger.getLogger(Visualisation.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}
	
	@Override
	public void addPopulationChild(double x, double y) {
		Platform.runLater(
			() -> {
				VisualisationController.getInstance().addPopulationChild(x, y);
			}
		);
		writeToResults(new String[]{"pop", Double.toString(x), Double.toString(y)});
	}

	@Override
	public void addValueGenerationParetoChild(double x, double y, int generation) {
		Platform.runLater(
			() -> {
				VisualisationController.getInstance().addValueGenerationParetoChild(x, y, generation);
			}
		);
		writeToResults(new String[]{Integer.toString(generation), Double.toString(x), Double.toString(y), ""});
	}

	@Override
	public void addValueGenerationParetoChild(double x, double y, int generation, String additionalText) {
		Platform.runLater(
			() -> {
				VisualisationController.getInstance().addValueGenerationParetoChild(x, y, generation, additionalText);
			}
		);
		writeToResults(new String[]{Integer.toString(generation), Double.toString(x), Double.toString(y), additionalText});
	}

	@Override
	public void setXAxisName(String xAxisName) {
		VisualisationController.getInstance().setXAxisName(xAxisName);
	}

	@Override
	public void setYAxisName(String yAxisName) {
		VisualisationController.getInstance().setYAxisName(yAxisName);
	}

	@Override
	public int newGeneration() {
		return VisualisationController.getInstance().newGeneration();
	}
	
}
