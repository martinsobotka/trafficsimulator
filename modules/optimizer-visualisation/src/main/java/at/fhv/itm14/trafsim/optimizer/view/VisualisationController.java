package at.fhv.itm14.trafsim.optimizer.view;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class VisualisationController implements IVisualisation {
	private final static DropShadow SELECTED_EFFECT;
	private static VisualisationController instance;

	static {
		int depth = 20;
		SELECTED_EFFECT = new DropShadow();
		SELECTED_EFFECT.setOffsetY(0f);
		SELECTED_EFFECT.setOffsetX(0f);
		SELECTED_EFFECT.setWidth(depth);
		SELECTED_EFFECT.setHeight(depth);
	}

	@FXML private ScatterChart chart;
	@FXML private NumberAxis xAxis;
	@FXML private NumberAxis yAxis;
	@FXML private ListView<CheckBox> lstGenerations;
	@FXML private TextArea txtInfo;

	private final XYChart.Series<Double, Double> population;
	private final Map<Integer, XYChart.Series<Double, Double>> generations;
	private int generationCounter = 0;

	private XYChart.Data<Double, Double> selected;
	private boolean populationCheckBox;


	public VisualisationController() {
		this.population = new XYChart.Series<>();
		this.population.setName("Pop.");
		this.generations = new HashMap<>();
		this.instance = this;
	}

	public static IVisualisation getInstance() {
		return instance;
	}

	@Override
	public void addPopulationChild(double x, double y) {
		addPopulationCheckBox();
		population.getData().add(new XYChart.Data<>(x, y));
	}

	@Override
	public void addValueGenerationParetoChild(double x, double y, int generation) {
		addValueGenerationParetoChild(x, y, generation, "");
	}

	@Override
	public void addValueGenerationParetoChild(double x, double y, int generation, String additionalText) {
		if (!generationExists(generation)) {
			createGeneration(generation);
		}
		// Create DataPoint
		XYChart.Data<Double, Double> point = new XYChart.Data<>(x, y, additionalText);
		generations.get(generation).getData().add(point);
		// Add tooltip and selection
		Tooltip.install(point.getNode(), new Tooltip(String.format("Gen: %d, x=%f, y=%f", generation, x, y)));
		point.getNode().setOnMouseReleased(mouseEvent -> this.selectPoint(point));
	}

	@Override
	public void setXAxisName(String xAxisName) {
		xAxis.setLabel(xAxisName);
	}

	@Override
	public void setYAxisName(String yAxisName) {
		yAxis.setLabel(yAxisName);
	}

	@Override
	public int newGeneration() {
		generationCounter++;
		return generationCounter;
	}

	
	private void addPopulationCheckBox() {
		if (!populationCheckBox) {
			CheckBox popCheckBox = new CheckBox("Population");
			lstGenerations.getItems().add(popCheckBox);
			popCheckBox.selectedProperty().addListener(
					(observableValue, oldValue, newValue) -> {
						this.checkBoxAction(population, oldValue, newValue);
					}
			);
			populationCheckBox = true;
		}
	}

	private void selectPoint(XYChart.Data<Double, Double> point) {
		if (selected != null) {
			// deselect
			selected.getNode().setEffect(null);
		}
		selected = point;
		selected.getNode().setEffect(SELECTED_EFFECT);
		txtInfo.setText(
				"x=" + point.getXValue() + ", y=" + point.getYValue() + "\n\n" +
				selected.getExtraValue().toString().replace("<br>", "\n")
		);
	}

	private void createGeneration(int generation) {
		// Create new series
		XYChart.Series<Double, Double> genSeries = new XYChart.Series<>();
		genSeries.setName(Integer.toString(generation));
		chart.getData().add(genSeries);
		generations.put(generation, genSeries);
		// Create and add checkbox
		CheckBox generationChecker = new CheckBox((Integer.toString(generation)));
		generationChecker.selectedProperty().setValue(true);
		generationChecker.selectedProperty().addListener(
				(observableValue, oldValue, newValue) -> {
					this.checkBoxAction(genSeries, oldValue, newValue);
				}
		);
		lstGenerations.getItems().add(generationChecker);
	}

	private void checkBoxAction(XYChart.Series<Double, Double> series, boolean oldValue, boolean newValue) {
		if (!oldValue && newValue) {
			chart.getData().add(series);
		}
		if (oldValue && !newValue) {
			chart.getData().remove(series);
		}
	}

	private boolean generationExists(int generation) {
		return generations.containsKey(generation);
	}

	public void selectAll(ActionEvent actionEvent) {
		lstGenerations.getItems().forEach(
				checkBox -> {
					checkBox.selectedProperty().setValue(true);
				}
		);
	}

	public void selectNone(ActionEvent actionEvent) {
		lstGenerations.getItems().forEach(
				checkBox -> {
					checkBox.selectedProperty().setValue(false);
				}
		);
	}

	public void openCSV(ActionEvent actionEvent) {
		FileChooser fileChooser = new FileChooser();
		File file = fileChooser.showOpenDialog(null);

		Task<Void> fileOpener = new Task<Void>() {
			@Override
			protected Void call() {
				try (CSVReader reader = new CSVReader(new FileReader(file))) {
					reader.forEach(this::parseCSVElements);
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;

			}

			private void parseCSVElements(String[] elements) {
				double x = Double.parseDouble(elements[1]);
				double y = Double.parseDouble(elements[2]);
				if ("pop".equals(elements[0])) {
					Platform.runLater(
						() -> addPopulationChild(x, y)
					);
				} else {
					int generation = Integer.parseInt(elements[0]);
					Platform.runLater(
						() -> addValueGenerationParetoChild(x, y, generation, elements[3].replace("<br>", "\n"))
					);
				}
			}
		};
		new Thread(fileOpener).start();
	}

	public void saveCSV(ActionEvent actionEvent) {
		FileChooser fileChooser = new FileChooser();
		File file = fileChooser.showOpenDialog(null);
		try (CSVWriter writer = new CSVWriter(new FileWriter(file))) {
			population.getData().forEach(
					data -> writer.writeNext(
								new String[]{
										"pop",
										Double.toString(data.getXValue()),
										Double.toString(data.getYValue())
								}
							)
			);
			generations.entrySet().forEach(entry -> {
				String gen = Integer.toString(entry.getKey());
				entry.getValue().getData().forEach(
						data -> writer.writeNext(
								new String[]{
										gen,
										Double.toString(data.getXValue()),
										Double.toString(data.getYValue()),
										data.getExtraValue().toString().replace("\n", "<br>")
								}
						)
				);
			});

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void close(ActionEvent actionEvent) {
		Platform.exit();
	}
}
