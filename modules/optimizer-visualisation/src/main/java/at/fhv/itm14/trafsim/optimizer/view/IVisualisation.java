package at.fhv.itm14.trafsim.optimizer.view;

public interface IVisualisation {

	void addPopulationChild(double x, double y);

	void addValueGenerationParetoChild(double x, double y, int generation);

	void addValueGenerationParetoChild(double x, double y, int generation, String additionalText);
	
	void setXAxisName(String xAxisName);
	
	void setYAxisName(String yAxisName);
	
	int newGeneration();
}
