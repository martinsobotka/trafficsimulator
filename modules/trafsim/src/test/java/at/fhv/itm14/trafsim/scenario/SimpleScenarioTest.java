package at.fhv.itm14.trafsim.scenario;

import at.fhv.itm14.trafsim.model.SimpleScenarioFactory;
import at.fhv.itm14.trafsim.model.TrafSimModel;
import at.fhv.itm14.trafsim.model.entities.Sink;
import desmoj.core.simulator.Experiment;
import desmoj.core.simulator.TimeInstant;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class SimpleScenarioTest {
	private Experiment exp;
	private TrafSimModel model;
	private SimpleScenarioFactory factory;
	
	@Rule
	public TestName name = new TestName();
	
	@Before
	public void setUp() {
		model = new TrafSimModel();
		exp = new Experiment(name.getMethodName(), false);
		// and connect them
		model.connectToExperiment(exp);

		factory = SimpleScenarioFactory.getInstance(model);
		
		// set experiment parameters
		exp.setShowProgressBar(false);
	}
	
	
	@Test
	public void simpleScenarioTest01() {
		double simulationDuration = 111;
		double streetTraverseTime = 0;
		double greenDuration = 20.01;
		double yellowDuration = 4.99;
		double turnaroundTime = 4 * greenDuration + 4 * yellowDuration;
		double startOffset = 0;
		double mainArrivalRate = 0.2;
		double otherArrivalRate = 0.2;
		double intersectionTraverseTime = 10;
		double accelerationTime = 1;
		double[][] routeProbability = new double[4][4];
		routeProbability[SimpleScenarioFactory.A_DIRECTION][SimpleScenarioFactory.B_DIRECTION] = 1;
		routeProbability[SimpleScenarioFactory.B_DIRECTION][SimpleScenarioFactory.A_DIRECTION] = 1;
		routeProbability[SimpleScenarioFactory.C_DIRECTION][SimpleScenarioFactory.D_DIRECTION] = 1;
		routeProbability[SimpleScenarioFactory.D_DIRECTION][SimpleScenarioFactory.C_DIRECTION] = 1;
		
		factory.buildSimpleScenario(
				streetTraverseTime,
				turnaroundTime,
				startOffset,
				yellowDuration,
				mainArrivalRate,
				otherArrivalRate,
				intersectionTraverseTime,
				accelerationTime,
				routeProbability);
		
		TimeInstant stopTime = new TimeInstant(simulationDuration, TimeUnit.SECONDS);
		exp.tracePeriod(new TimeInstant(0), stopTime);
		exp.stop(stopTime);
		exp.start();
		
		int expectedCarCount = (int)Math.ceil(greenDuration / accelerationTime);
		for (Sink s : factory.getSinks()) {
			assertEquals(expectedCarCount, s.getCarCounter());
		}
	}
	
	
	@Test
	public void simpleScenarioTest02() {
		double simulationDuration = 3001;
		double streetTraverseTime = 5;
		double greenDuration = 20.01;
		double yellowDuration = 4.99;
		double turnaroundTime = 4 * greenDuration + 4 * yellowDuration;
		double startOffset = 0;
		double mainArrivalRate = 5;
		double otherArrivalRate = 20;
		double intersectionTraverseTime = 4;
		double accelerationTime = 2;
		double[][] routeProbability = new double[4][4];
		routeProbability[SimpleScenarioFactory.A_DIRECTION][SimpleScenarioFactory.B_DIRECTION] = 1;
		routeProbability[SimpleScenarioFactory.B_DIRECTION][SimpleScenarioFactory.A_DIRECTION] = 1;
		routeProbability[SimpleScenarioFactory.C_DIRECTION][SimpleScenarioFactory.D_DIRECTION] = 1;
		routeProbability[SimpleScenarioFactory.D_DIRECTION][SimpleScenarioFactory.C_DIRECTION] = 1;
		
		factory.buildSimpleScenario(
				streetTraverseTime,
				turnaroundTime,
				startOffset,
				yellowDuration,
				mainArrivalRate,
				otherArrivalRate,
				intersectionTraverseTime,
				accelerationTime,
				routeProbability);
		
		TimeInstant stopTime = new TimeInstant(simulationDuration, TimeUnit.SECONDS);
		exp.tracePeriod(new TimeInstant(0), stopTime);
		exp.stop(stopTime);
		exp.start();
		exp.report();
		
		double anylogicSourceA = 600;
		double anylogicSourceB = 600;
		double anylogicSourceC = 150;
		double anylogicSourceD = 150;
		
		double anylogicSinkA = 328;
		double anylogicSinkB = 323;
		double anylogicSinkC = 148;
		double anylogicSinkD = 149;
		
		assertEquals(anylogicSourceA, factory.getSourceA().getCarCounter(), 1);
		assertEquals(anylogicSourceB, factory.getSourceB().getCarCounter(), 1);
		assertEquals(anylogicSourceC, factory.getSourceC().getCarCounter(), 1);
		assertEquals(anylogicSourceD, factory.getSourceD().getCarCounter(), 1);
		
		assertEquals(anylogicSinkA, factory.getSinkA().getCarCounter(), 2);
		assertEquals(anylogicSinkB, factory.getSinkB().getCarCounter(), 2);
		assertEquals(anylogicSinkC, factory.getSinkC().getCarCounter(), 2);
		assertEquals(anylogicSinkD, factory.getSinkD().getCarCounter(), 2);
	}
	
	
	@After
	public void tearDown() {
		exp.finish();
	}
}
