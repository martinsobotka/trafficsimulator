package at.fhv.itm14.trafsim.util;

import at.fhv.itm14.trafsim.model.ModelFactory;
import at.fhv.itm14.trafsim.model.Scenario;
import at.fhv.itm14.trafsim.model.ScenarioFactory;
import at.fhv.itm14.trafsim.model.entities.*;
import at.fhv.itm14.trafsim.model.entities.intersection.FixedCirculationController;
import at.fhv.itm14.trafsim.model.entities.intersection.Intersection;
import at.fhv.itm14.trafsim.persistence.model.ScenarioDTO;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;



public class XMLParserTest {
	
	private Scenario scenario;
	
	
	@Before
	public void setUp() {
		scenario = buildScenario();
	}
	
	
	@Test
	public void testSaveLoadXML() {
		try {
			// Create Scenario and export it
			XMLParser.saveScenario(scenario, "scenario.xml");
			
			// Import Scenario and convert it
			ScenarioDTO scenarioDTO = XMLParser.loadScenario("scenario.xml");
			Scenario imported = ScenarioFactory.createScenarioFromDTO(scenarioDTO);
			
			assertEquals(scenario.getName(), imported.getName());
			assertEquals(scenario.getIntersections().size(), imported.getIntersections().size());
			assertEquals(scenario.getStreets().size(), imported.getStreets().size());
			assertEquals(scenario.getSinks().size(), imported.getSinks().size());
			assertEquals(scenario.getSources().size(), imported.getSources().size());
			
			// Start both scenarios
			scenario.getExperiment().start();
			scenario.getExperiment().finish();
			
			imported.setSimulationDuration(scenario.getSimulationDuration());
			imported.getExperiment().start();
			imported.getExperiment().finish();
			
			for (int i = 0; i < scenario.getSinks().size(); i++) {
				assertTrue(scenario.getSinks().get(i).getCarCounter() > 0);
				assertEquals(scenario.getSinks().get(i).getCarCounter(), imported.getSinks().get(i).getCarCounter());
			}
			
		} catch (ScenarioReadException | ScenarioSaveException ex) {
			ex.printStackTrace();
			fail();
		}
	}
	
	
	private Scenario buildScenario() {
		int numberOfIntersections = 3;
		float turnaroundTime = 60;
		float[] phaseShiftTimes = new float[]{0.0F, 10.0F, 20.0F};
		double streetTraverseTime = 10.0;
		double intersectionTraverseTime = 5.0;
		double accelerationTime = 2.0;
		double yellowDuration = turnaroundTime / 8;
		double greenDuration = turnaroundTime / 2 - yellowDuration;
		int outDirection = 1;
		int inDirection = 0;
		double arrivalRate = 0.2;
		long scenarioDuration = 1000;
		
		// Scenario, Model, Experiment and ModelFactory
		Scenario s = new Scenario();
		ModelFactory modelFactory = ModelFactory.getInstance(s.getModel());
		
		int numberOfStreets = numberOfIntersections + 1;
		OneWayStreet[] streets = new OneWayStreet[numberOfStreets];
		//OneWayStreet[] tmpStreets = new OneWayStreet[numberOfStreets];
		Intersection[] intersections = new Intersection[numberOfIntersections];
		
		
		// Create all elements: sink, streets, intersections and source
		Sink sink = modelFactory.createSink();
		s.addSink(sink);
		AbstractConsumer out = sink;
		for (int i = numberOfIntersections - 1; i >= 0; i--) {
			streets[i + 1] = modelFactory.createOneWayStreet(streetTraverseTime, out);
			s.addStreet(streets[i + 1]);
			//tmpStreets[i + 1] = modelFactory.createOneWayStreet(streetTraverseTime, out);
			intersections[i] = modelFactory.create2Intersection();
			s.addIntersection(intersections[i]);
			intersections[i].setServiceDelay(accelerationTime);
			FixedCirculationController ic = modelFactory.createOneWayController(
					intersections[i],
					greenDuration,
					yellowDuration,
					phaseShiftTimes[i]
				);
			intersections[i].attachController(ic);
			out = intersections[i].toConsumer(inDirection);
		}
		streets[0] = modelFactory.createOneWayStreet(streetTraverseTime, out);
		s.addStreet(streets[0]);
		Source source = modelFactory.createSource(modelFactory.createContDistConstant(arrivalRate), streets[0].toConsumer());
		source.setDistValues(new Double[]{arrivalRate});
		s.addSource(source);
		
		// Connect intersection
		for (int i = 0; i < numberOfIntersections; i++) {
			AbstractProducer in = streets[i].toProducer();
			out = streets[i + 1].toConsumer();
			intersections[i].attachProducer(inDirection, in);
			intersections[i].attachConsumer(outDirection, out);
			intersections[i].createConnectionQueue(in, new AbstractConsumer[]{out}, new double[]{intersectionTraverseTime}, new double[]{1.0});
		}
		
		s.setSimulationDuration(scenarioDuration);
		return s;
	}
}
