package at.fhv.itm14.trafsim.model.entities;

import at.fhv.itm14.trafsim.model.ModelFactory;
import at.fhv.itm14.trafsim.model.TrafSimModel;
import at.fhv.itm14.trafsim.model.entities.intersection.Intersection;
import desmoj.core.simulator.Experiment;
import desmoj.core.simulator.TimeInstant;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class ModelTest {
	private Experiment exp;
	private TrafSimModel model;
	private ModelFactory factory;
	
	@Rule
	public TestName name = new TestName();
	
	@Before
	public void setUp() {
		model = new TrafSimModel();
		exp = new Experiment(name.getMethodName(), false);
		// and connect them
		model.connectToExperiment(exp);

		factory = ModelFactory.getInstance(model);
		
		// set experiment parameters
		exp.setShowProgressBar(false);
	}

	@Test
	public void sourceStreetSinkTest() {
		Sink sink = factory.createSink();
		double traverseTime = 10;
		OneWayStreet street = factory.createOneWayStreet(traverseTime, sink);
		Source source = factory.createSource(factory.createContDistUniform(5, 10), street.toConsumer());
		
		TimeInstant stopTime = new TimeInstant(10, TimeUnit.MINUTES);
		exp.tracePeriod(new TimeInstant(0), stopTime);
		exp.stop(stopTime);
		exp.start();
		
		System.out.println(source.getCarCounter() + ":" + sink.getCarCounter());
		
		assertEquals(source.getCarCounter(), street.getCarsOnStreet() + sink.getCarCounter());
	}
	
	
	@Test
	public void sourceIntersectionSinkTest() {
		double greenDuration = 100;
		double yellowDuration = 10;
		double turnaroundTime = 4 * greenDuration + 4 * yellowDuration;
		double startOffset = 0;
		
		List<Sink> sinks = new ArrayList<>();
		for (int i = 0; i < 4; i++) {
			sinks.add(factory.createSink());
		}
		
		Intersection intersection = ModelFactory.getInstance(model).createFull4Intersection(turnaroundTime, startOffset, yellowDuration);
		
		List<Source> sources = new ArrayList<>();
		for (int intersectionDirection = 0; intersectionDirection < 4; intersectionDirection++) {
			sources.add(factory.createSource(factory.createContDistUniform(5, 10), intersection.toConsumer(intersectionDirection)));
		}
		
		ModelFactory.getInstance(model).connectFull4Intersection(intersection, sources, sinks);
		
		TimeInstant stopTime = new TimeInstant(10, TimeUnit.MINUTES);
		exp.tracePeriod(new TimeInstant(0), stopTime);
		exp.stop(stopTime);
		exp.start();
		
		for (Source s : sources) {
			System.out.println("-- Source: " + s.getCarCounter());
		}
		for (Sink s : sinks) {
			System.out.println("-- Sink: " + s.getCarCounter());
		}
	}
	
	@After
	public void tearDown() {
		exp.finish();
	}
}
