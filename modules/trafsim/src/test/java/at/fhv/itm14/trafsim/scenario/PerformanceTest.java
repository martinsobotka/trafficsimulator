package at.fhv.itm14.trafsim.scenario;

import at.fhv.itm14.trafsim.model.ModelFactory;
import at.fhv.itm14.trafsim.model.Scenario;
import at.fhv.itm14.trafsim.model.ScenarioFactory;
import at.fhv.itm14.trafsim.statistics.GlobalStatistic;
import at.fhv.itm14.trafsim.util.ScenarioReadException;
import desmoj.core.dist.DistributionManager;
import desmoj.core.simulator.Experiment;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import java.awt.*;
import java.util.Random;

public class PerformanceTest {
	
	//@Test
	public void performanceTest() {
		final ChartHelper demo = new ChartHelper("Simulation Run Performance");
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);
		float turnAroundTime = (float)(Math.random() * 150 + 30);
		float[] phaseShifts = new float[] { (float)(Math.random() * 150 + 30) , (float)(Math.random() * 150 + 30) , (float)(Math.random() * 150 + 30) };
        
		for (int i = 0; i < 1000; i++) {
			long start = System.currentTimeMillis();
			System.out.print("Experiment start @" + start);
			try {
				//Scenario scenario = ScenarioFactory.createSingleRowOneWayIntersection(3, turnAroundTime, toPrimitiveFloat(phaseShifts));
				Scenario scenario = ScenarioFactory.createSingleRowOneWayIntersection("scenario_3x1.xml", turnAroundTime, phaseShifts);
				GlobalStatistic gs = GlobalStatistic.getInstance(scenario.getModel());
	
				scenario.getModel().seed(new DistributionManager("Distributions", new Random().nextLong()));
				Experiment exp = scenario.getExperiment();
				exp.setSilent(true);
				exp.setSeedGenerator((long)(Math.random() * 0xFFFFFFFF));
				exp.start();
				exp.finish();
	
				float avgStops = (float)gs.getAvgStops();
				float avgWaitingTime = (float)gs.getAvgWaitingTime();
	
				long end = System.currentTimeMillis();
				System.out.print(" and end @" + end);
				System.out.println(" - avgStops: " + avgStops + ", " + avgWaitingTime);
				GlobalStatistic.freeInstance(scenario.getModel());
				ScenarioFactory.cleanup(scenario);
				ModelFactory.freeInstance(scenario.getModel());
				demo.addSimulationRun(i, end - start);
			} catch (ScenarioReadException ex) {
				ex.printStackTrace();
			}
		}
		try {
			Thread.currentThread().join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private class ChartHelper extends ApplicationFrame {

		private static final long serialVersionUID = 1L;
		final XYSeries series1;
		
		public ChartHelper(String title) {
			super(title);
			
			series1 = new XYSeries("Simulation Runs");
			final XYSeriesCollection dataset = new XYSeriesCollection();
	        dataset.addSeries(series1);
	        final JFreeChart chart = createChart(dataset);
	        final ChartPanel chartPanel = new ChartPanel(chart);
	        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
	        setContentPane(chartPanel);
		}

		private JFreeChart createChart(final XYDataset dataset) {
	        
	        final JFreeChart chart = ChartFactory.createXYLineChart(
	            "Simulation Run Performance",      // chart title
	            "Simulation Runs",                 // x axis label
	            "Time Needed [ms]",                // y axis label
	            dataset,                  // data
	            PlotOrientation.VERTICAL,
	            true,                     // include legend
	            true,                     // tooltips
	            false                     // urls
	        );

	        chart.setBackgroundPaint(Color.white);

	        final XYPlot plot = chart.getXYPlot();
	        plot.setBackgroundPaint(Color.lightGray);
	        plot.setDomainGridlinePaint(Color.white);
	        plot.setRangeGridlinePaint(Color.white);
	        
	        final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
	        renderer.setSeriesLinesVisible(0, true);
	        renderer.setSeriesShapesVisible(0, true);
	        plot.setRenderer(renderer);

	        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
	        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
	                
	        return chart;
	    }
		
		public void addSimulationRun(int i, long time) {
			series1.add(i, time);
		}
	}

}
