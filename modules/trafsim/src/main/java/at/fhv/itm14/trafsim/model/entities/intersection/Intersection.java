package at.fhv.itm14.trafsim.model.entities.intersection;

import at.fhv.itm14.trafsim.model.ModelFactory;
import at.fhv.itm14.trafsim.model.entities.AbstractConsumer;
import at.fhv.itm14.trafsim.model.entities.AbstractMultiProSumer;
import at.fhv.itm14.trafsim.model.entities.AbstractProducer;
import at.fhv.itm14.trafsim.model.entities.Car;
import at.fhv.itm14.trafsim.model.entities.util.CarDistributor;
import at.fhv.itm14.trafsim.model.events.CarAcceleratorEvent;
import at.fhv.itm14.trafsim.model.events.CarDepartureEvent;
import at.fhv.itm14.trafsim.persistence.model.ElementDTO;
import at.fhv.itm14.trafsim.persistence.model.IntersectionDTO;
import at.fhv.itm14.trafsim.statistics.GlobalStatistic;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.Queue;
import desmoj.core.simulator.TimeSpan;
import desmoj.core.statistic.Count;
import desmoj.core.statistic.Tally;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

public class Intersection extends AbstractMultiProSumer {
	private final int size;
	private final AbstractConsumer[] outList;
	private final AbstractProducer[] inList;
	private final CarDistributor[] distList;
	private final HashMap<AbstractProducer, Integer> inMap;
	private final HashMap<AbstractConsumer, Integer> outMap;
	private final double[][] traverseTimes;
	private final double[][] probabilities;
	private double serviceDelay;
	private final Queue<Car>[][] queues;
	private final HashMap<AbstractProducer, AbstractConsumer[]> connections;
	private final Tally[][] greenTimeThroughput;
	private final Count[][] greenTimeThroughputCounter;
	private IntersectionController controller;
	private IntersectionDTO dto;

	public Intersection(Model owner, String name, boolean showInTrace, int size) {
		super(owner, name, showInTrace, size);
		this.size = size;
		this.outList = new AbstractConsumer[size];
		this.inList = new AbstractProducer[size];
		this.distList = new CarDistributor[size];
		for (int i = 0; i < distList.length; i++) {
			distList[i] = new CarDistributor();
		}
		this.inMap = new HashMap<>();
		this.outMap = new HashMap<>();
		this.queues = new Queue[size][size];
		this.greenTimeThroughput = new Tally[size][size];
		this.greenTimeThroughputCounter = new Count[size][size];
		this.traverseTimes = new double[size][size];
		this.serviceDelay = 1; // Default
		this.connections = new HashMap<>();
		this.probabilities = new double[size][size];
		GlobalStatistic.getInstance(owner).registerIntersection(this);
	}

	public void attachController(IntersectionController controller) {
		this.controller = controller;
	}

	public void attachConsumer(int outDirection, AbstractConsumer consumer) {
		outList[outDirection] = consumer;
		outMap.put(consumer, outDirection);
	}

	public void attachProducer(int inDirection, AbstractProducer producer) {
		inList[inDirection] = producer;
		inMap.put(producer, inDirection);
	}

	/**
	 * Creates a connection through the intersection: From "in" to "out" (could be more) with one queue.
	 * 
	 * @param in - in direction (producer)
	 * @param out - list of consumer: possible out connections
	 * @param traverseTimes - list of traverse time (same size as out)
	 * @param probabilities - list of possibility of out connection (same size as out)
	 */
	public void createConnectionQueue(AbstractProducer in, AbstractConsumer[] out, double[] traverseTimes, double[] probabilities) {
		int inIndex = inMap.get(in);
		Queue<Car> queue = ModelFactory.getInstance(getModel()).createQueue(Car.class, "_" + inIndex);
		for (int i = 0; i < out.length; i++) {
			int outIndex = outMap.get(out[i]);
			queues[inIndex][outIndex] = queue;
			this.traverseTimes[inIndex][outIndex] = traverseTimes[i];
			this.distList[inIndex].addDirection(outIndex, probabilities[i]);
			this.probabilities[inIndex][outIndex] = probabilities[i];
			// Create Statistic Containers
			greenTimeThroughput[inIndex][outIndex] = new Tally(getModel(), this.getName() + "_t_" + inIndex + "_" + outIndex, false, false);
			greenTimeThroughput[inIndex][outIndex].reset();
			greenTimeThroughputCounter[inIndex][outIndex] = new Count(getModel(), this.getName() + "_c_" + inIndex + "_" + outIndex, false, false);
			greenTimeThroughputCounter[inIndex][outIndex].reset();
		}
		connections.put(in, out);
	}

	public void setDistributor(int index, CarDistributor distributor) {
		distList[index] = distributor;
	}

	@Override
	public void carDelivered(CarDepartureEvent event, Car car, int outDirection, boolean successful) {
		if (successful) {
			// Statisitics
			car.leaveIntersection();
		}
	}

	@Override
	public void carEnter(Car car, int inDirection) {
		// Set outgoing direction
		int outIndex = distList[inDirection].getRandom();
		car.setNextDirection(outIndex);
		
		// Statisitics
		car.enterIntersection();
		
		if (queues[inDirection][outIndex].isEmpty() && controller.canDrive(inDirection, outIndex)) {
			// If queue is empty and green for this direction: Drive through --> schedule CarDeparture-Event
			drive(car, inDirection);
		} else {
			// Otherwise queue car
			queues[inDirection][outIndex].insert(car);
			car.startWaiting();
		}
	}

	@Override
	public boolean isFull() {
		// TODO
		return false;
	}

	public int getSize() {
		return size;
	}

	/**
	 * Notifies intersection, that a connection (in --> out) is open.
	 * Method is called from these events:
	 *   - TrafficLightGreenEvent
	 *   - CarAcceleratorEvent
	 * 
	 * @param inDirection
	 * @param outDirection
	 */
	public void startDrive(int inDirection, int outDirection) {
		// Check if "green" and queue is not empty
		if (controller.canDrive(inDirection, outDirection)) {
			if ((queues[inDirection][outDirection] != null) && !queues[inDirection][outDirection].isEmpty()) {
				// Pick first car from queue and "schedule acceleration"
				Car car = queues[inDirection][outDirection].removeFirst();
				
				CarAcceleratorEvent cae = ModelFactory.getInstance(getModel()).createCarAcceleratorEvent(inDirection, outDirection);
				cae.schedule(car, this, new TimeSpan(serviceDelay));
						
				// Statistics
				if (car.isWaiting()) {
					car.stopWaiting();
				}
			}
		}
	}

	/**
	 * Drive through intersection: Schedules a CarDeparture-Event
	 * 
	 * @param car
	 * @param inDirection 
	 */
	public void drive(Car car, int inDirection) {
		int outDirection = car.getNextDirection();
		CarDepartureEvent event = ModelFactory.getInstance(getModel()).createCarDepartureEvent();
		event.schedule(toProducer(outDirection), car, outList[outDirection], new TimeSpan(traverseTimes[inDirection][outDirection]));
		greenTimeThroughputCounter[inDirection][outDirection].update();
	}

	public void switchToGreen(int inDirection, int outDirection) {
		if (greenTimeThroughputCounter[inDirection][outDirection] != null) {
			GlobalStatistic.getInstance(getModel()).addGreenTimeThroughPutForIntersection(
					this,
					greenTimeThroughputCounter[inDirection][outDirection].getValue()
			);
			greenTimeThroughput[inDirection][outDirection].update(greenTimeThroughputCounter[inDirection][outDirection].getValue());
			greenTimeThroughputCounter[inDirection][outDirection].reset();
		}
	}

	public void switchToRed(int inDirection, int outDirection) {
		// Empty
	}

	public void setServiceDelay(double serviceDelay) {
		this.serviceDelay = serviceDelay;
	}

	@Override
	public IntersectionDTO toDTO() {
		if (dto != null) {
			return dto;
		}
		dto = new IntersectionDTO();
		dto.setName(getName());
		dto.setServiceDelay(serviceDelay);
		dto.setSize(size);
		dto.setIn(new LinkedList<>());
		int i = 0;
		for (AbstractProducer in : inList) {
			if (in == null) {
				dto.getIn().add(new ElementDTO(i++, null));
			} else {
				dto.getIn().add(new ElementDTO(i++, in.toDTO()));
			}
		}
		dto.setOut(new LinkedList<>());
		i = 0;
		for (AbstractConsumer out : outList) {
			if (out != null) {
				dto.getOut().add(new ElementDTO(i++, out.toDTO()));
			} else {
				dto.getOut().add(new ElementDTO(i++, null));
			}
		}
		dto.setConnectionQueues(new LinkedList<>());
		for (Entry<AbstractProducer, AbstractConsumer[]> e : connections.entrySet()) {
			IntersectionDTO.ConnectionQueueDTO connQueue = new IntersectionDTO.ConnectionQueueDTO();
			connQueue.outConnection = new LinkedList<>();
			connQueue.inIndex = inMap.get(e.getKey());
			for (AbstractConsumer out : e.getValue()) {
				IntersectionDTO.OutConnectionDTO outDTO = new IntersectionDTO.OutConnectionDTO();
				outDTO.outIndex =  outMap.get(out);
				outDTO.probability = probabilities[connQueue.inIndex][outDTO.outIndex];
				outDTO.traverseTime = traverseTimes[connQueue.inIndex][outDTO.outIndex];
				connQueue.outConnection.add(outDTO);
			}
			dto.getConnectionQueues().add(connQueue);
		}
		dto.setController(controller.toDTO());
		return dto;
	}

	public AbstractProducer getIn(Integer inIndex) {
		return inList[inIndex];
	}

	public AbstractConsumer getOut(int outIndex) {
		return outList[outIndex];
	}

	public IntersectionController getController() {
		return controller;
	}
}
