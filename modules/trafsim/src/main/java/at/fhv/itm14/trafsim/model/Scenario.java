package at.fhv.itm14.trafsim.model;

import at.fhv.itm14.trafsim.model.entities.OneWayStreet;
import at.fhv.itm14.trafsim.model.entities.Sink;
import at.fhv.itm14.trafsim.model.entities.Source;
import at.fhv.itm14.trafsim.model.entities.intersection.Intersection;
import at.fhv.itm14.trafsim.persistence.model.DTO;
import at.fhv.itm14.trafsim.persistence.model.DTOConvertable;
import at.fhv.itm14.trafsim.persistence.model.ScenarioDTO;
import desmoj.core.simulator.Experiment;
import desmoj.core.simulator.TimeInstant;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Scenario implements DTOConvertable {

	private TrafSimModel model;
	private long simulationDuration;
	private String name;
	private List<Source> sources;
	private List<Sink> sinks;
	private List<OneWayStreet> streets;
	private List<Intersection> intersections;
	private Experiment experiment;

	public Scenario() {
		this(null, false);
	}
	
	public Scenario(String name, boolean report) {
		this.name = name;
		sources = new LinkedList<>();
		sinks = new LinkedList<>();
		streets = new LinkedList<>();
		intersections = new LinkedList<>();
		// Create Model and Experiment
		model = new TrafSimModel();
		experiment = new Experiment(name + "_Experiment", report);
		// and connect them
		model.connectToExperiment(experiment);
		// set experiment parameters
		experiment.setShowProgressBar(false);
	}

	public List<Source> getSources() {
		return sources;
	}

	public void setSources(List<Source> sources) {
		this.sources = sources;
	}

	public void addSource(Source source) {
		this.sources.add(source);
	}

	public List<Sink> getSinks() {
		return sinks;
	}

	public void setSinks(List<Sink> sinks) {
		this.sinks = sinks;
	}
	
	public void addSink(Sink sink) {
		sinks.add(sink);
	}

	public List<OneWayStreet> getStreets() {
		return streets;
	}

	public void setStreets(List<OneWayStreet> streets) {
		this.streets = streets;
	}
	
	public void addStreet(OneWayStreet street) {
		streets.add(street);
	}

	public List<Intersection> getIntersections() {
		return intersections;
	}

	public void setIntersections(List<Intersection> intersections) {
		this.intersections = intersections;
	}

	public void addIntersection(Intersection intersection) {
		intersections.add(intersection);
	}

	public long getSimulationDuration() {
		return simulationDuration;
	}

	public void setSimulationDuration(long simulationDuration) {
		this.simulationDuration = simulationDuration;
		TimeInstant stopTime = new TimeInstant(simulationDuration, TimeUnit.SECONDS);
		this.experiment.stop(stopTime);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TrafSimModel getModel() {
		return model;
	}

	public void setModel(TrafSimModel model) {
		this.model = model;
	}

	public Experiment getExperiment() {
		return experiment;
	}

	public void setExpirment(Experiment exp) {
		this.experiment = exp;
	}

	@Override
	public DTO toDTO() {
		ScenarioDTO scenario = new ScenarioDTO();
		scenario.setName(this.name);
		
		scenario.setSources(new LinkedList<>());
		sources.forEach((source) -> {
			scenario.getSources().add(source.toDTO());
		});
		
		scenario.setIntersections(new LinkedList<>());
		intersections.forEach((intersection) -> {
			scenario.getIntersections().add(intersection.toDTO());
		});
		
		scenario.setStreets(new LinkedList<>());
		streets.forEach((street) -> {
			scenario.getStreets().add(street.toDTO());
		});
		
		scenario.setSinks(new LinkedList<>());
		sinks.forEach((sink) -> {
			scenario.getSinks().add(sink.toDTO());
		});

		return scenario;
	}

	void destroy() {
		sinks.clear();
		sinks = null;
		sources.clear();
		sources = null;
		intersections.clear();
		intersections = null;
		experiment = null;
	}
}
