package at.fhv.itm14.trafsim.model;

import at.fhv.itm14.trafsim.model.entities.AbstractConsumer;
import at.fhv.itm14.trafsim.model.entities.OneWayStreet;
import at.fhv.itm14.trafsim.model.entities.Sink;
import at.fhv.itm14.trafsim.model.entities.Source;
import at.fhv.itm14.trafsim.model.entities.intersection.FixedCirculationController;
import at.fhv.itm14.trafsim.model.entities.intersection.Intersection;
import desmoj.core.simulator.Model;

import java.util.ArrayList;
import java.util.List;

public class SimpleScenarioFactory {
	public final static int A_DIRECTION = 0;
	public final static int B_DIRECTION = 2;
	public final static int C_DIRECTION = 1;
	public final static int D_DIRECTION = 3;
	
	private static SimpleScenarioFactory instance;
	private static ModelFactory modelFactory;
	
	// Scenario model
	private OneWayStreet aToIntersection;
	private OneWayStreet aFromIntersection;
	private OneWayStreet bToIntersection;
	private OneWayStreet bFromIntersection;
	private OneWayStreet cToIntersection;
	private OneWayStreet cFromIntersection;
	private OneWayStreet dToIntersection;
	private OneWayStreet dFromIntersection;
	private Intersection intersection;
	private Source aSource;
	private Source bSource;
	private Source cSource;
	private Source dSource;
	private Sink aSink;
	private Sink bSink;
	private Sink cSink;
	private Sink dSink;
	private FixedCirculationController controller;
	
	
	public static SimpleScenarioFactory getInstance(Model model) {
		if (instance == null) {
			instance = new SimpleScenarioFactory();
		}
		modelFactory = ModelFactory.getInstance(model);
		return instance;
	}

	private SimpleScenarioFactory() {
	}
	
	public void buildSimpleScenario(
			double streetTraverseTime,
			double turnaroundTime,
			double startOffset,
			double yellowDuration,
			double mainArrivalRate,
			double otherArrivalRate,
			double intersectionTraverseTime,
			double accelerationTime,
			double[][] routeProbability
			) {
		
		// Create sinks
		aSink = modelFactory.createSink();
		bSink = modelFactory.createSink();
		cSink = modelFactory.createSink();
		dSink = modelFactory.createSink();
		
		// Create streets from intersection to sink and connect it
		aFromIntersection = modelFactory.createOneWayStreet(streetTraverseTime, aSink);
		bFromIntersection = modelFactory.createOneWayStreet(streetTraverseTime, bSink);
		cFromIntersection = modelFactory.createOneWayStreet(streetTraverseTime, cSink);
		dFromIntersection = modelFactory.createOneWayStreet(streetTraverseTime, dSink);
		
		// Create intersection & controller
		intersection = modelFactory.createFull4Intersection(turnaroundTime, startOffset, yellowDuration);
		intersection.setServiceDelay(accelerationTime);
		
		// Create streets towards intersection
		aToIntersection = modelFactory.createOneWayStreet(streetTraverseTime, intersection.toConsumer(A_DIRECTION));
		bToIntersection = modelFactory.createOneWayStreet(streetTraverseTime, intersection.toConsumer(B_DIRECTION));
		cToIntersection = modelFactory.createOneWayStreet(streetTraverseTime, intersection.toConsumer(C_DIRECTION));
		dToIntersection = modelFactory.createOneWayStreet(streetTraverseTime, intersection.toConsumer(D_DIRECTION));
		
		// Attach all street to intersection
		intersection.attachConsumer(A_DIRECTION, aFromIntersection.toConsumer());
		intersection.attachProducer(A_DIRECTION, aToIntersection.toProducer());
		intersection.attachConsumer(B_DIRECTION, bFromIntersection.toConsumer());
		intersection.attachProducer(B_DIRECTION, bToIntersection.toProducer());
		intersection.attachConsumer(C_DIRECTION, cFromIntersection.toConsumer());
		intersection.attachProducer(C_DIRECTION, cToIntersection.toProducer());
		intersection.attachConsumer(D_DIRECTION, dFromIntersection.toConsumer());
		intersection.attachProducer(D_DIRECTION, dToIntersection.toProducer());
		
		// Create connections, traverse times and probabilities
		double[] traverseTime = new double[]{intersectionTraverseTime};
		AbstractConsumer[] aToB = new AbstractConsumer[]{bFromIntersection.toConsumer()};
		AbstractConsumer[] aToC = new AbstractConsumer[]{cFromIntersection.toConsumer()};
		AbstractConsumer[] aToD = new AbstractConsumer[]{dFromIntersection.toConsumer()};
		intersection.createConnectionQueue(aToIntersection.toProducer(), aToB, traverseTime, new double[]{routeProbability[A_DIRECTION][B_DIRECTION]});
		intersection.createConnectionQueue(aToIntersection.toProducer(), aToC, traverseTime, new double[]{routeProbability[A_DIRECTION][C_DIRECTION]});
		intersection.createConnectionQueue(aToIntersection.toProducer(), aToD, traverseTime, new double[]{routeProbability[A_DIRECTION][D_DIRECTION]});
		AbstractConsumer[] bToA = new AbstractConsumer[]{aFromIntersection.toConsumer()};
		AbstractConsumer[] bToC = new AbstractConsumer[]{cFromIntersection.toConsumer()};
		AbstractConsumer[] bToD = new AbstractConsumer[]{dFromIntersection.toConsumer()};
		intersection.createConnectionQueue(bToIntersection.toProducer(), bToA, traverseTime, new double[]{routeProbability[B_DIRECTION][A_DIRECTION]});
		intersection.createConnectionQueue(bToIntersection.toProducer(), bToC, traverseTime, new double[]{routeProbability[B_DIRECTION][C_DIRECTION]});
		intersection.createConnectionQueue(bToIntersection.toProducer(), bToD, traverseTime, new double[]{routeProbability[B_DIRECTION][D_DIRECTION]});
		AbstractConsumer[] cToA = new AbstractConsumer[]{aFromIntersection.toConsumer()};
		AbstractConsumer[] cToB = new AbstractConsumer[]{bFromIntersection.toConsumer()};
		AbstractConsumer[] cToD = new AbstractConsumer[]{dFromIntersection.toConsumer()};
		intersection.createConnectionQueue(cToIntersection.toProducer(), cToA, traverseTime, new double[]{routeProbability[C_DIRECTION][A_DIRECTION]});
		intersection.createConnectionQueue(cToIntersection.toProducer(), cToB, traverseTime, new double[]{routeProbability[C_DIRECTION][B_DIRECTION]});
		intersection.createConnectionQueue(cToIntersection.toProducer(), cToD, traverseTime, new double[]{routeProbability[C_DIRECTION][D_DIRECTION]});
		AbstractConsumer[] dToA = new AbstractConsumer[]{aFromIntersection.toConsumer()};
		AbstractConsumer[] dToB = new AbstractConsumer[]{bFromIntersection.toConsumer()};
		AbstractConsumer[] dToC = new AbstractConsumer[]{cFromIntersection.toConsumer()};
		intersection.createConnectionQueue(dToIntersection.toProducer(), dToA, traverseTime, new double[]{routeProbability[D_DIRECTION][A_DIRECTION]});
		intersection.createConnectionQueue(dToIntersection.toProducer(), dToB, traverseTime, new double[]{routeProbability[D_DIRECTION][B_DIRECTION]});
		intersection.createConnectionQueue(dToIntersection.toProducer(), dToC, traverseTime, new double[]{routeProbability[D_DIRECTION][C_DIRECTION]});
		
		// Create sources and conntect to streets
		aSource = modelFactory.createSource(modelFactory.createContDistConstant(mainArrivalRate), aToIntersection.toConsumer());
		bSource = modelFactory.createSource(modelFactory.createContDistConstant(mainArrivalRate), bToIntersection.toConsumer());
		cSource = modelFactory.createSource(modelFactory.createContDistConstant(otherArrivalRate), cToIntersection.toConsumer());
		dSource = modelFactory.createSource(modelFactory.createContDistConstant(otherArrivalRate), dToIntersection.toConsumer());
	}

	public Iterable<Source> getSources() {
		List<Source> sources = new ArrayList<>();
		sources.add(aSource);
		sources.add(bSource);
		sources.add(cSource);
		sources.add(dSource);
		return sources;
	}
	
	public Iterable<Sink> getSinks() {
		List<Sink> sinks = new ArrayList<>();
		sinks.add(aSink);
		sinks.add(bSink);
		sinks.add(cSink);
		sinks.add(dSink);
		return sinks;
	}

	public Source getSourceA() {
		return aSource;
	}

	public Source getSourceB() {
		return bSource;
	}

	public Source getSourceC() {
		return cSource;
	}

	public Source getSourceD() {
		return dSource;
	}

	public Sink getSinkA() {
		return aSink;
	}

	public Sink getSinkB() {
		return bSink;
	}

	public Sink getSinkC() {
		return cSink;
	}

	public Sink getSinkD() {
		return dSink;
	}
}
