package at.fhv.itm14.trafsim.simopt;

import Adapter.Simulation.SimulationAdapter;
import DataTypes.ResultParameter;
import DataTypes.SettingParameter;
import at.fhv.itm14.trafsim.model.ModelFactory;
import at.fhv.itm14.trafsim.model.Scenario;
import at.fhv.itm14.trafsim.model.ScenarioFactory;
import at.fhv.itm14.trafsim.statistics.GlobalStatistic;
import at.fhv.itm14.trafsim.util.ScenarioReadException;
import desmoj.core.dist.DistributionManager;
import desmoj.core.simulator.Experiment;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import java.util.Random;
import java.util.logging.Logger;

public class SimOptSimulationAdapter extends SimulationAdapter {

	private static final Logger LOGGER = Logger.getLogger(SimOptSimulationAdapter.class.toString());

	private String scenarioFile;
	private Scenario scenario;
	private Float turnAroundTime;
	private Float[] phaseShifts;
	private Float avgStops;
	private Float avgWaitingTime;
	private Integer numberOfIntersections;
	
	public SimOptSimulationAdapter() {
		super();
		LOGGER.info("Initialize TrafSim - SimOptSimulationAdapter");
		PropertiesConfiguration prop;
		try {
			prop = new PropertiesConfiguration("trafsim.properties");
			scenarioFile = prop.getString("trafsim.scenario", "scenario.xml");
			numberOfIntersections = prop.getInt("trafsim.number_of_intersections", 1);
		} catch (ConfigurationException e) {

		}
		phaseShifts = new Float[numberOfIntersections];
	}
	
	@Override
	public void StartSimulation() {
		assignSettingParameters(this);
		LOGGER.fine("Start simulation: Scenario=" + scenarioFile +
					", turnAroundTime=" + turnAroundTime +
					", phaseShifts=" + phaseShifts.toString());

		try {
			scenario = ScenarioFactory.createSingleRowOneWayIntersection(scenarioFile, turnAroundTime, toPrimitiveFloat(phaseShifts));
			scenario.setSimulationDuration(getSimulationTime());
			GlobalStatistic gs = GlobalStatistic.getInstance(scenario.getModel());

			long start = System.currentTimeMillis();
			scenario.getModel().seed(new DistributionManager("Distributions", new Random().nextLong()));
			Experiment exp = scenario.getExperiment();
			exp.setSilent(true);
			//exp.setSeedGenerator((long)(Math.random() * 0xFFFFFFFF));
			exp.start();
			exp.finish();
			long stop = System.currentTimeMillis();
			
			avgStops = (float)gs.getAvgStops();
			avgWaitingTime = (float)gs.getAvgWaitingTime();

			extractResultParameters(this);
			//System.out.println("  Simulation done (runtime: " + (stop - start) + "ms): avgStops=" + avgStops + ", avgWaitingTime=" + avgWaitingTime);
			LOGGER.fine("Simulation done (runtime: " + (stop - start) + "ms): avgStops=" + avgStops + ", avgWaitingTime=" + avgWaitingTime);

			// Shutdown and clear up everything
			GlobalStatistic.freeInstance(scenario.getModel());
			ScenarioFactory.cleanup(scenario);
			ModelFactory.freeInstance(scenario.getModel());
		} catch (ScenarioReadException ex) {
			LOGGER.severe(ex.getMessage());
			ex.printStackTrace();
		}
	}
	
	public float[] toPrimitiveFloat(Float[] values) {
		float[] result = new float[values.length];
		for (int i = 0; i < result.length; i++) {
			result[i] = values[i];
		}
		return result;
	}

	@Override
	public Double getObjectiveValue() {
		return getMultiObjectiveValues()[0];
	}
	
	@Override
	public boolean isMultiObjectiveSimulation() {
		return true;
	}

	@Override
	public Double[] getMultiObjectiveValues() {
		Double[] result = new Double[2];
        for(ResultParameter r: this.getResultParameters()){
            if(r.getName().equals("avgStops")) {
                result[0] = Double.parseDouble(r.getValue().toString());
            } else if (r.getName().equals("avgWaitingTime")) {
            	result[1] = Double.parseDouble(r.getValue().toString());
            }
        }
        return result;
	}

	@Override
	public String getSimulationDescription() {
		return "Traffic simulation";
	}

	@Override
	public void initializeParameters() {
        try {
            addSettingParameter(new SettingParameter("turnAroundTime", Float.class, "Turnaround Time", 	100  /* def */, 10 /* min */, 200 /* max */, 1 /* step size */));
            addMultiSettingParameter(new SettingParameter("phaseShifts", Float.class, "Phase Shift",  	30	 /* def */,  0 /* min */, 200 /* max */, 1 /* step size */), numberOfIntersections /* Number of values */);

            addResultParameter(new ResultParameter("avgStops", Float.class, "Avg. Total Stops per Car"));
            addResultParameter(new ResultParameter("avgWaitingTime", Float.class, "Avg. Waiting Time per Car"));
            if (getResultParameters().size() > 2) {
            	System.out.println("too many result parameters");
			}
			// TODO: Fix me.. get value from xml
			setSimulationTime(60*60*2);
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

	public Float getTurnAroundTime() {
		return turnAroundTime;
	}

	public void setTurnAroundTime(Float turnAroundTime) {
		this.turnAroundTime = turnAroundTime;
	}
	
	public void setTurnAroundTime(Double turnAroundTime) {
		this.turnAroundTime = turnAroundTime.floatValue();
	}

	public Float getPhaseShifts(int index) {
		return phaseShifts[index];
	}
	
	public Float getPhaseShifts(Integer index) {
		return phaseShifts[index];
	}

	public void setPhaseShifts(Integer index, Float value) {
		this.phaseShifts[index] = value;
	}
	
	public void setPhaseShifts(Integer index, Double value) {
		this.phaseShifts[index] = value.floatValue();
	}

	public Float getAvgStops() {
		return avgStops;
	}

	public void setAvgStops(Float avgStops) {
		this.avgStops = avgStops;
	}

	public Float getAvgWaitingTime() {
		return avgWaitingTime;
	}

	public void setAvgWaitingTime(Float avgWaitingTime) {
		this.avgWaitingTime = avgWaitingTime;
	}

	public static void main(String[] args) {
		System.err.println("This program is not meant to be executed. It is a SimOptFramework simulation.");
		System.out.println("Testing simulation:");
		System.out.println("Simulation started...");

		try {
			for (int i = 0; i < 10; i++) {
				Scenario scenario = ScenarioFactory.createSingleRowOneWayIntersection("scenario_3x1.xml", 180.0f, new float[]{ 0f , 19f, 17f }, true); // new float[]{ 10.0f, 27.0f, 44.0f }); //
				GlobalStatistic gs = GlobalStatistic.getInstance(scenario.getModel());
				scenario.setSimulationDuration(7200);
				scenario.getModel().seed(new DistributionManager("Distributions", new Random().nextLong()));
				Experiment exp = scenario.getExperiment();
				exp.setSeedGenerator((long)(Math.random() * 0xFFFFFFFF));
				exp.setSilent(true);
				exp.start();
				exp.report();
				exp.finish();
	
				float avgStops = (float)gs.getAvgStops();
				float avgWaitingTime = (float)gs.getAvgWaitingTime();
				
				//System.out.println("Simulation done - avgStops: " + avgStops + ", avgWaitingTime: " + avgWaitingTime);
				System.out.println("" + (i + 1) + ";\t" + avgStops + ";\t" + avgWaitingTime + ";");
			}
		} catch (ScenarioReadException ex) {
			ex.printStackTrace();
		}
	}
}
