package at.fhv.itm14.trafsim.model.entities.util;

import at.fhv.itm14.trafsim.model.entities.Car;
import desmoj.core.simulator.Queue;

import java.util.ArrayList;
import java.util.HashMap;

public class CarDistributor {

	private final DistributedRandomNumberGenerator rnd;
	private final HashMap<Integer, ArrayList<Queue<Car>>> waitingLanes;
	private int carsInDistributor;

	private void init(int[][] directionProbabilities) {
		for (int i = 0; i < directionProbabilities.length; i++) {
			rnd.addNumber(directionProbabilities[i][0], directionProbabilities[i][1]);
		}
		carsInDistributor = 0;
	}

	public CarDistributor() {
		rnd = new DistributedRandomNumberGenerator();
		waitingLanes = new HashMap<Integer, ArrayList<Queue<Car>>>();
		int[][] directions = new int[0][0];
		init(directions);
	}

	public CarDistributor(int[][] directions) {
		rnd = new DistributedRandomNumberGenerator();
		waitingLanes = new HashMap<Integer, ArrayList<Queue<Car>>>();
		init(directions);
	}

	public void addDirection(int direction, double transitionProbability) {
		rnd.addNumber(direction, transitionProbability);
	}

	public int getRandom() {
		return rnd.getDistributedRandomNumber();
	}

}
