package at.fhv.itm14.trafsim.model.entities;

import at.fhv.itm14.trafsim.persistence.model.SinkDTO;
import at.fhv.itm14.trafsim.statistics.GlobalStatistic;
import desmoj.core.simulator.Model;
import desmoj.core.statistic.Count;

public class Sink extends AbstractConsumer {
	private Count carCounter;
	private SinkDTO dto;
			
	/**
	 * Constructor of the Sink entity.
	 *
	 * @param owner the model this entity belongs to
	 * @param name this port's name
	 * @param showInTrace flag to indicate if this entity shall produce output for the trace
	 */
	public Sink(Model owner, String name, boolean showInTrace) {
		super(owner, name, showInTrace);
		carCounter = new Count(owner, name + "_carcounter", true, false);
		carCounter.reset();
	}

	@Override
	public void carEnter(Car car) {
		car.leaveSystem();
		carCounter.update();
		GlobalStatistic.getInstance(getModel()).carLeavesSystem(car);
	}

	@Override
	public boolean isFull() {
		return false;
	}

	public long getCarCounter() {
		return carCounter.getObservations();
	}

	@Override
	public SinkDTO toDTO() {
		if (dto != null) {
			return dto;
		}
		dto = new SinkDTO();
		dto.setName(getName());
		return dto;
	}
}
