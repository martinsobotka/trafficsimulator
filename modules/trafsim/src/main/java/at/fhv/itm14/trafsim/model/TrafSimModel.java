package at.fhv.itm14.trafsim.model;

import at.fhv.itm14.trafsim.model.entities.Source;
import at.fhv.itm14.trafsim.model.entities.intersection.Intersection;
import at.fhv.itm14.trafsim.util.ScenarioReadException;
import desmoj.core.dist.DistributionManager;
import desmoj.core.simulator.Experiment;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.TimeInstant;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;


public class TrafSimModel extends Model {
	private List<Source> sources;
	private List<Intersection> intersections;
	
   // define model components here
	/**
	 * constructs a model
	 */
	public TrafSimModel() {
		super(null, "TrafSimModel", true, true);
		sources = new ArrayList<>();
		intersections = new ArrayList<>();
	}

	/**
	 * initialise static components
	 */
	@Override
	public void init() {
	}

	/**
	 * activate dynamic components
	 */
	@Override
	public void doInitialSchedules() {
		Consumer<Source> initSource = s -> s.start();
		sources.forEach(initSource);
		
		Consumer<Intersection> initIntersectionCtrl = i -> i.getController().start();
		intersections.forEach(initIntersectionCtrl);
	}

	/**
	 * returns a description of this model to be used in the report
	 */
	@Override
	public String description() {
		return "<Description of my model>";
	}

   // define any additional methods if necessary,
	// e.g. access methods to model components
	/**
	 * runs the model
	 */
	public static void main(String[] args) {
		/*
		// create model and experiment
		Model model = new TrafSimModel();
		Experiment exp = new Experiment("");
		// and connect them
		model.connectToExperiment(exp);
		*/
		try {
			System.out.println("read scenario");
			Scenario s = ScenarioFactory.createSingleRowOneWayIntersection("scenario_3x3.xml", 200, new float[]{1, 2, 3, 4, 5, 6, 7, 8, 9});
			Experiment exp = s.getExperiment();
			
			// set experiment parameters
			exp.setShowProgressBar(false);
			exp.setSilent(true);
			TimeInstant stopTime = new TimeInstant(1440, TimeUnit.MINUTES);
			exp.tracePeriod(new TimeInstant(0), stopTime);
			exp.stop(stopTime);

			// start experiment
			System.out.println("start simulation");
			exp.start();
			System.out.println("done... creating reports.");
			
			// generate report and shut everything off
			//exp.report();
			exp.finish();
			
		} catch (ScenarioReadException ex) {
			ex.printStackTrace();
		}

		
	}

	public void registerSource(Source s) {
		sources.add(s);
	}
	
	public void registerIntersection(Intersection i) {
		intersections.add(i);
	}
	
	public void seed(DistributionManager dm) {
		for (Source s : sources) {
//			dm.setSeed(dm.nextSeed());
			dm.register(s.getDistribution());
			dm.setSeed(dm.nextSeed());
		}
	}

	public void destroy() {
		this.sources.clear();
		this.sources = null;
		this.intersections.clear();
		this.intersections = null;
	}
}
