package at.fhv.itm14.trafsim.model.entities;

import at.fhv.itm14.trafsim.model.ModelFactory;
import at.fhv.itm14.trafsim.model.events.CarDepartureEvent;
import at.fhv.itm14.trafsim.persistence.model.OneWayStreetDTO;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.TimeSpan;

public class OneWayStreet extends AbstractProSumer {
	private final double traverseTime;
	private AbstractConsumer out;
	private int carsOnStreet;
	private final int capacity;
	private OneWayStreetDTO dto;
	
	/**
	 * Constructor of the OneWayStreet entity.
	 *
	 * @param owner the model this entity belongs to
	 * @param name this OneWayStreet's name
	 * @param showInTrace flag to indicate if this entity shall produce output for the trace
	 * @param traverseTime
	 * @param out
	 * @param capacity
	 */
	public OneWayStreet(Model owner, String name, boolean showInTrace, double traverseTime, AbstractConsumer out, int capacity) {
		super(owner, name, showInTrace);
		this.traverseTime = traverseTime;
		this.out = out;
		this.capacity = capacity;
	}

	@Override
	public void carEnter(Car car) {
		//TODO: handle if isFull()
		carsOnStreet++;
		CarDepartureEvent event = ModelFactory.getInstance(getModel()).createCarDepartureEvent();
		event.schedule(toProducer(), car, out, new TimeSpan(car.getSpeedFactor() * traverseTime));
	}

	@Override
	public boolean isFull() {
		return carsOnStreet >= capacity;
	}

	@Override
	public void carDelivered(CarDepartureEvent event, Car car, boolean successful) {
		//TODO: if not successful: enqueue car
		if(successful){
			carsOnStreet--;
		}
	}
	
	public int getCarsOnStreet() {
		return carsOnStreet;
	}
	
	@Override
	public OneWayStreetDTO toDTO() {
		if (dto != null) {
			return dto;
		}
		dto = new OneWayStreetDTO();
		dto.setName(getName());
		dto.setCapacity(capacity);
		dto.setConsumer(out.toDTO());
		dto.setTraverseTime(traverseTime);
		return dto;
	}

	public void setOut(AbstractConsumer out) {
		this.out = out;
	}
}
