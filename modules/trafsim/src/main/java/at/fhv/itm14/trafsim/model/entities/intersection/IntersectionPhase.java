package at.fhv.itm14.trafsim.model.entities.intersection;

import at.fhv.itm14.trafsim.persistence.model.DTOConvertable;
import at.fhv.itm14.trafsim.persistence.model.IntersectionPhaseDTO;

import java.util.LinkedList;
import java.util.List;

public class IntersectionPhase implements DTOConvertable<IntersectionPhaseDTO> {
	private double greenDurationPart;
	private double yellowDuration;
	private IntersectionController ctrl;
	private final List<IntersectionConnection> connections;

	public IntersectionPhase(double greenDurationPart, double yellowDuration) {
		connections = new LinkedList<>();
		this.greenDurationPart = greenDurationPart;
		this.yellowDuration = yellowDuration;
	}

	public void addConnection(IntersectionConnection c) {
		connections.add(c);
	}

	public List<IntersectionConnection> getConnections() {
		return connections;
	}

	public double getGreenDuration() {
		return ctrl.calcGreenDuration(greenDurationPart);
	}

	public double getYellowDuration() {
		return yellowDuration;
	}

	public void setCtrl(IntersectionController ctrl) {
		this.ctrl = ctrl;
	}
	
	@Override
	public IntersectionPhaseDTO toDTO() {
		IntersectionPhaseDTO dto = new IntersectionPhaseDTO();
		dto.setGreenDurationPart(greenDurationPart);
		dto.setYellowDuration(yellowDuration);
		dto.setConnections(new LinkedList<>());
		for (IntersectionConnection conn : connections) {
			IntersectionPhaseDTO.PhaseConnectionDTO connDto = new IntersectionPhaseDTO.PhaseConnectionDTO();
			connDto.inIndex = conn.getIn();
			connDto.outIndex = conn.getOut();
			dto.getConnections().add(connDto);
		}
		return dto;
	}
}
