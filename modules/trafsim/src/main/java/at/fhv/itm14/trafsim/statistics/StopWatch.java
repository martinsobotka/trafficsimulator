package at.fhv.itm14.trafsim.statistics;

import at.fhv.itm14.trafsim.util.DesmoJHelper;
import desmoj.core.simulator.Model;

public class StopWatch {
	
	private Model model;
	
	private boolean isRunning;
	private double startTime;
	
	public StopWatch(Model model) {
		this.model = model;
	}
	
	public void start() {
		if (isRunning) {
			throw new IllegalStateException("Stopwatch already running.");
		}
		isRunning = true;
		startTime = DesmoJHelper.getTimeFromModel(model);
	}
	
	public double stop() {
		if (!isRunning) {
			throw new IllegalStateException("Stopwatch was not started before.");
		}
		isRunning = false;
		double stopTime = DesmoJHelper.getTimeFromModel(model);
		return stopTime - startTime;
	}

	public boolean isRunning() {
		return isRunning;
	}

}
