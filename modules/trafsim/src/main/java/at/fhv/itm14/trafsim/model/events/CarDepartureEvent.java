package at.fhv.itm14.trafsim.model.events;

import at.fhv.itm14.trafsim.model.entities.AbstractConsumer;
import at.fhv.itm14.trafsim.model.entities.AbstractProducer;
import at.fhv.itm14.trafsim.model.entities.Car;
import desmoj.core.simulator.EventOf3Entities;
import desmoj.core.simulator.Model;

public class CarDepartureEvent extends EventOf3Entities<AbstractProducer, Car, AbstractConsumer> {

	public CarDepartureEvent(Model model, String string, boolean showInTrace) {
		super(model, string, showInTrace);
	}

	@Override
	public void eventRoutine(AbstractProducer producer, Car car, AbstractConsumer consumer) {
		if (consumer.isFull()) {
			throw new RuntimeException("Consumer queue is full: " + consumer.getName() + " " + consumer.getClass());
		}
		producer.carDelivered(this, car, true);
		consumer.carEnter(car);
	}
}
