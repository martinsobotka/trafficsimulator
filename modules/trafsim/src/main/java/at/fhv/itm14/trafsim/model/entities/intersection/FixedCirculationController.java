package at.fhv.itm14.trafsim.model.entities.intersection;

import at.fhv.itm14.trafsim.model.ModelFactory;
import at.fhv.itm14.trafsim.model.events.TrafficLightGreenEvent;
import at.fhv.itm14.trafsim.model.events.TrafficLightSwitchPhaseEvent;
import at.fhv.itm14.trafsim.persistence.model.IntersectionControllerDTO;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.TimeSpan;

import java.util.LinkedList;
import java.util.List;

public class FixedCirculationController extends IntersectionController {
	private double startOffset;
	private double turnaroundTime;
	private final boolean[][] open;
	private IntersectionPhase currentPhase;
	private int currentPhaseIndex = -1;

	public FixedCirculationController(Model owner, String name, boolean showInTrace,
			Intersection intersection, List<IntersectionPhase> phases, double turnaroundTime, double startOffset) {
		
		super(owner, name, showInTrace, intersection, phases);
		this.open = new boolean[intersection.getSize()][intersection.getSize()];
		this.turnaroundTime = turnaroundTime;
		this.startOffset = startOffset;
	}
	
	private void init() {
		// Initialize first event
		TrafficLightSwitchPhaseEvent event = ModelFactory.getInstance(getModel()).createTrafficLightSwitchEvent();
		event.schedule(this, new TimeSpan(startOffset));
	}

	@Override
	public boolean canDrive(int in, int out) {
		return open[in][out];
	}

	@Override
	public void nextPhase() {
		// Block current connections
		if (currentPhase != null) {
			setIntersectionConnections(false);
		}
	
		// Switch to yellow
		TrafficLightGreenEvent event = new TrafficLightGreenEvent(this.getModel(), this.getName(), this.traceIsOn());
		double yellowDuration = phases.get(nextPhaseIndex()).getYellowDuration();
		event.schedule(this , new TimeSpan(yellowDuration));
	}
	
	@Override
	public void switchToGreen() {
		// Switch to next phase, open connections and notify intersection
		currentPhaseIndex = nextPhaseIndex();
		currentPhase = phases.get(currentPhaseIndex);
		
		setIntersectionConnections(true);
		startDrive();
		
		// Schedule next switch event
		TrafficLightSwitchPhaseEvent event = new TrafficLightSwitchPhaseEvent(this.getModel(), this.getName(), this.traceIsOn());
		event.schedule(this, new TimeSpan(currentPhase.getGreenDuration()));
	}
	
	private void startDrive() {
		currentPhase.getConnections().forEach(
				connection -> {
					intersection.startDrive(connection.getIn(), connection.getOut());
				}
			);
	}
	
	private void setIntersectionConnections(boolean value) {
		currentPhase.getConnections().forEach(
				connection -> { 
					open[connection.getIn()][connection.getOut()] = value;
					if (value)
						intersection.switchToGreen(connection.getIn(), connection.getOut());
					else
						intersection.switchToRed(connection.getIn(), connection.getOut());
				}
			);
	}
	
	private int nextPhaseIndex() {
		return (currentPhaseIndex + 1) % phases.size();
	}

	public double getStartOffset() {
		return startOffset;
	}
	
	public void setStartOffset(double startOffset) {
		this.startOffset = startOffset;
	}

	public double getTurnaroundTime() {
		return turnaroundTime;
	}

	public void setTurnaroundTime(double turnaroundTime) {
		this.turnaroundTime = turnaroundTime;
	}
	
	@Override
	public double calcGreenDuration(double greenDurationPart) {
		double duration = turnaroundTime;
		for (IntersectionPhase phase : phases) {
			duration -= phase.getYellowDuration();
		}
		return Math.max(0, duration) * greenDurationPart;
	}
	
	@Override
	public IntersectionControllerDTO toDTO() {
		IntersectionControllerDTO dto = new IntersectionControllerDTO();
		dto.setName(getName());
		dto.setTurnaroundTime(turnaroundTime);
		dto.setStartOffset(startOffset);
		dto.setPhases(new LinkedList<>());
		for (IntersectionPhase phase : phases) {
			dto.getPhases().add(phase.toDTO());
		}
		return dto;
	}
	
	@Override
	public void start() {
		init();
	}
}
