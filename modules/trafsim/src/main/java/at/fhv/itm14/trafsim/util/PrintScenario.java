/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.itm14.trafsim.util;

import at.fhv.itm14.trafsim.persistence.model.ScenarioDTO;

/**
 *
 * @author soma
 */
public class PrintScenario {
	
	public static void main(String[] args) throws ScenarioReadException {
		ScenarioDTO scenario = XMLParser.loadScenario("scenario_3x3.xml");
		System.out.println(scenario.toString());
	}
	
}
