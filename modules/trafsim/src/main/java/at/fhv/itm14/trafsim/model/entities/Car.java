package at.fhv.itm14.trafsim.model.entities;

import at.fhv.itm14.trafsim.statistics.StopWatch;
import desmoj.core.simulator.Entity;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.TimeSpan;
import desmoj.core.statistic.Count;
import desmoj.core.statistic.Tally;

public class Car extends Entity {
	
	private int nextDirection;
	
	/**
	 * Statistical Data
	 */
	private final StopWatch systemStopWatch;
	private final Count stopCounter;
	private final Count intersectionCounter;
	private final StopWatch waitStopWatch;
	private final StopWatch intersectionStopWatch;
	private final Tally waitTime;
	private final Tally intersectionTime;

	private double systemTime;
	
	/**
	 * Constructor of the Car entity.
	 *
	 * @param owner the model this entity belongs to
	 * @param name this car's name
	 * @param showInTrace flag to indicate if this entity shall produce output for the trace
	 */
	public Car(Model owner, String name, boolean showInTrace) {
		super(owner, name, showInTrace);
		stopCounter = new Count(owner, name + "_stopcounter", false, false);
		stopCounter.reset();
		intersectionCounter = new Count(owner, name + "_intersectioncounter", false, false);
		intersectionCounter.reset();
		waitTime = new Tally(owner, name + "_waittime", false, false);
		waitTime.reset();
		intersectionTime = new Tally(owner, name + "_intersectiontime", false, false);
		intersectionTime.reset();
		waitStopWatch = new StopWatch(owner);
		intersectionStopWatch = new StopWatch(owner);
		systemStopWatch = new StopWatch(owner);
	}

	public double getSpeedFactor() {
		return 1;
	}
	
	@Override
	public String toString() {
		return getName();
	}

	public int getNextDirection() {
		return nextDirection;
	}

	public void setNextDirection(int nextDirection) {
		this.nextDirection = nextDirection;
	}

	public void enterSystem() {
		this.systemStopWatch.start();
	}
	
	public double leaveSystem() {
		systemTime = systemStopWatch.stop();
		return systemTime;
	}
	
	public void enterIntersection() {
		this.intersectionCounter.update();
		this.intersectionStopWatch.start();
	}
	
	public void leaveIntersection() {
		double res = this.intersectionStopWatch.stop();
		this.intersectionTime.update(new TimeSpan(res));
	}
	
	public void startWaiting() {
		waitStopWatch.start();
		this.stopCounter.update();
	}
	
	public void stopWaiting() {
		double res = waitStopWatch.stop();
		waitTime.update(new TimeSpan(res));
	}
	
	public boolean isWaiting() {
		return waitStopWatch.isRunning();
	}
	
	public double getTimeSpentInSystem() {
		if (systemStopWatch.isRunning()) {
			throw new IllegalStateException("Car is still in system");
		}
		return systemTime;
	}

	public double getMeanWaitingTime() {
		if (waitTime.getObservations() <= 0) return 0;
		return waitTime.getMean();
	}

	public long getStopCount() {
		return stopCounter.getValue();
	}
	
	public long getIntersectionPassedCount() {
		return intersectionCounter.getValue();
	}

	public boolean isWaitingInIntersection() {
		return waitStopWatch.isRunning();
	}

	public double getMeanIntersectionPassTime() {
		if (intersectionTime.getObservations() <= 0) return 0;
		return intersectionTime.getMean();
	}
}
