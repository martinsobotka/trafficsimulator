package at.fhv.itm14.trafsim.model.entities;

import at.fhv.itm14.trafsim.model.events.CarDepartureEvent;
import at.fhv.itm14.trafsim.persistence.model.DTO;
import at.fhv.itm14.trafsim.persistence.model.DTOConvertable;
import desmoj.core.simulator.Entity;
import desmoj.core.simulator.Model;

public abstract class AbstractMultiProSumer extends Entity implements DTOConvertable<DTO> {

	protected class MultiProSumerProducer extends AbstractProducer {

		private AbstractMultiProSumer parent;
		private int index;
		
		public MultiProSumerProducer(Model owner, String name, boolean showInTrace, AbstractMultiProSumer parent, int index) {
			super(owner, name, showInTrace);
			this.parent = parent;
			this.index = index;
		}

		@Override
		public void carDelivered(CarDepartureEvent event, Car car, boolean successful) {
			parent.carDelivered(event, car, index, successful);
		}

		@Override
		public DTO toDTO() {
			return parent.toDTO();
		}
	}
	
	protected class MultiProSumerConsumer extends AbstractConsumer {

		private AbstractMultiProSumer parent;
		private int index;
		
		public MultiProSumerConsumer(Model owner, String name, boolean showInTrace, AbstractMultiProSumer parent, int index) {
			super(owner, name, showInTrace);
			this.parent = parent;
			this.index = index;
		}

		@Override
		public void carEnter(Car car) {
			parent.carEnter(car, index);
		}

		@Override
		public boolean isFull() {
			return parent.isFull();
		}	

		@Override
		public DTO toDTO() {
			return parent.toDTO();
		}
	}
	
	private MultiProSumerConsumer[] consumers;
	private MultiProSumerProducer[] producers;
	
	private void init(Model owner, String name, boolean showInTrace, int noProducers, int noConsumers){
		consumers = new MultiProSumerConsumer[noConsumers];
		producers = new MultiProSumerProducer[noProducers];
		
		for(int i = 0; i < consumers.length; i++){
			consumers[i] = new MultiProSumerConsumer(owner, name, showInTrace, this, i);
		}
		for(int i = 0; i < producers.length; i++){
			producers[i] = new MultiProSumerProducer(owner, name, showInTrace, this, i);
		}
	}
	
	public AbstractMultiProSumer(Model owner, String name, boolean showInTrace, int noProducers, int noConsumers) {
		super(owner, name, showInTrace);
		init(owner, name, showInTrace, noProducers, noConsumers);
	}
	
	public AbstractMultiProSumer(Model owner, String name, boolean showInTrace, int noOfElements) {
		super(owner, name, showInTrace);
		init(owner, name, showInTrace, noOfElements, noOfElements);
	}
	
	public AbstractConsumer toConsumer(int index){
		if(index < 0 || index >= consumers.length){
			// TODO: appropriate exception
			throw new ArrayIndexOutOfBoundsException();
		}
		return consumers[index];
	}
	
	public AbstractProducer toProducer(int index){
		if(index < 0 || index >= producers.length){
			// TODO: appropriate exception
			throw new ArrayIndexOutOfBoundsException();
		}
		return producers[index];
	}
	
	public abstract void carEnter(Car car, int inIndex);
	
	public abstract boolean isFull();
	
	public abstract void carDelivered(CarDepartureEvent event, Car car, int outIndex, boolean successful);

}
