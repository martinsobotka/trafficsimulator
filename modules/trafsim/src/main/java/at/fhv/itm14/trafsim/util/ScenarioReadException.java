package at.fhv.itm14.trafsim.util;

public class ScenarioReadException extends Exception {

	public ScenarioReadException(String message) {
		super(message);
	}

	public ScenarioReadException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
