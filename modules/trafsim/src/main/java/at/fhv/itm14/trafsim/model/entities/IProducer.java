package at.fhv.itm14.trafsim.model.entities;

import at.fhv.itm14.trafsim.model.events.CarDepartureEvent;

public interface IProducer {

	public void carDelivered(CarDepartureEvent event, Car car, boolean successful);
}
