package at.fhv.itm14.trafsim.model.events;

import at.fhv.itm14.trafsim.model.entities.intersection.IntersectionController;
import desmoj.core.simulator.Event;
import desmoj.core.simulator.Model;

public class TrafficLightGreenEvent extends Event<IntersectionController> {

	public TrafficLightGreenEvent(Model model, String name, boolean showInTrace) {
		super(model, name, showInTrace);
	}

	@Override
	public void eventRoutine(IntersectionController controller) {
		controller.switchToGreen();
	}
}
