package at.fhv.itm14.trafsim.model.entities.intersection;

import at.fhv.itm14.trafsim.persistence.model.DTOConvertable;
import at.fhv.itm14.trafsim.persistence.model.IntersectionControllerDTO;
import desmoj.core.simulator.Entity;
import desmoj.core.simulator.Model;

import java.util.List;

public abstract class IntersectionController extends Entity implements DTOConvertable<IntersectionControllerDTO> {
	protected final Intersection intersection;
	protected final List<IntersectionPhase> phases;
	
	public IntersectionController(Model owner, String name, boolean showInTrace,
			Intersection intersection, List<IntersectionPhase> phases) {
		super(owner, name, showInTrace);
		this.intersection = intersection;
		this.phases = phases;
		// Connect Phases to Controller
		phases.forEach((phase) -> {
			phase.setCtrl(this);
		});
	}

	
	/**
	 * Initialize Controller
	 */
	public abstract void start();
	
	/**
	 * Returns true if a car can drive from 'in' index to 'out' index
	 * 
	 * @param in - in-index
	 * @param out - out-index
	 * @return true or false
	 */
	public abstract boolean canDrive(int in, int out);

	
	public abstract double calcGreenDuration(double greenDurationPart);

	/**
	 * Switch / prepare for next phase.
	 * Clear all traffic.
	 */
	public abstract void nextPhase();

	/**
	 * Switch current active Phase to green.
	 */
	public abstract void switchToGreen();
}
