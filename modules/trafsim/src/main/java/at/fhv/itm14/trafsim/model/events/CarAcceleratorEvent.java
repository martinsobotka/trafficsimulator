package at.fhv.itm14.trafsim.model.events;

import at.fhv.itm14.trafsim.model.entities.Car;
import at.fhv.itm14.trafsim.model.entities.intersection.Intersection;
import desmoj.core.simulator.EventOf2Entities;
import desmoj.core.simulator.Model;

public class CarAcceleratorEvent extends EventOf2Entities<Car, Intersection> {
	private final int inDirection;
	private final int outDirection;
	
	public CarAcceleratorEvent(Model model, String string, boolean showInTrace, int inDirection, int outDirection) {
		super(model, string, showInTrace);
		this.inDirection = inDirection;
		this.outDirection = outDirection;
	}

	@Override
	public void eventRoutine(Car car, Intersection intersection) {
		// accelerate done
		intersection.drive(car, inDirection);
		
		// next car: startDrive
		intersection.startDrive(inDirection, outDirection);
	}
}