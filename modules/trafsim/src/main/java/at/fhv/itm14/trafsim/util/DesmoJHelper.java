package at.fhv.itm14.trafsim.util;

import desmoj.core.simulator.Model;

public class DesmoJHelper {

	public static double getTimeFromModel(Model model) {
		return model.getExperiment().getSimClock().getTime().getTimeAsDouble();
	}
}
