package at.fhv.itm14.trafsim.util;

public class ScenarioSaveException extends Exception {

	public ScenarioSaveException() {
	}

	public ScenarioSaveException(String message) {
		super(message);
	}

	public ScenarioSaveException(String message, Throwable cause) {
		super(message, cause);
	}

	public ScenarioSaveException(Throwable cause) {
		super(cause);
	}

}
