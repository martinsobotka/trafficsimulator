package at.fhv.itm14.trafsim.model.entities;

import at.fhv.itm14.trafsim.model.ModelFactory;
import at.fhv.itm14.trafsim.model.events.CarDepartureEvent;
import at.fhv.itm14.trafsim.persistence.model.NumericalDistDTO;
import at.fhv.itm14.trafsim.persistence.model.SourceDTO;
import at.fhv.itm14.trafsim.statistics.GlobalStatistic;
import desmoj.core.dist.NumericalDist;
import desmoj.core.simulator.Model;
import desmoj.core.statistic.Count;

public class Source extends AbstractProducer {

	private NumericalDist<Double> dist;
	private Double[] distValues;
	private AbstractConsumer out;
	private boolean active;
	private Count carCounter;
	private SourceDTO dto;

	/**
	 * Constructor of the Port entity.
	 *
	 * @param owner
	 *            the model this entity belongs to
	 * @param name
	 *            this port's name
	 * @param showInTrace
	 *            flag to indicate if this entity shall produce output for the
	 *            trace
	 * @param dist
	 *            distribution
	 * @param out
	 */
	public Source(Model owner, String name, boolean showInTrace, NumericalDist<Double> dist, AbstractConsumer out) {
		super(owner, name, showInTrace);
		this.dist = dist;
		this.out = out;
		carCounter = new Count(owner, name + "_carcounter", true, showInTrace);
		carCounter.reset();
	}

	public void start() {
		active = true;
		CarDepartureEvent event = ModelFactory.getInstance(getModel()).createCarDepartureEvent();
		event.schedule(this, ModelFactory.getInstance(getModel()).createCar(), out, dist);
	}

	public void stop() {
		active = false;
	}

	@Override
	public void carDelivered(CarDepartureEvent event, Car car, boolean successful) {
		if (successful) {
			carCounter.update();
			GlobalStatistic.getInstance(getModel()).carEntersSystem(car);
			car.enterSystem();
		}
		if (active) {
			event.schedule(this, ModelFactory.getInstance(getModel()).createCar(), out, dist);
		}
	}

	public long getCarCounter() {
		return carCounter.getValue();
	}

	public Double[] getDistValues() {
		return distValues;
	}

	public void setDistValues(Double[] distValues) {
		this.distValues = distValues;
	}

	@Override
	public SourceDTO toDTO() {
		if (dto != null) {
			return dto;
		}
		dto = new SourceDTO();
		NumericalDistDTO distDTO = new NumericalDistDTO();
		distDTO.setName(dist.getName());
		distDTO.setType(dist.getClass().getName());
		distDTO.setValues(this.distValues);
		dto.setDist(distDTO);
		dto.setName(getName());
		dto.setConsumer(out.toDTO());
		return dto;
	}

	public void setConsumer(AbstractConsumer out) {
		this.out = out;
	}
	
	public NumericalDist<Double> getDistribution() {
		return this.dist;
	}
}
