package at.fhv.itm14.trafsim.model.entities;

import at.fhv.itm14.trafsim.model.events.CarDepartureEvent;
import at.fhv.itm14.trafsim.persistence.model.DTO;
import at.fhv.itm14.trafsim.persistence.model.DTOConvertable;
import desmoj.core.simulator.Entity;
import desmoj.core.simulator.Model;

import java.util.Objects;

public abstract class AbstractProSumer extends Entity implements IProducer, IConsumer, DTOConvertable<DTO> {

	protected class ProSumerProducer extends AbstractProducer {

		private AbstractProSumer parent;
		
		public ProSumerProducer(Model owner, String name, boolean showInTrace, AbstractProSumer parent) {
			super(owner, name, showInTrace);
			this.parent = parent;
		}

		@Override
		public void carDelivered(CarDepartureEvent event, Car car, boolean successful) {
			parent.carDelivered(event, car, successful);
		}

		@Override
		public DTO toDTO() {
			return parent.toDTO();
		}

		@Override
		public int hashCode() {
			int hash = 7;
			hash = 41 * hash + Objects.hashCode(this.parent);
			return hash;
		}
	}
	
	protected class ProSumerConsumer extends AbstractConsumer {

		private AbstractProSumer parent;
		
		public ProSumerConsumer(Model owner, String name, boolean showInTrace, AbstractProSumer parent) {
			super(owner, name, showInTrace);
			this.parent = parent;
		}

		@Override
		public void carEnter(Car car) {
			parent.carEnter(car);
		}

		@Override
		public boolean isFull() {
			return parent.isFull();
		}	

		@Override
		public DTO toDTO() {
			return parent.toDTO();
		}
	}
	
	
	private final ProSumerConsumer internConsumer;
	private final ProSumerProducer internProducer;
	
	public AbstractProSumer(Model owner, String name, boolean showInTrace) {
		super(owner, name, showInTrace);
		internConsumer = new ProSumerConsumer(owner, name, showInTrace, this);
		internProducer = new ProSumerProducer(owner, name, showInTrace, this);
	}
	
	public AbstractConsumer toConsumer() {
		return internConsumer;
	}

	public AbstractProducer toProducer() {
		return internProducer;
	}
}
