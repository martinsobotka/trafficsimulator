package at.fhv.itm14.trafsim.statistics;

import at.fhv.itm14.trafsim.model.entities.Car;
import at.fhv.itm14.trafsim.model.entities.intersection.Intersection;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.TimeSpan;
import desmoj.core.statistic.Count;
import desmoj.core.statistic.Tally;

import java.util.HashMap;

public class GlobalStatistic {
	private Model model;

	private Count carEnterCounter;
	private Count carLeaveCounter;
	private Count carInSystemCounter;
	private Count carStopCounter;
	private Count carIntersectionPassedCounter;
	private Tally carInSystemTimes;
	private Tally carWaitTimes;
	private Tally carIntersectionPassTimes;
	private HashMap<Intersection, Tally> greenTimeThroughput;

	private static HashMap<Model, GlobalStatistic> instances = new HashMap<>();
	
	private static Object lock = new Object();
	
	private GlobalStatistic(Model m) {
		model = m;
		carEnterCounter = new Count(m, "Cars Enter Count", true, false);
		carLeaveCounter = new Count(m, "Cars Leave Count", true, false);
		carInSystemCounter = new Count(m, "Cars Still in System", true, false);
		carStopCounter = new Count(m, "Overall Car Stops", true, false);
		carIntersectionPassedCounter = new Count(m, "Overall Car Intersection Passes", true, false);
		carInSystemTimes = new Tally(m, "Cars in System Time", true, false);
		carWaitTimes = new Tally(m, "Cars Mean Waiting Times", true, false);
		carIntersectionPassTimes = new Tally(m, "Cars Mean Intersectoin Pass Times", true, false);
		greenTimeThroughput = new HashMap<>();
		resetStatistic();
	}
	
	public void resetStatistic() {
		carEnterCounter.reset();
		carLeaveCounter.reset();
		carInSystemCounter.reset();
		carStopCounter.reset();
		carInSystemTimes.reset();
		carIntersectionPassedCounter.reset();
		carWaitTimes.reset();
		carIntersectionPassTimes.reset();
		greenTimeThroughput.clear();
	}
	
	public static GlobalStatistic getInstance(Model m) {
		synchronized (lock) {
			if (instances.get(m) == null) {
				instances.put(m, new GlobalStatistic(m));
			}
			return instances.get(m);
		}
	}
	
	public static void freeInstance(Model m) {
		GlobalStatistic gs = instances.get(m);
		synchronized (lock) {
			if (gs == null) {
				return;
			}
			instances.remove(m);
			gs.resetStatistic();
			gs.destroy();
			gs = null;
		}
	}

	public void carEntersSystem(Car c) {
		carEnterCounter.update();
		carInSystemCounter.update();
	}
	
	public void carLeavesSystem(Car c) {
		if (c.isWaitingInIntersection()) {
			throw new IllegalStateException("Car is leaving system but still waiting in intersection?!");
		}
		carLeaveCounter.update();
		carInSystemCounter.update(-1);
		carInSystemTimes.update(new TimeSpan(c.getTimeSpentInSystem()));
		carWaitTimes.update(c.getMeanWaitingTime());
		carStopCounter.update(c.getStopCount());
		carIntersectionPassedCounter.update(c.getIntersectionPassedCount());
		carIntersectionPassTimes.update(c.getMeanIntersectionPassTime());
	}

	public void registerIntersection(Intersection intersection) {
		Tally co = new Tally(model, intersection.getName() + " Green Time Throughput", true, false);
		co.reset();
		greenTimeThroughput.put(intersection, co);
	}

	public void addGreenTimeThroughPutForIntersection(
			Intersection intersection, long value) {
		if (greenTimeThroughput.get(intersection) == null) {
			throw new IllegalArgumentException("The given intersection is not registered!");
		}
		greenTimeThroughput.get(intersection).update(value);
	}

	public double getAvgStops() {
		return (double) (carStopCounter.getValue() / (float)(carLeaveCounter.getValue()));
	}

	public double getAvgWaitingTime() {
		return carWaitTimes.getMean();
	}
	
	public void destroy() {
		resetStatistic();
		carEnterCounter = null;
		carInSystemCounter = null;
		carInSystemTimes = null;
		carIntersectionPassTimes = null;
		carIntersectionPassedCounter = null;
		carLeaveCounter = null;
		carStopCounter = null;
		carWaitTimes = null;
		greenTimeThroughput = null;
		model = null;
	}
}
