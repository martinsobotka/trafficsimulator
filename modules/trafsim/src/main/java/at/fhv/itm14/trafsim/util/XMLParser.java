package at.fhv.itm14.trafsim.util;

import at.fhv.itm14.trafsim.model.Scenario;
import at.fhv.itm14.trafsim.persistence.model.ScenarioDTO;

import javax.xml.bind.JAXB;
import java.io.File;
import java.util.HashMap;

public class XMLParser {
	
	private static final HashMap<String, ScenarioDTO> cache = new HashMap<>();
	
	private XMLParser() {}
	
	
	public static ScenarioDTO loadScenario(String fileName) throws ScenarioReadException {
		if (cache.containsKey(fileName)) {
			return cache.get(fileName);
		}
		
		File scenarioFile = new File(fileName);
		if (!scenarioFile.exists()) {
			throw new ScenarioReadException(fileName + " not exists.");
		}
		
		ScenarioDTO scenarioDTO = JAXB.unmarshal(scenarioFile, ScenarioDTO.class);
		cache.put(fileName, scenarioDTO);
		
		return scenarioDTO;
	}
	
	
	public static void saveScenario(Scenario scenario, String fileName) throws ScenarioSaveException {
		File scenarioFile = new File(fileName);
		try {
			JAXB.marshal(scenario.toDTO(), scenarioFile);		
		} catch (Exception e) {
			throw new ScenarioSaveException(e);
		}
	}
}
