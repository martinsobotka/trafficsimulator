package at.fhv.itm14.trafsim.model.entities;

import at.fhv.itm14.trafsim.persistence.model.DTO;
import at.fhv.itm14.trafsim.persistence.model.DTOConvertable;
import desmoj.core.simulator.Entity;
import desmoj.core.simulator.Model;

public abstract class AbstractProducer extends Entity implements IProducer, DTOConvertable<DTO> {

	public AbstractProducer(Model model, String string, boolean bln) {
		super(model, string, bln);
	}
}
