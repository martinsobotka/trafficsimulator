package at.fhv.itm14.trafsim.model.entities.util;

public class DirectionTraverseTime {
	
	public final int direction;
	public final double traverseTime;
	
	public DirectionTraverseTime(int direction, double traverseTime){
		this.direction = direction;
		this.traverseTime = traverseTime;
	}
}
