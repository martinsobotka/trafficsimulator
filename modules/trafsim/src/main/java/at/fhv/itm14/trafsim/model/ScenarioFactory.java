package at.fhv.itm14.trafsim.model;

import at.fhv.itm14.trafsim.model.entities.intersection.FixedCirculationController;
import at.fhv.itm14.trafsim.model.entities.intersection.Intersection;
import at.fhv.itm14.trafsim.persistence.model.*;
import at.fhv.itm14.trafsim.util.ScenarioReadException;
import at.fhv.itm14.trafsim.util.XMLParser;

public class ScenarioFactory {	

	/**
	 * Private constructor -> Use static methods
	 */
	private ScenarioFactory() {
	}
	
	public static Scenario createSingleRowOneWayIntersection(
			String scenarioFile, float turnaroundTime, float[] phaseShiftTimes) throws ScenarioReadException {
		return createSingleRowOneWayIntersection(scenarioFile, turnaroundTime, phaseShiftTimes, false);
	}

	public static Scenario createSingleRowOneWayIntersection(
			String scenarioFile, float turnaroundTime, float[] phaseShiftTimes, boolean report) throws ScenarioReadException {
		
		ScenarioDTO scenarioDTO = XMLParser.loadScenario(scenarioFile);
		Scenario scenario = createScenarioFromDTO(scenarioDTO, report);
		
		if (phaseShiftTimes.length != scenario.getIntersections().size()) {
			throw new RuntimeException("Number of intersection from " + scenarioFile + " does not match length of phaseShiftTimes[]");
		}
		
		int i = 0;
		for (Intersection intersection : scenario.getIntersections()) {
			if (intersection.getController() instanceof FixedCirculationController) {
				FixedCirculationController ctrl = FixedCirculationController.class.cast(intersection.getController());
				ctrl.setStartOffset(phaseShiftTimes[i++]);
				ctrl.setTurnaroundTime(turnaroundTime);
			}
		}
		
		return scenario;
	}
	
	
	public static void cleanup(Scenario scenario) {
		ModelFactory mf = ModelFactory.getInstance(scenario.getModel());
		mf.destroy();
		scenario.destroy();
	}
	
	
	public static Scenario createScenarioFromDTO(ScenarioDTO scenarioDTO) {
		return createScenarioFromDTO(scenarioDTO, false);
	}

	
	public static Scenario createScenarioFromDTO(ScenarioDTO scenarioDTO, boolean report) {
		Scenario scenario = new Scenario(scenarioDTO.getName(), report);
		ModelFactory factory = ModelFactory.getInstance(scenario.getModel());
		
		scenario.setName(scenarioDTO.getName());
		
		for (SinkDTO sinkDTO : scenarioDTO.getSinks()) {
			scenario.addSink(factory.createSinkFromDTO(sinkDTO));
		}
		for (IntersectionDTO intersectionDTO : scenarioDTO.getIntersections()) {
			scenario.addIntersection(factory.createIntersectionFromDTO(intersectionDTO));
		}
		for (SourceDTO sourceDTO : scenarioDTO.getSources()) {
			scenario.addSource(factory.createSourceFromDTO(sourceDTO));
		}
		for (OneWayStreetDTO oneWayStreetDTO : scenarioDTO.getStreets()) {
			scenario.addStreet(factory.createOneWayStreetFromDTO(oneWayStreetDTO));
		}
		
		return scenario;
	}
}
