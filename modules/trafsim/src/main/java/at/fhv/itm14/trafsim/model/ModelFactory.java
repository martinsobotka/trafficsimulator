package at.fhv.itm14.trafsim.model;

import at.fhv.itm14.trafsim.model.entities.*;
import at.fhv.itm14.trafsim.model.entities.intersection.*;
import at.fhv.itm14.trafsim.model.events.CarAcceleratorEvent;
import at.fhv.itm14.trafsim.model.events.CarDepartureEvent;
import at.fhv.itm14.trafsim.model.events.TrafficLightSwitchPhaseEvent;
import at.fhv.itm14.trafsim.persistence.model.*;
import desmoj.core.dist.*;
import desmoj.core.simulator.Entity;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.Queue;

import java.util.*;

public class ModelFactory {

	private static final HashMap<Model, ModelFactory> instances;
	
	private Model model;
	private HashMap<SourceDTO, Source> sourceDTOCache;
	private HashMap<SinkDTO, Sink> sinkDTOCache;
	private HashMap<OneWayStreetDTO, OneWayStreet> streetDTOCache;
	private HashMap<IntersectionDTO, Intersection> intersectionDTOCache;
	private HashMap<IntersectionControllerDTO, IntersectionController> intersectionControllerDTOCache;
	private static int uid;
	private static Object lock = new Object();

	static {
		instances = new HashMap<>();
	}

	private ModelFactory(Model model) {
		this.model = model;
		sourceDTOCache = new HashMap<>();
		sinkDTOCache = new HashMap<>();
		streetDTOCache = new HashMap<>();
		intersectionDTOCache = new HashMap<>();
		intersectionControllerDTOCache = new HashMap<>();
	}

	public static ModelFactory getInstance(Model model) {
		synchronized (lock) {
			if (!instances.containsKey(model)) {
				instances.put(model, new ModelFactory(model));
			}
			return instances.get(model);
		}
	}

	public static void freeInstance(Model m) {
		ModelFactory gs = instances.get(m);
		synchronized (lock) {
			if (gs == null) {
				return;
			}
			instances.remove(m);
			gs = null;
		}
	}
	
	public void destroy() {
		model.reset();
		sourceDTOCache.clear();
		sourceDTOCache = null;
		sinkDTOCache.clear();
		sinkDTOCache = null;
		streetDTOCache.clear();
		streetDTOCache = null;
		intersectionDTOCache.clear();
		intersectionDTOCache = null;
		intersectionControllerDTOCache.clear();
		intersectionControllerDTOCache = null;
	}
			
	
	
	public ContDistUniform createContDistUniform(double minTime, double maxTime) {
		return createContDistUniform(generateName(ContDistUniform.class), minTime, maxTime);
	}
	
	public ContDistUniform createContDistUniform(String name, double minTime, double maxTime) {
		return new ContDistUniform(model, name, minTime, maxTime, false, false);
	}
	
	
	
	public ContDistExponential createContDistExponential(double mean) {
		return new ContDistExponential(model, generateName(ContDistExponential.class), mean, false, false);
	}
	
	public ContDistExponential createContDistExponential(String name, double mean) {
		return new ContDistExponential(model, name, mean, false, false);
	}

	
	
	public ContDist createContDistConstant(double constValue) {
		return createContDistConstant(generateName(ContDistUniform.class), constValue);
	}
	
	public ContDist createContDistConstant(String name, double constValue) {
		return new ContDistConstant(model, name, constValue, false, false);
	}

	
	
	public Source createSource(NumericalDist<Double> dist, AbstractConsumer out) {
		return createSource(generateName(Source.class), dist, out);
	}
	
	public Source createSource(String name, NumericalDist<Double> dist, AbstractConsumer out) {
		Source s = new Source(model, name, false, dist, out);
		((TrafSimModel) model).registerSource(s);
		return s;
	}
	
	public Source createSourceFromDTO(SourceDTO sourceDTO) {
		if (sourceDTOCache.containsKey(sourceDTO)) {
			return sourceDTOCache.get(sourceDTO);
		}
		
		NumericalDistDTO distDTO = sourceDTO.getDist();
		NumericalDist<Double> dist;
		switch (distDTO.getType()) {
			case "desmoj.core.dist.ContDistConstant":
				dist = createContDistConstant(distDTO.getName(), distDTO.getValues()[0]);
				break;
			case "desmoj.core.dist.ContDistUniform":
				dist = createContDistUniform(distDTO.getName(), distDTO.getValues()[0], distDTO.getValues()[1]);
				break;
			case "desmoj.core.dist.ContDistExponential":
				dist = createContDistExponential(distDTO.getName(), distDTO.getValues()[0]);
				break;
			default:
				throw new RuntimeException("Distribution " + distDTO +
						" not known. Choose desmoj.core.dist.ContDistConstant, desmoj.core.dist.ContDistUniform or desmoj.core.dist.ContDistExponential");
		}
		
		AbstractConsumer out = null;
		Source source = createSource(sourceDTO.getName(), dist, out);
		source.setDistValues(distDTO.getValues());
		sourceDTOCache.put(sourceDTO, source);
		
		DTO outDTO = sourceDTO.getConsumer();
		out = toAbstractConsumer(outDTO, sourceDTO);
		source.setConsumer(out);
		
		return source;
	}

	
	
	public Sink createSink() {
		return createSink(generateName(Sink.class));
	}

	public Sink createSink(String name) {
		return new Sink(model, name, false);
	}
	
	public Sink createSinkFromDTO(SinkDTO sinkDTO) {
		if (sinkDTOCache.containsKey(sinkDTO)) {
			return sinkDTOCache.get(sinkDTO);
		}
		
		Sink sink = createSink(sinkDTO.getName());
		sinkDTOCache.put(sinkDTO, sink);
		return sink;
	}
	
	
	
	public OneWayStreet createOneWayStreet(double traverseTime, AbstractConsumer out) {
		return createOneWayStreet(generateName(OneWayStreet.class), traverseTime, out);
	}

	public OneWayStreet createOneWayStreet(String name, double traverseTime, AbstractConsumer out) {
		return new OneWayStreet(model, name, false, traverseTime, out, Integer.MAX_VALUE);
	}
	
	public OneWayStreet createOneWayStreetFromDTO(OneWayStreetDTO oneWayStreetDTO) {
		if (streetDTOCache.containsKey(oneWayStreetDTO)) {
			return streetDTOCache.get(oneWayStreetDTO);
		}
		
		AbstractConsumer out = null;
		OneWayStreet street = createOneWayStreet(oneWayStreetDTO.getName(), oneWayStreetDTO.getTraverseTime(), out);
		streetDTOCache.put(oneWayStreetDTO, street);
		
		DTO outDTO = oneWayStreetDTO.getConsumer();
		if (outDTO == null) {
			System.out.println("Street: " + oneWayStreetDTO.getId() + " " + oneWayStreetDTO.getName() + " --> out is null");
		} else {
			out = toAbstractConsumer(outDTO, oneWayStreetDTO);
			street.setOut(out);
		}
		return street;
	}
	
	

	public CarDepartureEvent createCarDepartureEvent() {
		//return new CarDepartureEvent(model, generateName(CarDepartureEvent.class), false);
		return new CarDepartureEvent(model, null, false);
	}

	
	
	public Car createCar() {
		//return new Car(model, generateName(Car.class), false);
		return new Car(model, null, false);
	}

	
	
	public Intersection create2Intersection() {
		Intersection intersection = new Intersection(model, generateName(Intersection.class), false, 2);
		((TrafSimModel) model).registerIntersection(intersection);
		return intersection;
	}

	public Intersection createFull4Intersection(double turnaroundTime, double startOffset, double yellowTime) {
		Intersection intersection = new Intersection(model, generateName(Intersection.class), false, 4);
		IntersectionController ic = createStdFull4Controller(intersection, turnaroundTime, startOffset, yellowTime);
		intersection.attachController(ic);
		((TrafSimModel) model).registerIntersection(intersection);
		return intersection;
	}
	
	public Intersection createIntersectionFromDTO(IntersectionDTO intersectionDTO) {
		if (intersectionDTOCache.containsKey(intersectionDTO)) {
			return intersectionDTOCache.get(intersectionDTO);
		}
		Intersection intersection = new Intersection(model, intersectionDTO.getName(), false, intersectionDTO.getSize());
		intersectionDTOCache.put(intersectionDTO, intersection);
		
		intersection.setServiceDelay(intersectionDTO.getServiceDelay());
		for (ElementDTO in : intersectionDTO.getIn()) {
			if (in.getRef() != null) {
				AbstractProducer producer = toAbstractProducer(in.getRef(), intersectionDTO);
				intersection.attachProducer(in.getIndex(), producer);
			}
		}
		for (ElementDTO out : intersectionDTO.getOut()) {
			if (out.getRef() != null) {
				AbstractConsumer consumer = toAbstractConsumer(out.getRef(), intersectionDTO);
				intersection.attachConsumer(out.getIndex(), consumer);
			}
		}
		for (IntersectionDTO.ConnectionQueueDTO connQueue : intersectionDTO.getConnectionQueues()) {
			AbstractProducer inIndex = intersection.getIn(connQueue.inIndex);
			int outSize = connQueue.outConnection.size();
			AbstractConsumer[] outIndex = new AbstractConsumer[outSize];
			double[] traverseTimes = new double[outSize];
			double[] probabilities = new double[outSize];
			int i = 0;
			for (IntersectionDTO.OutConnectionDTO out : connQueue.outConnection) {
				outIndex[i] = intersection.getOut(out.outIndex);
				traverseTimes[i] = out.traverseTime;
				probabilities[i] = out.probability;
				i++;
			}
			intersection.createConnectionQueue(inIndex, outIndex, traverseTimes, probabilities);
		}
		IntersectionController ctrl = createIntersectionControllerFromDTO(intersectionDTO.getController(), intersection);
		intersection.attachController(ctrl);
		
		((TrafSimModel) model).registerIntersection(intersection);
		return intersection;
	}
	
	public IntersectionController createIntersectionControllerFromDTO(IntersectionControllerDTO ctrlDTO, Intersection intersection) {
		if (intersectionControllerDTOCache.containsKey(ctrlDTO)) {
			return intersectionControllerDTOCache.get(ctrlDTO);
		}
		
		List<IntersectionPhase> phases = new LinkedList<>();
		for (IntersectionPhaseDTO phaseDTO : ctrlDTO.getPhases()) {
			IntersectionPhase phase = new IntersectionPhase(phaseDTO.getGreenDurationPart(), phaseDTO.getYellowDuration());
			for (IntersectionPhaseDTO.PhaseConnectionDTO connDTO : phaseDTO.getConnections()) {
				phase.addConnection(new IntersectionConnection(connDTO.inIndex, connDTO.outIndex));
			}
			phases.add(phase);
		}
		FixedCirculationController ctrl = new FixedCirculationController(
				model,
				ctrlDTO.getName(),
				false,
				intersection,
				phases,
				ctrlDTO.getTurnaroundTime(),
				ctrlDTO.getStartOffset());
		
		intersectionControllerDTOCache.put(ctrlDTO, ctrl);
		return ctrl;
	}
	
	public FixedCirculationController createOneWayController(Intersection intersection, double turnaroundTime, double startOffset, double yellowDuration) {
		List<IntersectionPhase> phases = new ArrayList<>(1);
		IntersectionPhase phase = new IntersectionPhase(1.0, yellowDuration);
		phase.addConnection(new IntersectionConnection(0, 1));
		phases.add(phase);
		return new FixedCirculationController(model, generateName(FixedCirculationController.class), false, intersection, phases, turnaroundTime, startOffset);
	}

	public void connectFull4Intersection(Intersection intersection, List<? extends AbstractProducer> producers, List<? extends AbstractConsumer> consumers) {
		for (int i = 0; i < 4; i++) {
			intersection.attachConsumer(i, consumers.get(i));
			intersection.attachProducer(i, producers.get(i));
		}
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (j == i) {
					continue;
				}
				AbstractConsumer[] toConsumers = new AbstractConsumer[1];
				double[] traverseTimes = new double[1];
				double[] probabilities = new double[1];

				toConsumers[0] = consumers.get(j);
				traverseTimes[0] = 4.0;
				Arrays.fill(probabilities, 1);

				intersection.createConnectionQueue(producers.get(i), toConsumers, traverseTimes, probabilities);
			}
		}
	}

	public FixedCirculationController createStdFull4Controller(Intersection intersection, double turnaroundTime, double startOffset, double yellowTime) {
		List<IntersectionPhase> phases = new ArrayList<>(4);
		for (int i = 0; i < 4; i++) {
			IntersectionPhase phase = new IntersectionPhase(1.0 / 4.0, yellowTime);
			for (int j = 0; j < 4; j++) {
				if (i == j) {
					continue;
				}
				phase.addConnection(new IntersectionConnection(i, j));
			}
			phases.add(phase);
		}
		return new FixedCirculationController(model, generateName(FixedCirculationController.class), false, intersection, phases, turnaroundTime, startOffset);
	}

	public <T extends Entity> Queue<T> createQueue(Class<T> cls) {
		return new Queue<>(model, generateName(Queue.class), true, false);
	}

	public <T extends Entity> Queue<T> createQueue(Class<T> cls, String namePostfix) {
		return new Queue<>(model, generateName(Queue.class) + namePostfix, true, false);
	}

	
	
	public TrafficLightSwitchPhaseEvent createTrafficLightSwitchEvent() {
		//return new TrafficLightSwitchPhaseEvent(model, generateName(TrafficLightSwitchPhaseEvent.class), false);
		return new TrafficLightSwitchPhaseEvent(model, null, false);
	}

	
	
	public CarAcceleratorEvent createCarAcceleratorEvent(int inDirection, int outDirection) {
		//return new CarAcceleratorEvent(model, generateName(CarAcceleratorEvent.class), false, inDirection, outDirection);
		return new CarAcceleratorEvent(model, null, false, inDirection, outDirection);
	}
	
	
	
	private AbstractProducer toAbstractProducer(DTO toProducer, DTO owner) {
		AbstractProducer producer;
		switch (toProducer.getClass().getSimpleName()) {
			case "SourceDTO":
				producer = createSourceFromDTO(SourceDTO.class.cast(toProducer));
				break;
			case "OneWayStreetDTO":
				producer = createOneWayStreetFromDTO(OneWayStreetDTO.class.cast(toProducer)).toProducer();
				break;
			case "IntersectionDTO":
				int index = IntersectionDTO.class.cast(toProducer).getOutDirectionId(owner);
				producer = createIntersectionFromDTO(IntersectionDTO.class.cast(toProducer)).toProducer(index);
				break;	
			default:
				producer = null;
		}
		return producer;
	}
	
	private AbstractConsumer toAbstractConsumer(DTO toConsumer, DTO owner) {
		AbstractConsumer consumer;
		switch (toConsumer.getClass().getSimpleName()) {
			case "SinkDTO":
				consumer = createSinkFromDTO(SinkDTO.class.cast(toConsumer));
				break;
			case "OneWayStreetDTO":
				consumer = createOneWayStreetFromDTO(OneWayStreetDTO.class.cast(toConsumer)).toConsumer();
				break;
			case "IntersectionDTO":
				int index = IntersectionDTO.class.cast(toConsumer).getInDirectionId(owner);
				consumer = createIntersectionFromDTO(IntersectionDTO.class.cast(toConsumer)).toConsumer(index);
				break;
			default:
				consumer = null;
		}
		return consumer;
	}
	
	private String generateName(Class<?> entity) {
		return entity.getSimpleName() + "_" + getUniqueID();
	}

	private static String getUniqueID() {
		// UUID.randomUUID().toString();
		synchronized (lock) {
			return Integer.toString(uid++);
		}
	}

}
