package at.fhv.itm14.trafsim.model.entities.intersection;

public class IntersectionConnection {
	private final int in;
	private final int out;

	public IntersectionConnection(int in, int out) {
		this.in = in;
		this.out = out;
	}

	public int getIn() {
		return in;
	}

	public int getOut() {
		return out;
	}
}
