package at.fhv.itm14.trafsim.model.entities;

import at.fhv.itm14.trafsim.persistence.model.DTO;
import at.fhv.itm14.trafsim.persistence.model.DTOConvertable;
import desmoj.core.simulator.Entity;
import desmoj.core.simulator.Model;

public abstract class AbstractConsumer extends Entity implements IConsumer, DTOConvertable<DTO> {

	public AbstractConsumer(Model owner, String name, boolean showInTrace) {
		super(owner, name, showInTrace);
	}

}
