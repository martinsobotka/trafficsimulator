package at.fhv.itm14.trafsim.model.entities;

public interface IConsumer {

	public void carEnter(Car car);
	
	public boolean isFull();
}
