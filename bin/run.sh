#!/bin/sh

if [ -f description.xml ]
then
	java -cp "simopt-framework-0.1.jar:optimizer-visualisation-0.1.jar" Main description.xml false
	echo "Done"
else
	echo "Error: description.xml not found."
fi
