@echo off

echo Generating description file using SimOptFramework.jar

java -cp simopt-framework-0.1.jar Main trafsim-0.1.jar optimizer-nsga2-0.1.jar description.xml

start /B /LOW /WAIT echo Done
PAUSE
