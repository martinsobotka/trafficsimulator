@echo off

if exist description.xml (
	java -cp "simopt-framework-0.1.jar;optimizer-visualisation-0.1.jar" Main description.xml false
	start /B /LOW /WAIT echo Done
	PAUSE
) else (
	echo description.xml not found
	echo Run generate_desc_file.bat first
	start /B /LOW /WAIT echo ERROR
	PAUSE
)

