package streetnetwork.solver;

public class Direction {
	public String direction;
	int intersection_id;

	public Direction(String direction, int intersection_id, double left, double rechts, double straight,
			         int phLeft, int phRight, int phStraight, double flow, double sRate) {
		this.direction = direction;
		this.intersection_id = intersection_id;

		this.left = left;
		this.right = rechts;
		this.straight = straight;
		this.phLeft = phLeft;
		this.phRight = phRight;
		this.phStraight = phStraight;
		this.flow = flow;
		this.sRate = sRate;
	}

	public double left = 0.25;
	public double right = 0.25;
	public double straight = 0.5;
	public int phLeft=1;
	public int phRight=1;
	public int phStraight=1;
	public double flow = 0.0;
	public double sRate = 0;
}
