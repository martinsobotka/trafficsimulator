package streetnetwork.solver;

public class Intersection {
	public Direction A;
	public Direction B;
	public Direction C;
	public Direction D;

	public static int id = 1;
	public int numPhases=4;

	protected double flowA;
	protected double flowB;
	protected double flowC;
	protected double flowD;
	
	protected double sRateA;
	protected double sRateB;
	protected double sRateC;
	protected double sRateD;

	
	private int intersectionId;

	public Intersection(int numPhases, double aLeft, double aRight, double aStraight, double bLeft, double bRight,
			double bStraight, double cLeft, double cRight, double cStraight, double dLeft, double dRight,
			double dStraight, 
			int aLeftPh, int aRightPh, int aStraightPh, int bLeftPh, int bRightPh,
			int bStraightPh, int cLeftPh, int cRightPh, int cStraightPh, int dLeftPh, int dRightPh,
			int dStraightPh, double flowA, double flowB, double flowC, double flowD,
			double sRateA, double sRateB, double sRateC, double sRateD) {
		this.numPhases = numPhases;
		intersectionId = id++;

		A = new Direction("A", intersectionId, aLeft, aRight, aStraight, aLeftPh, aRightPh, aStraightPh, flowA, sRateA);
		B = new Direction("B", intersectionId, bLeft, bRight, bStraight, bLeftPh, bRightPh, bStraightPh, flowB, sRateB);
		C = new Direction("C", intersectionId, cLeft, cRight, cStraight, cLeftPh, cRightPh, cStraightPh, flowC, sRateC);
		D = new Direction("D", intersectionId, dLeft, dRight, dStraight, dLeftPh, dRightPh, dStraightPh, flowD, sRateD);
	}

	public Intersection(double aLeft, double aRight, double aStraight, double bLeft, double bRight,
			double bStraight, double cLeft, double cRight, double cStraight, double dLeft, double dRight,
			double dStraight) {
		intersectionId = id++;

		A = new Direction("A", intersectionId, aLeft, aRight, aStraight, 1, 1, 1, 1.0, 0.0);
		B = new Direction("B", intersectionId, bLeft, bRight, bStraight, 2, 2, 2, 1.0, 0.0);
		C = new Direction("C", intersectionId, cLeft, cRight, cStraight, 3, 3, 3, 1.0, 0.0);
		D = new Direction("D", intersectionId, dLeft, dRight, dStraight, 4, 4, 4, 1.0, 0.0);
	}

	
	
	public String print(double epsilon) {
		//calculate the utilization constraints
		StringBuilder sb = new StringBuilder();
		
		Direction directions[] = new Direction[]{A, B, C, D};
		double qexp[][] = new double[numPhases][4]; 
		//qexp[phase][i]: expected part of flow from entrance i (A, B, C or D) in phase, 

		char sDirs[] = {'A', 'B', 'C', 'D'};
		String[] sExp = new String[4];
		String[] sUtil = new String[numPhases];
		// variable names for utilization factors in stability equations
		// u<id>_<phase><sDir>
		int cUtil = 0;  //counter for sUtil
//1. calculate the expected flows from directions in phases, and
//   initialize the combinatorial array: keeps the number of directions for each phase
//		int[] combinatorial = new int[numPhases];
		boolean[][] dirInPhase = new boolean[numPhases][4];
		boolean[][] done = new boolean[numPhases][4];
		
		for (int dir=0;dir<4;dir++)
			sExp[dir] = "i" + intersectionId + sDirs[dir];
		
		for (int phase=1; phase<=numPhases; phase++)  {
//cycle over phases
//		   combinatorial[phase-1] = 0;
		   for (int i=0;i<4;i++)  {
//cycle over directions
			   done[phase-1][i] = false;
			   qexp[phase-1][i] = 0;
			   
			   if (directions[i].phLeft == phase)   
				   	qexp[phase-1][i] += directions[i].left;
			   if (directions[i].phRight == phase) 
				      qexp[phase-1][i] += directions[i].right;
			   if (directions[i].phStraight == phase) 
				      qexp[phase-1][i] += directions[i].straight;
			   if (qexp[phase-1][i] > 0) {
//				   	combinatorial[phase-1]++;
				   	dirInPhase[phase-1][i] = true;
			   }
			   else
				   	dirInPhase[phase-1][i] = false;
		   }
		   
		}

//initialize the counter iComb for "numPhases" nested loops
		boolean finished = false;
		int[] iComb = new int[numPhases];
		for (int phase=1; phase<=numPhases; phase++) {
			iComb[phase-1] = -1;
			for (int dir = 0; dir<4; dir++)
			   if ( dirInPhase[phase-1][dir]) {
					iComb[phase-1] = dir;
					sUtil[phase-1] = "u"+intersectionId+"_"+phase+sDirs[dir]; //+(cUtil++);
					break;
			   }
		}
	
//print the stability inequalities
		while (!finished)  {
//while-loop simulates "numPhases" nested loops: for each phase with possibly more
//than one incoming direction, we need a "maximum" of utilization factors which, in LP, 
//has to be mapped to as many equations as there are directions in the phase
			StringBuilder sbIneq = new StringBuilder("1-" + epsilon + " > ");
			  
			for (int phase=1; phase<=numPhases; phase++)  
			{
//cycle over phases: one combination
				if (iComb[phase-1]<0)
					System.err.println("phase without direction");
				if (!done[phase-1][iComb[phase-1]] )  {
				  // utilization = expflow/maxflow      from direction iComb[phase-1] in phase
				  sb.append(sUtil[phase-1] + " = " + (qexp[phase-1][iComb[phase-1]] / directions[iComb[phase-1]].flow) + " " + sExp[iComb[phase-1]] + ";\n");
				  done[phase-1][iComb[phase-1]] = true;
				}
				
				if (phase>1)
					sbIneq.append(" + ");
				sbIneq.append(sUtil[phase-1]); 
				
			}
			sb.append(sbIneq + ";\n");

//advance nested counter iComb:
			finished = true;
			for (int phase=numPhases; phase>0; phase--)  {
				int dirOld=iComb[phase-1];
				
				for (int dir=dirOld+1; dir<=iComb[phase-1]+4; dir++)
					if ( dirInPhase[phase-1][dir%4]) {
						iComb[phase-1] = dir%4;
						sUtil[phase-1] = "u"+intersectionId+"_"+phase+sDirs[dir%4]; //+(cUtil++);
						break;
				    }
				if (iComb[phase-1]>dirOld) {
				   finished = false;
				   break;
				}
			}
				
		}
		
		return  "// stability equations: \n"
				+ sb 
				+ "// Intersection " + intersectionId + "\n" + "o" + intersectionId + "A = " + B.straight + " i" + intersectionId
				+ "B + " + C.left + " i" + intersectionId + "C + " + D.right + " i" + intersectionId + "D" + ";\n" + "o"
				+ intersectionId + "B = " + A.straight + " i" + intersectionId + "A + " + C.right + " i" + intersectionId
				+ "C + " + D.left + " i" + intersectionId + "D" + ";\n" + "o" + intersectionId + "C = " + A.right + " i"
				+ intersectionId + "A + " + B.left + " i" + intersectionId + "B + " + D.straight + " i" + intersectionId + "D"
				+ ";\n" + "o" + intersectionId + "D = " + A.left + " i" + intersectionId + "A + " + B.right + " i"
				+ intersectionId + "B + " + C.straight + " i" + intersectionId + "C" + ";\n";
	}
}
