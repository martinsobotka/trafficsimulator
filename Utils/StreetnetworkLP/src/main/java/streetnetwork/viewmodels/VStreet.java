package streetnetwork.viewmodels;

public class VStreet {
	private final int id;
	private final int row;
	private final int column;
	private final VDirection direction;
	private double traverseTime;
	private int capacity;

	public VStreet(int id, int row, int column, VDirection direction) {
		this.id = id;
		this.row = row;
		this.column = column;
		this.direction = direction;
	}

	public void setDefaults() {
		
	}
	
	public int getId() {
		return id;
	}

	public int getRow() {
		return row;
	}

	public int getColumn() {
		return column;
	}

	public double getTraverseTime() {
		return traverseTime;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setTraverseTime(double traverseTime) {
		this.traverseTime = traverseTime;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public VDirection getDirection() {
		return direction;
	}

	public void setDefault() {
		this.capacity = Integer.MAX_VALUE;
		this.traverseTime = 10.0;
	}
}
