package streetnetwork.viewmodels;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Markus Mohanty <markus.mo at gmx.net>
 */
@XmlRootElement(name = "intersection")
//@XmlAccessorType (XmlAccessType.FIELD)
public class VIntersection
{

	public static final int A = 0;
	public static final int B = 2;
	public static final int C = 1;
	public static final int D = 3;
    
    private int id;
    private int x;
    private int y;
    private int numPhases;
	private double[][] probabilities;
	private double[][] traverseTimes;
	private int[][] phases;
    //A
    private double flowA;
    //B
    private double flowB;
    //C
    private double flowC;
    //D
    private double flowD;
    
    private VSource[] sources;

    private boolean active;
	private double serviceDelay;
	private double yellowDuration;
	private HashMap<Integer, Double> greenDurationParts;
	
	
	public VIntersection(){
		this.probabilities = new double[4][4];
		this.traverseTimes = new double[4][4];
		this.phases = new int[4][4];
		this.sources = new VSource[4];
		this.greenDurationParts = new HashMap<>();
	}

    public VIntersection(int id, int x, int y)
    {
		this();
        this.id = id;
        this.x = x;
        this.y = y;
        this.setDefault();
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

	@XmlElement(name = "active")
    public boolean isActive()
    {
        return active;
    }
	
    public void setId(int id)
    {
        this.id = id;
    }

    public void setX(int x)
    {
        this.x = x;
    }

    public void setY(int y)
    {
        this.y = y;
    }

	@XmlElement(name = "x")
    public int getX()
    {
        return x;
    }

	@XmlElement(name = "y")
    public int getY()
    {
        return y;
    }
    
	@XmlElement(name = "id")
    public int getId()
    {
        return id;
    }

	@XmlElement(name = "flowA")
    public double getFlowA()
    {
        return flowA;
    }

    public void setFlowA(double flowA)
    {
        this.flowA = flowA;
    }

	@XmlElement(name = "flowB")
    public double getFlowB()
    {
        return flowB;
    }

    public void setFlowB(double flowB)
    {
        this.flowB = flowB;
    }

	@XmlElement(name = "flowC")
    public double getFlowC()
    {
        return flowC;
    }

    public void setFlowC(double flowC)
    {
        this.flowC = flowC;
    }

	@XmlElement(name = "flowD")
    public double getFlowD()
    {
        return flowD;
    }

    public void setFlowD(double flowD)
    {
        this.flowD = flowD;
    }
    
	@XmlElement(name = "probAB")
    public double getProbAB()
    {
        return probabilities[A][B];
    }

    public void setProbAB(double probAB)
    {
        this.probabilities[A][B] = probAB;
    }

	@XmlElement(name = "probAC")
    public double getProbAC()
    {
        return probabilities[A][C];
    }

    public void setProbAC(double probAC)
    {
        this.probabilities[A][C] = probAC;
    }

	@XmlElement(name = "probAD")
    public double getProbAD()
    {
        return probabilities[A][D];
    }

    public void setProbAD(double probAD)
    {
		this.probabilities[A][D] = probAD;
    }

	@XmlElement(name = "probBA")
    public double getProbBA()
    {
        return probabilities[B][A];
    }

    public void setProbBA(double probBA)
    {
        this.probabilities[B][A] = probBA;
    }

	@XmlElement(name = "probBC")
    public double getProbBC()
    {
        return probabilities[B][C];
    }

    public void setProbBC(double probBC)
    {
        this.probabilities[B][C] = probBC;
    }

	@XmlElement(name = "probBD")
    public double getProbBD()
    {
        return probabilities[B][D];
    }

    public void setProbBD(double probBD)
    {
        this.probabilities[B][D] = probBD;
    }

	@XmlElement(name = "probCA")
    public double getProbCA()
    {
        return probabilities[C][A];
    }

    public void setProbCA(double probCA)
    {
        this.probabilities[C][A] = probCA;
    }

	@XmlElement(name = "probCB")
    public double getProbCB()
    {
        return probabilities[C][B];
    }

    public void setProbCB(double probCB)
    {
        this.probabilities[C][B] = probCB;
    }

	@XmlElement(name = "probCD")
    public double getProbCD()
    {
        return probabilities[C][D];
    }

    public void setProbCD(double probCD)
    {
		this.probabilities[C][D] = probCD;
    }

	@XmlElement(name = "probDA")
    public double getProbDA()
    {
        return probabilities[D][A];
    }

    public void setProbDA(double probDA)
    {
		this.probabilities[D][A] = probDA;
    }

	@XmlElement(name = "probDB")
    public double getProbDB()
    {
        return probabilities[D][B];
    }

    public void setProbDB(double probDB)
    {
		this.probabilities[D][B] = probDB;
    }

	@XmlElement(name = "probDC")
    public double getProbDC()
    {
        return probabilities[D][C];
    }

    public void setProbDC(double probDC)
    {
		this.probabilities[D][C] = probDC;
    }

	public double getProbability(int from, int to) {
		return probabilities[from][to];
	}
	
	@XmlElement(name = "sourceNorth")
    public VSource getSourceNorth()
    {
        return sources[VDirection.North.ordinal()];
    }

    public void setSourceNorth(VSource sourceNorth)
    {
        this.sources[VDirection.North.ordinal()] = sourceNorth;
    }

	@XmlElement(name = "sourceSouth")
    public VSource getSourceSouth()
    {
        return sources[VDirection.South.ordinal()];
    }

    public void setSourceSouth(VSource sourceSouth)
    {
        this.sources[VDirection.South.ordinal()] = sourceSouth;
    }

	@XmlElement(name = "sourceWest")
    public VSource getSourceWest()
    {
        return sources[VDirection.West.ordinal()];
    }

    public void setSourceWest(VSource sourceWest)
    {
        this.sources[VDirection.West.ordinal()] = sourceWest;
    }

	@XmlElement(name = "sourceEast")
    public VSource getSourceEast()
    {
        return sources[VDirection.East.ordinal()];
    }

    public void setSourceEast(VSource sourceEast)
    {
        this.sources[VDirection.East.ordinal()] = sourceEast;
    }
	
	public VSource getSource(VDirection direction) {
		return sources[direction.ordinal()];
	}

    public void setDefault()
    {
        //default settings
    	this.numPhases = 4;
        this.probabilities[A][B] = 0.5;
        this.probabilities[A][C] = 0.25;
        this.probabilities[A][D] = 0.25;
        this.phases[A][B] = 1;
        this.phases[A][C] = 1;
        this.phases[A][D] = 1;
        this.flowA = 0.3;
        
        this.probabilities[B][A] = 0.25;
        this.probabilities[B][C] = 0.5;
        this.probabilities[B][D] = 0.25;
        this.phases[B][A] = 2;
        this.phases[B][C] = 2;
        this.phases[B][D] = 2;
        this.flowB = 0.3;
        
        this.probabilities[C][A] = 0.25;
        this.probabilities[C][B] = 0.25;
        this.probabilities[C][D] = 0.5;
        this.phases[C][A] = 3;
        this.phases[C][B] = 3;
        this.phases[C][D] = 3;
        this.flowC = 0.3;
        
        this.probabilities[D][A] = 0.25;
        this.probabilities[D][B] = 0.25;
        this.probabilities[D][C] = 0.5;
        this.phases[D][A] = 4;
        this.phases[D][B] = 4;
        this.phases[D][C] = 4;
        this.flowD = 0.3;
		
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (i == j) {
					continue;
				}
				this.traverseTimes[i][j] = 5.0;
			}
		}
    }

    @Override
    public String toString()
    {
        String retstr = "x=" + x
                + ", y=" + y
                + ", id=" + id
                + ", numPhases=" + numPhases
                + ", probAB=" + probabilities[A][B] 
                + ", probAC=" + probabilities[A][C] 
                + ", probAD=" + probabilities[A][D] 
                + ", phaseAB=" + phases[A][B] 
                + ", phaseAC=" + phases[A][C] 
                + ", phaseAD=" + phases[A][D] 
                + ", flowA=" + flowA 
                + ", probBA=" + probabilities[B][A] 
                + ", probBC=" + probabilities[B][C] 
                + ", probBD=" + probabilities[B][D] 
                + ", phaseBA=" + phases[B][A]
                + ", phaseBC=" + phases[B][C] 
                + ", phaseBD=" + phases[B][D] 
                + ", flowB=" + flowB 
                + ", probCA=" + probabilities[C][A] 
                + ", probCB=" + probabilities[C][B] 
                + ", probCD=" + probabilities[C][D] 
                + ", phaseCA=" + phases[C][A] 
                + ", phaseCB=" + phases[C][D]
                + ", phaseCD=" + phases[C][D] 
                + ", flowC=" + flowC 
                + ", probDA=" + probabilities[D][A] 
                + ", probDB=" + probabilities[D][B]
                + ", probDC=" + probabilities[D][C] 
                + ", phaseDA=" + phases[D][A] 
                + ", phaseDB=" + phases[D][B] 
                + ", phaseDC=" + phases[D][C] 
                + ", flowD=" + flowD
                + ", active=" + active;
        if(sources[VDirection.North.ordinal()] != null)
                retstr += ", sourceNorth=" + sources[VDirection.North.ordinal()].toString();
        if(sources[VDirection.South.ordinal()] != null)
                retstr += ", sourceSouth=" + sources[VDirection.South.ordinal()].toString();
        if(sources[VDirection.West.ordinal()] != null)
                retstr += ", sourceWest=" + sources[VDirection.West.ordinal()].toString();
        if(sources[VDirection.East.ordinal()] != null)
                retstr += "sourceEast=" + sources[VDirection.East.ordinal()].toString();
        
        return retstr + "\n\r";
    }

	@XmlElement(name = "numPhases")
	public int getNumPhases() {
		return numPhases;
	}

	public void setNumPhases(int numPhases) {
		this.numPhases = numPhases;
	}

	@XmlElement(name = "phaseAB")
	public int getPhaseAB() {
		return phases[A][B];
	}

	public void setPhaseAB(int phaseAB) {
		this.phases[A][B] = phaseAB;
		if (numPhases<phaseAB)
			numPhases = phaseAB;
	}

	@XmlElement(name = "phaseAC")
	public int getPhaseAC() {
		return phases[A][C];
	}

	public void setPhaseAC(int phaseAC) {
		this.phases[A][C] = phaseAC;
		if (numPhases<phaseAC)
			numPhases = phaseAC;
	}

	@XmlElement(name = "phaseAD")
	public int getPhaseAD() {
		return phases[A][D];
	}

	public void setPhaseAD(int phaseAD) {
		this.phases[A][D] = phaseAD;
		if (numPhases<phaseAD)
			numPhases = phaseAD;
	}

	@XmlElement(name = "phaseBA")
	public int getPhaseBA() {
		return phases[B][A];
	}

	public void setPhaseBA(int phaseBA) {
		this.phases[B][A] = phaseBA;
		if (numPhases<phaseBA)
			numPhases = phaseBA;
	}

	@XmlElement(name = "phaseBC")
	public int getPhaseBC() {
		return phases[B][C];
	}

	public void setPhaseBC(int phaseBC) {
		this.phases[B][C] = phaseBC;
		if (numPhases<phaseBC)
			numPhases = phaseBC;
	}

	@XmlElement(name = "phaseBD")
	public int getPhaseBD() {
		return phases[B][C];
	}

	public void setPhaseBD(int phaseBD) {
		this.phases[B][D] = phaseBD;
		if (numPhases<phaseBD)
			numPhases = phaseBD;
	}

	@XmlElement(name = "phaseCA")
	public int getPhaseCA() {
		return phases[C][A];
	}

	public void setPhaseCA(int phaseCA) {
		this.phases[C][A] = phaseCA;
		if (numPhases<phaseCA)
			numPhases = phaseCA;
	}

	@XmlElement(name = "phaseCB")
	public int getPhaseCB() {
		return phases[C][B];
	}

	public void setPhaseCB(int phaseCB) {
		this.phases[C][B] = phaseCB;
		if (numPhases<phaseCB)
			numPhases = phaseCB;
	}

	@XmlElement(name = "phaseCD")
	public int getPhaseCD() {
		return phases[C][D];
	}

	public void setPhaseCD(int phaseCD) {
		this.phases[C][D] = phaseCD;
		if (numPhases<phaseCD)
			numPhases = phaseCD;
	}

	@XmlElement(name = "phaseDA")
	public int getPhaseDA() {
		return phases[D][A];
	}

	public void setPhaseDA(int phaseDA) {
		this.phases[D][A] = phaseDA;
		if (numPhases<phaseDA)
			numPhases = phaseDA;
	}

	@XmlElement(name = "phaseDB")
	public int getPhaseDB() {
		return phases[D][B];
	}

	public void setPhaseDB(int phaseDB) {
		this.phases[D][B] = phaseDB;
		if (numPhases<phaseDB)
			numPhases = phaseDB;
	}

	@XmlElement(name = "phaseDC")
	public int getPhaseDC() {
		return phases[D][C];
	}

	public void setPhaseDC(int phaseDC) {
		this.phases[D][C] = phaseDC;
		if (numPhases<phaseDC)
			numPhases = phaseDC;
	}
	
	public List<int[]> getPhaseConnections(int phase) {
		List<int[]> connections = new LinkedList<>();
		for (int from = 0; from < 4; from++) {
			for (int to = 0; to < 4; to++) {
				if (phases[from][to] == phase) {
					connections.add(new int[]{from, to});
				}
			}
		}
		return connections;
	}

	public Double getTraverseTime(int from, int to) {
		return this.traverseTimes[from][to];
	}

	@XmlElement(name = "serviceDelay")
	public double getServiceDelay() {
		return serviceDelay;
	}

	public void setServiceDelay(double serviceDelay) {
		this.serviceDelay = serviceDelay;
	}

	@XmlElement(name = "yellowDuration")
	public double getYellowDuration() {
		return yellowDuration;
	}

	public void setYellowDuration(double yellowDuration) {
		this.yellowDuration = yellowDuration;
	}
	
	public double getGreenDurationPart(int phase) {
		if (greenDurationParts.containsKey(phase)) {
			return greenDurationParts.get(phase);
		}
		return -1;
	}
	
	public void setGreenDurationPart(int phase, double part) {
		greenDurationParts.put(phase, part);
	}

	public void setTraverseTimes(double[][] traverseTimes) {
		this.traverseTimes = traverseTimes;
	}
}
