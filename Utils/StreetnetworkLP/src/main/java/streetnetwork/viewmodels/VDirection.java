package streetnetwork.viewmodels;

/**
 *
 * @author Markus Mohanty <markus.mo at gmx.net>
 */
public enum VDirection 
{
    North,
    East,
    South,
    West
}
