/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package streetnetwork.gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;
import streetnetwork.controller.StreetNetworkController;
import streetnetwork.viewmodels.VDirection;
import java.awt.Color;

/**
 *
 * @author Markus Mohanty <markus.mo at gmx.net>
 */
public class JunctionSettingsDialog extends JDialog
{
    private int number;
    private int row;
    private int column;
    
    /**
     * Creates new form JunctionSettingsDialog
     * @param parent
     * @param modal
     * @param number
     * @param row
     * @param column
     */
    public JunctionSettingsDialog(java.awt.Frame parent, boolean modal, int number, int row, int column)
    {
        super(parent, modal);
        initComponents();
        this.number = number;
        this.row = row;
        this.column = column;
        String text = this.titleLabel.getText();
        this.titleLabel.setText(text + " " + number);
        URL resource = this.getClass().getClassLoader().getResource("images/junction.png");
        ImageIcon backgroundImage = new ImageIcon(resource);
        
        JLabel background = new JLabel(backgroundImage);
        
        background.setVisible(true);
        getContentPane().add(background);
        
        //A
        probAB.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getProbAB());
        probAC.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getProbAC());
        probAD.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getProbAD());
        phaseAB.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getPhaseAB());
        phaseAC.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getPhaseAC());
        phaseAD.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getPhaseAD());
        if (StreetNetworkController.getInstance().getIntersection(row, column).getSourceNorth()==null)
        {
            rateA.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getFlowA());
        }
        else
        {
            SourceA.setSelected(true);
            rateA.setText(StreetNetworkController.getInstance().getIntersection(row, column).getFlowA() + ","+StreetNetworkController.getInstance().getIntersection(row, column).getSourceNorth().getRate());
        }
        //B
        probBA.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getProbBA());
        probBC.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getProbBC());
        probBD.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getProbBD());
        phaseBA.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getPhaseBA());
        phaseBC.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getPhaseBC());
        phaseBD.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getPhaseBD());
        if (StreetNetworkController.getInstance().getIntersection(row, column).getSourceSouth()==null)
        {
            rateB.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getFlowB());
        }else
        {
            SourceB.setSelected(true);
            rateB.setText(StreetNetworkController.getInstance().getIntersection(row, column).getFlowB() + ","+StreetNetworkController.getInstance().getIntersection(row, column).getSourceSouth().getRate());
        }
        
        //C
        probCA.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getProbCA());
        probCB.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getProbCB());
        probCD.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getProbCD());
        phaseCA.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getPhaseCA());
        phaseCB.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getPhaseCB());
        phaseCD.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getPhaseCD());
        if (StreetNetworkController.getInstance().getIntersection(row, column).getSourceWest()==null)
        {
            rateC.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getFlowC());
        }else
        {
            SourceC.setSelected(true);
            rateC.setText(StreetNetworkController.getInstance().getIntersection(row, column).getFlowC() + ","+StreetNetworkController.getInstance().getIntersection(row, column).getSourceWest().getRate());
        }
        
        //D
        probDA.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getProbDA());
        probDB.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getProbDB());
        probDC.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getProbDC());
        phaseDA.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getPhaseDA());
        phaseDB.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getPhaseDB());
        phaseDC.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getPhaseDC());
        if (StreetNetworkController.getInstance().getIntersection(row, column).getSourceEast()==null)
        {
            rateD.setText(""+StreetNetworkController.getInstance().getIntersection(row, column).getFlowD());
        }else
        {
            SourceD.setSelected(true);
            rateD.setText(StreetNetworkController.getInstance().getIntersection(row, column).getFlowD() + ","+StreetNetworkController.getInstance().getIntersection(row, column).getSourceEast().getRate());
        }
        
    }
    
    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        okButton = new JButton();
        titleLabel = new JLabel();
        probAC = new JTextField();
        probAC.setBackground(new Color(255, 255, 153));
        probAB = new JTextField();
        probAB.setBackground(new Color(255, 255, 153));
        probAD = new JTextField();
        probAD.setBackground(new Color(255, 255, 153));
        probBC = new JTextField();
        probBC.setBackground(new Color(255, 255, 153));
        probBA = new JTextField();
        probBA.setBackground(new Color(255, 255, 153));
        probBD = new JTextField();
        probBD.setBackground(new Color(255, 255, 153));
        probCA = new JTextField();
        probCA.setBackground(new Color(255, 255, 153));
        probCD = new JTextField();
        probCD.setBackground(new Color(255, 255, 153));
        probCB = new JTextField();
        probCB.setBackground(new Color(255, 255, 153));
        probDA = new JTextField();
        probDA.setBackground(new Color(255, 255, 153));
        probDC = new JTextField();
        probDC.setBackground(new Color(255, 255, 153));
        probDB = new JTextField();
        probDB.setBackground(new Color(255, 255, 153));
        jLabel1 = new JLabel();
        jLabel2 = new JLabel();
        jLabel3 = new JLabel();
        jLabel4 = new JLabel();
        jLabel5 = new JLabel();
        jLabel6 = new JLabel();
        jLabel7 = new JLabel();
        jLabel8 = new JLabel();
        jLabel9 = new JLabel();
        jLabel10 = new JLabel();
        jLabel11 = new JLabel();
        jLabel12 = new JLabel();
//HV: 19.12.05
        phaseAC = new JTextField();
        phaseAB = new JTextField();
        phaseAD = new JTextField();
        phaseBC = new JTextField();
        phaseBA = new JTextField();
        phaseBD = new JTextField();
        phaseCA = new JTextField();
        phaseCD = new JTextField();
        phaseCB = new JTextField();
        phaseDA = new JTextField();
        phaseDC = new JTextField();
        phaseDB = new JTextField();        
//end HV        
        SourceA = new JCheckBox();
        jLabel13 = new JLabel();
        rateA = new JTextField();
        rateA.setBackground(new Color(204, 255, 255));
        SourceB = new JCheckBox();
        rateB = new JTextField();
        rateB.setBackground(new Color(204, 255, 255));
        jLabel14 = new JLabel();
        SourceD = new JCheckBox();
        jLabel15 = new JLabel();
        rateD = new JTextField();
        rateD.setBackground(new Color(204, 255, 255));
        SourceC = new JCheckBox();
        jLabel16 = new JLabel();
        rateC = new JTextField();
        rateC.setBackground(new Color(204, 255, 255));
        forAllButton = new JButton();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Junction Settings");

        okButton.setText("OK");
        okButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                okButtonActionPerformed(evt);
            }
        });

        titleLabel.setFont(new Font("Lucida Grande", 1, 13)); // NOI18N
        titleLabel.setText("Junction No. ");

        probAC.setToolTipText("turn probability: A to B");

        probAB.setToolTipText("turn probability: A to B");

        probAD.setToolTipText("turn probability: A to D");

        probBC.setToolTipText("turn probability: B to C");

        probBA.setToolTipText("turn probability: B to A");

        probBD.setToolTipText("turn probability: B to D");

        probCA.setToolTipText("turn probability: C to A");

        probCD.setToolTipText("turn probability: C to D");

        probCB.setToolTipText("turn probability: C to B");

        probDA.setToolTipText("turn probability: D to A");

        probDC.setToolTipText("turn probability: D to C");

        probDB.setToolTipText("turn probability: D to B");
        
        phaseAC.setToolTipText("phase: A to B");

        phaseAB.setToolTipText("phase: A to B");

        phaseAD.setToolTipText("phase: A to D");

        phaseBC.setToolTipText("phase: B to C");

        phaseBA.setToolTipText("phase: B to A");

        phaseBD.setToolTipText("phase: B to D");

        phaseCA.setToolTipText("phase: C to A");

        phaseCD.setToolTipText("phase: C to D");

        phaseCB.setToolTipText("phase: C to B");

        phaseDA.setToolTipText("phase: D to A");

        phaseDC.setToolTipText("phase: D to C");

        phaseDB.setToolTipText("phase: D to B");
        

        jLabel1.setText("DA");

        jLabel2.setText("DC");

        jLabel3.setText("DB");

        jLabel4.setText("CA");

        jLabel5.setText("CD");

        jLabel6.setText("CB");

        jLabel7.setText("BC");

        jLabel8.setText("BA");

        jLabel9.setText("BD");

        jLabel10.setText("AC");

        jLabel11.setText("AB");

        jLabel12.setText("AD");

        SourceA.setText("Source");
        SourceA.setToolTipText("when checked, then flow is regarded as source rate");

        jLabel13.setFont(new Font("Lucida Grande", 1, 13)); // NOI18N
        jLabel13.setText("A");

        rateA.setToolTipText("flow of street");

        SourceB.setText("Source");
        SourceB.setToolTipText("when checked, then flow is regarded as source rate");

        rateB.setToolTipText("flow of street");

        jLabel14.setFont(new Font("Lucida Grande", 1, 13)); // NOI18N
        jLabel14.setText("B");

        SourceD.setText("Source");
        SourceD.setToolTipText("when checked, then flow is regarded as source rate");

        jLabel15.setFont(new Font("Lucida Grande", 1, 13)); // NOI18N
        jLabel15.setText("D");

        rateD.setToolTipText("flow of street");

        SourceC.setText("Source");
        SourceC.setToolTipText("when checked, then flow is regarded as source rate");

        jLabel16.setFont(new Font("Lucida Grande", 1, 13)); // NOI18N
        jLabel16.setText("C");

        rateC.setToolTipText("flow of street");

        forAllButton.setText("for all");
        forAllButton.setToolTipText("take settings for all junctions");
        forAllButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                forAllButtonActionPerformed(evt);
            }
        });

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                            .addComponent(rateC)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(39, 39, 39)
                                .addComponent(jLabel16))
                            .addComponent(SourceC, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 177, Short.MAX_VALUE)
                                .addComponent(forAllButton)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(okButton)
                                .addGap(54, 54, 54))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addComponent(phaseCD, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                                    .addComponent(phaseCA, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                                    .addComponent(phaseCB, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel6))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addComponent(probCD, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
                                    .addComponent(probCA, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
                                    .addComponent(probCB, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE))
                                .addGap(143, 143, 143)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(probDC, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel2)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(phaseDC, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))                                                                                
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(probDB, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel3)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(phaseDB, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))                                                                                                                        
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(probDA, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel1)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(phaseDA, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)))                                                                                                                                                            		
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                    .addComponent(rateD)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(39, 39, 39)
                                        .addComponent(jLabel15))
                                    .addComponent(SourceD, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(97, 97, 97)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                            .addComponent(probAC, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel10)
                                                .addGap(17, 17, 17))
                                            .addComponent(phaseAC, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                            .addComponent(probAB, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
                                            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addComponent(jLabel11)
                                                .addGap(20, 20, 20))
                                            .addComponent(phaseAB, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                            .addComponent(probAD, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(13, 13, 13)
                                                .addComponent(jLabel12))
                                            .addComponent(phaseAD, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(probBC, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel7)
                                                .addGap(21, 21, 21))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(phaseBC, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)))
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(probBA, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED))
                                            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addComponent(jLabel8)
                                                .addGap(21, 21, 21))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(phaseBA, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)))
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                            .addComponent(probBD, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(14, 14, 14)
                                                .addComponent(jLabel9))
                                            .addComponent(phaseBD, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                            .addComponent(rateB, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(SourceB, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
                                        .addGap(15, 15, 15))
                                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(rateA, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(SourceA, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
                                        .addGroup(layout.createSequentialGroup()
                                            .addGap(36, 36, 36)
                                            .addComponent(jLabel13)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 13, GroupLayout.PREFERRED_SIZE)))))))
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(201, 201, 201)
                            .addComponent(titleLabel))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(215, 215, 215)
                            .addComponent(jLabel14))))
                .addContainerGap())
        );
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titleLabel)
                .addGap(37, 37, 37)
                .addComponent(jLabel13)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rateA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(SourceA)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(phaseAC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(phaseAB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(phaseAD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(probAC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(probAB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(probAD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(probCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(phaseCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel4))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(probCD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(phaseCD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel5))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(probCB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(phaseCB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel6)))
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(probDA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(phaseDA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel1))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(probDC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(phaseDC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel2))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(probDB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(phaseDB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel3)))
                        .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(jLabel16)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(rateC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(SourceC)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rateD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(SourceD)))
                .addGap(9, 9, 9)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(probBC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(probBA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(probBD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)                    
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(phaseBC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(phaseBA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(phaseBD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(SourceB)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rateB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel14)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(okButton)
                    .addComponent(forAllButton))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(ActionEvent evt)//GEN-FIRST:event_okButtonActionPerformed
    {//GEN-HEADEREND:event_okButtonActionPerformed
        this.setJunctionSettings();
        this.dispose();
    }//GEN-LAST:event_okButtonActionPerformed

    private void forAllButtonActionPerformed(ActionEvent evt)//GEN-FIRST:event_forAllButtonActionPerformed
    {//GEN-HEADEREND:event_forAllButtonActionPerformed
        this.setJunctionSettings();
        StreetNetworkController.getInstance().forAll(row, column);
        this.dispose();
    }//GEN-LAST:event_forAllButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JCheckBox SourceA;
    private JCheckBox SourceB;
    private JCheckBox SourceC;
    private JCheckBox SourceD;
    private JButton forAllButton;
    private JLabel jLabel1;
    private JLabel jLabel10;
    private JLabel jLabel11;
    private JLabel jLabel12;
    private JLabel jLabel13;
    private JLabel jLabel14;
    private JLabel jLabel15;
    private JLabel jLabel16;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JLabel jLabel8;
    private JLabel jLabel9;
    private JButton okButton;
    private JTextField probAB;
    private JTextField probAC;
    private JTextField probAD;
    private JTextField probBA;
    private JTextField probBC;
    private JTextField probBD;
    private JTextField probCA;
    private JTextField probCB;
    private JTextField probCD;
    private JTextField probDA;
    private JTextField probDB;
    private JTextField probDC;
    private JTextField phaseAB;
    private JTextField phaseAC;
    private JTextField phaseAD;
    private JTextField phaseBA;
    private JTextField phaseBC;
    private JTextField phaseBD;
    private JTextField phaseCA;
    private JTextField phaseCB;
    private JTextField phaseCD;
    private JTextField phaseDA;
    private JTextField phaseDB;
    private JTextField phaseDC;
    private JTextField rateA;
    private JTextField rateB;
    private JTextField rateC;
    private JTextField rateD;
    private JLabel titleLabel;
    // End of variables declaration//GEN-END:variables

    private void setJunctionSettings()
    {
        //A
        StreetNetworkController.getInstance().addProbabilityAB(row, column, Double.parseDouble(probAB.getText()));
        StreetNetworkController.getInstance().addProbabilityAC(row, column, Double.parseDouble(probAC.getText()));
        StreetNetworkController.getInstance().addProbabilityAD(row, column, Double.parseDouble(probAD.getText()));
        StreetNetworkController.getInstance().addPhaseAB(row, column, Integer.parseInt(phaseAB.getText()));
        StreetNetworkController.getInstance().addPhaseAC(row, column, Integer.parseInt(phaseAC.getText()));
        StreetNetworkController.getInstance().addPhaseAD(row, column, Integer.parseInt(phaseAD.getText()));
        if (SourceA.isSelected())
        {
        	String[] rates = rateA.getText().split(",");
//            StreetNetworkController.getInstance().addSource(row, column, VDirection.North, Double.parseDouble(rateA.getText()));
            StreetNetworkController.getInstance().addSource(row, column, VDirection.North, Double.parseDouble(rates[1]));
            StreetNetworkController.getInstance().addFlowA(row, column, Double.parseDouble(rates[0]));
        }else
        {
            StreetNetworkController.getInstance().addFlowA(row, column, Double.parseDouble(rateA.getText()));
        }
        //B
        StreetNetworkController.getInstance().addProbabilityBA(row, column, Double.parseDouble(probBA.getText()));
        StreetNetworkController.getInstance().addProbabilityBC(row, column, Double.parseDouble(probBC.getText()));
        StreetNetworkController.getInstance().addProbabilityBD(row, column, Double.parseDouble(probBD.getText()));
        StreetNetworkController.getInstance().addPhaseBA(row, column, Integer.parseInt(phaseBA.getText()));
        StreetNetworkController.getInstance().addPhaseBC(row, column, Integer.parseInt(phaseBC.getText()));
        StreetNetworkController.getInstance().addPhaseBD(row, column, Integer.parseInt(phaseBD.getText()));
        if (SourceB.isSelected())
        {
        	String[] rates = rateB.getText().split(",");
//            StreetNetworkController.getInstance().addSource(row, column, VDirection.South, Double.parseDouble(rateB.getText()));
            StreetNetworkController.getInstance().addSource(row, column, VDirection.South, Double.parseDouble(rates[1]));
            StreetNetworkController.getInstance().addFlowB(row, column, Double.parseDouble(rates[0]));
        }else
        {
            StreetNetworkController.getInstance().addFlowB(row, column, Double.parseDouble(rateB.getText()));
        }
        //C
        StreetNetworkController.getInstance().addProbabilityCA(row, column, Double.parseDouble(probCA.getText()));
        StreetNetworkController.getInstance().addProbabilityCB(row, column, Double.parseDouble(probCB.getText()));
        StreetNetworkController.getInstance().addProbabilityCD(row, column, Double.parseDouble(probCD.getText()));
        StreetNetworkController.getInstance().addPhaseCA(row, column, Integer.parseInt(phaseCA.getText()));
        StreetNetworkController.getInstance().addPhaseCB(row, column, Integer.parseInt(phaseCB.getText()));
        StreetNetworkController.getInstance().addPhaseCD(row, column, Integer.parseInt(phaseCD.getText()));
        if (SourceC.isSelected())
        {
        	String[] rates = rateC.getText().split(",");
//            StreetNetworkController.getInstance().addSource(row, column, VDirection.West, Double.parseDouble(rateC.getText()));
            StreetNetworkController.getInstance().addSource(row, column, VDirection.West, Double.parseDouble(rates[1]));
            StreetNetworkController.getInstance().addFlowC(row, column, Double.parseDouble(rates[0]));
        }else
        {
            StreetNetworkController.getInstance().addFlowC(row, column, Double.parseDouble(rateC.getText()));
        }
        //D
        StreetNetworkController.getInstance().addProbabilityDA(row, column, Double.parseDouble(probDA.getText()));
        StreetNetworkController.getInstance().addProbabilityDB(row, column, Double.parseDouble(probDB.getText()));
        StreetNetworkController.getInstance().addProbabilityDC(row, column, Double.parseDouble(probDC.getText()));
        StreetNetworkController.getInstance().addPhaseDA(row, column, Integer.parseInt(phaseDA.getText()));
        StreetNetworkController.getInstance().addPhaseDB(row, column, Integer.parseInt(phaseDB.getText()));
        StreetNetworkController.getInstance().addPhaseDC(row, column, Integer.parseInt(phaseDC.getText()));
        if (SourceD.isSelected())
        {
        	String[] rates = rateD.getText().split(",");
//            StreetNetworkController.getInstance().addSource(row, column, VDirection.East, Double.parseDouble(rateD.getText()));
            StreetNetworkController.getInstance().addSource(row, column, VDirection.East, Double.parseDouble(rates[1]));
            StreetNetworkController.getInstance().addFlowD(row, column, Double.parseDouble(rates[0]));
        }else
        {
            StreetNetworkController.getInstance().addFlowD(row, column, Double.parseDouble(rateD.getText()));
        }
    }
}
