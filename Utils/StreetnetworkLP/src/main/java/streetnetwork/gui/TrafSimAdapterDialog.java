package streetnetwork.gui;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import streetnetwork.controller.StreetNetworkController;
import streetnetwork.viewmodels.VDirection;
import streetnetwork.viewmodels.VIntersection;

public class TrafSimAdapterDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	// for possible extension when called from JunctionSettingsDialog
	private int row;
	private int column;

	private final JPanel contentPanel = new JPanel();
	private JTextField txtStreetTraverseA;
	private JTextField txtStreetTraverseD;
	private JTextField txtStreetTraverseC;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_6;
	private JTextField txtStreetTraverseB;
	private JLabel label;
	private JTextField txtIntersectionTraverseTimeCD;
	private JTextField txtIntersectionTraverseTimeCB;
	private JTextField txtIntersectionTraverseTimeCA;
	private JTextField txtIntersectionTraverseTimeDA;
	private JTextField txtIntersectionTraverseTimeDC;
	private JTextField txtIntersectionTraverseTimeDB;
	private JTextField txtIntersectionTraverseTimeAC;
	private JTextField txtIntersectionTraverseTimeAB;
	private JTextField txtIntersectionTraverseTimeAD;
	private JTextField txtIntersectionTraverseTimeBC;
	private JTextField txtIntersectionTraverseTimeBA;
	private JTextField txtIntersectionTraverseTimeBD;
	private JLabel lblNewLabel_7;
	private JLabel lblCd;
	private JLabel lblCb;
	private JLabel lblNewLabel_8;
	private JLabel lblNewLabel_9;
	private JLabel lblNewLabel_10;
	private JLabel lblNewLabel_11;
	private JLabel lblNewLabel_12;
	private JLabel lblNewLabel_13;
	private JLabel lblNewLabel_14;
	private JLabel lblNewLabel_15;
	private JLabel lblNewLabel_16;
	private JLabel lblNewLabel_17;
	private JLabel lblNewLabel_18;
	private JLabel lblNewLabel_19;
	private JTextField txtServiceDelay;
	private JLabel lblNewLabel_20;
	private JLabel lblNewLabel_21;
	private JLabel lblNewLabel_22;
	private JTextField txtGreenPercentageC;
	private JTextField txtGreenPercentageD;
	private JLabel lblNewLabel_23;
	private JTextField txtGreenPercentageB;
	private JLabel lblNewLabel_24;
	private JTextField txtGreenPercentageA;
	private JTextField txtYellowDuration;
	private JButton exportButton;

	public TrafSimAdapterDialog(Frame owner) {
		this(owner, -1, -1);
	}

	/**
	 * To be called from @JunctionSettingsDialog
	 * 
	 * @param row
	 * @param column
	 */
	public TrafSimAdapterDialog(Frame owner, int row, int column) {
		super(owner, true);
		this.row = row;
		this.column = column;
		initComponents();
		// TODO 
		// initValues();
	}

	/**
	 * Create the dialog.
	 */
	private void initComponents() {
		setBounds(100, 100, 683, 515);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		{
			lblNewLabel_2 = new JLabel("Street traverse time");
		}
		{
			txtStreetTraverseC = new JTextField();
			txtStreetTraverseC.setColumns(10);
		}
		{
			txtStreetTraverseD = new JTextField();
			txtStreetTraverseD.setColumns(10);
		}
		{
			lblNewLabel_1 = new JLabel("Steet traverse time");
		}
		{
			lblNewLabel = new JLabel("Street traverse time");
		}
		{
			txtStreetTraverseA = new JTextField();
			txtStreetTraverseA.setColumns(10);
		}

		JLabel lblNewLabel_3 = new JLabel("A   ");

		JLabel lblNewLabel_4 = new JLabel("C   ");

		JLabel lblNewLabel_5 = new JLabel("B   ");
		lblNewLabel_6 = new JLabel("D   ");

		txtStreetTraverseB = new JTextField();
		txtStreetTraverseB.setColumns(10);
		label = new JLabel("Street traverse time");
		txtIntersectionTraverseTimeCD = new JTextField();
		txtIntersectionTraverseTimeCD.setColumns(10);
		txtIntersectionTraverseTimeCB = new JTextField();
		txtIntersectionTraverseTimeCB.setColumns(10);
		txtIntersectionTraverseTimeCA = new JTextField();
		txtIntersectionTraverseTimeCA.setColumns(10);
		txtIntersectionTraverseTimeDA = new JTextField();
		txtIntersectionTraverseTimeDA.setColumns(10);
		txtIntersectionTraverseTimeDC = new JTextField();
		txtIntersectionTraverseTimeDC.setColumns(10);
		txtIntersectionTraverseTimeDB = new JTextField();
		txtIntersectionTraverseTimeDB.setColumns(10);
		txtIntersectionTraverseTimeAC = new JTextField();
		txtIntersectionTraverseTimeAC.setColumns(10);
		txtIntersectionTraverseTimeAB = new JTextField();
		txtIntersectionTraverseTimeAB.setColumns(10);
		txtIntersectionTraverseTimeAD = new JTextField();
		txtIntersectionTraverseTimeAD.setColumns(10);
		txtIntersectionTraverseTimeBC = new JTextField();
		txtIntersectionTraverseTimeBC.setColumns(10);
		txtIntersectionTraverseTimeBA = new JTextField();
		txtIntersectionTraverseTimeBA.setColumns(10);
		txtIntersectionTraverseTimeBD = new JTextField();
		txtIntersectionTraverseTimeBD.setColumns(10);
		lblNewLabel_7 = new JLabel("CA");
		lblCd = new JLabel("CD");
		lblCb = new JLabel("CB");
		lblNewLabel_8 = new JLabel("DA");
		lblNewLabel_9 = new JLabel("DC");
		lblNewLabel_10 = new JLabel("DB");
		lblNewLabel_11 = new JLabel("Intersection traverse times");
		lblNewLabel_12 = new JLabel("Intersection traverse times");

		lblNewLabel_13 = new JLabel("BC");

		lblNewLabel_14 = new JLabel("BA");

		lblNewLabel_15 = new JLabel("BD");

		lblNewLabel_16 = new JLabel("AC");

		lblNewLabel_17 = new JLabel("AB");

		lblNewLabel_18 = new JLabel("AD");

		lblNewLabel_19 = new JLabel("Service delay");

		txtServiceDelay = new JTextField();
		txtServiceDelay.setColumns(10);

		lblNewLabel_20 = new JLabel("Yellow duration");

		lblNewLabel_21 = new JLabel("Green percentage");

		lblNewLabel_22 = new JLabel("Green percentage");

		txtGreenPercentageC = new JTextField();
		txtGreenPercentageC.setColumns(10);

		txtGreenPercentageD = new JTextField();
		txtGreenPercentageD.setColumns(10);

		lblNewLabel_23 = new JLabel("Green percentage");

		txtGreenPercentageB = new JTextField();
		txtGreenPercentageB.setColumns(10);

		lblNewLabel_24 = new JLabel("Green percentage");

		txtGreenPercentageA = new JTextField();
		txtGreenPercentageA.setColumns(10);

		txtYellowDuration = new JTextField();
		txtYellowDuration.setColumns(10);
		{
			exportButton = new JButton("Export");
			exportButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					exportButtonActionPerformed(arg0);
				}
			});
			getRootPane().setDefaultButton(exportButton);
		}
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup().addGap(49).addGroup(gl_contentPanel
						.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup().addGap(257).addComponent(lblNewLabel_3))
						.addGroup(gl_contentPanel.createSequentialGroup()
								.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
										.addGroup(gl_contentPanel.createSequentialGroup().addComponent(
												lblNewLabel_2).addGap(24))
								.addGroup(gl_contentPanel.createSequentialGroup().addGroup(gl_contentPanel
										.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_contentPanel.createSequentialGroup().addComponent(lblNewLabel_4)
												.addGap(18)
												.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
														.addComponent(lblNewLabel_21)
														.addComponent(txtStreetTraverseC, GroupLayout.PREFERRED_SIZE,
																43, GroupLayout.PREFERRED_SIZE)
														.addComponent(txtGreenPercentageC, GroupLayout.PREFERRED_SIZE,
																44, GroupLayout.PREFERRED_SIZE)))
										.addGroup(gl_contentPanel.createSequentialGroup().addComponent(lblNewLabel_20)
												.addPreferredGap(ComponentPlacement.UNRELATED)
												.addComponent(txtYellowDuration, GroupLayout.PREFERRED_SIZE, 0,
														Short.MAX_VALUE))
										.addGroup(gl_contentPanel.createSequentialGroup().addComponent(lblNewLabel_19)
												.addGap(18).addComponent(txtServiceDelay, 0, 0, Short.MAX_VALUE)))
										.addGap(27)))
								.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_contentPanel.createSequentialGroup()
												.addGroup(gl_contentPanel.createParallelGroup(
														Alignment.LEADING).addComponent(lblNewLabel_7)
												.addComponent(lblCd).addComponent(lblCb))
												.addPreferredGap(ComponentPlacement.RELATED)
												.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
														.addComponent(lblNewLabel)
														.addGroup(gl_contentPanel.createSequentialGroup()
																.addGroup(gl_contentPanel
																		.createParallelGroup(Alignment.LEADING)
																		.addGroup(gl_contentPanel
																				.createParallelGroup(Alignment.TRAILING,
																						false)
																				.addComponent(
																						txtIntersectionTraverseTimeBC,
																						Alignment.LEADING, 0, 0,
																						Short.MAX_VALUE)
																				.addComponent(
																						txtIntersectionTraverseTimeCB,
																						Alignment.LEADING, 0, 0,
																						Short.MAX_VALUE)
																				.addComponent(lblNewLabel_16,
																						Alignment.LEADING)
																				.addComponent(
																						txtIntersectionTraverseTimeAC,
																						Alignment.LEADING, 0, 0,
																						Short.MAX_VALUE)
																				.addComponent(txtStreetTraverseA,
																						Alignment.LEADING,
																						GroupLayout.DEFAULT_SIZE, 44,
																						Short.MAX_VALUE)
																				.addComponent(
																						txtIntersectionTraverseTimeCA,
																						Alignment.LEADING,
																						GroupLayout.DEFAULT_SIZE, 44,
																						Short.MAX_VALUE)
																				.addComponent(
																						txtIntersectionTraverseTimeCD,
																						Alignment.LEADING, 0, 0,
																						Short.MAX_VALUE))
																		.addComponent(lblNewLabel_13))
																.addGroup(gl_contentPanel
																		.createParallelGroup(Alignment.LEADING)
																		.addGroup(gl_contentPanel
																				.createSequentialGroup().addGap(20)
																				.addComponent(lblNewLabel_14))
																		.addGroup(gl_contentPanel
																				.createSequentialGroup().addGap(18)
																				.addComponent(
																						txtIntersectionTraverseTimeBA,
																						GroupLayout.PREFERRED_SIZE, 45,
																						GroupLayout.PREFERRED_SIZE))
																		.addGroup(gl_contentPanel
																				.createSequentialGroup().addGap(18)
																				.addGroup(gl_contentPanel
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addComponent(lblNewLabel_17)
																						.addComponent(
																								txtIntersectionTraverseTimeAB,
																								GroupLayout.PREFERRED_SIZE,
																								45,
																								GroupLayout.PREFERRED_SIZE)))))
														.addComponent(label, GroupLayout.PREFERRED_SIZE, 107,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(txtStreetTraverseB, GroupLayout.PREFERRED_SIZE,
																44, GroupLayout.PREFERRED_SIZE)))
										.addComponent(lblNewLabel_11))
								.addGap(18)
								.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
										.addComponent(lblNewLabel_23).addComponent(lblNewLabel_15)
										.addComponent(lblNewLabel_18)
										.addComponent(txtGreenPercentageA, GroupLayout.PREFERRED_SIZE, 45,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(lblNewLabel_24)
										.addComponent(txtIntersectionTraverseTimeAD, GroupLayout.PREFERRED_SIZE, 44,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(txtIntersectionTraverseTimeBD, GroupLayout.PREFERRED_SIZE, 45,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(txtGreenPercentageB, GroupLayout.PREFERRED_SIZE, 43,
												GroupLayout.PREFERRED_SIZE)
										.addGroup(gl_contentPanel.createSequentialGroup().addGroup(gl_contentPanel
												.createParallelGroup(Alignment.LEADING).addComponent(lblNewLabel_12)
												.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
														.addGroup(gl_contentPanel.createSequentialGroup()
																.addComponent(lblNewLabel_10)
																.addPreferredGap(ComponentPlacement.RELATED)
																.addComponent(txtIntersectionTraverseTimeDB,
																		GroupLayout.PREFERRED_SIZE, 45,
																		GroupLayout.PREFERRED_SIZE))
														.addGroup(gl_contentPanel.createSequentialGroup()
																.addComponent(lblNewLabel_9)
																.addPreferredGap(ComponentPlacement.RELATED)
																.addComponent(txtIntersectionTraverseTimeDC,
																		GroupLayout.PREFERRED_SIZE, 45,
																		GroupLayout.PREFERRED_SIZE))
														.addGroup(gl_contentPanel.createSequentialGroup()
																.addComponent(lblNewLabel_8)
																.addPreferredGap(ComponentPlacement.RELATED)
																.addComponent(txtIntersectionTraverseTimeDA,
																		GroupLayout.PREFERRED_SIZE, 45,
																		GroupLayout.PREFERRED_SIZE))))
												.addPreferredGap(ComponentPlacement.RELATED)
												.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
														.addGroup(gl_contentPanel.createSequentialGroup().addGap(18)
																.addComponent(lblNewLabel_1))
														.addGroup(gl_contentPanel.createSequentialGroup().addGap(19)
																.addGroup(gl_contentPanel
																		.createParallelGroup(Alignment.LEADING)
																		.addComponent(lblNewLabel_22)
																		.addGroup(gl_contentPanel
																				.createSequentialGroup()
																				.addComponent(txtStreetTraverseD,
																						GroupLayout.PREFERRED_SIZE, 44,
																						GroupLayout.PREFERRED_SIZE)
																				.addGap(60).addComponent(lblNewLabel_6))
																		.addComponent(txtGreenPercentageD,
																				GroupLayout.PREFERRED_SIZE, 44,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(exportButton))))))))
						.addGap(69))
				.addGroup(gl_contentPanel.createSequentialGroup().addGap(270).addComponent(lblNewLabel_5)
						.addContainerGap(389, Short.MAX_VALUE)));
		gl_contentPanel.setVerticalGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPanel.createSequentialGroup().addContainerGap(30, Short.MAX_VALUE)
						.addComponent(lblNewLabel_3).addGap(18)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE).addComponent(lblNewLabel)
								.addComponent(lblNewLabel_24))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPanel.createSequentialGroup()
										.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
												.addComponent(txtStreetTraverseA, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(txtGreenPercentageA, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
										.addComponent(lblNewLabel_16).addComponent(lblNewLabel_18)
										.addComponent(lblNewLabel_17, GroupLayout.PREFERRED_SIZE, 14,
												GroupLayout.PREFERRED_SIZE)))
								.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
										.addComponent(lblNewLabel_19).addComponent(txtServiceDelay,
												GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(txtIntersectionTraverseTimeAC, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtIntersectionTraverseTimeAD, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtIntersectionTraverseTimeAB, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_20).addComponent(txtYellowDuration,
										GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addGap(19)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_contentPanel.createSequentialGroup()
										.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
												.addComponent(lblNewLabel_11).addComponent(lblNewLabel_2))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
												.addComponent(txtIntersectionTraverseTimeCA, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(lblNewLabel_7)
												.addComponent(txtStreetTraverseC, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(lblNewLabel_4))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
												.addComponent(txtIntersectionTraverseTimeCD, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(lblCd).addComponent(lblNewLabel_21))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
												.addComponent(txtIntersectionTraverseTimeCB, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(lblCb).addComponent(txtGreenPercentageC,
														GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE)))
								.addGroup(gl_contentPanel.createSequentialGroup()
										.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
												.addComponent(lblNewLabel_12).addComponent(lblNewLabel_1))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
												.addComponent(lblNewLabel_6)
												.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
														.addComponent(lblNewLabel_8)
														.addComponent(txtIntersectionTraverseTimeDA,
																GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(txtStreetTraverseD, GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
												.addComponent(txtIntersectionTraverseTimeDC, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(lblNewLabel_9).addComponent(lblNewLabel_22))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
												.addComponent(txtIntersectionTraverseTimeDB, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(lblNewLabel_10).addComponent(txtGreenPercentageD,
														GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE))))
						.addGap(23)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_contentPanel.createSequentialGroup()
										.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
												.addComponent(lblNewLabel_13).addComponent(lblNewLabel_15)
												.addComponent(lblNewLabel_14))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
												.addComponent(txtIntersectionTraverseTimeBC, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(txtIntersectionTraverseTimeBD, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(txtIntersectionTraverseTimeBA, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
												.addComponent(label).addComponent(lblNewLabel_23))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
												.addComponent(txtStreetTraverseB, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(txtGreenPercentageB, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(ComponentPlacement.UNRELATED).addComponent(lblNewLabel_5))
								.addComponent(exportButton))
						.addGap(56)));
		contentPanel.setLayout(gl_contentPanel);
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout.createSequentialGroup()
						.addComponent(contentPanel, GroupLayout.DEFAULT_SIZE, 632, Short.MAX_VALUE).addContainerGap()));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addComponent(contentPanel,
				GroupLayout.DEFAULT_SIZE, 536, Short.MAX_VALUE));
		getContentPane().setLayout(groupLayout);
	}

	private void initValues() {
		// TODO when called from JunctionSettingsDialog

		VIntersection intersection = StreetNetworkController.getInstance().getIntersection(row, column);
		// A
		txtIntersectionTraverseTimeAC.setText("" + intersection.getTraverseTime(VIntersection.A, VIntersection.C));
		txtIntersectionTraverseTimeAB.setText("" + intersection.getTraverseTime(VIntersection.A, VIntersection.B));
		txtIntersectionTraverseTimeAD.setText("" + intersection.getTraverseTime(VIntersection.A, VIntersection.D));
		// B
		txtIntersectionTraverseTimeBC.setText("" + intersection.getTraverseTime(VIntersection.B, VIntersection.C));
		txtIntersectionTraverseTimeBA.setText("" + intersection.getTraverseTime(VIntersection.B, VIntersection.A));
		txtIntersectionTraverseTimeBD.setText("" + intersection.getTraverseTime(VIntersection.B, VIntersection.D));
		// C
		txtIntersectionTraverseTimeCA.setText("" + intersection.getTraverseTime(VIntersection.C, VIntersection.A));
		txtIntersectionTraverseTimeCD.setText("" + intersection.getTraverseTime(VIntersection.C, VIntersection.D));
		txtIntersectionTraverseTimeCB.setText("" + intersection.getTraverseTime(VIntersection.C, VIntersection.B));
		// D
		txtIntersectionTraverseTimeDA.setText("" + intersection.getTraverseTime(VIntersection.D, VIntersection.A));
		txtIntersectionTraverseTimeDC.setText("" + intersection.getTraverseTime(VIntersection.D, VIntersection.C));
		txtIntersectionTraverseTimeDB.setText("" + intersection.getTraverseTime(VIntersection.D, VIntersection.B));

		// service delay
		txtServiceDelay.setText("" + intersection.getServiceDelay());

		// yellow duration
		txtYellowDuration.setText("" + intersection.getYellowDuration());

		// green percentage per phase
		txtGreenPercentageA.setText("" + intersection.getGreenDurationPart(VIntersection.A));
		txtGreenPercentageB.setText("" + intersection.getGreenDurationPart(VIntersection.B));
		txtGreenPercentageC.setText("" + intersection.getGreenDurationPart(VIntersection.C));
		txtGreenPercentageD.setText("" + intersection.getGreenDurationPart(VIntersection.D));

		// street traverse times
		// TODO getter for streetTraverseTime...
		// txtStreetTraverseA.setText("" +
		// intersection.getStreetTraverseTime(VDirection.North));
		// txtStreetTraverseB.setText("" +
		// intersection.getStreetTraverseTime(VDirection.South));
		// txtStreetTraverseC.setText("" +
		// intersection.getStreetTraverseTime(VDirection.West));
		// txtStreetTraverseD.setText("" +
		// intersection.getStreetTraverseTime(VDirection.East));
	}

	private void exportButtonActionPerformed(ActionEvent evt) {
		// TODO test the setSettings method
		setSettings();
		JFileChooser fch = new JFileChooser();

		int retval = fch.showSaveDialog(this);
		if (retval == JFileChooser.APPROVE_OPTION) {
			File selected = fch.getSelectedFile();
			StreetNetworkController.getInstance().saveTrafSimXML(selected.getAbsolutePath());
		}
		dispose();
	}

	private void setSettings() {
		
		StreetNetworkController controllerInstance = StreetNetworkController.getInstance();
		
		double[][] traverseTimes = new double[4][4];
		// A
		traverseTimes[VIntersection.A][VIntersection.C] = checkAndParseDouble(txtIntersectionTraverseTimeAC.getText());
		traverseTimes[VIntersection.A][VIntersection.B] = checkAndParseDouble(txtIntersectionTraverseTimeAB.getText());
		traverseTimes[VIntersection.A][VIntersection.D] = checkAndParseDouble(txtIntersectionTraverseTimeAD.getText());
		// B
		traverseTimes[VIntersection.B][VIntersection.C] = checkAndParseDouble(txtIntersectionTraverseTimeBC.getText());
		traverseTimes[VIntersection.B][VIntersection.A] = checkAndParseDouble(txtIntersectionTraverseTimeBA.getText());
		traverseTimes[VIntersection.B][VIntersection.D] = checkAndParseDouble(txtIntersectionTraverseTimeBD.getText());
		// C
		traverseTimes[VIntersection.C][VIntersection.A] = checkAndParseDouble(txtIntersectionTraverseTimeCA.getText());
		traverseTimes[VIntersection.C][VIntersection.D] = checkAndParseDouble(txtIntersectionTraverseTimeCD.getText());
		traverseTimes[VIntersection.C][VIntersection.B] = checkAndParseDouble(txtIntersectionTraverseTimeCB.getText());
		// D
		traverseTimes[VIntersection.D][VIntersection.A] = checkAndParseDouble(txtIntersectionTraverseTimeDA.getText());
		traverseTimes[VIntersection.D][VIntersection.C] = checkAndParseDouble(txtIntersectionTraverseTimeDC.getText());
		traverseTimes[VIntersection.D][VIntersection.B] = checkAndParseDouble(txtIntersectionTraverseTimeDB.getText());

		int rows = controllerInstance.getRows();
		int columns = controllerInstance.getColumns();

		VIntersection intersection;

		for (int iRow = 0; iRow < rows; iRow++) {
			for (int iColumn = 0; iColumn < columns; iColumn++) {
				intersection = controllerInstance.getIntersection(iRow, iColumn);
				intersection.setTraverseTimes(traverseTimes);

				// service delay
				intersection.setServiceDelay(checkAndParseDouble(txtServiceDelay.getText()));

				// yellow duration
				intersection.setYellowDuration(checkAndParseDouble(txtYellowDuration.getText()));

				// green percentage per phase
				intersection.setGreenDurationPart(VIntersection.A, checkAndParseDouble(txtGreenPercentageA.getText()));
				intersection.setGreenDurationPart(VIntersection.B, checkAndParseDouble(txtGreenPercentageB.getText()));
				intersection.setGreenDurationPart(VIntersection.C, checkAndParseDouble(txtGreenPercentageC.getText()));
				intersection.setGreenDurationPart(VIntersection.D, checkAndParseDouble(txtGreenPercentageD.getText()));

				// street traverse times
				// TODO when used from the JunctionSettingsDialog the street
				// traverse times may be overwritten | it is
				// possible to set one street from two different
				// intersections...(init the UI with the specified value)
				controllerInstance.getStreet(iRow, iColumn, VDirection.North)
						.setTraverseTime(checkAndParseDouble(txtStreetTraverseA.getText()));
				controllerInstance.getStreet(iRow, iColumn, VDirection.South)
						.setTraverseTime(checkAndParseDouble(txtStreetTraverseB.getText()));
				controllerInstance.getStreet(iRow, iColumn, VDirection.West)
						.setTraverseTime(checkAndParseDouble(txtStreetTraverseC.getText()));
				controllerInstance.getStreet(iRow, iColumn, VDirection.East)
						.setTraverseTime(checkAndParseDouble(txtStreetTraverseD.getText()));
			}
		}
	}

	private double checkAndParseDouble(String strNumber) {
		if (strNumber != null && strNumber.length() > 0) {
			try {
				return Double.parseDouble(strNumber);
			} catch (Exception e) {
				return Double.NEGATIVE_INFINITY;
			}
		} else {
			return Double.NEGATIVE_INFINITY;
		}
	}
}
