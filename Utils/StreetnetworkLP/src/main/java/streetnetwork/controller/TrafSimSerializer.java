package streetnetwork.controller;

import at.fhv.itm14.trafsim.persistence.model.ElementDTO;
import at.fhv.itm14.trafsim.persistence.model.IntersectionControllerDTO;
import at.fhv.itm14.trafsim.persistence.model.IntersectionDTO;
import at.fhv.itm14.trafsim.persistence.model.IntersectionPhaseDTO;
import at.fhv.itm14.trafsim.persistence.model.NumericalDistDTO;
import at.fhv.itm14.trafsim.persistence.model.OneWayStreetDTO;
import at.fhv.itm14.trafsim.persistence.model.ScenarioDTO;
import at.fhv.itm14.trafsim.persistence.model.SinkDTO;
import at.fhv.itm14.trafsim.persistence.model.SourceDTO;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.JAXB;
import streetnetwork.viewmodels.VDirection;
import streetnetwork.viewmodels.VIntersection;
import streetnetwork.viewmodels.VStreet;

public class TrafSimSerializer {

	private static TrafSimSerializer instance;

	private TrafSimSerializer() {
	}

	public static TrafSimSerializer getInstance() {
		if (instance == null) {
			instance = new TrafSimSerializer();
		}
		return instance;
	}

	public void saveScenario(String fileName, VIntersection[][] intersections, int rows, int cols, VStreet[][][] streets) {
		ScenarioDTO scenario = new ScenarioDTO();
		scenario.setIntersections(new LinkedList<>());
		scenario.setSinks(new LinkedList<>());
		scenario.setSources(new LinkedList<>());
		scenario.setStreets(new LinkedList<>());

		HashMap<Integer, OneWayStreetDTO> streetsDir1 = new HashMap<>();
		HashMap<Integer, OneWayStreetDTO> streetsDir2 = new HashMap<>();

		for (int row = 0; row < rows; row++) {
			for (int col = 0; col < cols; col++) {
				IntersectionDTO intersection = new IntersectionDTO();
				intersection.setName("Intersection_" + row + "_" + col);
				intersection.setSize(4);
				intersection.setServiceDelay(intersections[row][col].getServiceDelay());

				// Connect streets
				intersection.setIn(new LinkedList<>());
				intersection.setOut(new LinkedList<>());
				// Create Streets
				for (VDirection dir : VDirection.values()) {
					int streetId = streets[row][col][dir.ordinal()].getId();
					if (!streetsDir1.containsKey(streetId)) {
						OneWayStreetDTO street = new OneWayStreetDTO();
						street.setName("Street_Dir1_" + row + "_" + col + "_" + dir);
						street.setTraverseTime(streets[row][col][dir.ordinal()].getTraverseTime());
						streetsDir1.put(streetId, street);
					}
					if (!streetsDir2.containsKey(streetId)) {
						OneWayStreetDTO street = new OneWayStreetDTO();
						street.setName("Street_Dir2_" + row + "_" + col + "_" + dir);
						street.setTraverseTime(streets[row][col][dir.ordinal()].getTraverseTime());
						streetsDir2.put(streetId, street);
					}
					SinkDTO sink = null;
					switch (dir) {
						case North:
							if (row == 0) {
								sink = new SinkDTO();
								sink.setName("Sink_" + row + "_" + col + "_" + dir);
							}
							if (intersections[row][col].getSource(dir) != null) {
								SourceDTO source = createSource(row, col, intersections[row][col].getSource(dir).getRate(), streetsDir1.get(streetId));
								scenario.getSources().add(source);
							}
							intersection.getIn().add(new ElementDTO(dir.ordinal(), streetsDir1.get(streetId)));
							streetsDir1.get(streetId).setConsumer(intersection);
							intersection.getOut().add(new ElementDTO(dir.ordinal(), streetsDir2.get(streetId)));
							if (sink != null) {
								streetsDir2.get(streetId).setConsumer(sink);
							}
							break;
						case West:
							if (col == 0) {
								sink = new SinkDTO();
								sink.setName("Sink_" + row + "_" + col + "_" + dir);
							}
							if (intersections[row][col].getSource(dir) != null) {
								SourceDTO source = createSource(row, col, intersections[row][col].getSource(dir).getRate(), streetsDir1.get(streetId));
								scenario.getSources().add(source);
							}
							intersection.getIn().add(new ElementDTO(dir.ordinal(), streetsDir1.get(streetId)));
							streetsDir1.get(streetId).setConsumer(intersection);
							intersection.getOut().add(new ElementDTO(dir.ordinal(), streetsDir2.get(streetId)));
							if (sink != null) {
								streetsDir2.get(streetId).setConsumer(sink);
							}
							break;

						case South:
							if (row == (rows - 1)) {
								sink = new SinkDTO();
								sink.setName("Sink_" + row + "_" + col + "_" + dir);
							}
							if (intersections[row][col].getSource(dir) != null) {
								SourceDTO source = createSource(row, col, intersections[row][col].getSource(dir).getRate(), streetsDir2.get(streetId));
								scenario.getSources().add(source);
							}
							intersection.getIn().add(new ElementDTO(dir.ordinal(), streetsDir2.get(streetId)));
							streetsDir2.get(streetId).setConsumer(intersection);
							intersection.getOut().add(new ElementDTO(dir.ordinal(), streetsDir1.get(streetId)));
							if (sink != null) {
								streetsDir1.get(streetId).setConsumer(sink);
							}
							break;
						case East:
							if (col == (cols - 1)) {
								sink = new SinkDTO();
								sink.setName("Sink_" + row + "_" + col + "_" + dir);
							}
							if (intersections[row][col].getSource(dir) != null) {
								SourceDTO source = createSource(row, col, intersections[row][col].getSource(dir).getRate(), streetsDir2.get(streetId));
								scenario.getSources().add(source);
							}
							intersection.getIn().add(new ElementDTO(dir.ordinal(), streetsDir2.get(streetId)));
							streetsDir2.get(streetId).setConsumer(intersection);
							intersection.getOut().add(new ElementDTO(dir.ordinal(), streetsDir1.get(streetId)));
							if (sink != null) {
								streetsDir1.get(streetId).setConsumer(sink);
							}
							break;
					}
					if (sink != null) {
						scenario.getSinks().add(sink);
					}
				}

				// Create Connections
				intersection.setConnectionQueues(new LinkedList<>());
				// From x to y with one queue
				for (int from = 0; from < 4; from++) {
					for (int to = 0; to < 4; to++) {
						if (to == from) {
							continue;
						}
						IntersectionDTO.ConnectionQueueDTO connQueue = new IntersectionDTO.ConnectionQueueDTO();
						connQueue.inIndex = from;
						connQueue.outConnection = new LinkedList<>();
						IntersectionDTO.OutConnectionDTO out = new IntersectionDTO.OutConnectionDTO();
						out.outIndex = to;
						out.probability = intersections[row][col].getProbability(from, to);
						out.traverseTime = intersections[row][col].getTraverseTime(from, to);
						connQueue.outConnection.add(out);
						intersection.getConnectionQueues().add(connQueue);
					}
				}

				// Create Controller
				IntersectionControllerDTO ctrl = new IntersectionControllerDTO();
				// Create Controller Phases
				List<IntersectionPhaseDTO> phases = new LinkedList<>();
				for (int p = 1; p <= intersections[row][col].getNumPhases(); p++) {
					IntersectionPhaseDTO phase = new IntersectionPhaseDTO();
					phase.setGreenDurationPart(intersections[row][col].getGreenDurationPart(p));
					phase.setYellowDuration(intersections[row][col].getYellowDuration());
					phase.setConnections(new LinkedList<>());
					for (int[] conn : intersections[row][col].getPhaseConnections(p)) {
						IntersectionPhaseDTO.PhaseConnectionDTO phaseConn = new IntersectionPhaseDTO.PhaseConnectionDTO();
						phaseConn.inIndex = conn[0];
						phaseConn.outIndex = conn[1];
						phase.getConnections().add(phaseConn);
					}
					phases.add(phase);
				}
				ctrl.setPhases(phases);
				intersection.setController(ctrl);

				scenario.getIntersections().add(intersection);
			}
		}

		scenario.getStreets().addAll(streetsDir1.values());
		scenario.getStreets().addAll(streetsDir2.values());

		File scenarioFile = new File(fileName);
		JAXB.marshal(scenario, scenarioFile);
	}

	private static SourceDTO createSource(int row, int col, double rate, OneWayStreetDTO consumer) {
		SourceDTO source = new SourceDTO();
		source.setName("Source_" + row + "_" + col);
		NumericalDistDTO dist = new NumericalDistDTO();
		dist.setName("Dist_" + row + "_" + col);
		dist.setType("desmoj.core.dist.ContDistConstant");
		dist.setValues(new Double[]{rate});
		source.setDist(dist);
		source.setConsumer(consumer);
		return source;
	}
}
