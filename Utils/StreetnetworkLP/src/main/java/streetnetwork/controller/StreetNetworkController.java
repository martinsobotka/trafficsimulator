package streetnetwork.controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;
import lpsolve.LpSolve;
import lpsolve.LpSolveException;
import streetnetwork.solver.Intersection;
import streetnetwork.solver.LpBuilder;
import streetnetwork.solver.Source;
import streetnetwork.solver.Street;
import streetnetwork.viewmodels.VIntersection;
import streetnetwork.viewmodels.VSource;
import streetnetwork.viewmodels.VDirection;
import streetnetwork.viewmodels.VStreet;

/**
 *
 * @author Markus Mohanty <markus.mo at gmx.net>
 */
public class StreetNetworkController
{
    private static VIntersection[][] vintersections;
	private static VStreet[][][] vstreets;
    private static int rows;
    private static int columns;
    private static StreetNetworkController INSTANCE;
    public static String LPproblem;
    public static boolean doNotActivateSolver = true;

    public static StreetNetworkController getInstance()
    {
        if (INSTANCE == null)
        {
            INSTANCE = new StreetNetworkController();
            LPproblem = null;
        }
        return INSTANCE;
    }

    public static void initialize(int srows, int scolumns)
    {
        rows = srows;
        columns = scolumns;
        vintersections = new VIntersection[srows][scolumns];
		int id = 0;
        for (int row = 0; row < rows; row++)
        {
            for (int column = 0; column < columns; column++)
            {
                vintersections[row][column] = new VIntersection(id++, row, column);
                vintersections[row][column].setDefault();
            }
        }
		createStreets(rows, columns);
    }
	
	public static void createStreets(int rows, int cols) {
		int id = 1000;
		vstreets = new VStreet[rows][cols][4];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
				if (row == 0) {
					vstreets[row][col][VDirection.North.ordinal()] = new VStreet(id++, row, col, VDirection.North);
				} else {
					vstreets[row][col][VDirection.North.ordinal()] = vstreets[row - 1][col][VDirection.South.ordinal()];
				}
				if (col == 0) {
					vstreets[row][col][VDirection.West.ordinal()] = new VStreet(id++, row, col, VDirection.West);
				} else {
					vstreets[row][col][VDirection.West.ordinal()] = vstreets[row][col - 1][VDirection.East.ordinal()];
				}
				vstreets[row][col][VDirection.East.ordinal()] = new VStreet(id++, row, col, VDirection.East);
				vstreets[row][col][VDirection.South.ordinal()] = new VStreet(id++, row, col, VDirection.South);
			}
		}
	}

    public Triplet generateLP()
    {
        Intersection[][] intersections = new Intersection[rows][columns];
        List<Street> streets = new ArrayList<Street>();
        List<Source> sources = new ArrayList<Source>();

        //init intersections
        for (int row = 0; row < rows; row++)
        {
            for (int column = 0; column < columns; column++)
            {
                if (vintersections[row][column].isActive())
                {
                	double[] sRates = new double[4];
                	if (vintersections[row][column].getSourceNorth() != null)
                		sRates[0] = vintersections[row][column].getSourceNorth().getRate();
                	else
                		sRates[0] = 0;
                	if (vintersections[row][column].getSourceSouth() != null)
                		sRates[1] = vintersections[row][column].getSourceSouth().getRate();
                	else
                		sRates[1] = 0;
                	if (vintersections[row][column].getSourceWest() != null)
                		sRates[2] = vintersections[row][column].getSourceWest().getRate();
                	else
                		sRates[2] = 0;
                	if (vintersections[row][column].getSourceEast() != null)
                		sRates[3] = vintersections[row][column].getSourceEast().getRate();
                	else
                		sRates[3] = 0;
                	
                    intersections[row][column] = new Intersection(vintersections[row][column].getNumPhases(),
                            vintersections[row][column].getProbAD(), vintersections[row][column].getProbAC(), vintersections[row][column].getProbAB(),
                            vintersections[row][column].getProbBC(), vintersections[row][column].getProbBD(), vintersections[row][column].getProbBA(),
                            vintersections[row][column].getProbCA(), vintersections[row][column].getProbCB(), vintersections[row][column].getProbCD(),
                            vintersections[row][column].getProbDB(), vintersections[row][column].getProbDA(), vintersections[row][column].getProbDC(),
                            vintersections[row][column].getPhaseAD(), vintersections[row][column].getPhaseAC(), vintersections[row][column].getPhaseAB(),
                            vintersections[row][column].getPhaseBC(), vintersections[row][column].getPhaseBD(), vintersections[row][column].getPhaseBA(),
                            vintersections[row][column].getPhaseCA(), vintersections[row][column].getPhaseCB(), vintersections[row][column].getPhaseCD(),
                            vintersections[row][column].getPhaseDB(), vintersections[row][column].getPhaseDA(), vintersections[row][column].getPhaseDC(),
                            vintersections[row][column].getFlowA(),vintersections[row][column].getFlowB(),vintersections[row][column].getFlowC(),vintersections[row][column].getFlowD(),
                            sRates[0],sRates[1],sRates[2],sRates[3]);

                    //add sources
                    if (vintersections[row][column].getSourceNorth() != null)
                    {
                        sources.add(new Source(intersections[row][column].A, vintersections[row][column].getSourceNorth().getRate()));
                    }
                    if (vintersections[row][column].getSourceWest() != null)
                    {
                        sources.add(new Source(intersections[row][column].C, vintersections[row][column].getSourceWest().getRate()));
                    }
                    if (vintersections[row][column].getSourceSouth() != null)
                    {
                        sources.add(new Source(intersections[row][column].B, vintersections[row][column].getSourceSouth().getRate()));
                    }
                    if (vintersections[row][column].getSourceEast() != null)
                    {
                        sources.add(new Source(intersections[row][column].D, vintersections[row][column].getSourceEast().getRate()));
                    }
                }
            }
        }

        //add bidirectional streets
        for (int row = 0; row < rows; row++)
        {
            for (int column = 0; column < columns; column++)
            {
                if (vintersections[row][column].isActive())
                {
/* HV
                    //check for neighbors
                    //only down and right
                	if (row == 0 && column == 0)
                    {

                        if (intersections[row + 1][column] != null
                                && intersections[row][column + 1] != null)
                        {
                            //down
                            streets.add(new Street(intersections[row][column].B, intersections[row + 1][column].A, vintersections[row][column].getFlowB()));
                            streets.add(new Street(intersections[row + 1][column].A, intersections[row][column].B, vintersections[row + 1][column].getFlowA()));
                            //right
                            streets.add(new Street(intersections[row][column].D, intersections[row][column + 1].C, vintersections[row][column].getFlowD()));
                            streets.add(new Street(intersections[row][column + 1].C, intersections[row][column].D, vintersections[row][column + 1].getFlowC()));

                        }
                    }
                    // only up and left
                    if (row == rows - 1 && column == columns - 1)
                    {
                        if (intersections[row - 1][column] != null
                                && intersections[row][column - 1] != null)
                        {
                            streets.add(new Street(intersections[row][column].A, intersections[row - 1][column].B, vintersections[row][column].getFlowA()));
                            streets.add(new Street(intersections[row - 1][column].B, intersections[row][column].A, vintersections[row - 1][column].getFlowB()));

                            streets.add(new Street(intersections[row][column].C, intersections[row][column - 1].D, vintersections[row][column].getFlowC()));
                            streets.add(new Street(intersections[row][column - 1].D, intersections[row][column].C, vintersections[row][column - 1].getFlowD()));
                        }
                    }

                    //all directions
                    if (row < rows - 1 && row > 0 && column < column - 1 && column > 0)
                    {
                        if (intersections[row + 1][column] != null
                                && intersections[row][column + 1] != null
                                && intersections[row - 1][column] != null
                                && intersections[row][column - 1] != null)
                        {
*/
                			//down
                        	if (row<rows-1) {
                        		if (intersections[row + 1][column] != null)  {
                        			streets.add(new Street(intersections[row][column].B, intersections[row + 1][column].A, vintersections[row+1][column].getFlowA()));
                 //       			streets.add(new Street(intersections[row + 1][column].A, intersections[row][column].B, vintersections[row + 1][column].getFlowA()));
                        		}
                        	}
                        	//right
                        	if (column<columns-1) {
                        		if (intersections[row][column+1] != null)  {
                        			streets.add(new Street(intersections[row][column].D, intersections[row][column + 1].C, vintersections[row][column+1].getFlowC()));
                 //       			streets.add(new Street(intersections[row][column + 1].C, intersections[row][column].D, vintersections[row][column + 1].getFlowC()));
                        		}
                        	}
                        	//up
                        	if (row>0) {
                        		if (intersections[row - 1][column] != null)  {

                        			streets.add(new Street(intersections[row][column].A, intersections[row - 1][column].B, vintersections[row-1][column].getFlowB()));
                 //           		streets.add(new Street(intersections[row - 1][column].B, intersections[row][column].A, vintersections[row - 1][column].getFlowB()));
                        		}
                        	}
                        	//left
                        	if (column>0) {
                        		if (intersections[row][column-1] != null)  {
                        			streets.add(new Street(intersections[row][column].C, intersections[row][column - 1].D, vintersections[row][column-1].getFlowD()));
                 //       			streets.add(new Street(intersections[row][column - 1].D, intersections[row][column].C, vintersections[row][column - 1].getFlowD()));
                        		}
                        	}
                        
                    
                }
            }
        }
        String lpString = new LpBuilder().createLp(rows, columns, intersections, streets, sources);
        LPproblem = lpString;
        
        return new Triplet(intersections, streets, sources);
        
    }
    
    public String solveLP() throws LpSolveException, FileNotFoundException, UnsupportedEncodingException, IOException
    {
        Triplet triplet = generateLP();
        String filename = "model.lp";
        PrintWriter writer = new PrintWriter(filename, "UTF-8");
        writer.print(LPproblem);
        writer.close();

        StringBuilder builder = new StringBuilder();

        if (doNotActivateSolver)
        	return "use external LP solver";
        //Get operating system
        String OS = System.getProperty("os.name");
        
        if (OS.equals("Mac OS X"))
        {
            //builder.append(lpString);
            Runtime rt = Runtime.getRuntime();
            String path = StreetNetworkController.class.getProtectionDomain().getCodeSource().getLocation().getPath();
            //lp_solve installed with http://brew.sh/
            Process exec = rt.exec("/usr/local/bin/lp_solve " + "model.lp");
            InputStreamReader reader = new InputStreamReader(exec.getInputStream());
            BufferedReader br = new BufferedReader(reader);
            String line = null;
            while ((line = br.readLine()) != null)
            {
                if (line.contains("Value of objective function"))
                {
                    builder.append(line).append("\n\r");
                }
                else
                {
                    for (Source source : triplet.getSources())
                    {
                        if (line.contains(source.getName()))
                        {
                            builder.append(line).append("\n\r");
                        }
                    }
                }
            }
        }
        else if( OS.contains("nix") || OS.contains("nux") || OS.contains("aix"))
        {
            Runtime rt = Runtime.getRuntime();
            String path = StreetNetworkController.class.getProtectionDomain().getCodeSource().getLocation().getPath();
            //installed with "apt-get install lp-solve" on ubuntu
            Process exec = rt.exec("/usr/bin/lp_solve " + "model.lp");
            InputStreamReader reader = new InputStreamReader(exec.getInputStream());
            BufferedReader br = new BufferedReader(reader);
            String line = null;
            while ((line = br.readLine()) != null)
            {
                if (line.contains("Value of objective function"))
                {
                    builder.append(line).append("\n\r");
                }
                else
                {
                    for (Source source : triplet.getSources())
                    {
                        if (line.contains(source.getName()))
                        {
                            builder.append(line).append("\n\r");
                        }
                    }
                }
            }
        }
        //on windows
        else
        {
            LpSolve lpProblem = LpSolve.readLp(filename, LpSolve.NORMAL, "intersection model");
            lpProblem.setUseNames(true, true);
            lpProblem.solve();

            builder.append("Value of objective function ");
            builder.append(lpProblem.getObjective());
            builder.append("\n\n");
            double[] var = lpProblem.getPtrVariables();
            //get the Is where source is true
            for (Source source : triplet.getSources())
            {
                int index = lpProblem.getNameindex(source.getName(), false);
                builder.append("Value of ").append(source.getName());
                builder.append(" ");
                builder.append(var[index - 1]).append("\n\r");
            }
        }
        return builder.toString();
    }

    public void forAll(int row, int column)
    {
        for (int irow = 0; irow < rows; irow++)
        {
            for (int icolumn = 0; icolumn < columns; icolumn++)
            {
              if (irow != row || icolumn != column)  {
                addFlowA(irow, icolumn, vintersections[row][column].getFlowA());
                addFlowB(irow, icolumn, vintersections[row][column].getFlowB());
                addFlowC(irow, icolumn, vintersections[row][column].getFlowC());
                addFlowD(irow, icolumn, vintersections[row][column].getFlowD());

                //A
                addProbabilityAB(irow, icolumn, vintersections[row][column].getProbAB());
                addProbabilityAC(irow, icolumn, vintersections[row][column].getProbAC());
                addProbabilityAD(irow, icolumn, vintersections[row][column].getProbAD());
                addPhaseAB(irow, icolumn, vintersections[row][column].getPhaseAB());
                addPhaseAC(irow, icolumn, vintersections[row][column].getPhaseAC());
                addPhaseAD(irow, icolumn, vintersections[row][column].getPhaseAD());

                //B
                addProbabilityBA(irow, icolumn, vintersections[row][column].getProbBA());
                addProbabilityBC(irow, icolumn, vintersections[row][column].getProbBC());
                addProbabilityBD(irow, icolumn, vintersections[row][column].getProbBD());
                addPhaseBA(irow, icolumn, vintersections[row][column].getPhaseBA());
                addPhaseBC(irow, icolumn, vintersections[row][column].getPhaseBC());
                addPhaseBD(irow, icolumn, vintersections[row][column].getPhaseBD());

                //C
                addProbabilityCA(irow, icolumn, vintersections[row][column].getProbCA());
                addProbabilityCB(irow, icolumn, vintersections[row][column].getProbCB());
                addProbabilityCD(irow, icolumn, vintersections[row][column].getProbCD());
                addPhaseCA(irow, icolumn, vintersections[row][column].getPhaseCA());
                addPhaseCB(irow, icolumn, vintersections[row][column].getPhaseCB());
                addPhaseCD(irow, icolumn, vintersections[row][column].getPhaseCD());

                //D
                addProbabilityDA(irow, icolumn, vintersections[row][column].getProbDA());
                addProbabilityDB(irow, icolumn, vintersections[row][column].getProbDB());
                addProbabilityDC(irow, icolumn, vintersections[row][column].getProbDC());
                addPhaseDA(irow, icolumn, vintersections[row][column].getPhaseDA());
                addPhaseDB(irow, icolumn, vintersections[row][column].getPhaseDB());
                addPhaseDC(irow, icolumn, vintersections[row][column].getPhaseDC());

                if (vintersections[row][column].getSourceNorth() != null)
                {
                    addSource(irow, icolumn, VDirection.North, vintersections[row][column].getSourceNorth().getRate());
                }

                if (vintersections[row][column].getSourceEast() != null)
                {
                    addSource(irow, icolumn, VDirection.East, vintersections[row][column].getSourceEast().getRate());
                }

                if (vintersections[row][column].getSourceSouth() != null)
                {
                    addSource(irow, icolumn, VDirection.South, vintersections[row][column].getSourceSouth().getRate());
                }

                if (vintersections[row][column].getSourceWest() != null)
                {
                    addSource(irow, icolumn, VDirection.West, vintersections[row][column].getSourceWest().getRate());
                }
              }
            }
        }
    }

    public int getRows()
    {
        return rows;
    }

    public int getColumns()
    {
        return columns;
    }

    public void addIntersection(int id, int row, int column)
    {
        vintersections[row][column] = new VIntersection(id, row, column);
    }
    
    //A
    public void addProbabilityAB(int row, int column, double prob)
    {
        vintersections[row][column].setProbAB(prob);
    }

    public void addProbabilityAC(int row, int column, double prob)
    {

        vintersections[row][column].setProbAC(prob);
    }

    public void addProbabilityAD(int row, int column, double prob)
    {
        vintersections[row][column].setProbAD(prob);
    }

    public void addPhaseAB(int row, int column, int phase)
    {
        vintersections[row][column].setPhaseAB(phase);
    }

    public void addPhaseAC(int row, int column, int phase)
    {

        vintersections[row][column].setPhaseAC(phase);
    }

    public void addPhaseAD(int row, int column, int phase)
    {
        vintersections[row][column].setPhaseAD(phase);
    }

    //B
    public void addProbabilityBA(int row, int column, double prob)
    {
        vintersections[row][column].setProbBA(prob);
    }

    public void addProbabilityBC(int row, int column, double prob)
    {
        vintersections[row][column].setProbBC(prob);
    }

    public void addProbabilityBD(int row, int column, double prob)
    {
        vintersections[row][column].setProbBD(prob);
    }

    public void addPhaseBA(int row, int column, int phase)
    {
        vintersections[row][column].setPhaseBA(phase);
    }

    public void addPhaseBC(int row, int column, int phase)
    {

        vintersections[row][column].setPhaseBC(phase);
    }

    public void addPhaseBD(int row, int column, int phase)
    {
        vintersections[row][column].setPhaseBD(phase);
    }

    //C
    public void addProbabilityCA(int row, int column, double prob)
    {
        vintersections[row][column].setProbCA(prob);
    }

    public void addProbabilityCB(int row, int column, double prob)
    {
        vintersections[row][column].setProbCB(prob);
    }

    public void addProbabilityCD(int row, int column, double prob)
    {
        vintersections[row][column].setProbCD(prob);
    }

    public void addPhaseCA(int row, int column, int phase)
    {
        vintersections[row][column].setPhaseCA(phase);
    }

    public void addPhaseCB(int row, int column, int phase)
    {

        vintersections[row][column].setPhaseCB(phase);
    }

    public void addPhaseCD(int row, int column, int phase)
    {
        vintersections[row][column].setPhaseCD(phase);
    }

    //D
    public void addProbabilityDA(int row, int column, double prob)
    {
        vintersections[row][column].setProbDA(prob);
    }

    public void addProbabilityDB(int row, int column, double prob)
    {
        vintersections[row][column].setProbDB(prob);
    }

    public void addProbabilityDC(int row, int column, double prob)
    {
        vintersections[row][column].setProbDC(prob);
    }

    public void addPhaseDA(int row, int column, int phase)
    {
        vintersections[row][column].setPhaseDA(phase);
    }

    public void addPhaseDB(int row, int column, int phase)
    {

        vintersections[row][column].setPhaseDB(phase);
    }

    public void addPhaseDC(int row, int column, int phase)
    {
        vintersections[row][column].setPhaseDC(phase);
    }

    public void addFlowA(int row, int column, double flow)
    {
        vintersections[row][column].setFlowA(flow);
    }

    public void addFlowB(int row, int column, double flow)
    {
        vintersections[row][column].setFlowB(flow);
    }

    public void addFlowC(int row, int column, double flow)
    {
        vintersections[row][column].setFlowC(flow);
    }

    public void addFlowD(int row, int column, double flow)
    {
        vintersections[row][column].setFlowD(flow);
    }

    public void addSource(int row, int column, VDirection direction, double rate)
    {
        VIntersection junction = vintersections[row][column];
        switch (direction)
        {
            case North:
                junction.setSourceNorth(new VSource(rate));
                break;
            case East:
                junction.setSourceEast(new VSource(rate));
                break;
            case South:
                junction.setSourceSouth(new VSource(rate));
                break;
            case West:
                junction.setSourceWest(new VSource(rate));
                break;
        }
    }

    public VIntersection getIntersection(int row, int column)
    {
        return vintersections[row][column];
    }
	
	public VStreet getStreet(int row, int column, VDirection direction) {
		return vstreets[row][column][direction.ordinal()];
	}

    public void saveLP(String filepath) throws SaveLPException
    {
        try
        {
            Serializer.getInstance().writeToFile(filepath, vintersections, rows, columns);
        } catch (FileNotFoundException ex)
        {
            Logger.getLogger(StreetNetworkController.class.getName()).log(Level.SEVERE, null, ex);
            throw new SaveLPException(ex);
        } catch (JAXBException ex)
        {
            Logger.getLogger(StreetNetworkController.class.getName()).log(Level.SEVERE, null, ex);
            throw new SaveLPException(ex);
        }
    }
    
    public void loadLP(String filepath) throws LoadLPException
    {
        try
        {
            vintersections = Serializer.getInstance().readFromFile(filepath);
            rows = vintersections.length;
            columns = vintersections[0].length;
            //generateLP();
			createStreets(rows, columns);
			
        } catch (JAXBException ex)
        {
            Logger.getLogger(StreetNetworkController.class.getName()).log(Level.SEVERE, null, ex);
            throw new LoadLPException(ex);
        }
    }

	public void saveTrafSimXML(String fileName) {
		TrafSimSerializer.getInstance().saveScenario(fileName, vintersections, rows, columns, vstreets);
	}
}
